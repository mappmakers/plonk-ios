//
//  PKAccordionTableViewCell.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 14/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKAccordionTableViewCell: AEAccordionTableViewCell {

    @IBOutlet weak var headerView: PKAccordionHeaderView!
    @IBOutlet weak var detailView: PKAccordionDetailView!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: - Override
    
    override func setExpanded(expanded: Bool, animated: Bool) {
        super.setExpanded(expanded, animated: animated)
        
        if !animated {
            toggleCell()
        } else {
            
            
            toggleCell()
            /*
            let alwaysOptions: UIViewAnimationOptions = [.AllowUserInteraction, .BeginFromCurrentState, .TransitionCrossDissolve]
            let expandedOptions: UIViewAnimationOptions = [.TransitionFlipFromTop, .CurveEaseOut]
            let collapsedOptions: UIViewAnimationOptions = [.TransitionFlipFromBottom, .CurveEaseIn]
            let options: UIViewAnimationOptions = expanded ? alwaysOptions.union(expandedOptions) : alwaysOptions.union(collapsedOptions)
            
            
            
            
            UIView.transitionWithView(detailView, duration: 0.3, options: options, animations: { () -> Void in
                self.toggleCell()
                }, completion: nil)
*/
        }
    }
    
    // MARK: - Helpers
    
    private func toggleCell() {
        detailView.hidden = !expanded
//        headerView.imageView.transform = expanded ? CGAffineTransformMakeRotation(CGFloat(M_PI)) : CGAffineTransformIdentity
    }
    

    
    
}
