//
//  DetailView.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 14/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

typealias PKAccordionDetailViewPassCardCompletion = (sender : UIButton) -> Void

class PKAccordionDetailView: UIView {
    @IBOutlet weak var parkerName: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var totalTime: UILabel!
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var btnPasscard: UIButton!
    
    var passcardCompletion:PKAccordionDetailViewPassCardCompletion?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnPasscard.layer.cornerRadius = 5
        btnPasscard.layer.masksToBounds = true
    }

    @IBAction func passcardPressed(sender:UIButton) {
    
        if passcardCompletion != nil {
            passcardCompletion!(sender: sender)
        }
        
    }
}
