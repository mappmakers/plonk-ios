//
//  PKQRTextInputTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 19/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

typealias CodeInputCompletion = (inputCode : String, status: String?) -> Void

class PKQRTextInputTableViewController: UITableViewController {

    let inputCodeCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
    let confirmCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)

    var arrayCells:Array<UITableViewCell>?
    var codeInputCompletion:CodeInputCompletion?
    
    var qrCodeStatus:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Enter QR Code"

        customizeForStaticTableStyle()
        
        inputCodeCell.textField.placeholder = "QR Code"
        inputCodeCell.textField.delegate = self
        inputCodeCell.textField.returnKeyType = UIReturnKeyType.Done
        inputCodeCell.textField.becomeFirstResponder()
        inputCodeCell.textField.floatingLabelTextColor = UIColor.appThemeColor()
        inputCodeCell.textField.tintColor = UIColor.appThemeColor()
        
        inputCodeCell.textField.textColor = UIColor.appThemeColor()
        inputCodeCell.backgroundColor = UIColor.whiteColor()
        
        confirmCell.textLabel?.text = "Confirm"
        confirmCell.backgroundColor = UIColor.appThemeColor()
        
        arrayCells = [inputCodeCell, confirmCell]

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayCells?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        return PKConstants.CellTextFieldHeight-10
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return arrayCells![indexPath.section]
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 1 {
            
            navigationController?.popViewControllerAnimated(true)
            if codeInputCompletion != nil {
                codeInputCompletion!(inputCode: (arrayCells![0] as! PKTextFieldLoginTableViewCell).textField.text ?? "", status: qrCodeStatus)
            }
            
        }
        
    }
}
