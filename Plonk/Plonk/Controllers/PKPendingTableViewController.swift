//
//  PKPendingTableViewController.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 27/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD
import DZNEmptyDataSet

class PKPendingTableViewController: UITableViewController,NSFetchedResultsControllerDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    var pendingParkingFRC:NSFetchedResultsController?
    
    var parentNavigationController:UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pendingParkingFRC = PKUserPendingSlot.MR_fetchAllSortedBy("parking_lot_id", ascending: true, withPredicate: nil, groupBy: nil, delegate: self, inContext: NSManagedObjectContext.MR_defaultContext())
        pendingParkingFRC?.delegate = self

        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        tableView.backgroundColor=UIColor.groupTableViewBackgroundColor()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return pendingParkingFRC?.sections?.count ?? 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let sectionInfo = pendingParkingFRC!.sections![section]
        return sectionInfo.numberOfObjects
    }

    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("myReuseIdentifier") as? PKParkingTableViewCell
        
        if cell == nil {
            
            cell = PKParkingTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "myReuseIdentifier")
        }
        
        configureCell(cell!, atIndexPath: indexPath)
        // Configure the cell...

        return cell!
    }
    
    func configureCell(cell:PKParkingTableViewCell, atIndexPath indexPath:NSIndexPath) {
        
        let parkingLotFeed = pendingParkingFRC!.objectAtIndexPath(indexPath) as! PKUserPendingSlot
        cell.textLabel?.text = parkingLotFeed.parking_lot_name ?? "NA"
        cell.detailTextLabel?.text = parkingLotFeed.parking_lot_address ?? "NA"
        
        cell.selectionStyle = .None
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }

    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 5
    }
    
    //empty dataset method
    
    // empty data methods
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        
        // return UIImage(named: "ic_parking_map")
        
        return UIImage(named: "ic_pending")
        
        
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let emptyTitle:String = ""
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        let emptyTitle:String = "Pending lots will appear here "
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
        
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView!) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
//    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        
//        
//        tableView.deselectRowAtIndexPath(indexPath, animated: true)
//        
//        
//        let parkingLotFeed = pendingParkingFRC!.objectAtIndexPath(indexPath) as! PKUserPendingSlot
//        
//        getParkingDetails(parkingLotFeed.tenant_lot_id!)
//    }
//    
    func getParkingDetails(tenantLotId:String)
    {
        SVProgressHUD.showWithMaskType(.Gradient)
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ParkingLotDetails.rawValue), parameters:["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"Lot_id":tenantLotId]) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1
            {

                let json = result as! Dictionary<String,AnyObject>
                
                let parkingDetailsDicArray = json["prkLots_details"] as! NSArray
                
                let parkingDetailsDic = parkingDetailsDicArray.objectAtIndex(0) as! Dictionary<String,AnyObject>
                
                let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameMyParkingLot, bundle: nil)

                let parkingLotDetailsVC = storyboard.instantiateViewControllerWithIdentifier("parkingLotDetailsVC") as! PKParkingLotDetailsTableViewController
                
                parkingLotDetailsVC.parkingId = tenantLotId

                parkingLotDetailsVC.parkingDetailsDic = parkingDetailsDic

                self.navigationController?.pushViewController(parkingLotDetailsVC, animated: true)
                
            }
            else
            {
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
            
        }
    }

    // MARK: - NSFetchedResultsControllerDelegate
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        let tableView:UITableView = self.tableView
        
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Update:
            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!) as! PKParkingTableViewCell, atIndexPath: indexPath!)
            break
        default:
            break
        }
    }
    
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
            
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
        default:break
        }
        
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
}
