//
//  PKMyParkingParentViewController.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 28/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import PagingMenuController

class PKMyParkingParentViewController: UIViewController,PagingMenuControllerDelegate {

    var pagingMenuController:PagingMenuController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "My Parking lots"

        view.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        let controller1 = PKParkingLotsTableViewController()
        controller1.title = "Approved"
        //controller1.parentNavigationController = self.navigationController
        
        let controller2 = PKPendingTableViewController(style: UITableViewStyle.Plain)
        controller2.title = "Pending"
        //controller2.parentNavigationController = self.navigationController
        
        let controller3 = PKRejectTableViewController(style: UITableViewStyle.Plain)
        controller3.title = "Rejected"
        let viewControllers = [controller1,controller2,controller3]
        
        let options = PagingMenuOptions()
        options.menuItemMargin = 5
        options.menuDisplayMode = .SegmentedControl
        options.selectedTextColor=UIColor.appThemeColor()
        options.textColor=UIColor.appThemeColor()
        options.font=UIFont(name: "Aller", size: 15)!
        options.selectedFont=UIFont(name: "Aller", size: 17)!
        options.menuItemMode = .Underline(height: 3, color: UIColor.appThemeColor(), horizontalPadding: 0, verticalPadding: 0)
        
        pagingMenuController = PagingMenuController(viewControllers: viewControllers, options: options)
        pagingMenuController.delegate = self
        self.addChildViewController(pagingMenuController)
        self.view.addSubview(pagingMenuController.view)
        pagingMenuController.didMoveToParentViewController(self)
        
        pagingMenuController.delegate=self

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchMyParkingLots()
    }
    
    func fetchMyParkingLots()
    {
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.MyParkingLots.rawValue), parameters:["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]) { (success:Int!, result:AnyObject?)-> Void in
            if success == 1
            {
                
                let json = result as! Dictionary<String,AnyObject>

                PKUserParkingLot.MR_truncateAll()
                
                let myParkingLotArray = json["prkLots_list"] as! NSArray
                
                if myParkingLotArray.count > 0
                {
                    for parkingLotDict in myParkingLotArray {
                        
                        let userLotDict = parkingLotDict as! Dictionary<String,AnyObject>
                        PKUserParkingLot.userParkingLotWithParameters(userLotDict)
                        
                    }
                    
                }
                
                PKUserPendingSlot.MR_truncateAll()
                
                let pendingLotArray = json["pending_prkLots_list"] as! NSArray
                
                if pendingLotArray.count > 0
                {
                    
                    for parkingLotDict in pendingLotArray
                    {
                        
                        let userLotDict = parkingLotDict as! Dictionary<String,AnyObject>
                        PKUserPendingSlot.userParkingLotWithParameters(userLotDict)
                        
                    }
                    
                }
                
                PKUserRejectSlot.MR_truncateAll()
                
                let rejectLotArray = json["reject_prkLots_list"] as! NSArray
                
                if rejectLotArray.count > 0
                {
                    
                    for parkingLotDict in rejectLotArray
                    {
                        
                        let userLotDict = parkingLotDict as! Dictionary<String,AnyObject>
                        PKUserRejectSlot.userParkingLotWithParameters(userLotDict)
                    }
                    
                }
                
            }
            else
            {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func  didMoveToMenuPage(page: Int) {
        print("did move to page")
        
        fetchMyParkingLots()
        
    }
    
    func  willMoveToMenuPage(page: Int) {
        print("will move to page")
    }

}
