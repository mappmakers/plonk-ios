//
//  PKPlansListTableViewController.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 19/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD

class PKPlansListTableViewController: UITableViewController {

    var parkingLotId:String!
    var plansArray:Array<Dictionary<String, AnyObject>>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "My plans"
        
        plansArray=[]
        
        listPlans()

    }

    
    func listPlans()
    {
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ListUserPlans.rawValue), parameters:["Lot_id":parkingLotId]) { (success:Int!, result:AnyObject?) -> Void in
            
            if success == 1
            {
                let json = result as! Dictionary<String,AnyObject>
                
                for resultDic in json["Lots_plan_list"] as! NSArray {
                    
                    var planDic = resultDic as! Dictionary<String, AnyObject>
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    
                    let entryDate = dateFormatter.dateFromString(resultDic["start_date"] as? String ?? "")
                    
                    let exitDate = dateFormatter.dateFromString(resultDic["end_date"] as? String ?? "")
                    
                    dateFormatter.dateFormat = "dd MMM yyyy"
                    
                    if let startDte = entryDate
                    {
                    planDic["start_date"] = dateFormatter.stringFromDate(startDte)
                    }
                    
                    if let endDte = exitDate
                    {
                    planDic["end_date"] = dateFormatter.stringFromDate((exitDate == nil ? entryDate : exitDate)!)
                    }
                    
                    self.plansArray.append(planDic)
                    
                }
                
                self.tableView.reloadData()
            }
            else
            {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return plansArray.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:PKPlanTableViewCell=tableView.dequeueReusableCellWithIdentifier("planCell", forIndexPath: indexPath) as! PKPlanTableViewCell
        
        let userDict = plansArray[indexPath.section]
        
        let dateRange1:String = (userDict["start_date"] as? String)!
        let dateRange2:String = (userDict["end_date"] as? String)!
        
        let rupeeStringg = (userDict["fare"] as? String)!
        
        cell.planCost.text = "₹ \(rupeeStringg)"
        
        cell.planDateRange.text = "\(dateRange1) - \(dateRange2)"
        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let userDict = plansArray[indexPath.section]
        let userPlanId = userDict["tenant_plan_id"] as! String
        let userLotId = parkingLotId as! String
        
        getPlanDetails(userPlanId, userLotId: userLotId)
    }
    
    func getPlanDetails(userPlanId:String,userLotId:String)
    {
        SVProgressHUD.showWithMaskType(.Gradient)
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.PlanDetailsUrl.rawValue), parameters:["plan_id":userPlanId]) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1
            {
                let json = result as! Dictionary<String,AnyObject>
                
                let resultJsonArray = json["Lots_plan_details"] as! NSArray
                let resultJson = resultJsonArray.objectAtIndex(0) as! Dictionary<String,AnyObject>
                
                
                let planDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier("planDetailVC") as! PKPlanDetailTableViewController
                
                planDetailsVC.resultJson = resultJson
                planDetailsVC.planId = userPlanId
                planDetailsVC.lotId = userLotId
                planDetailsVC.navigationItem.title = "Plan details"
                self.navigationController?.pushViewController(planDetailsVC, animated: true)
                
            }
            else
            {
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
            
        }
    }
   
}
