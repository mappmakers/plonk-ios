//
//  PKQRCodeExitViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 06/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

import UIKit
import MaterialKit
import MaterialControls
import SVProgressHUD

struct PKQRCodeExitConstants {
    
    static let TitleScan = "Scan"
    static let TitleNext = "ENTER CODE MANUALLY"
    
}

enum PKParkingExitDataError: ErrorType {
    case NoParkingSlots, NoVehicles
}

extension PKParkingExitDataError: CustomStringConvertible {
    var description: String {
        switch self {
        case NoParkingSlots: return "No Parking Slots found"
        case NoVehicles: return "No Vehicles info found"
        }
    }
}


class PKQRCodeExitViewController: UIViewController {
    
    let scanner = FDQRScanner()

    private var nextButton:MKButton!
    
    var codeInputCompletion:CodeInputCompletion?
    
    var scannerView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = PKQRCodeConstants.TitleScan

        nextButton = MKButton(type: .Custom)
        nextButton.backgroundColor = UIColor.appThemeColor()
        nextButton.setTitle(PKQRCodeConstants.TitleNext, forState: .Normal)
        nextButton.titleLabel?.font = UIFont.PKAllerRegular(14)
        nextButton.setImage(UIImage(named: "ic_right"), forState: .Normal)
        nextButton.addTarget(self, action: "textInputPressed:", forControlEvents: .TouchUpInside)
        nextButton.imageEdgeInsets = UIEdgeInsetsMake(0, UIScreen.mainScreen().bounds.size.width-30, 0, 0)
        view.addSubview(nextButton)
        
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        nextButton.addBottomEdgeAttachConstraint(view)
        nextButton.addHeightConstraint(50)
        nextButton.addRightEdgeAttachConstraint(view)
        nextButton.addLeftEdgeAttachConstraint(view)
        
        scannerView = UIView(frame: CGRectZero)
        scannerView.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        view.addSubview(scannerView)
        
        scannerView.translatesAutoresizingMaskIntoConstraints = false
        scannerView.addTopEdgeAttachConstraint(view)
        scannerView.addBottomEdgeAttachConstraint(nextButton, viewEdge: .Top, offset: 0)
        scannerView.addLeftEdgeAttachConstraint(view)
        scannerView.addRightEdgeAttachConstraint(view)
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.startScanningSession()
        }
        
        let delayTimeSecond = dispatch_time(DISPATCH_TIME_NOW, Int64(1.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTimeSecond, dispatch_get_main_queue()) {
            
            let qrYellowImageView = UIImageView(image: UIImage(named: "ic_qr_yellow"))
            self.scannerView.addSubview(qrYellowImageView)
            
            qrYellowImageView.translatesAutoresizingMaskIntoConstraints = false
            qrYellowImageView.addCenterXConstraint(self.scannerView)
            qrYellowImageView.addCenterYConstraint(self.scannerView)
            qrYellowImageView.addWidthConstraint(100)
            qrYellowImageView.addHeightConstraint(100)
            
        }
    }
    
    func startScanningSession() {
        
        scanner.startScanning(scannerView, metadataObjectTypes: nil) { (decodedString, type) -> Void in
            print(" scanner completed \(decodedString)")
            
            if self.codeInputCompletion != nil {
                self.codeInputCompletion!(inputCode: decodedString, status: "")
            }
            
        }
        
    }

    func textInputPressed(sender:UIButton?) {
        
        let qrInputController = PKExitQRTableViewController(style: .Grouped)
        qrInputController.codeInputCompletion = codeInputCompletion
        navigationController?.pushViewController(qrInputController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
