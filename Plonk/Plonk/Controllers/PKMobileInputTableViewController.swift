//
//  PKMobileInputTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 16/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

private struct PKMobileNumberInputConstants {
    
    static let TitleHeaderMobile:String = "Please enter your phone number"
    static let TitleFooterMobile:String = "For verification, an OTP will be sent to your mobile number"
    
}

class PKMobileInputTableViewController: PKNumberInputTableViewController {

    private var mobileCell:PKMobileInputTableViewCell?
    override func viewDidLoad() {
        super.viewDidLoad()

        let headerCell = self.createHeaderCell()
        headerCell.textLabel?.text = PKMobileNumberInputConstants.TitleHeaderMobile
        
        let footerCell = self.createFooterCell()
        footerCell.textLabel?.text = PKMobileNumberInputConstants.TitleFooterMobile
        footerCell.textLabel?.font = UIFont.PKAllerLight(12)
        
        arrayCells = [headerCell, createMobileNumberCell(), footerCell]
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = false
        navigationController?.navigationBarHidden = false
    }
    
    func createMobileNumberCell() -> UITableViewCell {
        
        mobileCell = PKMobileInputTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        mobileCell!.textFieldMobileNumber.inputAccessoryView = accessoryViewContinue
        if UIDevice.currentDevice().modelName != "iPhone 4" && UIDevice.currentDevice().modelName != "iPhone 4S" {
            
            mobileCell!.textFieldMobileNumber.becomeFirstResponder()
            
        }
        
        return mobileCell!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func continuePressed(sender:UIButton) {
        
        view.endEditing(true)

        let isValid = mobileCell!.textFieldMobileNumber.text!.characters.count==10
        
        if isValid {
            
            let parameters:Dictionary<String, AnyObject> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"user_mobile":mobileCell!.textFieldMobileNumber.text!]
            
            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserRegisterMobile.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
                
                switch success {
                case 0:
                    let error:NSError = (result as! NSError)
                    let title = String(error.code) + ": " + error.domain
                    let message = error.localizedDescription
                    self.handleError(title, message: message)
                    break
                case 1:
                    let json = result as! Dictionary<String, AnyObject>
                    
                    /*
                    let title = ""
                    let message = ""
                    self.handleError(title, message: message)
                    */
                    if (json["success"] as! String) == "true" {
                        PKDataModel.sharedInstance.loggedUser.otp_pasword = json["user_otp"] as? String
                        
                        let verifyOTPController = PKOTPInputTableViewController(style: UITableViewStyle.Plain)
                        verifyOTPController.authenticationDelegate = self.authenticationDelegate
                        verifyOTPController.mobileNumber = self.mobileCell!.textFieldMobileNumber.text!
                        self.navigationController?.pushViewController(verifyOTPController, animated: true)
                    }else {
                        self.handleError(PKConstants.TitleErrorHeader, message: "Mobile Number already exists")
                    }
                    
                    
                    break

                default:break
                }
                
            }
        }else {
            
            self.handleError(PKConstants.TitleErrorHeader, message: "Please enter a valid mobile number")
        }
    }
}
