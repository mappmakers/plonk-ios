//
//  MapViewController.swift
//  Feed Me
//
/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit
import MaterialKit

class MapViewController: UIViewController, UISearchControllerDelegate {
  
  @IBOutlet weak var mapView: GMSMapView!
  @IBOutlet weak var mapCenterPinImage: UIImageView!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var pinImageVerticalConstraint: NSLayoutConstraint!
  var searchedTypes = ["bakery", "bar", "cafe", "grocery_or_supermarket", "restaurant"]
  let locationManager = CLLocationManager()
  let dataProvider = GoogleDataProvider()
  let searchRadius: Double = 1000
  
    var currentAddress : GMSAddress?
    
    private var searchController:UISearchController?
    private var searchField:MKTextField!
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Select a placemark"
    
    locationManager.delegate = self
    locationManager.requestWhenInUseAuthorization()
    
    mapView.delegate = self
    
    createOverlayViews()
    
    handleError("Warning", message: "It is important that you pick your lot's location accurately. This will determine how your lot is plotted in the app for all users.")
  }
  
    func createOverlayViews() {
        
        let searchBarView = UIView(frame: CGRectZero)
        
        searchBarView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.8)
        searchBarView.layer.cornerRadius = 5.0
        searchBarView.layer.masksToBounds = true
        
        view.addSubview(searchBarView)
        
        searchBarView.translatesAutoresizingMaskIntoConstraints = false
        searchBarView.addTopEdgeAttachConstraint(view, offset:15)
        searchBarView.addLeftEdgeAttachConstraint(view, offset: 15)
        searchBarView.addRightEdgeAttachConstraint(view, offset: 15)
        searchBarView.addHeightConstraint(48)
        
        let directionImageView = UIImageView(frame: CGRectZero)
        directionImageView.image = UIImage(named: "ic_direction_gray")
        
        searchBarView.addSubview(directionImageView)
       
        directionImageView.translatesAutoresizingMaskIntoConstraints = false
        directionImageView.addLeftEdgeAttachConstraint(searchBarView, offset:10)
        directionImageView.addCenterYConstraint(searchBarView)
        directionImageView.addWidthConstraint(25)
        directionImageView.addHeightConstraint(25)
        
        let separatorVerticalLineView = UIView(frame: CGRectZero)
        separatorVerticalLineView.backgroundColor = UIColor.darkGrayColor()
        
        searchBarView.addSubview(separatorVerticalLineView)
        
        separatorVerticalLineView.translatesAutoresizingMaskIntoConstraints = false
        separatorVerticalLineView.addLeftEdgeAttachConstraint(directionImageView, viewEdge: .Right, offset:10)
        separatorVerticalLineView.addTopEdgeAttachConstraint(searchBarView, offset:4)
        separatorVerticalLineView.addBottomEdgeAttachConstraint(searchBarView, offset:4)
        separatorVerticalLineView.addWidthConstraint(1)
        
        searchField = MKTextField(frame: CGRectZero)
        searchField.textColor = UIColor.darkGrayColor()
        searchField.tintColor = UIColor.darkGrayColor()
        searchField.layer.borderColor = UIColor.clearColor().CGColor
        searchField.borderStyle = UITextBorderStyle.None
        searchField.returnKeyType = .Done
        searchField.clearButtonMode = .WhileEditing
        searchField.placeholder = "Enter Location"
        searchField.floatingPlaceholderEnabled = false
        searchField.rippleLocation = .TapLocation
        searchField.rippleLayerColor = UIColor.whiteColor()
        searchField.padding = CGSizeMake(15, 0)
        searchField.delegate = self
        
        searchBarView.addSubview(searchField)
        
        searchField.translatesAutoresizingMaskIntoConstraints = false
        searchField.addLeftEdgeAttachConstraint(separatorVerticalLineView, viewEdge: .Right, offset: 0)
        searchField.addTopEdgeAttachConstraint(separatorVerticalLineView)
        searchField.addBottomEdgeAttachConstraint(separatorVerticalLineView)
        searchField.addRightEdgeAttachConstraint(searchBarView, offset: 5)
    }
    
    // MARK:- UITextFieldDelegate
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if searchController == nil {
            
            let resultsController = PKMapSearchResultsTableViewController(style:.Plain)
            resultsController.autocompleteCallback = { (placePrediction:GMSAutocompletePrediction?, error:NSError?) -> Void in
                
                self.searchController = nil
                
                if placePrediction != nil {
                    
                    GMSPlacesClient.sharedClient().lookUpPlaceID(placePrediction!.placeID, callback: { (place: GMSPlace?, error: NSError?) -> Void in
                        if let error = error {
                            print("lookup place id query error: \(error.localizedDescription)")
                            return
                        }
                        
                        if let place = place {
                            print("Place name \(place.name)")
                            print("Place address \(place.formattedAddress)")
                            print("Place placeID \(place.placeID)")
                            print("Place attributions \(place.attributions)")
                            
                            //self.configureViewWithPlace(place)
                            self.mapView.camera = GMSCameraPosition.cameraWithLatitude(place.coordinate.latitude,
                                longitude:place.coordinate.longitude, zoom:16)
                            
                            self.reverseGeocodeCoordinate(place.coordinate)
                            
                        } else {
                            print("No place details for \(placePrediction!.placeID)")
                        }
                    })
                    
                }
                
            }
            
            searchController = UISearchController(searchResultsController: resultsController)
            searchController!.searchBar.barTintColor = UIColor.appThemeColor()
            searchController!.searchBar.backgroundColor = UIColor.appThemeColor()
            searchController!.searchBar.hidden = false
            searchController!.searchBar.showsCancelButton = true
            searchController!.delegate = self
            searchController!.searchResultsUpdater = resultsController
            searchController!.hidesNavigationBarDuringPresentation = false
            searchController!.searchBar.frame = CGRectMake(searchController!.searchBar.frame.origin.x, searchController!.searchBar.frame.origin.y, searchController!.searchBar.frame.size.width, 44)
            searchController!.searchBar.delegate = resultsController
            searchController!.searchBar.sizeToFit()
            searchController!.dimsBackgroundDuringPresentation = false
            
            presentViewController(searchController!, animated: true) { () -> Void in
                self.searchController!.searchBar.becomeFirstResponder()
            }
        }
        
        return false
    }
    
    func configureViewWithPlace(place:GMSPlace?) {
        
        if place != nil {
            self.searchField.text = place!.formattedAddress!
            let camera = GMSCameraPosition.cameraWithLatitude(place!.coordinate.latitude,
                longitude:place!.coordinate.longitude, zoom:16)
            self.mapView.camera = camera
        }
        
    }
    
    override func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var txtAfterUpdate:NSString = NSString(string: textField.text!)
        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
        
        searchWithText(txtAfterUpdate)
        
        return true
    }
    
    func searchWithText(searchText:NSString) {
        print(searchText)
    }
  
    @IBAction func nextPressed(sender:AnyObject) {
        
        if currentAddress != nil {
            let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameMyParkingLot, bundle: nil)
            let addParkingLotVC:PKAddParkingLotViewController = storyboard.instantiateViewControllerWithIdentifier("addParkingLotVC") as! PKAddParkingLotViewController
            //let currentPlace = GooglePlace(dictionary: ["geometry":["location":["lat":mapView.camera.target.latitude,"lng":mapView.camera.target.longitude]]], acceptedTypes: [])
            addParkingLotVC.placemark = currentAddress
            addParkingLotVC.navigationItem.title = "Add Parking Lot"
            navigationController?.pushViewController(addParkingLotVC, animated: true)
        }else {
            self.handleError("Sorry", message: "Please select a placemark")
        }
        
    }
    
  func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
    let geocoder = GMSGeocoder()
    
    geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
      self.addressLabel.unlock()
      if let address = response?.firstResult() {
        
        self.currentAddress = address
        
        let lines = address.lines as! [String]
        self.addressLabel.text = lines.joinWithSeparator("\n")
        self.searchField.text = lines.joinWithSeparator("\n")
        
        let labelHeight = self.addressLabel.intrinsicContentSize().height
        self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: labelHeight, right: 0)

        UIView.animateWithDuration(0.25) {
          self.pinImageVerticalConstraint.constant = ((labelHeight - self.topLayoutGuide.length) * 0.5)
          self.view.layoutIfNeeded()
        }
        
      }
    }
  }
  
  func fetchNearbyPlaces(coordinate: CLLocationCoordinate2D) {
    mapView.clear()
    dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes) { places in
      for place: GooglePlace in places {
        let marker = PlaceMarker(place: place)
        marker.map = self.mapView
      }
    }
  }
    
  @IBAction func refreshPlaces(sender: AnyObject) {
    fetchNearbyPlaces(mapView.camera.target)
  }
  
}

// MARK: - CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedWhenInUse {
      locationManager.startUpdatingLocation()
      mapView.myLocationEnabled = true
      mapView.settings.myLocationButton = true
    }
  }
  
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = locations.first {
        mapView.camera = GMSCameraPosition.cameraWithLatitude(location.coordinate.latitude,
            longitude:location.coordinate.longitude, zoom:16)
      locationManager.stopUpdatingLocation()
      fetchNearbyPlaces(location.coordinate)
    }
  }
}

// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
  func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
    reverseGeocodeCoordinate(position.target)
  }
  
  func mapView(mapView: GMSMapView!, willMove gesture: Bool) {
    addressLabel.lock()
    
    if (gesture) {
      mapCenterPinImage.fadeIn(0.25)
      mapView.selectedMarker = nil
    }
  }
  
  func mapView(mapView: GMSMapView!, markerInfoContents marker: GMSMarker!) -> UIView! {
    let placeMarker = marker as! PlaceMarker
    
    if let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView {
      infoView.nameLabel.text = placeMarker.place.name
      
      if let photo = placeMarker.place.photo {
        infoView.placePhoto.image = photo
      } else {
        infoView.placePhoto.image = UIImage(named: "generic")
      }
      
      return infoView
    } else {
      return nil
    }
  }
  
  func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
    mapCenterPinImage.fadeOut(0.25)
    return false
  }
  
  func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
    mapCenterPinImage.fadeIn(0.25)
    mapView.selectedMarker = nil
    return false
  }
}