//
//  PKShareViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import Social
import MessageUI
import SafariServices

class PKShareViewController: UIViewController,MFMailComposeViewControllerDelegate, GIDSignInUIDelegate, GIDSignInDelegate, SFSafariViewControllerDelegate, GINInviteDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title = PKConstants.StoryboardNameShare
        
    }
    @IBAction func facebookShareAction(sender: AnyObject) {
        
        let shareToFacebook:SLComposeViewController=SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        
        shareToFacebook.completionHandler = { (result) in
            
            switch(result) {
                //  This means the user cancelled without sending the Share
            case .Cancelled:
                break
                //  This means the user hit 'Send'
                
            case .Done:
                self.handleError("", message: "Shared Successfully")
                break
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.whiteColor()
                
                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    
                    UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.whiteColor()
                })
                
            })
            
        }
        
        shareToFacebook.setInitialText("Go Plonk!\n\nCheck out Plonk. This app really helped me snatch the best parking lots around the city.\n\nVisit www.goplonk.com")
        shareToFacebook.addImage(UIImage(named: "ic_plonk_small"))
        shareToFacebook.addURL(NSURL(string: "http://www.goplonk.com"))
        UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.blackColor()
        self.presentViewController(shareToFacebook, animated: true) { () -> Void in
            
            UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.blackColor()
            
        }
        
    }
    @IBAction func twitterShare(sender: AnyObject) {
        
        let shareToTwitter:SLComposeViewController=SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        
        shareToTwitter.completionHandler = { (result) in
            
            switch(result) {
                //  This means the user cancelled without sending the Share
            case .Cancelled:
                break
                //  This means the user hit 'Send'
                
            case .Done:
                self.handleError("", message: "Shared Successfully")
                break
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.whiteColor()
                
                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    
                    
                    UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.whiteColor()
                })
                
            })
            
        }
        
        shareToTwitter.setInitialText("Go Plonk!\nPlonk that helps me snatch the best parking lots.\nVisit www.goplonk.com")
        shareToTwitter.addImage(UIImage(named: "ic_plonk_small"))
        shareToTwitter.addURL(NSURL(string: "http://www.goplonk.com"))
        UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.blackColor()
        self.presentViewController(shareToTwitter, animated: true) { () -> Void in
            
            UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.blackColor()
            
        }
    }
    
    @IBAction func gPlusShareAction(sender: AnyObject) {
        
        if (GIDSignIn.sharedInstance().hasAuthInKeychain()) {
            let invite = GINInvite.inviteDialog()
            invite.setMessage("Check out Plonk that helps me snatch the best parking lots.\n\nVisit www.goplonk.com")
            invite.setTitle("Go Plonk")
            invite.setDeepLink("/invite")
            invite.setInviteDelegate(self)
            invite.open()
        }else {
            
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().signIn()
            
        }
        
        /*
        
        // Construct the Google+ share URL
        NSURLComponents* urlComponents = [[NSURLComponents alloc]
        initWithString:@"https://plus.google.com/share"];
        urlComponents.queryItems = @[[[NSURLQueryItem alloc]
        initWithName:@"url"
        value:[shareURL absoluteString]]];
        NSURL* url = [urlComponents URL];
        
        if ([SFSafariViewController class]) {
        // Open the URL in SFSafariViewController (iOS 9+)
        SFSafariViewController* controller = [[SFSafariViewController alloc]
        initWithURL:url];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
        } else {
        // Open the URL in the device's browser
        [[UIApplication sharedApplication] openURL:url];
        }
        
        */
        
        /*
        let urlComponents = NSURLComponents(string: "https://plus.google.com/share")
        let shareURL = NSURL(string: "http://www.goplonk.com")
        urlComponents?.queryItems = [NSURLQueryItem(name: "url", value: shareURL!.absoluteString)]
        
        if #available(iOS 9.0, *) {
            
            let controller = SFSafariViewController(URL: urlComponents!.URL!)
            controller.delegate = self
            presentViewController(controller, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            UIApplication.sharedApplication().openURL(urlComponents!.URL!)
        }
        */
        
    }
    
    @IBAction func emailShareAction(sender: AnyObject) {
        let mailComposeViewController = configuredMailComposeViewController()
       
        self.presentViewController(mailComposeViewController, animated: true, completion: nil)

    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setSubject("Go Plonk")
        mailComposerVC.setMessageBody("Check out Plonk. This app really helped me snatch the best parking lots around the city.\n\nVisit www.goplonk.com", isHTML: false)
        
        UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.blackColor()
        
        return mailComposerVC
    }
    
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.whiteColor()
        
        switch result.rawValue {
        case MFMailComposeResultCancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResultSaved.rawValue:
            print("Mail saved")
        case MFMailComposeResultSent.rawValue:
            print("Mail sent")
            handleError("", message: "Shared Successfully")
        case MFMailComposeResultFailed.rawValue:
            print("Mail sent failure: \(error!.localizedDescription)")
        default:
            break
        }
        self.dismissViewControllerAnimated(false, completion: nil)
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // [START signin_handler]
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
        if (error == nil) {
            // User Successfully signed in.
            gPlusShareAction(self)
            
        } else {
            print("\(error.localizedDescription)")

        }
    }
    // [START invite_finished]
    func inviteFinishedWithInvitations(invitationIds: [AnyObject]!, error: NSError!) {
        if (error != nil) {
            print("Failed: " + error.localizedDescription)
        } else {
            print("Invitations sent")
            handleError("", message: "Shared Successfully")
        }
    }
    // [END invite_finished]
}
