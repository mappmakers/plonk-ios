//
//  PKParkingHistoryTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class PKParkingHistoryTableViewController: AEAccordionTableViewController, NSFetchedResultsControllerDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    var myParkingHistoryFRC:NSFetchedResultsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "My Parking History"
        
        tableView.separatorStyle = .None

        let cellNib = UINib(nibName: "PKAccordionTableViewCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "accordionCell")

        myParkingHistoryFRC = PKUserParkingDetail.MR_fetchAllSortedBy("exitDate", ascending: false, withPredicate: nil, groupBy: nil, delegate: self, inContext: NSManagedObjectContext.MR_defaultContext())
        myParkingHistoryFRC?.delegate = self
        myParkingLotHistoryList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func myParkingLotHistoryList()
    {
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.MyParkingLotHistory.rawValue), parameters: ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]) { (success:Int!, result:AnyObject?) -> Void in
           
            
            if success == 1
            {
                PKUserParkingDetail.MR_truncateAll()

                let json = result as! Dictionary<String,AnyObject>
                let myParkingLotArray = json["parking_history"] as! NSArray
                
                if myParkingLotArray.count > 0
                {
                    for userHistoryDict in myParkingLotArray {
                        
                        let totalDuration = userHistoryDict["total_duration"] as? String
                        if totalDuration != "00:00:00" {
                            PKUserParkingDetail.userParkingHistoryWithParameters(userHistoryDict as! Dictionary<String,AnyObject>)
                        }
 
                    }
                    
                }else {
                    self.tableView.emptyDataSetSource = self
                    self.tableView.emptyDataSetDelegate = self
                    
                    self.tableView.reloadData()
                }
            }
            else
            {
                self.tableView.emptyDataSetSource = self
                self.tableView.emptyDataSetDelegate = self
                
                self.tableView.reloadData()
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
            }
            
            
        }
        
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return myParkingHistoryFRC?.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        let sectionInfo = myParkingHistoryFRC!.sections![section]
        return sectionInfo.numberOfObjects
    }

    // MARK: - NSFetchedResultsControllerDelegate
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        let tableView:UITableView = self.tableView
        
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Update:
            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!) as! PKAccordionTableViewCell, atIndexPath: indexPath!)
            break
        default:
            break
        }
    }
    
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
            
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
        default:break
        }
        
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("accordionCell", forIndexPath: indexPath) as! PKAccordionTableViewCell
        
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        configureCell(cell, atIndexPath: indexPath)
        
        return cell
    }
    
    func configureCell(cell:PKAccordionTableViewCell, atIndexPath indexPath:NSIndexPath) {
        
        let parkingLotFeed = myParkingHistoryFRC!.objectAtIndexPath(indexPath) as! PKUserParkingDetail
        
        cell.headerView.topLabel.text = parkingLotFeed.parking_lot_name

        cell.headerView.bottomLabel.text = parkingLotFeed.exit_time
        
        if let amount = parkingLotFeed.amount {
            let parkingAmtString = "₹ "+amount
            cell.headerView.CostLabel.text = parkingAmtString
            
        }
        
        if let slotName = parkingLotFeed.display_message
        {
            let slotNameString = "You parked at "+slotName
            cell.detailView.parkerName.text = slotNameString
        }

        cell.detailView.totalTime.text = parkingLotFeed.total_duration
        cell.detailView.startTime.text = parkingLotFeed.entry_time?.componentsSeparatedByString(" on ")[0]
        cell.detailView.endTime.text = parkingLotFeed.exit_time?.componentsSeparatedByString(" on ")[0]
        
        cell.detailView.passcardCompletion = { (sender) -> Void in
            
            self.presentExitCard(parkingLotFeed)
        }
        
        if let carName = parkingLotFeed.make_name {
            cell.detailView.carName.text = "\(carName) [\(parkingLotFeed.registeration_no!)]"
        }

    }
    
    func presentExitCard(parkingDetail:PKUserParkingDetail) {
        
        let cardController = UIStoryboard(name: PKConstants.StoryboardNameParkingLot, bundle: nil).instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingExitCard) as! PKParkingExitCardBaseTableViewController
        cardController.userParkingDetail = parkingDetail
        navigationController?.presentViewController(cardController, animated: true, completion: { () -> Void in
            
        })
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return expandedIndexPaths.contains(indexPath) ? 220.0 : 60.0
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        
       // return UIImage(named: "ic_parking_map")
        
         return UIImage(named: "ic_sad")
        
        
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let emptyTitle:String = ""
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        let emptyTitle:String = "You haven't used any parking lots yet"
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
        
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView!) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
}
