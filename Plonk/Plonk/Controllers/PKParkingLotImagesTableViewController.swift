//
//  PKParkingLotImagesTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 04/12/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD
import AFNetworking
import JTSImageViewController

private let reuseIdentifier = "ImageCollectionCell"

class PKParkingLotImagesTableViewController: UITableViewController, UICollectionViewDataSource, UICollectionViewDelegate  {
    
    private var collectionImages:Array<UIImage>?
    
    var parkingLotId:String!
    
    let attachmentCell = PKAttachmentCell(style:UITableViewCellStyle.Default , reuseIdentifier: PKConstants.CellBasicIdentifier)

    var dataArray:Array<UITableViewCell>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Add lot Image"

        tableView.separatorStyle = .None
        
        let backBtn = createBackButton()
        backBtn.addTarget(self, action: "backPressed:", forControlEvents: .TouchUpInside)
        
        let btnRight = UIButton(type: UIButtonType.Custom)
        btnRight.setTitle("Skip", forState: .Normal)
        btnRight.addTarget(self, action: "backPressed:", forControlEvents: .TouchUpInside)
        btnRight.titleLabel?.font = UIFont.PKLaChataNormal(14)
        btnRight.frame = CGRectMake(0, 0, 60, 30)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnRight)

        collectionImages = []
        
        attachmentCell.collectionView?.dataSource = self
        attachmentCell.collectionView?.delegate = self
        
        // Register cell classes
        attachmentCell.collectionView!.registerClass(PKCollectionImageCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    
        let finishCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        finishCell.textLabel?.text = "Save"
        finishCell.backgroundColor = UIColor.appThemeColor()
        
        dataArray = [attachmentCell,finishCell]
        
        self.handleError("", message: "Your parking lot has been successfully added and will be reviewed as soon as possible. Please add images of your parking lot to complete the setup.", handler: { (action) -> Void in
            
        })
    }

    func backPressed(sender:UIButton) {
        handleDecisionPrompt("", message: "Are you sure you want to skip adding images") { (action) -> Void in
            
            for (_, value) in self.navigationController!.viewControllers.enumerate() {
                
                if value.classForCoder == PKMyParkingParentViewController.classForCoder() {
                    self.navigationController!.popToViewController(value, animated: true)
                }
                
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return dataArray!.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
 
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0{
            return 46
        }
        return 0.001
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 140
        }
        return 50
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return dataArray![indexPath.section]
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView:UIView = UIView(frame: CGRectZero)
            let label:UILabel = UILabel(frame: CGRectZero)
            label.font = UIFont.PKAllerRegular(12)
            label.textColor = UIColor.appThemeColor()
            label.numberOfLines = 2
            label.text = "Usage of graphical or violent content will lead to a permanent ban from our services."
            headerView.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            label.addLeftEdgeAttachConstraint(headerView, offset: 10)
            label.addRightEdgeAttachConstraint(headerView, offset: -10)
            label.addTopEdgeAttachConstraint(headerView, offset: 10)
            label.addBottomEdgeAttachConstraint(headerView)
            
            return headerView
        }
        return nil
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        if indexPath.section == 1 {
            
            if collectionImages!.count != 0 {
                let parameters : Dictionary<String, AnyObject> =
                [
                    "user_id":PKDataModel.sharedInstance.loggedUser.user_id!,
                    "secure_key":NSUserDefaults.standardUserDefaults().objectForKey(PKConstants.UserDefaultTokenValue)!,
                    "Lot_id":parkingLotId!
                ]
                
                SVProgressHUD.showWithMaskType(.Gradient)
                let manager = AFHTTPRequestOperationManager()
                manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json","text/plain") as Set<NSObject>
                let operation = manager.POST(PKWebServiceRoute.Path(PKWebServiceEndPoints.AddParkingLotImage.rawValue).URLString, parameters: parameters, constructingBodyWithBlock: { (formData: AFMultipartFormData!) -> Void in
                    
                    for image in self.collectionImages! {
                        let data = UIImageJPEGRepresentation(image, 0.5)
                        let index = self.collectionImages!.indexOf(image)
                        
                        formData.appendPartWithFileData(data!, name: "upload_image\(index!+1)", fileName: "photo.jpg", mimeType: "image/jpeg")
                    }
                    
                    
                    }, success: { (operation, responseObject) -> Void in
                        print(responseObject)
                        
                        SVProgressHUD.dismiss()
                        
                        if let responseJson = responseObject as? Dictionary<String, AnyObject> {
                            
                            if let errorMessage = responseJson["error"] as? String {
                                
                                if errorMessage == "Authentication failed" {
                                    
                                    self.handleError("Session Expired", message: "Your current ssession has been expired. You wil now be redirected to login page. Please login to continue.", handler: { (action) -> Void in
                                        
                                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                        
                                        appDelegate.customizeForAuthenticationRequired()
                                        
                                    })
                                    
                                    return
                                    
                                }
                                
                            }
                            
                        }
                        
                        self.handleError("", message: "Parking lot images have been successully saved", handler: { (action) -> Void in
                            
                            for (_, value) in self.navigationController!.viewControllers.enumerate() {
                                
                                if value.classForCoder == PKMyParkingParentViewController.classForCoder() {
                                    self.navigationController!.popToViewController(value, animated: true)
                                }
                                
                            }
                            
                        })
                        
                    }) { (operation, error) -> Void in
                        print(error)
                        SVProgressHUD.dismiss()
                        self.handleError("Sorry!", message: "Something went wrong")
                }
                
                operation!.start()
            }else {
                
                handleDecisionPrompt("", message: "You haven't added any images for this parking lot. Do you want to skip adding images?", confirmDecisionHandler: { (action) -> Void in
                    
                    for (_, value) in self.navigationController!.viewControllers.enumerate() {
                        
                        if value.classForCoder == PKMyParkingParentViewController.classForCoder() {
                            self.navigationController!.popToViewController(value, animated: true)
                        }
                        
                    }
                    
                })
                
            }
            
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1+(collectionImages?.count ?? 0)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as? PKCollectionImageCell
        
        if cell == nil {
            cell = PKCollectionImageCell(frame: CGRectZero)
        }
        
        if indexPath.section != 0 {
            cell?.collectionImageView!.image = collectionImages![indexPath.section-1]
            cell?.collectionImageView!.contentMode = .ScaleAspectFit
            
        }else {
            cell?.collectionImageView!.image = UIImage(named: "add_parking")
            cell?.collectionImageView!.contentMode = .Center
        }
        return cell!
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0{
            if collectionImages!.count<5 {
                
                btnImagePickerClicked(collectionView)
                
            }else {
                handleError("", message: "Only a maximum of 5 images can be added")
            }
            

        }else {
            
            let alert = UIAlertController(title: "Choose an Action", message: "Please select an option from below", preferredStyle: UIAlertControllerStyle.ActionSheet)
            
            alert.addAction(UIAlertAction(title: "Preview", style: .Default, handler: { (action) -> Void in
                
                let imageInfo = JTSImageInfo()
                imageInfo.image = self.collectionImages![indexPath.section-1]
                imageInfo.referenceRect = self.tableView.rectForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
                imageInfo.referenceView = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
                let imageController = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.Image, backgroundStyle: JTSImageViewControllerBackgroundOptions.Blurred)
                imageController.showFromViewController(self, transition: .FromOriginalPosition)
                
            }))
            alert.addAction(UIAlertAction(title: "Remove", style: .Destructive, handler: { (action) -> Void in
                self.collectionImages!.removeAtIndex(indexPath.section-1)
                
                self.attachmentCell.collectionView?.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
            alert.view.tintColor = UIColor.blackColor()
            self.presentViewController(alert, animated: true, completion: { () -> Void in
                alert.view.tintColor = UIColor.blackColor()
            })
            alert.view.tintColor = UIColor.blackColor()
            
        }
    }
    
    internal func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
        dismissViewControllerAnimated(true) { () -> Void in
            
            self.collectionImages?.append(image)
            
            self.attachmentCell.collectionView?.reloadData()
            
        }
    }
    
    
    
}
