//
//  PKSignUpTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD
import MZSelectableLabel

private struct PKSignUpConstants {
    
    static let TitleNavBar:String = "Sign Up"
    
    static let TitleNext:String = "Next"
    static let TitleAlreadyAccount:String = "Already have an account? "
    static let TitleSignIn:String = "Sign In"
    
    static let PlaceholderFullName:String = "F U L L  N A M E"
    
}

enum PKSignUpError: ErrorType {
    case NoFullName, NoEmail, NoPassword
}

extension PKSignUpError: CustomStringConvertible {
    var description: String {
        switch self {
        case NoFullName: return "Please enter your full name"
        case NoEmail: return "Please enter a valid email address"
        case NoPassword: return "Please enter a valid password"
        }
    }
}

class PKSignUpTableViewController: PKAuthenticationFormTableViewController, CLLocationManagerDelegate {
   
    var hasAcceptedTerms : Bool?
    
    var currentPlace:GMSPlace?

//    var currentPlace:CLPlacemark?
    
    func initializeCurrentLocation()
    {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        locationManager.requestWhenInUseAuthorization()
        
        GMSPlacesClient.sharedClient().currentPlaceWithCallback({ (placeLikelihood, error) -> Void in
            if error != nil {
                print("Current Place error: \(error!.localizedDescription)")
                return
            }
            
            var currentLikelihood:Double = 0.0
            for likelihood in placeLikelihood!.likelihoods {
                if let likelihood = likelihood as? GMSPlaceLikelihood {
                    let place = likelihood.place
                    print("Current Place name \(place.name) at likelihood \(likelihood.likelihood)")
                    print("Current Place address \(place.formattedAddress)")
                    print("Current Place attributions \(place.attributions)")
                    print("Current PlaceID \(place.placeID)")
                    
                    if currentLikelihood == 0.0 {
                        self.currentPlace = likelihood.place
                    }else if likelihood.likelihood > currentLikelihood {
                        currentLikelihood = likelihood.likelihood
                        self.currentPlace = likelihood.place
                    }
                    
                }
            }
        })

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }else {
            print("Location services are not enabled")
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        manager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .NotDetermined, .Restricted, .Denied:
                print("No access")
                break
            case .AuthorizedAlways, .AuthorizedWhenInUse:
                print("Access")
                break
            }
        } else {
            print("Location services are not enabled")
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = PKSignUpConstants.TitleNavBar
        
        arrayCells?.append(fullNameCell())
        arrayCells?.append(emailCell)
        arrayCells?.append(passwordCell)
        
        arrayCells?.append(termConditionCell())

        whiteCell.textLabel?.text = PKSignUpConstants.TitleNext
        
        arrayCells?.append(whiteCell)
        
        arrayCells?.append(signInCell())
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.initializeCurrentLocation()
        }
    }
    
    private func fullNameCell() -> PKTextFieldLoginTableViewCell {
        
        let cell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        cell.textField.placeholder = PKSignUpConstants.PlaceholderFullName
        cell.textField.delegate = self
        cell.textField.returnKeyType = UIReturnKeyType.Next
        cell.textField.nextField = emailCell.textField
        
    
        return cell
    }
    
    private func termConditionCell() -> UITableViewCell  {
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        let titleString = "By signing up, you are agreeing to the Terms Of Use & Privacy Policy."
        
        let termsString:NSMutableAttributedString = NSMutableAttributedString(string: titleString, attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName:UIFont.PKAllerRegular(11)])
        termsString.addAttributes([NSUnderlineStyleAttributeName : NSNumber(integer: 1),NSUnderlineColorAttributeName:UIColor.whiteColor()], range: NSString(string: titleString).rangeOfString("Terms Of Use"))
        termsString.addAttributes([NSUnderlineStyleAttributeName : NSNumber(integer: 1),NSUnderlineColorAttributeName:UIColor.whiteColor()], range: NSString(string: titleString).rangeOfString("Privacy Policy"))
        cell.textLabel?.attributedText = termsString
        
        //        cell.customizeForAttributedTextWith("By singing in, you are agreeing to the ", secondText: "terms and conditions & privacy policy.")
        
        return cell
        
    }
    
    private func termsPrivacyCell() -> UITableViewCell {
        
        let cell = UITableViewCell(style: .Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = .None

        let checkbox = UIButton(type: .Custom)
        checkbox.setImage(UIImage(named: "ic_checked_white"), forState: .Selected)
        checkbox.setImage(UIImage(named: "ic_unchecked_white"), forState: .Normal)
        checkbox.addTarget(self, action: "checkboxTapped:", forControlEvents: .TouchDown)
        checkbox.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        
        cell.contentView.addSubview(checkbox)
        
        checkbox.translatesAutoresizingMaskIntoConstraints = false
        checkbox.addTopEdgeAttachConstraint(cell.contentView, offset: 20)
        checkbox.addLeftEdgeAttachConstraint(cell.contentView, offset: 10)
        checkbox.addWidthConstraint(25)
        checkbox.addHeightConstraint(25)
        
        let agreeView = PKClickableLabel(frame: CGRectZero)
        //            agreeView.parentController = self
        
        //            agreeView.termsCompletion = { (sender) in Void()
        //
        //
        //            }
        
        cell.contentView.addSubview(agreeView)
        agreeView.translatesAutoresizingMaskIntoConstraints = false
        agreeView.addLeftEdgeAttachConstraint(checkbox, viewEdge: .Right, offset: 10)
        agreeView.addBottomEdgeAttachConstraint(cell.contentView)
        agreeView.addRightEdgeAttachConstraint(cell.contentView, offset: 10)
        agreeView.addTopEdgeAttachConstraint(cell.contentView, offset: 15)
        
        //            var termsString:String = "By proceeding, you agree to the supplier #agreement #<ts>terms and conditions."
        //            if UIScreen.mainScreen().bounds.size.height > 568 {
        //                termsString = "By proceeding, you agree to the supplier agreement #<ts>terms and conditions."
        //            }
        agreeView.buildAgreeTextViewFromString("By singing up, you are agreeing to the #<ts>terms of use# & #<pp>privacy policy.")

        return cell
    }

    
    func checkboxTapped(sender:UIButton) {
        
        sender.selected = !sender.selected
        
        hasAcceptedTerms = sender.selected
        
        
    }

    
    private func signInCell() -> UITableViewCell {
        
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        
        cell.customizeForAttributedTextWith(PKSignUpConstants.TitleAlreadyAccount, secondText: PKSignUpConstants.TitleSignIn)
        
        return cell
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = false
        navigationController?.navigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return PKAuthenticationFormConstants.TableHeaderHeight
        }
        return PKConstants.CellTextFieldHeight
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.view.endEditing(true)
        
        if indexPath.row == 4 {
            
            let webViewController = PKWebViewController()
            webViewController.htmlKey = "terms_cond";
            self.navigationController?.pushViewController(webViewController, animated: true)

            
        }
        if indexPath.row == 5 {

            signUpPressed()
            
//            if hasAcceptedTerms == true {
//                signUpPressed()
//            }else {
//                handleError("", message: "Please agree to the terms and conditions & privacy policy before you proceed")
//            }
           
            /*
            do {
                try signUpPressed()
            } catch let error as PKSignUpError {
                print(error.description)
                handleError(PKConstants.TitleErrorHeader, message: error.description)
            } catch {
                handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
            }
            */
        }else if indexPath.row == 6 {
            navigationController?.popViewControllerAnimated(true)
        }

    }
    
    func signUpPressed()
    {

        let fullNameStr = (arrayCells![1] as! PKTextFieldLoginTableViewCell).textField.text!
        let emailStr = emailCell.textField.text!
        let passwordStr = passwordCell.textField.text!
        
        var isValid:Bool = true
        let title:String = "\n"
        var message:String = ""
        if !emailStr.isValidEmail() {
            isValid = false
            
            message += String.bulletinedText("Enter a valid email address\n")
        }
        
        if passwordStr.characters.count<8 {
            isValid = false
            message += String.bulletinedText("Your password should have atleast 8 characters\n")
        }
        
        var placemarkStr = ""
        
        if currentPlace != nil {
            placemarkStr = currentPlace!.formattedAddress!
        }
        
        if isValid {
            
            let parameters:Dictionary<String, String> = ["user_email":emailStr,"user_password":passwordStr,"user_name":fullNameStr,"user_place_mark":placemarkStr]
            
            SVProgressHUD.showWithMaskType(.Gradient)

            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserSignUp.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    // time-consuming task
                    dispatch_async(dispatch_get_main_queue(), {
                        SVProgressHUD.dismiss()
                    })
                })
                
                if success == 1 {

                    var json = result as! Dictionary<String, AnyObject>

                    if json["success"] as! String == "false" {
                        
                        let title = "User email already exists"
                        let message = "A user has already been registered with the email address you provided"
                        self.handleError(title, message: message)
 
                    }else {
                        
                        json["user_email"] = emailStr
                        json["otp_status"] = ""
                        json["user_consumerno"] = ""
                        json["user_mobile"] = ""
                        json["user_place_mark"] = placemarkStr
                        json["user_name"] = fullNameStr
                        
                        let token = json["secure_key"] as! String
                        
                        NSUserDefaults.standardUserDefaults().setObject(token, forKey: PKConstants.UserDefaultTokenValue)
                        NSUserDefaults.standardUserDefaults().synchronize()
                        
                        PKDataModel.sharedInstance.loggedUser = PKUser.userWithDictionaryData(json)
                       
                        let confirmMobileController = PKMobileInputTableViewController(style: UITableViewStyle.Plain)
                        confirmMobileController.authenticationDelegate = self.authenticationDelegate
                        self.navigationController?.pushViewController(confirmMobileController, animated: true)
                    }
                    
                }else {
                    
                    let error:NSError = (result as! NSError)
                    let title = String(error.code) + ": " + error.domain
                    let message = error.localizedDescription
                    self.handleError(title, message: message)
                    
                }
                
            }
            
        }else {
            
            self.handleError(title, message: message)
        }

    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.navigationController?.navigationBarHidden = true
        })
    }
    
    override func textFieldDidEndEditing(textField: UITextField) {
        super.textFieldDidEndEditing(textField)
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.navigationController?.navigationBarHidden = false
        })
        
    }
    
}
