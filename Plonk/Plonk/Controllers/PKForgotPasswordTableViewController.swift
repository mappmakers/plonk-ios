//
//  PKForgotPasswordTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD

private struct PKForgotPasswordConstants {
    
    static let Title:String = "Forgot Password"
    
    static let TitleReset:String = "Reset Password"
    static let TitleJustRemebered:String = "Just remembered? "
    static let TitleResetPassword:String = "Sign in"

}


class PKForgotPasswordTableViewController: PKAuthenticationFormTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = PKForgotPasswordConstants.Title
        emailCell.textField.returnKeyType = UIReturnKeyType.Done
        emailCell.textField.autocapitalizationType = .None
        arrayCells?.append(emailCell)
        arrayCells?.append(forgotPasswordCell())
        whiteCell.textLabel?.text = PKForgotPasswordConstants.TitleReset
        arrayCells?.append(whiteCell)
    }

    private func forgotPasswordCell() -> UITableViewCell {
        
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        
        cell.customizeForAttributedTextWith(PKForgotPasswordConstants.TitleJustRemebered, secondText: PKForgotPasswordConstants.TitleResetPassword)
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = false
        navigationController?.navigationBarHidden = false
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return PKAuthenticationFormConstants.TableHeaderHeight
        }
        return PKConstants.CellTextFieldHeight
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        view.endEditing(true)
        
        switch indexPath.row {
            
        case 2:
            navigationController?.popViewControllerAnimated(true)
            break
        case 3:
            
             let emailUser=emailCell.textField.text!
             
             SVProgressHUD.showWithMaskType(.Gradient)
             
             PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserForgotPassword.rawValue), parameters:["user_email":emailUser], completionClosure: { (success:Int!, result:AnyObject?) -> Void in
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    // time-consuming task
                    dispatch_async(dispatch_get_main_queue(), {
                        SVProgressHUD.dismiss()
                    })
                })
                
                if success == 1 {
                    let json = result as! Dictionary<String, AnyObject>
                    
                    if json["success"] as! String == "false" {
                        
                        let title = "User doesnt exist"
                        let message = "Sorry, The email doesnt exist in our database"
                        self.handleError(title, message: message)
                        
                    }
                    else
                    {
                        let title = "Success"
                        let message = "The updated password has been sent to your email id"
                        self.handleError(title, message: message)
                        self.navigationController?.popViewControllerAnimated(true)
                        
                    }
                    
                    
                }
                else {
                    
                    let error:NSError = (result as! NSError)
                    let title = String(error.code) + ": " + error.domain
                    let message = error.localizedDescription
                    self.handleError(title, message: message)
                    
                }
                

                
                
                
             })

            
            //navigationController?.popViewControllerAnimated(true)
            break

        default:
            break
            
        }
    }
}
