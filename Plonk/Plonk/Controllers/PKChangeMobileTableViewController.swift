//
//  PKChangeMobileTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 28/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

typealias UpdateMobileCompletion = (updatedMobile : String) -> Void

class PKChangeMobileTableViewController: PKMobileInputTableViewController {

    var updatedMobileCompletion:UpdateMobileCompletion?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Change Mobile Number"
        
        tableView.backgroundColor = UIColor.whiteColor()
        
        let headerCell = arrayCells![0]
        headerCell.textLabel?.textColor = UIColor.appThemeColor()
        
        let footerCell = arrayCells![2]
        footerCell.textLabel?.textColor = UIColor.appThemeColor()
        
        let mobileNumberCell = arrayCells![1] as! PKMobileInputTableViewCell
        mobileNumberCell.backgroundColor = UIColor.whiteColor()
        mobileNumberCell.textFieldMobileNumber.layer.borderColor = UIColor.appThemeColor().CGColor
        mobileNumberCell.textFieldMobileNumber.layer.borderWidth = 2.0
        mobileNumberCell.btnCountryCode.layer.borderColor = UIColor.appThemeColor().CGColor
        mobileNumberCell.btnCountryCode.layer.borderWidth = 2.0
        mobileNumberCell.textFieldMobileNumber.textColor = UIColor.appThemeColor()
        mobileNumberCell.btnCountryCode.setTitleColor(UIColor.appThemeColor(), forState: .Normal)
        
        accessoryViewContinue.backgroundColor = UIColor.appThemeColor()
        accessoryBtnContinue.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.hidesBackButton = false
        navigationController?.navigationBarHidden = false
    }
}
