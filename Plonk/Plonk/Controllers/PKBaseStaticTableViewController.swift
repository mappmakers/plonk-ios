//
//  PKBaseStaticTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 21/09/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKBaseStaticTableViewController: UITableViewController {

    var arrayCells:Array<UITableViewCell>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customizeForStaticTableStyle()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return arrayCells?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return arrayCells![indexPath.row]
    }

}
