
//
//  PKMapSearchResultsTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 07/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

public typealias GMSPlaceResultAutocompletePredictionCallback = (GMSAutocompletePrediction?, NSError?) -> Void

class PKMapSearchResultsTableViewController: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating {

    private var arrayResults:Array<GMSAutocompletePrediction>?
    var autocompleteCallback:GMSPlaceResultAutocompletePredictionCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: PKConstants.CellBasicIdentifier)
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.keyboardDismissMode = .OnDrag
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayResults?.count ?? 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(PKConstants.CellBasicIdentifier, forIndexPath: indexPath)

        // Configure the cell...
        let result = arrayResults![indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.attributedText = result.attributedFullText
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if autocompleteCallback != nil {
            autocompleteCallback!(arrayResults![indexPath.row],nil)
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func placeAutocomplete(searchText:String?) {
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.NoFilter

        GMSPlacesClient.sharedClient().autocompleteQuery(searchText ?? "", bounds: nil, filter: filter, callback: { (results, error: NSError?) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
            }
            
            if results != nil {
                for result in results! {
                    if let result = result as? GMSAutocompletePrediction {
                        print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                    }
                }
                self.arrayResults = results as? Array<GMSAutocompletePrediction>
            }else {
                self.arrayResults = Array<GMSAutocompletePrediction>()
            }
            self.tableView.reloadData()

        })
    }
    
    // MARK:- UISearchResultsUpdating
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        placeAutocomplete(searchController.searchBar.text)
        
    }

    // MARK:- UISearchBarDelegate
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        
    }

}
