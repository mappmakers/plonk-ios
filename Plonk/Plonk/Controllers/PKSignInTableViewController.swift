//
//  PKSignInTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import MaterialKit
import MagicalRecord
import SVProgressHUD

private struct PKSignInConstants {

    static let ForgotButtonVerticalBorderOffset:CGFloat = 1
    static let ForgotButtoLeftBorderOffset:CGFloat = 30
    static let ForgotButtonWidth:CGFloat = 120
    static let ForgotButtonHeight:CGFloat = 44
    
    static let SignInButtonVerticalBorderOffset:CGFloat = 10
    static let SignInButtonWidth:CGFloat = 200
    static let SignInButtonHeight:CGFloat = 44

    static let TitleSignIn:String = "Sign In"
    static let TitleTroubleLogin:String = "Trouble Logging in? "
    static let TitleResetPassword:String = "Reset your password"
    static let TitleDontHaveAccount:String = "Don't have an account? "
    static let TitleSignup:String = "Sign Up"
    
    static let TitleGPlus:String = "Connect with\nGoogle"
    static let TitleFacebook:String = "Connect with\nFacebook"
}

class PKSignInTableViewController: PKAuthenticationFormTableViewController, GIDSignInDelegate, GIDSignInUIDelegate, FBSDKLoginButtonDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
//        emailCell.textField.text = "farhan@focaloid.com"
//        passwordCell.textField.text = "w6c3p"
        
        arrayCells?.append(emailCell)
        arrayCells?.append(passwordCell)
        
        arrayCells?.append(forgotPasswordCell())

        whiteCell.textLabel?.text = PKSignInConstants.TitleSignIn
        
        arrayCells?.append(whiteCell)
        
        arrayCells?.append(termConditionCell())

        arrayCells?.append(signupCell())
        
        arrayCells?.append(footerCell())
        
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "receiveToggleAuthUINotification:", name: "ToggleAuthUINotification", object: nil)
        
    }
    
    
    
    private func forgotPasswordCell() -> UITableViewCell {
    
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        
        cell.customizeForAttributedTextWith(PKSignInConstants.TitleTroubleLogin, secondText: PKSignInConstants.TitleResetPassword)
        
        return cell
    }

    private func signupCell() -> UITableViewCell {
    
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        
        cell.customizeForAttributedTextWith(PKSignInConstants.TitleDontHaveAccount, secondText: PKSignInConstants.TitleSignup)
        
        return cell
        
    }
    
    private func termConditionCell() -> UITableViewCell  {
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        let titleString = "By signing in, you are agreeing to the Terms Of Use & Privacy Policy."
        
        let termsString:NSMutableAttributedString = NSMutableAttributedString(string: titleString, attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName:UIFont.PKAllerRegular(11)])
        termsString.addAttributes([NSUnderlineStyleAttributeName : NSNumber(integer: 1),NSUnderlineColorAttributeName:UIColor.whiteColor()], range: NSString(string: titleString).rangeOfString("Terms Of Use"))
        termsString.addAttributes([NSUnderlineStyleAttributeName : NSNumber(integer: 1),NSUnderlineColorAttributeName:UIColor.whiteColor()], range: NSString(string: titleString).rangeOfString("Privacy Policy"))
        cell.textLabel?.attributedText = termsString
        
//        cell.customizeForAttributedTextWith("By singing in, you are agreeing to the ", secondText: "terms and conditions & privacy policy.")
        
        return cell
        
    }
    private func termsPrivacyCell() -> UITableViewCell {
        
        let cell = UITableViewCell(style: .Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = .None
        
        let agreeView = PKClickableLabel(frame: CGRectZero)
        //            agreeView.parentController = self
        
        //            agreeView.termsCompletion = { (sender) in Void()
        //
        //
        //            }
        
        cell.contentView.addSubview(agreeView)
        agreeView.translatesAutoresizingMaskIntoConstraints = false
        agreeView.addLeftEdgeAttachConstraint(cell.contentView, offset: 10)
        agreeView.addBottomEdgeAttachConstraint(cell.contentView)
        agreeView.addRightEdgeAttachConstraint(cell.contentView, offset: 10)
        agreeView.addTopEdgeAttachConstraint(cell.contentView, offset: 15)
        
        //            var termsString:String = "By proceeding, you agree to the supplier #agreement #<ts>terms and conditions."
        //            if UIScreen.mainScreen().bounds.size.height > 568 {
        //                termsString = "By proceeding, you agree to the supplier agreement #<ts>terms and conditions."
        //            }
        
        agreeView.buildAgreeTextViewFromString("By singing in, you are agreeing to the #<ts>terms of use# & #<pp>privacy policy.")
        
        return cell
    }

    
    private func footerCell() -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        let gplusView = GIDSignInButton(frame: CGRectZero)
        gplusView.colorScheme = .Light
        gplusView.style = .Wide
        
        cell.contentView.addSubview(gplusView)
        
        gplusView.translatesAutoresizingMaskIntoConstraints = false

        gplusView.addLeftEdgeAttachConstraint(cell.contentView, offset: 15)
        gplusView.addRightEdgeAttachConstraint(cell.contentView, offset: 15)
        gplusView.addTopEdgeAttachConstraint(cell.contentView)
        gplusView.addHeightConstraint(44)
        
        let fbLoginView = FBSDKLoginButton(type: .Custom)
        fbLoginView.readPermissions = ["email"]
        fbLoginView.tooltipColorStyle = .NeutralGray
        fbLoginView.delegate = self
        
        cell.contentView.addSubview(fbLoginView)
        
        fbLoginView.translatesAutoresizingMaskIntoConstraints = false
        
        fbLoginView.addLeftEdgeAttachConstraint(gplusView)
        fbLoginView.addRightEdgeAttachConstraint(gplusView)
        fbLoginView.addTopEdgeAttachConstraint(gplusView, viewEdge: .Bottom, offset: 15)
        fbLoginView.addHeightConstraint(44)
        
        /*
        let gPlusView = UIView(frame: CGRectZero)
        gPlusView.backgroundColor = UIColor.clearColor()
        cell.contentView.addSubview(gPlusView)
        
        let googleTapGesture = UITapGestureRecognizer(target: self, action: "didPressGoogleSignIn:")
        gPlusView.addGestureRecognizer(googleTapGesture)
        
        gPlusView.translatesAutoresizingMaskIntoConstraints = false
        
        gPlusView.addLeftEdgeAttachConstraint(cell.contentView, offset: 15)
        gPlusView.addTopEdgeAttachConstraint(cell.contentView)
        gPlusView.addBottomEdgeAttachConstraint(cell.contentView)
        gPlusView.addWidthConstraint(tableView.frame.size.width/2-5)
        
        let lineView = UIView(frame: CGRectZero)
        lineView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.3)
        
        cell.contentView.addSubview(lineView)
        
        lineView.translatesAutoresizingMaskIntoConstraints = false
        
        lineView.addTopEdgeAttachConstraint(cell.contentView)
        lineView.addHeightConstraint(1)
        lineView.addLeftEdgeAttachConstraint(cell.contentView, offset: 25)
        lineView.addRightEdgeAttachConstraint(cell.contentView, offset: 25)
        
        let gplusImageView = MKImageView(image: UIImage(named: "ic_gplus_white"))
        gplusImageView.rippleLocation = .TapLocation
        gplusImageView.rippleLayerColor = UIColor.whiteColor()
        gPlusView.addSubview(gplusImageView)
        gplusImageView.translatesAutoresizingMaskIntoConstraints = false
        
        
        gplusImageView.addTopEdgeAttachConstraint(gPlusView, offset: 20)
        gplusImageView.addCenterXConstraint(gPlusView)
        gplusImageView.addWidthConstraint(30)
        gplusImageView.addHeightConstraint(30)
        
        let gplusLabel = UILabel(frame: CGRectZero)
        gplusLabel.text = PKSignInConstants.TitleGPlus
        gplusLabel.numberOfLines = 2
        gplusLabel.font = UIFont.PKAllerRegular(14)
        gplusLabel.textColor = UIColor.whiteColor()
        gplusLabel.textAlignment = NSTextAlignment.Center
        gPlusView.addSubview(gplusLabel)
        
        gplusLabel.translatesAutoresizingMaskIntoConstraints = false
        
        gplusLabel.addTopEdgeAttachConstraint(gplusImageView, viewEdge: NSLayoutAttribute.Bottom, offset: 10)
        gplusLabel.addLeftEdgeAttachConstraint(gPlusView, offset: 20)
        gplusLabel.addRightEdgeAttachConstraint(gPlusView, offset: 20)
        gplusLabel.addHeightConstraint(35)
        
        let fbView = UIView(frame: CGRectZero)
        fbView.backgroundColor = UIColor.clearColor()
        cell.contentView.addSubview(fbView)
        
        fbView.translatesAutoresizingMaskIntoConstraints = false
        
        fbView.addTopEdgeAttachConstraint(cell.contentView)
        fbView.addRightEdgeAttachConstraint(cell.contentView, offset: 15)
        fbView.addBottomEdgeAttachConstraint(cell.contentView)
        fbView.addWidthConstraint(tableView.frame.size.width/2-5)

        let fbTapGesture = UITapGestureRecognizer(target: self, action: "didPressFacebookSignIn:")
        fbView.addGestureRecognizer(fbTapGesture)
        
        let fbImageView = MKImageView(image: UIImage(named: "ic_fb_white"))
        fbImageView.rippleLocation = .TapLocation
        fbImageView.rippleLayerColor = UIColor.whiteColor()
        fbView.addSubview(fbImageView)
        fbImageView.translatesAutoresizingMaskIntoConstraints = false
        
        fbImageView.addCenterXConstraint(fbView)
        fbImageView.addTopEdgeAttachConstraint(fbView, offset: 20)
        fbImageView.addWidthConstraint(30)
        fbImageView.addHeightConstraint(30)
        
        let fbLabel = UILabel(frame: CGRectZero)
        fbLabel.text = PKSignInConstants.TitleFacebook
        fbLabel.numberOfLines = 2
        fbLabel.font = UIFont.PKAllerRegular(14)
        fbLabel.textColor = UIColor.whiteColor()
        fbLabel.textAlignment = NSTextAlignment.Center
        fbView.addSubview(fbLabel)
        
        fbLabel.translatesAutoresizingMaskIntoConstraints = false
        
        fbLabel.addTopEdgeAttachConstraint(fbImageView, viewEdge: NSLayoutAttribute.Bottom, offset: 10)
        fbLabel.addLeftEdgeAttachConstraint(fbView, offset: 20)
        fbLabel.addRightEdgeAttachConstraint(fbView, offset: 20)
        fbLabel.addHeightConstraint(35)
        */
        
        return cell
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = false
        navigationController?.navigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return PKAuthenticationFormConstants.TableHeaderHeight
        }else if indexPath.row == arrayCells!.count-3 {
            return PKConstants.CellTextFieldHeight-20
        }else if indexPath.row == arrayCells!.count-2 {
            return PKConstants.CellTextFieldHeight-10
        }else if indexPath.row == arrayCells!.count-1 {
            return PKAuthenticationFormConstants.TableFooterHeight
        }
        return PKConstants.CellTextFieldHeight
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        view.endEditing(true)

        switch indexPath.row {
    
        case 3:
            let forgotController = PKForgotPasswordTableViewController(style: UITableViewStyle.Plain)
            navigationController?.pushViewController(forgotController, animated: true)
            
            break
        case 4:
            
            signInPressed()
            
            break
        case 6:
            let signupController = PKSignUpTableViewController(style: UITableViewStyle.Plain)
            signupController.authenticationDelegate = authenticationDelegate
            navigationController?.pushViewController(signupController, animated: true)
            break
        case 5:
            let webViewController = PKWebViewController()
            webViewController.htmlKey = "terms_cond"
            self.navigationController?.pushViewController(webViewController, animated: true)
            break
        default:
            break
            
        }

    }
    
    func signInPressed() {
        
        let emailStr = emailCell.textField.text!
        let passwordStr = passwordCell.textField.text!
        
        var isValid:Bool = true
        let title:String = "\n"
        var message:String = ""
        
        if !emailStr.isValidEmail() {
            isValid = false
            message += String.bulletinedText("Enter a valid email address\n")
        }
        
        if passwordStr.characters.count<8 {
            isValid = false
            message += String.bulletinedText("Your password should have atleast 8 characters\n")
        }
        
        if isValid {
            
            SVProgressHUD.showWithMaskType(.Gradient)
            
            let parameters:Dictionary<String, String> = ["user_email":emailStr,"user_password":passwordStr]
            
            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserLogin.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    // time-consuming task
                    dispatch_async(dispatch_get_main_queue(), {
                        SVProgressHUD.dismiss()
                    })
                })
                
                if success == 1 {
                    
                    let json = result as! Dictionary<String, AnyObject>
                    
                    let dataDIC = json["user_details"] as! Dictionary<String, AnyObject>
                    
                    if dataDIC["success"] as! String == "false" {
                        
                        if dataDIC["error_status"] as! String == "2" {
                            
                            let title = "Incorrect Password"
                            let message = "Sorry, The password you entered is incorrect"
                            self.handleError(title, message: message)
                            
                        }else {
                            
                            let title = "User not found"
                            let message = "No user has been registered with the email address you provided"
                            self.handleError(title, message: message)
                            
                        }
                        
                    }else {
                        
                        let token = dataDIC["secure_key"] as! String
                        
                        NSUserDefaults.standardUserDefaults().setObject(token, forKey: PKConstants.UserDefaultTokenValue)
                        NSUserDefaults.standardUserDefaults().synchronize()
                        
                        PKDataModel.sharedInstance.loggedUser = PKUser.userWithDictionaryData(dataDIC)
                        
                        let isVerifiedWithMobile = PKDataModel.sharedInstance.loggedUser.otp_verified == "1"
                        
                        if !isVerifiedWithMobile {
                            self.authenticateWithMobile()
                        }else {
                            self.authenticationDelegate?.authenticationController(self, didSuccessfullyAuthenticateUser: PKDataModel.sharedInstance.loggedUser)
                        }
                        
                    }
                    
                }else {
                    
                    let error:NSError = (result as! NSError)
                    let title = String(error.code) + ": " + error.domain
                    let message = error.localizedDescription
                    self.handleError(title, message: message)
                    
                }
                
            }
            
        }else {
            
            self.handleError(title, message: message)
        }
        
    }
    
    func authenticateWithMobile() {
        let confirmMobileController = PKMobileInputTableViewController(style: UITableViewStyle.Plain)
        confirmMobileController.authenticationDelegate = authenticationDelegate
        navigationController?.pushViewController(confirmMobileController, animated: true)
    }
    
    // MARK: - ACTIONS
    
    func didPressGoogleSignIn(gesture:UITapGestureRecognizer) {
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    func didPressFacebookSignIn(gesture:UITapGestureRecognizer) {

        let facebookManager = FBSDKLoginManager()
        
        facebookManager.logInWithReadPermissions(["email"], fromViewController: self) { (result, error) -> Void in
            
            UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.whiteColor()
            
            if (error != nil) {
                print("Process error");
            } else if (result.isCancelled) {
                print("Cancelled");
            } else {
                print("Logged in")
                
                SVProgressHUD.showWithMaskType(.Gradient)
                
                if FBSDKAccessToken.currentAccessToken() != nil {
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, name, email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                        
                        print(result)
                        if error == nil {
                            
                            let facebookResult = result as! Dictionary<String, AnyObject>
                            
                            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserFacebookSignIn.rawValue), parameters:["facebook_id":facebookResult["id"] ?? "","user_name":facebookResult["name"]!,"user_email":facebookResult["email"]!], completionClosure: { (success:Int!, result:AnyObject?) -> Void in
                                
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                                    // time-consuming task
                                    dispatch_async(dispatch_get_main_queue(), {
                                        SVProgressHUD.dismiss()
                                    })
                                })
                                
                                if success == 1 {
                                    
                                    let json = result as! Dictionary<String, AnyObject>
                                    let dataDIC = json["user_details"] as! Dictionary<String, AnyObject>
                                    
                                    let token = dataDIC["secure_key"] as! String
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(token, forKey: PKConstants.UserDefaultTokenValue)
                                    NSUserDefaults.standardUserDefaults().synchronize()

                                    PKDataModel.sharedInstance.loggedUser = PKUser.userWithDictionaryData(dataDIC)
                                    
                                    if PKDataModel.sharedInstance.loggedUser.otp_verified != "1" {
                                        self.authenticateWithMobile()
                                    }else {
                                        self.authenticationDelegate?.authenticationController(self, didSuccessfullyAuthenticateUser: PKDataModel.sharedInstance.loggedUser)
                                    }
                                    
                                }
                                else {
                                    
                                    let error:NSError = (result as! NSError)
                                    let title = String(error.code) + ": " + error.domain
                                    let message = error.localizedDescription
                                    self.handleError(title, message: message)
                                    
                                }
                                
                                
                                
                            })
                            
                        }
                        
                        
                    })
                }
                
            }
        }
        
    }
    
    // MARK : - FBSDKLoginButtonDelegate
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.whiteColor()

        if (FBSDKAccessToken.currentAccessToken() != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, name, email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                
                if error == nil {
                    SVProgressHUD.showWithMaskType(.Gradient)
                    
                    let facebookResult = result as! Dictionary<String, AnyObject>
                    
                    PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserFacebookSignIn.rawValue), parameters:["facebook_id":facebookResult["id"]!,"user_name":facebookResult["name"]!,"user_email":facebookResult["email"]!], completionClosure: { (success:Int!, result:AnyObject?) -> Void in
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                            // time-consuming task
                            dispatch_async(dispatch_get_main_queue(), {
                                SVProgressHUD.dismiss()
                            })
                        })
                        
                        if success == 1 {
                            
                            let json = result as! Dictionary<String, AnyObject>
                            let dataDIC = json["user_details"] as! Dictionary<String, AnyObject>
                            
                            let token = dataDIC["secure_key"] as! String
                            
                            NSUserDefaults.standardUserDefaults().setObject(token, forKey: PKConstants.UserDefaultTokenValue)
                            NSUserDefaults.standardUserDefaults().synchronize()
                            
                            PKDataModel.sharedInstance.loggedUser = PKUser.userWithDictionaryData(dataDIC)
                            
                            if PKDataModel.sharedInstance.loggedUser.otp_verified != "1" {
                                self.authenticateWithMobile()
                            }else {
                                self.authenticationDelegate?.authenticationController(self, didSuccessfullyAuthenticateUser: PKDataModel.sharedInstance.loggedUser)
                            }
                            
                        }
                        else {
                            
                            let error:NSError = (result as! NSError)
                            let title = String(error.code) + ": " + error.domain
                            let message = error.localizedDescription
                            self.handleError(title, message: message)
                            
                        }
                        
                    })
                }
                
            })
        }
        
        
        
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        UIApplication.sharedApplication().keyWindow!.tintColor = UIColor.blackColor()

        return true
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func receiveToggleAuthUINotification(notification:NSNotification) {
        if notification.name == "ToggleAuthUINotification" {
            // Configure UI
        }
    }
    
    // MARK: - GIDSignInDelegate
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
        withError error: NSError!) {
            
            if (error == nil) {
                // Perform any operations on signed in user here.
                let userId = user.userID                  // For client-side use only!
                let idToken = user.authentication.idToken // Safe to send to the server
                let name = user.profile.name
                let email = user.profile.email
                print("\(userId)")
                print("\(idToken)")
                print("\(name)")
                print("\(email)")
                
                SVProgressHUD.showWithMaskType(.Gradient)
                
                PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserGplusSignIn.rawValue), parameters:["google_id":userId,"user_name":name,"user_email":email], completionClosure: { (success:Int!, result:AnyObject?) -> Void in
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                        // time-consuming task
                        dispatch_async(dispatch_get_main_queue(), {
                            SVProgressHUD.dismiss()
                        })
                    })
                    
                    if success == 1 {
                        
                        let json = result as! Dictionary<String, AnyObject>
                        let dataDIC = json["user_details"] as! Dictionary<String, AnyObject>

                        let token = dataDIC["secure_key"] as! String
                        
                        NSUserDefaults.standardUserDefaults().setObject(token, forKey: PKConstants.UserDefaultTokenValue)
                        NSUserDefaults.standardUserDefaults().synchronize()
                        
                        PKDataModel.sharedInstance.loggedUser = PKUser.userWithDictionaryData(dataDIC)
                        
                        if PKDataModel.sharedInstance.loggedUser.otp_verified != "1" {
                            self.authenticateWithMobile()
                        }else {
                            self.authenticationDelegate?.authenticationController(self, didSuccessfullyAuthenticateUser: PKDataModel.sharedInstance.loggedUser)
                        }
                        
                        
                    }
                    else {
                        
                        let error:NSError = (result as! NSError)
                        let title = String(error.code) + ": " + error.domain
                        let message = error.localizedDescription
                        self.handleError(title, message: message)
                        
                    }


                    
                })
                
                
                // ...
            } else {
                print("\(error.localizedDescription)")
            }
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
        withError error: NSError!) {
            // Perform any operations when the user disconnects from app here.
            // ...
    }
}
