//
//  PKAuthenticationFormTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 03/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

struct PKAuthenticationFormConstants {
    
    static let TableHeaderHeight:CGFloat = 130
    static let TableFooterHeight:CGFloat = 115
    
    static let PlaceholderEmail:String = "E M A I L"
    static let PlaceholderPassword:String = "P A S S W O R D"
}


class PKAuthenticationFormTableViewController: PKAuthenticationBaseTableViewController {

    var emailCell:PKTextFieldLoginTableViewCell!
    var passwordCell:PKTextFieldLoginTableViewCell!
    var whiteCell:PKCenteredLabelTableViewCell!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.backgroundColor = UIColor.appThemeColor()
        customizeForStaticTableStyle()
        //addTapToDismiss()
        
        arrayCells = [UITableViewCell.headerCell()]
        
        emailCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        emailCell.textField.delegate = self
        emailCell.textField.placeholder = PKAuthenticationFormConstants.PlaceholderEmail
        emailCell.textField.returnKeyType = UIReturnKeyType.Next
        emailCell.textField.keyboardType = UIKeyboardType.EmailAddress
        emailCell.textField.autocapitalizationType = .None

        passwordCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        emailCell.textField.nextField = passwordCell.textField
        passwordCell.textField.placeholder = PKAuthenticationFormConstants.PlaceholderPassword
        passwordCell.textField.delegate = self
        passwordCell.textField.returnKeyType = UIReturnKeyType.Done
        passwordCell.textField.secureTextEntry = true
        
        whiteCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        whiteCell.textLabel?.textColor = UIColor.appThemeColor()
        whiteCell.rippleLayerColor = UIColor.appThemeColor()
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField.text?.characters.count > 0 {
            
            if textField.keyboardType == UIKeyboardType.EmailAddress {
                
                emailCell.accessoryAlertIcon.hidden = textField.text!.isValidEmail()
                
            }
//            else if textField.secureTextEntry == true {
//                
//                passwordCell.accessoryAlertIcon.hidden = textField.text!.characters.count>=6
//                
//            }
            
        }
        

    }
}
