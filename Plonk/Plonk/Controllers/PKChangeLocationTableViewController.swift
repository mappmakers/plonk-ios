//
//  PKChangeLocationTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 18/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

typealias UpdateLocationCompletion = (updatedLocation : String) -> Void

class PKChangeLocationTableViewController: UITableViewController {
    
    var updatedLocationCompletion:UpdateLocationCompletion?
    
    let inputLocationCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
    let confirmCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
    
    var arrayCells:Array<UITableViewCell>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Change Placemark"
        
        customizeForStaticTableStyle()
        
        inputLocationCell.textField.placeholder = "New Placemark"
        inputLocationCell.textField.delegate = self
        inputLocationCell.textField.keyboardType = .EmailAddress
        inputLocationCell.textField.returnKeyType = UIReturnKeyType.Done
        inputLocationCell.textField.becomeFirstResponder()
        inputLocationCell.textField.floatingLabelTextColor = UIColor.appThemeColor()
        inputLocationCell.textField.textColor = UIColor.appThemeColor()
        inputLocationCell.backgroundColor = UIColor.whiteColor()
        inputLocationCell.textField.tintColor = UIColor.appThemeColor()
        
        confirmCell.textLabel?.text = "Confirm"
        confirmCell.backgroundColor = UIColor.appThemeColor()
        
        arrayCells = [inputLocationCell, confirmCell]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayCells?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return PKConstants.CellTextFieldHeight-10
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return arrayCells![indexPath.section]
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 1 {
            
            let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"user_place":inputLocationCell.textField.text!]
            
            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UpdatePlaceMark.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
                
                if success != 1
                {
                    let error:NSError = (result as! NSError)
                    let title = String(error.code) + ": " + error.domain
                    let message = error.localizedDescription
                    self.handleError(title, message: message)
                }else {
                    
                    let json = result as! Dictionary<String,AnyObject>
                    if json["success"] as! String == "true" {
                        
                        let location = (self.arrayCells![0] as! PKTextFieldLoginTableViewCell).textField.text ?? ""
                        PKDataModel.sharedInstance.loggedUser.user_place_mark = location
                        
                        self.navigationController?.popViewControllerAnimated(true)
                        if self.updatedLocationCompletion != nil {
                            self.updatedLocationCompletion!(updatedLocation: location)
                        }
                        
                    }
                    
                }
            }
            
        }
        
    }
    
}
