//
//  PKParkingExitCardTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 09/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

enum PKParkingExitCardLabelPatternAType:Int {
    case PassNow, VehicleNo, EnterTime, ExitTime
}

enum PKParkingExitCardLabelPatternBType:Int {
    case Username, AmountValue
}

enum PKParkingExitCardLabelPatternCType:Int {
    case Title, HoursUsed, TimeUnit, TotalAmount, PaymentType
}

class PKParkingExitCardTableViewController: UITableViewController {

    @IBOutlet var btnClose: UIButton! // darkgray/white
    @IBOutlet var labelsPatternA: [UILabel]! // black
    @IBOutlet var labelsPatternB: [UILabel]! // blue/yellow
    @IBOutlet var labelsPatternC: [UILabel]! // dark gray/white
    @IBOutlet var linesSeparator: [UILabel]! // blue/white
    @IBOutlet var imageViewCar: UIImageView! // blue/white
    @IBOutlet var labelTime: UILabel! // red/black
    @IBOutlet var btnPaytm: UIButton! // hidden/visible
    
    var userParkingDetail:PKUserParkingDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.backgroundColor = UIColor.clearColor()
        
        if userParkingDetail.payment_mode == "cash" {
            
            btnClose.setImage(UIImage(named: "ic_cross_gray"), forState: .Normal) // dark gray
            
            for label in labelsPatternB {
                label.textColor = UIColor.appThemeColor()
            }
            
            for label in labelsPatternC {
                label.textColor = UIColor.darkGrayColor()
            }
            
            for line in linesSeparator {
                line.textColor = UIColor.appThemeColor()
            }
            
            imageViewCar.image = UIImage(named: "ic_car")
            
            labelTime.textColor = UIColor.redColor()
            
            btnPaytm.hidden = true
            
            labelsPatternC[PKParkingExitCardLabelPatternCType.PaymentType.rawValue].text = "Parking charge collected by security"
            
        }else {
            
            btnPaytm.layer.cornerRadius = 5
            btnPaytm.layer.masksToBounds = true
            
        }
        
        labelsPatternA[PKParkingExitCardLabelPatternAType.VehicleNo.rawValue].text = userParkingDetail.registeration_no
        labelsPatternA[PKParkingExitCardLabelPatternAType.EnterTime.rawValue].text = userParkingDetail.entry_time
        labelsPatternA[PKParkingExitCardLabelPatternAType.ExitTime.rawValue].text = userParkingDetail.exit_time
        
        labelsPatternB[PKParkingExitCardLabelPatternBType.Username.rawValue].text = PKDataModel.sharedInstance.loggedUser.user_name
        labelsPatternB[PKParkingExitCardLabelPatternBType.AmountValue.rawValue].text = "₹ "+userParkingDetail.amount!
        
        labelTime.text = userParkingDetail.total_duration
        
        if userParkingDetail.fine_status == "1" {
            
            let parkingAmount = Double(userParkingDetail.amount!)
            let fineAmount = Double(userParkingDetail.fine_amount!)
            
            self.handleAlertError("Thank You\n", message: "You have been charge extra for your parking\n\nParking charge :  ₹ \(parkingAmount!-fineAmount!)\n\nFine charged : ₹ \(fineAmount!)\n\nTotal Amount : ₹ \(parkingAmount!)")
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func crossPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
