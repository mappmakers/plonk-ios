//
//  PKQRCodeViewController.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 05/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit
import MaterialControls
import SVProgressHUD

struct PKQRCodeConstants {
    
    static let TitleScan = "Scan"
    static let TitleNext = "ENTER CODE MANUALLY"
    
}

enum PKParkingDataError: ErrorType {
    case NoParkingSlots, NoVehicles
}

extension PKParkingDataError: CustomStringConvertible {
    var description: String {
        switch self {
        case NoParkingSlots: return "No Parking Slots found"
        case NoVehicles: return "No Vehicles info found"
        }
    }
}


class PKQRCodeViewController: UIViewController {
    
    let scanner = FDQRScanner()
    
    var fare:String!
    
    private var parkingSlots:Array<PKParkingSlot>?
    private var vehicles:Array<PKUserVehicle>?
    private var nextButton:MKButton!
    
    private var scannerView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = PKQRCodeConstants.TitleScan
        
        view.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        parkingSlots = []
        vehicles = []
        
        nextButton = MKButton(type: .Custom)
        nextButton.backgroundColor = UIColor.appThemeColor()
        nextButton.setTitle(PKQRCodeConstants.TitleNext, forState: .Normal)
        nextButton.titleLabel?.font = UIFont.PKAllerRegular(14)
        nextButton.setImage(UIImage(named: "ic_right"), forState: .Normal)
        nextButton.addTarget(self, action: "textInputPressed:", forControlEvents: .TouchUpInside)
        nextButton.imageEdgeInsets = UIEdgeInsetsMake(0, UIScreen.mainScreen().bounds.size.width-30, 0, 0)
        view.addSubview(nextButton)
        
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        nextButton.addBottomEdgeAttachConstraint(view)
        nextButton.addHeightConstraint(50)
        nextButton.addRightEdgeAttachConstraint(view)
        nextButton.addLeftEdgeAttachConstraint(view)
        
        scannerView = UIView(frame: CGRectZero)
        scannerView.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        view.addSubview(scannerView)
        
        scannerView.translatesAutoresizingMaskIntoConstraints = false
        scannerView.addTopEdgeAttachConstraint(view)
        scannerView.addBottomEdgeAttachConstraint(nextButton, viewEdge: .Top, offset: 0)
        scannerView.addLeftEdgeAttachConstraint(view)
        scannerView.addRightEdgeAttachConstraint(view)
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.startScanningSession()
        }
        
        let delayTimeSecond = dispatch_time(DISPATCH_TIME_NOW, Int64(1.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTimeSecond, dispatch_get_main_queue()) {
            
            let qrYellowImageView = UIImageView(image: UIImage(named: "ic_qr_yellow"))
            self.scannerView.addSubview(qrYellowImageView)
            
            qrYellowImageView.translatesAutoresizingMaskIntoConstraints = false
            qrYellowImageView.addCenterXConstraint(self.scannerView)
            qrYellowImageView.addCenterYConstraint(self.scannerView)
            qrYellowImageView.addWidthConstraint(100)
            qrYellowImageView.addHeightConstraint(100)
            
        }

    }
    
    func startScanningSession() {
        
        scanner.startScanning(scannerView, metadataObjectTypes: nil) { (decodedString, type) -> Void in
            print(" scanner completed \(decodedString)")
            
            //MDToast(text: decodedString, duration: Double(kMDToastDurationLong)).show()
            
            self.fetchParkingLotForQYCode(decodedString)
            
        }
        
    }
    
    func textInputPressed(sender:UIButton?) {
        
        let qrInputController = PKQRTextInputTableViewController(style: .Grouped)
        qrInputController.codeInputCompletion = { (inputCode, qrCodeStatus) in
            
            print(" entering code completed \(inputCode)")
            self.fetchParkingLotForQYCode(inputCode)
            
        }
        navigationController?.pushViewController(qrInputController, animated: true)
    }
    
    func pushToConfirmationPage() throws {
        
        guard parkingSlots!.count > 0 else { throw PKParkingDataError.NoParkingSlots }
//        guard vehicles!.count > 0 else { throw PKParkingDataError.NoVehicles }
        
        scanner.stopScanning()
        navigationController?.popToViewController(self, animated: false)
        
        let parkingConfirmation = UIStoryboard(name: PKConstants.StoryboardNameParkingLot, bundle: nil).instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingConfirm) as! PKParkingConfirmTableViewController
        parkingConfirmation.parkingSlot = parkingSlots![0]
        parkingConfirmation.vehicles = vehicles
        navigationController?.pushViewController(parkingConfirmation, animated: true)
        
    }
    
    func fetchParkingLotForQYCode(qrCode:String) {
        
        SVProgressHUD.showWithMaskType(.Gradient)
        
        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"fare":fare,"qr_code":qrCode]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ScanParkingLots.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                let json = result as! Dictionary<String, AnyObject>
                
                let scanResultDic = json["scan_res"] as! Dictionary<String, String>
                if (scanResultDic["success"] == "true") {
                    
                    let parkingSlotsArray = json["parking_slots"] as! Array<Dictionary<String, AnyObject>>
                    
                    for parkingSlot in parkingSlotsArray {
                        
                        self.parkingSlots!.append(PKParkingSlot.parkingSlotWithDictionaryData(parkingSlot))
                        
                    }
                    
                    let vehiclesArray = json["vech_det"] as! Array<Dictionary<String, AnyObject>>
                    
                    for vehicle in vehiclesArray {
                        
                        self.vehicles!.append(PKUserVehicle.vehicleWithDictionaryData(vehicle))
                        
                    }
                    
                    do {
                        try self.pushToConfirmationPage()
                    } catch let error as PKParkingDataError {
                        print(error.description)
                        self.handleError(PKConstants.TitleErrorHeader, message: error.description)
                    } catch {
                        self.handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
                    }
                }else {
                    
                    self.startScanningSession()
                    
                    if scanResultDic["error_code"] == "0" {
                        self.handleError("Sorry", message: "You can't start a new parking when a parking meter is already running for this QR Code")
                    }else {
                        self.handleError("Sorry", message: "There is no parking lot for this QRCode")
                    }
                }
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
