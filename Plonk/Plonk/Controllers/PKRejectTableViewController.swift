//
//  PKRejectTableViewController.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 27/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD
import DZNEmptyDataSet

class PKRejectTableViewController: UITableViewController ,NSFetchedResultsControllerDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource{

    var parentNavigationController:UINavigationController?
    
     var rejectParkingFRC:NSFetchedResultsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        rejectParkingFRC = PKUserRejectSlot.MR_fetchAllSortedBy("parking_lot_id", ascending: true, withPredicate: nil, groupBy: nil, delegate: self, inContext: NSManagedObjectContext.MR_defaultContext())
        rejectParkingFRC?.delegate = self
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        tableView.backgroundColor = UIColor.groupTableViewBackgroundColor()

    }

    // MARK: - NSFetchedResultsControllerDelegate
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        let tableView:UITableView = self.tableView
        
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Update:
            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!) as! PKParkingTableViewCell, atIndexPath: indexPath!)
            break
        default:
            break
        }
    }
    
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
            
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
        default:break
        }
        
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
         return rejectParkingFRC?.sections?.count ?? 0    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let sectionInfo = rejectParkingFRC!.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("myReuseIdentifier") as? PKParkingTableViewCell
        
        if cell == nil {
            
            cell = PKParkingTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "myReuseIdentifier")
        }
        
        configureCell(cell!, atIndexPath: indexPath)
        // Configure the cell...
        
        return cell!

    }
    
    
    func configureCell(cell:PKParkingTableViewCell, atIndexPath indexPath:NSIndexPath) {
        
        let parkingLotFeed = rejectParkingFRC!.objectAtIndexPath(indexPath) as! PKUserRejectSlot
        cell.textLabel?.text = parkingLotFeed.parking_lot_name
        cell.detailTextLabel?.text = parkingLotFeed.parking_lot_address
        
        cell.selectionStyle = .None
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 5
    }
    
    // empty data methods
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        
        // return UIImage(named: "ic_parking_map")
        
        return UIImage(named: "Ic_cancel")
        
        
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let emptyTitle:String = ""
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        let emptyTitle:String = "Rejected lots will appear here"
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
        
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView!) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
//    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        
//        
//        tableView.deselectRowAtIndexPath(indexPath, animated: true)
//        
//        
//        let parkingLotFeed = rejectParkingFRC!.objectAtIndexPath(indexPath) as! PKUserRejectSlot
//        
//        getParkingDetails(parkingLotFeed.tenant_lot_id!)
//    }
    
    func getParkingDetails(tenantLotId:String)
    {
        SVProgressHUD.showWithMaskType(.Gradient)
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ParkingLotDetails.rawValue), parameters:["Lot_id":tenantLotId]) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1
            {
                
                
                
                let json = result as! Dictionary<String,AnyObject>
                
                let parkingDetailsDicArray = json["prkLots_details"] as! NSArray
                
                
                
                
                
                let parkingDetailsDic = parkingDetailsDicArray.objectAtIndex(0) as! Dictionary<String,AnyObject>
                
                
                
                
                
                let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameMyParkingLot, bundle: nil)
                
                
                
                
                
                
                let parkingLotDetailsVC = storyboard.instantiateViewControllerWithIdentifier("parkingLotDetailsVC") as! PKParkingLotDetailsTableViewController
                
                
                
                
                
                parkingLotDetailsVC.parkingId = tenantLotId
                
                
                
                
                
                
                
                
                parkingLotDetailsVC.parkingDetailsDic = parkingDetailsDic
                
                
                
                
                
                
                
                self.navigationController?.pushViewController(parkingLotDetailsVC, animated: true)
                
            }
            else
            {
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
            
        }
    }
    

    
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
