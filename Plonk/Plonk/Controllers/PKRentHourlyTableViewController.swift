//
//  PKRentHourlyTableViewController.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 09/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKRentHourlyTableViewController: UITableViewController {
    
    let minDurationArray = ["1 Hour", "2 Hours", "3 Hours","4 Hours", "5 Hours", "6 Hours","7 Hour", "8 Hours", "9 Hours","10 Hours", "11 Hours"]
    let incrementsArray = ["30 Minutes", "60 Minutes", "90 Minutes","120 Minutes"]
    let incrementsTime = ["00:30:00", "01:00:00", "01:30:00","02:00:00"]
    
    var daysArray:NSMutableArray!
    var parkingIdRent:String!
    
    var parkingLotData:[String:AnyObject]?
    
    private var arrayTotalSlots:[String] = [String]()
    
    @IBOutlet weak var wholeDayButtonOutlet: UIButton!
    @IBOutlet weak var extraChargesLabel: UILabel!
    @IBOutlet weak var minimumRateLabel: UILabel!
    @IBOutlet weak var endTime: PKTextField!
    @IBOutlet weak var startTime: PKTextField!
    @IBOutlet weak var startTextField: PKTextField!
    @IBOutlet weak var endTextfield: PKTextField!
    @IBOutlet weak var rateTextField: PKTextField!
    @IBOutlet weak var minDurationTextfield: PKTextField!
    @IBOutlet weak var incrementsTextField: PKTextField!
    @IBOutlet weak var extraChargeTextfield: PKTextField!
    
    @IBOutlet var totalSlotsTextField: PKTextField!
    
    @IBOutlet weak var mondayButtonOutlet: PKDayButton!
    
    @IBOutlet weak var thursdayButtonOutlet: PKDayButton!
    
    @IBOutlet weak var tuesdayOutletButton: PKDayButton!
    
    @IBOutlet weak var saturdayButtonOutlet: PKDayButton!
    
    @IBOutlet weak var wednesdayButtonOutlet: PKDayButton!
    
    @IBOutlet weak var fridayButtonOutlet: PKDayButton!
    
    @IBOutlet weak var sundayButtonOutlet: PKDayButton!
    
    let dateFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Create plan"
        
        setTexfieldDelegates()
        
        daysArray = []
        
        endTime.layer.borderColor = UIColor.appThemeColor().CGColor
        endTime.layer.borderWidth = 1
        
        startTime.layer.borderColor = UIColor.appThemeColor().CGColor
        startTime.layer.borderWidth = 1
        
        wholeDayButtonOutlet.layer.borderColor = UIColor.appThemeColor().CGColor
        wholeDayButtonOutlet.layer.borderWidth = 1
        wholeDayButtonOutlet.backgroundColor = UIColor.appThemeColor()
        
        incrementsTextField.attachFieldWithPickerData(incrementsArray, selectedItem: nil)
        minDurationTextfield.attachFieldWithPickerData(minDurationArray, selectedItem: minDurationArray[1])
        
        startTime.attachDatePickerWithMode(.Time)
        startTime.datePicker?.minimumDate = NSDate().startOfDay
        endTime.attachDatePickerWithMode(.Time)
        endTime.datePicker?.minimumDate = NSDate().startOfDay
        
        startTextField.attachDatePickerWithMode(.Date)
        startTextField.datePicker?.minimumDate = NSDate()
        endTextfield.attachDatePickerWithMode(.Date)
        endTextfield.datePicker?.minimumDate = NSDate()
        
        rateTextField.attachDoneToolbar()
        extraChargeTextfield.attachDoneToolbar()

        let totalCount = Int(parkingLotData!["slots_total_count"] as? String ?? "0")
        
        var index: Int
        for index = 1; index <= totalCount; ++index {
            
            if index == 1 {
                arrayTotalSlots.append("\(index) Slot")
            }else {
                arrayTotalSlots.append("\(index) Slots")
            }
            
            print("\(index) Slots")
        }
        
        if totalCount == 1 {
            totalSlotsTextField.attachFieldWithPickerData(arrayTotalSlots, selectedItem: arrayTotalSlots[0])
        }else {
            totalSlotsTextField.attachFieldWithPickerData(arrayTotalSlots, selectedItem: nil)
        }
        
        
        
        cusPKtextfieldView(startTextField)
        cusPKtextfieldView(endTextfield)
        cusPKtextfieldView(rateTextField)
        cusPKtextfieldView(minDurationTextfield)
        cusPKtextfieldView(incrementsTextField)
        cusPKtextfieldView(extraChargeTextfield)
        cusPKtextfieldView(totalSlotsTextField)

    }
    
    func cusUItextfieldView(textField:UITextField)
    {
        
        textField.layer.borderColor = UIColor.whiteColor().CGColor
        textField.layer.borderWidth = 1
        
    }
    func cusPKtextfieldView(textField:PKTextField)
    {
        
        textField.layer.borderColor = UIColor.whiteColor().CGColor
        textField.layer.borderWidth = 1
        
    }
    /*
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row < 10 {
            if DeviceType.IS_IPHONE_6 {
                return super.tableView(tableView, heightForRowAtIndexPath: indexPath)+5.8
            }else if DeviceType.IS_IPHONE_6P {
                return super.tableView(tableView, heightForRowAtIndexPath: indexPath)+13.0
            }
        }
        return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        
    }
    */
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    

    // Textfield delegate methods
    
    
    func setTexfieldDelegates()
    {
        startTextField.delegate = self
        endTextfield.delegate = self
        rateTextField.delegate = self
        minDurationTextfield.delegate = self
        incrementsTextField.delegate = self
        extraChargeTextfield.delegate = self
        startTime.delegate = self
        endTime.delegate = self
        
        startTextField.tintColor = UIColor.appThemeColor()
        endTextfield.tintColor = UIColor.appThemeColor()
        rateTextField.tintColor = UIColor.appThemeColor()
        minDurationTextfield.tintColor = UIColor.appThemeColor()
        incrementsTextField.tintColor = UIColor.appThemeColor()
        extraChargeTextfield.tintColor = UIColor.appThemeColor()
        startTime.tintColor = UIColor.appThemeColor()
        endTime.tintColor = UIColor.appThemeColor()
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if textField == startTextField {
            if endTextfield.selectedPickerItem != nil {
                startTextField.datePicker!.maximumDate = endTextfield.selectedPickerItem as? NSDate
            }
            
        }
        else if textField == endTextfield {
            if startTextField.selectedPickerItem == nil {
                self.handleError("Warning", message: "Please choose a starting date first")
                return false
            }
        }else if textField == startTime || textField == endTime {
            
            if textField == endTime {
                if startTime.selectedPickerItem == nil {
                    self.handleError("Warning", message: "Please choose the start time first")
                    return false
                }
            }
            
            let selectedTime = (textField as! PKTextField).selectedPickerItem
            if selectedTime != nil {
                (textField as! PKTextField).datePicker!.date = selectedTime as! NSDate
            }
            
            if textField == startTime {
                if endTime.selectedPickerItem != nil {
                    startTime.datePicker!.maximumDate = (endTime.selectedPickerItem as! NSDate).maxStartDay
                }
            }
        }

        return true
    }
    
    func textFieldDidEndEditing(textField: PKTextField) {
        if (textField == minDurationTextfield || textField == rateTextField)
        {
            let string:NSString = NSString(format: "Minimum charge set to Rs %@ for the first %@",rateTextField.text!,minDurationTextfield.text! )
            minimumRateLabel.text = string as String
            
            
        }
        else if (textField == extraChargeTextfield || textField == incrementsTextField){
            
            let string:NSString = NSString(format: "Extra charges set to Rs %@ for every extra %@",extraChargeTextfield.text!,incrementsTextField.text! )
            extraChargesLabel.text = string as String
            
            
        }else if textField == startTextField {
            if startTextField.selectedPickerItem != nil {
                endTextfield.datePicker?.minimumDate = startTextField.selectedPickerItem as? NSDate
            }
        }else if textField == endTextfield {
            if endTextfield.selectedPickerItem != nil {
                startTextField.datePicker?.maximumDate = endTextfield.selectedPickerItem as? NSDate
            }
        }
    }
    
    func handleSelection(sender:PKDayButton)
    {
        sender.selected = !sender.selected
        
        sender.selected == true ? sender.customizeForSelected():sender.customizeForUnSelected()
        
    }
    
    @IBAction func mondaySelectAction(sender: PKDayButton) {
        
        handleSelection(sender)
        
    }
    
    @IBAction func tuesdaySelectAction(sender: PKDayButton) {
        handleSelection(sender)
    }
    
    @IBAction func wednesdaySelectAction(sender: PKDayButton) {
        handleSelection(sender)
    }
    
    @IBAction func thursdaySelectAction(sender: PKDayButton) {
        handleSelection(sender)
    }
    
    @IBAction func fridaySelectAction(sender: PKDayButton) {
        handleSelection(sender)
    }
    
    @IBAction func saturdaySelectAction(sender: PKDayButton) {
        handleSelection(sender)
    }
    
    @IBAction func sundaySelectAction(sender: PKDayButton) {
        handleSelection(sender)
    }
    
    @IBAction func wholeDayAction(sender: AnyObject) {
        
        startTime.selectedPickerItem = NSDate().startOfDay
        endTime.selectedPickerItem = NSDate().endOfDay ?? NSDate()
        
        startTime.datePicker?.date = NSDate().startOfDay
        endTime.datePicker?.date = NSDate().endOfDay ?? NSDate()
        
        startTime.text="12:00 AM"
        endTime.text="11:59 PM"
    }
    
    func addDayToArray(dayButton:UIButton,posString:NSString)
    {
        if dayButton.selected == true
        {
            
            daysArray.addObject(posString)
            
        }
        
    }
 
    @IBAction func addSlabAction(sender: AnyObject) {
        
        addDayToArray(mondayButtonOutlet, posString:"1")
        addDayToArray(tuesdayOutletButton, posString:"2")
        addDayToArray(wednesdayButtonOutlet, posString:"3")
        addDayToArray(thursdayButtonOutlet, posString:"4")
        addDayToArray(fridayButtonOutlet, posString:"5")
        addDayToArray(saturdayButtonOutlet, posString:"6")
        addDayToArray(sundayButtonOutlet, posString:"7")
        
        if daysArray.count == 0
        {
            handleError("", message: "Please select atleast one day")
        }
        else
        {
            
            if startTextField.text == "" || endTextfield.text == "" || startTime.text == "" || endTime.text == "" || rateTextField.text == "" || minDurationTextfield.text == "" ||
                incrementsTextField.text == "" || extraChargeTextfield.text == "" || totalSlotsTextField.text == ""
            {
                
                self.handleError("Warning", message: "Please enter all the details for the plan")
                
            }
                
            else
            {
                let daysString = daysArray.componentsJoinedByString(",")
                
                let indexOfMinSelected = minDurationArray.indexOf(minDurationTextfield.selectedPickerItem as! String)
                let hourString = NSString(format: "%02d:00:00", indexOfMinSelected!+1)
                
                let indexOfTotalSlotsSelected = arrayTotalSlots.indexOf(totalSlotsTextField.selectedPickerItem as! String)
                
                let indexOfIncrementsSelected = incrementsArray.indexOf(incrementsTextField.selectedPickerItem as! String)

                dateFormatter.dateFormat = "HH:mm:ss"
                
                let startTimeValue = dateFormatter.stringFromDate((startTime.selectedPickerItem as! NSDate))
                let endTimeValue = dateFormatter.stringFromDate((endTime.selectedPickerItem as! NSDate))
                
                let parameters = ["Lot_id":parkingIdRent,"start_date":startTextField.text!,"end_date":endTextfield.text!,"start_time":startTimeValue,"end_time":endTimeValue,"open_days":daysString,"rate":rateTextField.text!,"min_duration":hourString,"increments":incrementsTime[indexOfIncrementsSelected!],"extra_charge":extraChargeTextfield.text!,"plan_slots_count":"\(indexOfTotalSlotsSelected!+1)"]
                
                PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.AddParkingLotSlabPlan.rawValue), parameters:parameters, completionClosure: { (success:Int!, result:AnyObject?) -> Void in
                    
                    if success == 1 {
                        
                        
                        self.handleError("Success", message: "Your parking plan has been added successfully", handler: { (action) -> Void in
                            
                            self.daysArray.removeAllObjects()
                            self.daysArray=[]
                            
                            self.navigationController?.popViewControllerAnimated(true)
                            
                        })
                        
                        
                    }
                    else
                    {
                        
                        let error:NSError = (result as! NSError)
                        let title = String(error.code) + ": " + error.domain
                        let message = error.localizedDescription
                        self.handleError(title, message: message)
                        
                    }
                })
                
            }
        }
        
    }
    
    
}
