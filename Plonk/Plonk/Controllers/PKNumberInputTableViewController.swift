//
//  PKNumberInputTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit

enum PKNumberInputType: Int {
    case MobileNumber = 0
    case OTP = 1
}

private struct PKNumberInputConstants {

    static let TitleHeaderOTP:String = "Enter verification code"
    static let TitleFooterFirstOTP:String = "Hang On! This may take a minute\nDidn't get it? "
    static let TitleFooterSecondOTP:String = "Tap here to resend"
    static let TitleAccessoryContinue:String = "Continue"
}

class PKNumberInputTableViewController: PKAuthenticationBaseTableViewController {

    var accessoryViewContinue:UIView!
    var accessoryBtnContinue:MKButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //let backBtn = createBackButton()
        //backBtn.addTarget(self, action: "backPressed:", forControlEvents: .TouchUpInside)
        
        accessoryViewContinue = UIView(frame: CGRectZero)
        accessoryViewContinue.backgroundColor = UIColor.whiteColor()
        accessoryViewContinue.frame = CGRectMake(0, view.frame.origin.y, view.frame.size.width, 44)
        
        accessoryBtnContinue = MKButton(type: UIButtonType.Custom)
        accessoryBtnContinue.rippleLocation = .TapLocation
        accessoryBtnContinue.rippleLayerColor = UIColor.appThemeColor()
        accessoryBtnContinue.setTitle(PKNumberInputConstants.TitleAccessoryContinue, forState: UIControlState.Normal)
        accessoryBtnContinue.titleLabel?.font = UIFont.PKAllerLight(14)
        accessoryBtnContinue.backgroundColor = UIColor.clearColor()
        accessoryBtnContinue.setTitleColor(UIColor.appThemeColor(), forState: UIControlState.Normal)
        accessoryBtnContinue.addTarget(self, action: "continuePressed:", forControlEvents: UIControlEvents.TouchUpInside)

        UIView.layoutView(accessoryBtnContinue, fitView: accessoryViewContinue, transparent: true)

        let lineView = UIView(frame: CGRectZero)
        lineView.backgroundColor = UIColor.grayColor()
        
        accessoryViewContinue.addSubview(lineView)
        
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.addLeftEdgeAttachConstraint(accessoryViewContinue)
        lineView.addRightEdgeAttachConstraint(accessoryViewContinue)
        lineView.addBottomEdgeAttachConstraint(accessoryViewContinue)
        lineView.addHeightConstraint(0.5)
        
    }
    
    func backPressed(sender:UIButton) {
        self.handleDecisionPrompt("Go Back ?", message: "Are you sure you want to go back ?") { (action) -> Void in
            
            self.navigationController?.popViewControllerAnimated(true)
            
        }
    }
    
    func createHeaderCell() -> PKCenteredLabelTableViewCell {
        
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cell.backgroundColor = UIColor.clearColor()

        return cell
    }

    func createFooterCell() -> PKCenteredLabelTableViewCell {
        
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 80
        case 0:
            return 44
        default:
            return 80
        }
    }

}
