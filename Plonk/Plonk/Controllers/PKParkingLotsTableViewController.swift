//
//  PKParkingLotsTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD
import DZNEmptyDataSet

struct PKParkingLotConstants {
    static let TitleMyParkingLots = "My Parking lots"
}

class PKParkingLotsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate  {

    var tableView:UITableView!

    var myParkingLotFRC:NSFetchedResultsController?
    
    var parentNavigationController:UINavigationController?
    
    private var placePicker:GMSPlacePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableview()
        setupAddButton()

        view.backgroundColor = UIColor.whiteColor()
        myParkingLotFRC = PKUserParkingLot.MR_fetchAllSortedBy("parking_lot_id", ascending: true, withPredicate: nil, groupBy: nil, delegate: self, inContext:NSManagedObjectContext.MR_defaultContext())
        myParkingLotFRC?.delegate = self

        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        self.tableView.reloadData()
        
        automaticallyAdjustsScrollViewInsets = false

    }
    
    func setupAddButton()
    {
        let addButton:UIButton=UIButton(frame: CGRectZero)
       // addButton.setImage(UIImage(named: "ic_addParking"), forState: UIControlState.Normal)
        
        addButton.setBackgroundImage(UIImage(named: "ic_addParking"), forState: UIControlState.Normal)
        
        self.view .addSubview(addButton)
        addButton.translatesAutoresizingMaskIntoConstraints=false
        addButton.addWidthConstraint(50)
        addButton.addHeightConstraint(50)
        addButton.addRightEdgeAttachConstraint(self.view, offset:20)
        addButton.addBottomEdgeAttachConstraint(self.view, offset: 40)
        addButton.addTarget(self, action: "addButtonPressed", forControlEvents: .TouchUpInside)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("myReuseIdentifier") as? PKParkingTableViewCell
        
        if cell == nil {
            
            cell = PKParkingTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "myReuseIdentifier")
        }
        
        configureCell(cell!, atIndexPath: indexPath)
        
        return cell!
    }
    
    func configureCell(cell:PKParkingTableViewCell, atIndexPath indexPath:NSIndexPath) {
        
        let parkingLotFeed = myParkingLotFRC!.objectAtIndexPath(indexPath) as! PKUserParkingLot
        cell.textLabel?.text = parkingLotFeed.parking_lot_name
        cell.detailTextLabel?.text = parkingLotFeed.parking_lot_address
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        let parkingLotFeed = myParkingLotFRC!.objectAtIndexPath(indexPath) as! PKUserParkingLot

        getParkingDetails(parkingLotFeed.tenant_lot_id!)
    }
    
    func getParkingDetails(tenantLotId:String)
    {
        SVProgressHUD.showWithMaskType(.Gradient)
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ParkingLotDetails.rawValue), parameters:["Lot_id":tenantLotId]) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1
            {
                let json = result as! Dictionary<String,AnyObject>
                  
                let parkingDetailsDicArray = json["prkLots_details"] as! NSArray
                let parkingDetailsDic = parkingDetailsDicArray.objectAtIndex(0) as! Dictionary<String,AnyObject>

                let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameMyParkingLot, bundle: nil)
                let parkingLotDetailsVC = storyboard.instantiateViewControllerWithIdentifier("parkingLotDetailsVC") as! PKParkingLotDetailsTableViewController
                parkingLotDetailsVC.parkingId = tenantLotId
                parkingLotDetailsVC.parkingDetailsDic = parkingDetailsDic
                self.navigationController?.pushViewController(parkingLotDetailsVC, animated: true)
                
            }
            else
            {
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
            
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        return myParkingLotFRC?.sections?.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let sectionInfo = myParkingLotFRC!.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func addButtonPressed()
    {
        /*
        let southWestSyndneyCord:CLLocationCoordinate2D = CLLocationCoordinate2DMake(-33.8659, 151.1953)
        let northEastSydneyCord:CLLocationCoordinate2D = CLLocationCoordinate2DMake(-33.8645, 151.1969)
        let sydneyBounds = GMSCoordinateBounds(coordinate: southWestSyndneyCord, coordinate: northEastSydneyCord)
        let config = GMSPlacePickerConfig(viewport: sydneyBounds)
        placePicker = GMSPlacePicker(config: config)
        placePicker.pickPlaceWithCallback { (place:GMSPlace?, error:NSError?) -> Void in
            
            
            let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameMyParkingLot, bundle: nil)
            let addParkingLotVC:PKAddParkingLotViewController = storyboard.instantiateViewControllerWithIdentifier("addParkingLotVC") as! PKAddParkingLotViewController
            addParkingLotVC.placemark = place
            addParkingLotVC.navigationItem.title = "Add parking"
            
            self.navigationController?.pushViewController(addParkingLotVC, animated: true)
            
            //            if place != nil {
            //                self.configureViewWithPlace(place)
            //            }
            //            else if error != nil {
            //
            //            } else {
            //
            //            }
            
        }
        */
        
        let storyBoard = UIStoryboard(name: PKConstants.StoryboardNameMyParkingLot, bundle: nil)
        let mapPlacePicker = storyBoard.instantiateViewControllerWithIdentifier("MapPlacePicker") as! MapViewController
        navigationController?.pushViewController(mapPlacePicker, animated: true)
        
        
    }

    func setupTableview()
    {
        tableView = UITableView(frame: CGRectZero, style: UITableViewStyle.Grouped)
        tableView.dataSource = self
        tableView.delegate = self

        UIView.layoutView(tableView, fitView: view, transparent: false)
        
        
    }

    // MARK: - NSFetchedResultsControllerDelegate
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        let tableView:UITableView = self.tableView
        
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Update:
            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!) as! PKParkingTableViewCell, atIndexPath: indexPath!)
            break
        default:
            break
        }
    }
    
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
            
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
        default:break
        }
        
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
// empty data methods
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        
        // return UIImage(named: "ic_parking_map")
        
        return UIImage(named: "ic_plonk_large_whitebg")
        
        
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let emptyTitle:String = "Do you have a parking spot that isn't being fully utitlized? And, would you like to generate income from it? Click on the add icon to get started.\n"
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        let emptyTitle:String = "Visit goplonk.com for more details"
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
        
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView!) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
}
