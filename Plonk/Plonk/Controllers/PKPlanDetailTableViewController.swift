//
//  PKPlanDetailTableViewController.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 20/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKPlanDetailTableViewController: UITableViewController {
    
    var planId:String!
    var lotId:String!
    
    @IBOutlet weak var activeInactiveLabel: UILabel!

    @IBOutlet weak var dateRange: UILabel!
    
    @IBOutlet weak var costLabel: UILabel!
    
    @IBOutlet weak var minimumDuration: UILabel!
    
    @IBOutlet weak var incrementTime: UILabel!
    
    @IBOutlet weak var extraCharge: UILabel!
    
    @IBOutlet var daysView: PKSegmentControl!

    var resultJson:Dictionary<String,AnyObject>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let entryDate = dateFormatter.dateFromString(resultJson["start_date"] as? String ?? "")
        
        let exitDate = dateFormatter.dateFromString(resultJson["end_date"] as? String ?? "")
        
        dateFormatter.dateFormat = "dd MMM"
        
        var startDate:String!
        var endDate:String!
    
        
        if let sd = entryDate
        {
        startDate = dateFormatter.stringFromDate(sd)
        }
        
        if let ed = exitDate
        {
        endDate = dateFormatter.stringFromDate((exitDate == nil ? entryDate : exitDate)!)
        }
        
        
        
        dateRange.text = "\(startDate) - \(endDate)"
        let fare:NSString = "₹ "+((resultJson["fare"] as? NSString)! as String)
        
        let extrachrge:NSString = "₹ "+((resultJson["extra_charge"] as? NSString)! as String)
        self.costLabel.text = fare as String
        self.extraCharge.text = extrachrge as String
        
        let string:NSString = (resultJson["open_days"] as? NSString)!
        let daysArray:NSArray = string.componentsSeparatedByString(",")
        
        for day in daysArray {
            let dayButton = daysView.segmentButtons[NSString(string: day as! String).intValue-1]
            dayButton.selected = true
            dayButton.customizeForSelected()
        }
        
        let minimumRawValue = resultJson["min_duration"] as? String
        
        let timeRangeCompenents = minimumRawValue?.componentsSeparatedByString(":")
        
        minimumDuration.text = ""
        
        if timeRangeCompenents?.count == 3 {
            let hour = timeRangeCompenents![0] == "00" ? "": "\(NSString(string: timeRangeCompenents![0]).intValue)hr"
            let minute = timeRangeCompenents![1] == "00" ? "" : "\(NSString(string: timeRangeCompenents![1]).intValue)min"
            let seconds = timeRangeCompenents![2] == "00" ? "" : "\(NSString(string: timeRangeCompenents![2]).intValue)sec"
            
            minimumDuration.text = (hour.characters.count == 0 ? "" : hour)+(minute.characters.count == 0 ? "" : minute)+(seconds.characters.count == 0 ? "" : seconds)
            
        }
        
        let incrementRawValue = resultJson["increments"] as? String
        
        if incrementRawValue != "00:00:00" {
            
            let incrementRangeCompenents = incrementRawValue?.componentsSeparatedByString(":")
            
            incrementTime.text = ""
            
            if incrementRangeCompenents?.count == 3 {
                let hour = incrementRangeCompenents![0] == "00" ? "": "\(NSString(string: incrementRangeCompenents![0]).intValue)hr"
                let minute = incrementRangeCompenents![1] == "00" ? "" : "\(NSString(string: incrementRangeCompenents![1]).intValue)min"
                let seconds = incrementRangeCompenents![2] == "00" ? "" : "\(NSString(string: incrementRangeCompenents![2]).intValue)sec"
                
                incrementTime.text = (hour.characters.count == 0 ? "" : hour)+(minute.characters.count == 0 ? "" : minute)+(seconds.characters.count == 0 ? "" : seconds)
                
            }
        }else {
            incrementTime.text = "NA"
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func switchAction(sender: UISwitch) {
        
        if sender.on
        {
            activeInactiveLabel.text = "Active"
        }
        else
        {
            activeInactiveLabel.text = "Inactive"
        }
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.DeactivatePlan.rawValue), parameters:["plan_id":planId,"Lot_id":lotId]) { (success:Int!, result:AnyObject?) -> Void in
            
            if success == 1
            {
                
            }
            else
            {
                
                
            }
        }
    }

}
