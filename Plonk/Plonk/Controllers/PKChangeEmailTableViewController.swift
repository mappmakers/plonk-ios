//
//  PKChangeEmailTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 28/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

typealias UpdateEmailCompletion = (updatedEmail : String) -> Void

class PKChangeEmailTableViewController: UITableViewController {
    
    var updatedEmailCompletion:UpdateEmailCompletion?
    
    let inputEmailCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
    let confirmCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
    
    var arrayCells:Array<UITableViewCell>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Change Email Address"
        
        customizeForStaticTableStyle()
        
        inputEmailCell.textField.placeholder = "New Email Address"
        inputEmailCell.textField.delegate = self
        inputEmailCell.textField.keyboardType = .EmailAddress
        inputEmailCell.textField.returnKeyType = UIReturnKeyType.Done
        inputEmailCell.textField.autocapitalizationType = .None
        inputEmailCell.textField.becomeFirstResponder()
        inputEmailCell.textField.floatingLabelTextColor = UIColor.appThemeColor()
        inputEmailCell.textField.textColor = UIColor.appThemeColor()
        inputEmailCell.backgroundColor = UIColor.whiteColor()
        inputEmailCell.textField.tintColor = UIColor.appThemeColor()
        
        confirmCell.textLabel?.text = "Confirm"
        confirmCell.backgroundColor = UIColor.appThemeColor()
        
        arrayCells = [inputEmailCell, confirmCell]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayCells?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return PKConstants.CellTextFieldHeight-10
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return arrayCells![indexPath.section]
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 1 {
            
            let emailStr = inputEmailCell.textField.text!

            if !emailStr.isValidEmail() {

                let title:String = "\n"

                handleError(title, message: String.bulletinedText("Enter a valid email address\n"))
            }else {
                
                let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"user_email":emailStr]
                
                PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UpdateEmail.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
                    
                    if success != 1
                    {
                        let error:NSError = (result as! NSError)
                        let title = String(error.code) + ": " + error.domain
                        let message = error.localizedDescription
                        self.handleError(title, message: message)
                    }else {
                        
                        let json = result as! Dictionary<String,AnyObject>
                        if json["success"] as! String == "true" {
                            
                            let email = (self.arrayCells![0] as! PKTextFieldLoginTableViewCell).textField.text ?? ""
                            PKDataModel.sharedInstance.loggedUser.user_email = email
                            self.navigationController?.popViewControllerAnimated(true)
                            if self.updatedEmailCompletion != nil {
                                self.updatedEmailCompletion!(updatedEmail: email)
                            }
                            
                        }
                        
                    }
                }
            }
        }
        
    }

}
