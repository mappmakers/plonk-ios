//
//  PKParkingLotOverViewTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

enum PKParkingLotOverViewType: Int {
    case Name = 0
    case Status = 1
    case Price = 2
    case Timing = 3
    case ParkingType = 4
    case Capacity = 5
}

class PKParkingLotOverViewTableViewController: UITableViewController {

    @IBOutlet var imageViewParkingLot: PKRoundedImageView!
    @IBOutlet var labelsParkingOverView: [UILabel]!
    
    @IBOutlet var daysView: PKSegmentControl!
    var parkingLot:PKParkingLot!
    
    var searchFare:String!
    
    var favoriteCompletion:FavoriteCompletion?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customizeSubViews()
    }
    
    func customizeSubViews() {
        
        labelsParkingOverView[PKParkingLotOverViewType.Name.rawValue].text = parkingLot.parking_lot_name
        
        let availableSlots = NSString(string: parkingLot.difference!).floatValue
        let totalSlots = NSString(string: parkingLot.parking_total_slots!).floatValue
        let percentage = availableSlots/totalSlots*100
        
        labelsParkingOverView[PKParkingLotOverViewType.Status.rawValue].text = percentage<50 ? "Filling Fast": "Available"
        
        
        labelsParkingOverView[PKParkingLotOverViewType.Status.rawValue].textColor = percentage<50 ? UIColor.redColor(): UIColor.darkGrayColor()
        labelsParkingOverView[PKParkingLotOverViewType.Price.rawValue].text = "₹ "+parkingLot.fare_range!
        labelsParkingOverView[PKParkingLotOverViewType.Timing.rawValue].text = parkingLot.time_range
        labelsParkingOverView[PKParkingLotOverViewType.ParkingType.rawValue].text = parkingLot.parking_type
        
        let arrayDays = parkingLot.parking_days!.componentsSeparatedByString(",")
        for day in arrayDays {
            let dayButton = daysView.segmentButtons[NSString(string: day).intValue-1]
            dayButton.selected = true
            dayButton.customizeForSelected()
        }
        
        imageViewParkingLot.loadImageFromURLString(PKAmazonRoute.ImagePath(parkingLot.parking_lot_image ?? "").URLString, placeholderImage: nil) { (finished, error) -> Void in
            
            if error != nil {
                self.imageViewParkingLot.image = UIImage(named: "ic_goplonk")
            }
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                    
                    self.view.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 360.0)
                    
                    }, completion: { (completed:Bool) -> Void in
                        
                        self.parentViewController?.removeChildView(PKParkingLotOverViewTableViewController.classForCoder())
                     })
                
                break
            case 1:
                pushToDetailController()
                break
            default:
                break
            }
        }else {
            pushToDetailController()
        }
        
    }
    
    @IBAction func qrCodePressed(sender: AnyObject) {
        let qrCodeViewController:PKQRCodeViewController = PKQRCodeViewController()
        qrCodeViewController.fare = searchFare
        navigationController?.pushViewController(qrCodeViewController, animated: true)
    }
    
    func pushToDetailController() {
        
        let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameParkingLot, bundle: nil)
        let parkingDetailController:PKParkingDetailsTableViewController = storyboard.instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingDetail) as! PKParkingDetailsTableViewController
        parkingDetailController.parkingLot = parkingLot
        parkingDetailController.favoriteCompletion = favoriteCompletion
        parentViewController?.navigationController?.pushViewController(parkingDetailController, animated: true)
    }
    
    override func willMoveToParentViewController(parent: UIViewController?) {
        view.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 64)
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        let viewHeight:CGFloat = 360.0
        view.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height-viewHeight, UIScreen.mainScreen().bounds.size.width, viewHeight)
    }
}
