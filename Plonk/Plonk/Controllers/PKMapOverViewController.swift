//
//  PKMapOverViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit
import MaterialControls
import ENSwiftSideMenu
import SVProgressHUD

class PKMapOverViewController: UIViewController, GMSMapViewDelegate, UISearchControllerDelegate, ENSideMenuDelegate {

    private var parkingLotShortSummaryController:PKParkingLotOverViewTableViewController!
    private var searchController:UISearchController?
    private var placePicker:GMSPlacePicker!
    private var mapView:GMSMapView!
    private var searchField:MKTextField!
    private var sliderView:MDSlider!
    private var labelFareInfo:UILabel!
    private var btnActiveParkingTimer:MKButton?
    
    private var firstLocationUpdate:Bool = false
    private var MyLocationObservingContext = 0
    private let ObservatingKeyPath = "myLocation"
    private var myLocation:CLLocation?
    private var currentSliderValue:Float = 20
    
    private var parkingSlotObject:Dictionary<String, AnyObject>?
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if context == &MyLocationObservingContext {
            
            if !firstLocationUpdate {
                
                firstLocationUpdate = true
                
                myLocation = change![NSKeyValueChangeNewKey] as? CLLocation
                mapView.camera = GMSCameraPosition.cameraWithLatitude(myLocation!.coordinate.latitude,
                    longitude:myLocation!.coordinate.longitude, zoom:16)
                
                fetchAllNearbyLocations()
            }
            return
            
        }
        super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)

    }
    
    deinit {
        mapView.removeObserver(self, forKeyPath: ObservatingKeyPath, context: &MyLocationObservingContext)
        //NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        findActiveTimerInfo()
        
        self.title = PKConstants.AppTitle
        
        createBurgerIcon()
        
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        self.sideMenuController()?.sideMenu?.menuWidth = screenWidth - screenWidth/5
        
        self.sideMenuController()?.sideMenu?.bouncingEnabled = false
        
//        let leftEdgeSwipeGestureRezognizer = UIScreenEdgePanGestureRecognizer(target: self, action: "handlePan:")
//        leftEdgeSwipeGestureRezognizer.edges = UIRectEdge.Left
//        self.view.addGestureRecognizer(leftEdgeSwipeGestureRezognizer)

        // Bangalore
        let camera = GMSCameraPosition.cameraWithLatitude(12.9715987,
            longitude:77.5945627, zoom:16)
        
        mapView = GMSMapView.mapWithFrame(CGRectZero, camera:camera)
        mapView.delegate = self
        mapView.settings.compassButton = true;
        mapView.settings.myLocationButton = true
        mapView.myLocationEnabled = true
        
        view.addSubview(mapView)
        
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.addTopEdgeAttachConstraint(view)
        mapView.addLeftEdgeAttachConstraint(view)
        mapView.addRightEdgeAttachConstraint(view)
        mapView.addBottomEdgeAttachConstraint(view, offset: 64)
        
        createOverlayViews()

        mapView.addObserver(self, forKeyPath: ObservatingKeyPath, options: .New, context: &MyLocationObservingContext)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "findActiveTimerInfo", name: PKConstants.NotificationParkingSlotConfirmed, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "removeBlinkingView", name: PKConstants.NotificationParkingMeterStopped, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fetchAllNearbyLocations", name: PKConstants.NotificationUserLotAdded, object: nil)
        
        dispatch_async(dispatch_get_main_queue(), {
            self.mapView.myLocationEnabled = true
        });

        view.sendSubviewToBack(mapView)

        fetchAllNearbyLocations()
    }
    
    func removeBlinkingView() {
        
        if btnActiveParkingTimer != nil {
            
            btnActiveParkingTimer!.removeFromSuperview()
            btnActiveParkingTimer = nil
        }
        
    }

    func findActiveTimerInfo() {
        
        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ActiveParkingTimer.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in

            if success == 1 {

                let json = result as! Dictionary<String, AnyObject>
                
                let dataArray = json["res"] as! Array<Dictionary<String, AnyObject>>
                
                if dataArray.count > 0 {
                    
                    for parkingSlot in dataArray {
                        
                        self.parkingSlotObject = parkingSlot
                    }

                    
                    self.btnActiveParkingTimer = MKButton(type: .Custom)
                    self.btnActiveParkingTimer!.setImage(UIImage(named: "ic_running_clock"), forState: .Normal)
                    self.btnActiveParkingTimer!.rippleLocation = .TapLocation
                    self.btnActiveParkingTimer!.rippleLayerColor = UIColor.whiteColor()
                    self.btnActiveParkingTimer?.tintColor = UIColor(rgba: "#F44336")
                    self.btnActiveParkingTimer!.addTarget(self, action: "didTapBlinkingTimer:", forControlEvents: .TouchUpInside)
                    
                    self.view.addSubview(self.btnActiveParkingTimer!)
                    
                    self.btnActiveParkingTimer!.translatesAutoresizingMaskIntoConstraints = false
                    self.btnActiveParkingTimer!.addRightEdgeAttachConstraint(self.view, offset:5)
                    self.btnActiveParkingTimer!.addWidthConstraint(60)
                    self.btnActiveParkingTimer!.addHeightConstraint(60)
                    self.btnActiveParkingTimer!.addBottomEdgeAttachConstraint(self.view, offset: 204)
                    
                    let theAnimation = CABasicAnimation (keyPath: "opacity")
                    theAnimation.duration = 1.5
                    theAnimation.repeatCount = 10000000;
                    theAnimation.autoreverses = true;
                    theAnimation.removedOnCompletion = false;
                    theAnimation.fromValue = NSNumber(double: 1.0);
                    theAnimation.toValue = NSNumber(double: 0.5)
                    self.btnActiveParkingTimer!.layer.addAnimation(theAnimation, forKey: "animateOpacity")

                    let anim = CABasicAnimation (keyPath: "transform.scale")
                    anim.duration = 1.5
                    anim.fromValue = NSNumber(double: 1.0);
                    anim.toValue = NSNumber(double: 0.5)
                    anim.repeatCount = 10000000;
                    anim.removedOnCompletion = false;
                    anim.autoreverses = true;
                    anim.fillMode = kCAFillModeBoth;
                    
                    self.btnActiveParkingTimer!.layer.addAnimation(anim, forKey: "animateScale")
                    /*
                    
                    let rippleBlinkBtn = BTRippleButtton(image: UIImage(named: "ic_running_clock"), andFrame: CGRectMake(self.view.frame.size.width-65, self.view.frame.size.height-264, 60, 60), onCompletion: { (success) -> Void in
                        
                        let parkingSLot = PKParkingSlot.MR_createEntityInContext(nil)
                        parkingSLot.updateSlotWithDictionary(self.parkingSlotObject!)
                        
                        let selectedVehicle = PKUserVehicle.MR_createEntityInContext(nil)
                        selectedVehicle.make_name = self.parkingSlotObject!["make_name"] as? String
                        selectedVehicle.user_vehicle_id = self.parkingSlotObject!["user_vehicle_id"] as? String
                        selectedVehicle.registeration_no = self.parkingSlotObject!["registeration_no"] as? String
                        selectedVehicle.make_id = ""
                        selectedVehicle.model_id = ""
                        selectedVehicle.model_name = ""
                        selectedVehicle.user_id = ""
                        selectedVehicle.vehicle_type = ""
                        
                        let parkingMeterStartViewController = UIStoryboard(name: PKConstants.StoryboardNameParkingLot, bundle: nil).instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingMeterStart) as! PKParkingMeterStartTableViewController
                        parkingMeterStartViewController.selectedVehicle = selectedVehicle
                        parkingMeterStartViewController.parkingId = self.parkingSlotObject!["user_parking_id"] as? String
                        parkingMeterStartViewController.parkingSlot = parkingSLot
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        let entryDate = dateFormatter.dateFromString(self.parkingSlotObject!["entry_time"] as? String ?? "")
                        
                        parkingMeterStartViewController.startDate = entryDate
                        
                        self.navigationController?.pushViewController(parkingMeterStartViewController, animated: true)
                        
                    })
                    rippleBlinkBtn.setRippeEffectEnabled(true)
                    rippleBlinkBtn.setRippleEffectWithColor(UIColor.appThemeColor())

                    self.view.addSubview(rippleBlinkBtn)
                    
                    rippleBlinkBtn.translatesAutoresizingMaskIntoConstraints = false
                    rippleBlinkBtn.addRightEdgeAttachConstraint(self.view, offset:5)
                    rippleBlinkBtn.addWidthConstraint(60)
                    rippleBlinkBtn.addHeightConstraint(60)
                    rippleBlinkBtn.addBottomEdgeAttachConstraint(self.view, offset: 204)
                    
//                    let blinkIconButton = self.createRightIcon()
//                    blinkIconButton.addTarget(self, action: "didTapBlinkingTimer:", forControlEvents: UIControlEvents.TouchDown)
                    */
                }
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
        
    }
//    func handlePan(gesture: UISwipeGestureRecognizer) {
//        
//        switch (gesture.state) {
//        case UIGestureRecognizerState.Began:
//            break
//        case UIGestureRecognizerState.Ended:
////            self.toggleSideMenuView()
//            break
//        default:
//            break
//        }
//
//    }
    
    func didTapSearchQRButton(sender:UIButton) {
        
        let qrCodeViewController:PKQRCodeViewController = PKQRCodeViewController()
        qrCodeViewController.fare = String(sliderView.value)
        navigationController?.pushViewController(qrCodeViewController, animated: true)
        
    }
    
    func fetchAllNearbyLocations() {
        
        let sliderFareRange = String(sliderView.value)
        let parameters:Dictionary<String, String> =
        [
            "user_id":PKDataModel.sharedInstance.loggedUser.user_id!,
            "cur_latitude":String(mapView.camera.target.latitude),
            "cur_longitude":String(mapView.camera.target.longitude)
        ]
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            self.mapView.clear()
            
        })
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.SearchParkingLots.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            if success == 1 {
                
                self.mapView.clear()
                
                let json = result as! Dictionary<String, AnyObject>
                
                let dataDIC = json["search_details"] as! Array<Dictionary<String, AnyObject>>
                
                for mapPoint in dataDIC {
                    
                    let marker = GMSMarker()
                    
                    let favoriteLot = PKParkingLot.MR_findFirstByAttribute("parking_lot_id", withValue: mapPoint["parking_lot_id"]!, inContext: NSManagedObjectContext.MR_defaultContext())
                    
                    if favoriteLot == nil || favoriteLot.favourite_id == nil {
                        
                        let parkingLot = PKParkingLot.parkingLotWithDictionaryData(mapPoint)
                        
                        marker.position = CLLocationCoordinate2DMake(NSString(string: parkingLot.parking_lot_latitude!).doubleValue, NSString(string: parkingLot.parking_lot_longitude!).doubleValue)
                        marker.userData = parkingLot
                        marker.appearAnimation = kGMSMarkerAnimationPop
                        
                        var parkingMaxFare = parkingLot.fare_range ?? ""
                        if parkingMaxFare.containsString("-") {
                            parkingMaxFare = parkingLot.fare_range!.componentsSeparatedByString("-")[1]
                        }
                        if Float(parkingMaxFare) <= Float(sliderFareRange) {
                            marker.icon = UIImage(named: "ic_plonk_small")
                        }else {
                            marker.icon = UIImage(named: "ic_plonk_priceup")
                        }
                        
                    }else {
                        marker.icon = UIImage(named: "ic_plonk_fav")
                        
                        marker.position = CLLocationCoordinate2DMake(NSString(string: favoriteLot.parking_lot_latitude!).doubleValue, NSString(string: favoriteLot.parking_lot_longitude!).doubleValue)
                        marker.userData = favoriteLot
                        marker.appearAnimation = kGMSMarkerAnimationPop
                    }

                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        marker.map = self.mapView
                        
                    })                    
                }
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
    }
    
    func createOverlayViews() {
        
        let searchBarView = UIView(frame: CGRectZero)
        
        searchBarView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.8)
        searchBarView.layer.cornerRadius = 5.0
        searchBarView.layer.masksToBounds = true
        
        view.addSubview(searchBarView)
        
        searchBarView.translatesAutoresizingMaskIntoConstraints = false
        searchBarView.addTopEdgeAttachConstraint(view, offset:15)
        searchBarView.addLeftEdgeAttachConstraint(view, offset: 15)
        searchBarView.addRightEdgeAttachConstraint(view, offset: 15)
        searchBarView.addHeightConstraint(48)
        
        let directionImageView = UIImageView(frame: CGRectZero)
        directionImageView.image = UIImage(named: "ic_direction_gray")
        
        searchBarView.addSubview(directionImageView)
        
        let googlePlacesTapGesture = UITapGestureRecognizer(target: self, action: "didTapLocationIcon:")
        searchBarView.addGestureRecognizer(googlePlacesTapGesture)
        
        directionImageView.translatesAutoresizingMaskIntoConstraints = false
        directionImageView.addLeftEdgeAttachConstraint(searchBarView, offset:10)
        directionImageView.addCenterYConstraint(searchBarView)
        directionImageView.addWidthConstraint(25)
        directionImageView.addHeightConstraint(25)
        
        let separatorVerticalLineView = UIView(frame: CGRectZero)
        separatorVerticalLineView.backgroundColor = UIColor.darkGrayColor()
        
        searchBarView.addSubview(separatorVerticalLineView)
        
        separatorVerticalLineView.translatesAutoresizingMaskIntoConstraints = false
        separatorVerticalLineView.addLeftEdgeAttachConstraint(directionImageView, viewEdge: .Right, offset:10)
        separatorVerticalLineView.addTopEdgeAttachConstraint(searchBarView, offset:4)
        separatorVerticalLineView.addBottomEdgeAttachConstraint(searchBarView, offset:4)
        separatorVerticalLineView.addWidthConstraint(1)
        
        searchField = MKTextField(frame: CGRectZero)
        searchField.textColor = UIColor.darkGrayColor()
        searchField.tintColor = UIColor.darkGrayColor()
        searchField.layer.borderColor = UIColor.clearColor().CGColor
        searchField.borderStyle = UITextBorderStyle.None
        searchField.returnKeyType = .Done
        searchField.clearButtonMode = .WhileEditing
        searchField.placeholder = "Enter Location"
        searchField.floatingPlaceholderEnabled = false
        searchField.rippleLocation = .TapLocation
        searchField.rippleLayerColor = UIColor.whiteColor()
        searchField.padding = CGSizeMake(15, 0)
        searchField.delegate = self
        
        searchBarView.addSubview(searchField)
        
        searchField.translatesAutoresizingMaskIntoConstraints = false
        searchField.addLeftEdgeAttachConstraint(separatorVerticalLineView, viewEdge: .Right, offset: 0)
        searchField.addTopEdgeAttachConstraint(separatorVerticalLineView)
        searchField.addBottomEdgeAttachConstraint(separatorVerticalLineView)
        searchField.addRightEdgeAttachConstraint(searchBarView, offset: 5)

        let sliderBackGroundView = UIView(frame: CGRectZero)
        sliderBackGroundView.backgroundColor = UIColor.whiteColor()
        
        view.addSubview(sliderBackGroundView)
        
        sliderBackGroundView.translatesAutoresizingMaskIntoConstraints = false
        
        sliderBackGroundView.addLeftEdgeAttachConstraint(view)
        sliderBackGroundView.addRightEdgeAttachConstraint(view)
        sliderBackGroundView.addBottomEdgeAttachConstraint(view)
        sliderBackGroundView.addHeightConstraint(64)

        sliderView = MDSlider(frame:CGRectZero)
        sliderView.minimumValue = 10
        sliderView.maximumValue = 100
        sliderView.step = 10
        sliderView.enabledValueLabel = true
        sliderView.thumbOnColor = UIColor.appThemeColor()
        sliderView.trackOnColor = UIColor.appThemeColor()
        sliderView.thumbOffColor = UIColor.appThemeColor()
        sliderView.trackOffColor = UIColor.lightGrayColor()

        if let sliderValue = NSUserDefaults.standardUserDefaults().objectForKey(PKConstants.UserDefaultSliderValue) {
            currentSliderValue = (sliderValue as! NSNumber).floatValue
            sliderView.value = currentSliderValue
        }else {
            sliderView.value = currentSliderValue
            NSUserDefaults.standardUserDefaults().setObject(NSNumber(float: currentSliderValue), forKey: PKConstants.UserDefaultSliderValue)
        }
        
        sliderView.setValue(UIFont.PKAllerRegular(9), forKeyPath: "thumbView.bubble.label.font")
        sliderView.tickMarksColor = UIColor.appThemeColor()
        sliderView.backgroundColor = UIColor.clearColor()
        sliderView.addTarget(self, action: "slideValueChanged:", forControlEvents: .ValueChanged)
        sliderBackGroundView.addSubview(sliderView)
        
        sliderView.translatesAutoresizingMaskIntoConstraints = false
        UIView.layoutView(sliderView, fitView: sliderBackGroundView, transparent: true)
        
        let labelMinInfo = UILabel(frame: CGRectZero)
        labelMinInfo.text = "₹10"
        labelMinInfo.font = UIFont.PKAllerBold(12)
        labelMinInfo.textAlignment = .Center
        
        sliderView.addSubview(labelMinInfo)
        
        labelMinInfo.translatesAutoresizingMaskIntoConstraints = false
        labelMinInfo.addLeftEdgeAttachConstraint(sliderView, offset:5)
        labelMinInfo.addTopEdgeAttachConstraint(sliderView, offset:5)
        labelMinInfo.addWidthConstraint(30)
        labelMinInfo.addHeightConstraint(21)
        
        sliderView.sendSubviewToBack(labelMinInfo)
        
        let labelMaxInfo = UILabel(frame: CGRectZero)
        labelMaxInfo.text = "₹100"
        labelMaxInfo.font = UIFont.PKAllerBold(12)
        labelMaxInfo.textAlignment = .Center
        
        sliderView.addSubview(labelMaxInfo)
        
        labelMaxInfo.translatesAutoresizingMaskIntoConstraints = false
        labelMaxInfo.addRightEdgeAttachConstraint(sliderView, offset:5)
        labelMaxInfo.addTopEdgeAttachConstraint(sliderView, offset:5)
        labelMaxInfo.addWidthConstraint(30)
        labelMaxInfo.addHeightConstraint(21)
        
        sliderView.sendSubviewToBack(labelMaxInfo)
        
        labelFareInfo = UILabel(frame: CGRectZero)
        labelFareInfo.text = "Current fare - ₹ \(Int(currentSliderValue))"
        labelFareInfo.font = UIFont.PKAllerBold(12)
        labelFareInfo.textAlignment = .Center
        
        sliderView.addSubview(labelFareInfo)
        
        labelFareInfo.translatesAutoresizingMaskIntoConstraints = false
        labelFareInfo.addLeftEdgeAttachConstraint(labelMinInfo, viewEdge: .Right, offset:5)
        labelFareInfo.addTopEdgeAttachConstraint(sliderView, offset:5)
        labelFareInfo.addRightEdgeAttachConstraint(labelMaxInfo, viewEdge: .Left, offset:5)
        labelFareInfo.addHeightConstraint(21)

        sliderView.sendSubviewToBack(labelFareInfo)
        
        let btnQRCode = MKButton(type: .Custom)
        btnQRCode.setBackgroundImage(UIImage(named: "qrscan"), forState: .Normal)
        btnQRCode.rippleLocation = .TapLocation
        btnQRCode.rippleLayerColor = UIColor.whiteColor()
        btnQRCode.addTarget(self, action: "didTapSearchQRButton:", forControlEvents: .TouchUpInside)
        
        view.addSubview(btnQRCode)
        
        btnQRCode.translatesAutoresizingMaskIntoConstraints = false
        btnQRCode.addRightEdgeAttachConstraint(view, offset:5)
        btnQRCode.addWidthConstraint(60)
        btnQRCode.addHeightConstraint(60)
        btnQRCode.addBottomEdgeAttachConstraint(sliderView, viewEdge: .Top, offset: 70)
        
        
    }
 
    func didTapBlinkingTimer(sender:UIButton) {
        
        SVProgressHUD.showWithMaskType(.Gradient)
        
        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ActiveParkingTimer.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                let json = result as! Dictionary<String, AnyObject>
                
                let dataArray = json["res"] as! Array<Dictionary<String, AnyObject>>
                
                if dataArray.count > 0 {
                    
                    let parkingSLot = PKParkingSlot.MR_createEntityInContext(nil)
                    parkingSLot.updateSlotWithDictionary(self.parkingSlotObject!)
                    parkingSLot.parking_pymnt_mode = "cash"
                    
                    let selectedVehicle = PKUserVehicle.MR_createEntityInContext(nil)
                    selectedVehicle.make_name = self.parkingSlotObject!["make_name"] as? String
                    selectedVehicle.user_vehicle_id = self.parkingSlotObject!["user_vehicle_id"] as? String
                    selectedVehicle.registeration_no = self.parkingSlotObject!["registeration_no"] as? String
                    selectedVehicle.make_id = ""
                    selectedVehicle.model_id = ""
                    selectedVehicle.model_name = ""
                    selectedVehicle.user_id = ""
                    selectedVehicle.vehicle_type = ""
                    
                    let parkingMeterStartViewController = UIStoryboard(name: PKConstants.StoryboardNameParkingLot, bundle: nil).instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingMeterStart) as! PKParkingMeterStartTableViewController
                    parkingMeterStartViewController.selectedVehicle = selectedVehicle
                    parkingMeterStartViewController.parkingId = self.parkingSlotObject!["user_parking_id"] as? String
                    parkingMeterStartViewController.parkingSlot = parkingSLot
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    let entryDate = dateFormatter.dateFromString(self.parkingSlotObject!["entry_time"] as? String ?? "")
                    
                    parkingMeterStartViewController.startDate = entryDate
                    
                    self.navigationController?.pushViewController(parkingMeterStartViewController, animated: true)
                    
                }else {
                    self.handleError("Sorry!", message: "You don't seem to have any active parking", handler: { (action) -> Void in
                        
                        self.removeBlinkingView()
                        
                    })
                }
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
    }

    func slideValueChanged(sender:MDSlider) {
        
        if sender.value%10 == 0.0 && sender.value != currentSliderValue {
            
            currentSliderValue = sender.value
            
            let sliderValueObject = NSNumber(float: currentSliderValue)
            
            let parameters = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"pref_price":sliderValueObject]
            
            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UpdateUserDefaultPrice.rawValue), parameters: parameters) { (success, result) -> Void in
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    // time-consuming task
                    dispatch_async(dispatch_get_main_queue(), {
                        SVProgressHUD.dismiss()
                    })
                })
                
                NSUserDefaults.standardUserDefaults().setObject(sliderValueObject, forKey: PKConstants.UserDefaultSliderValue)
                NSUserDefaults.standardUserDefaults().synchronize()
            }

            labelFareInfo.text = "Current fare - ₹ \(Int(currentSliderValue))"
            fetchAllNearbyLocations()
            
        }else {
            print(sender.value)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- UITextFieldDelegate
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {

        if searchController == nil {
            
            let resultsController = PKMapSearchResultsTableViewController(style:.Plain)
            resultsController.autocompleteCallback = { (placePrediction:GMSAutocompletePrediction?, error:NSError?) -> Void in
                
                self.searchController = nil
                
                if placePrediction != nil {
                    
                    GMSPlacesClient.sharedClient().lookUpPlaceID(placePrediction!.placeID, callback: { (place: GMSPlace?, error: NSError?) -> Void in
                        if let error = error {
                            print("lookup place id query error: \(error.localizedDescription)")
                            return
                        }
                        
                        if let place = place {
                            print("Place name \(place.name)")
                            print("Place address \(place.formattedAddress)")
                            print("Place placeID \(place.placeID)")
                            print("Place attributions \(place.attributions)")
                            
                            self.configureViewWithPlace(place)
                            
                        } else {
                            print("No place details for \(placePrediction!.placeID)")
                        }
                    })

                }
                
            }
            
            searchController = UISearchController(searchResultsController: resultsController)
            searchController!.searchBar.barTintColor = UIColor.appThemeColor()
            searchController!.searchBar.backgroundColor = UIColor.appThemeColor()
            searchController!.searchBar.hidden = false
            searchController!.searchBar.showsCancelButton = true
            searchController!.delegate = self
            searchController!.searchResultsUpdater = resultsController
            searchController!.hidesNavigationBarDuringPresentation = false
            searchController!.searchBar.frame = CGRectMake(searchController!.searchBar.frame.origin.x, searchController!.searchBar.frame.origin.y, searchController!.searchBar.frame.size.width, 44)
            searchController!.searchBar.delegate = resultsController
            searchController!.searchBar.sizeToFit()
            searchController!.dimsBackgroundDuringPresentation = false
            
            presentViewController(searchController!, animated: true) { () -> Void in
                self.searchController!.searchBar.becomeFirstResponder()
            }
        }
        
        return false
    }
    
    func didTapLocationIcon(sender:UITapGestureRecognizer) {
    
        /*
        let southWestSyndneyCord:CLLocationCoordinate2D = CLLocationCoordinate2DMake(-33.8659, 151.1953)
        let northEastSydneyCord:CLLocationCoordinate2D = CLLocationCoordinate2DMake(-33.8645, 151.1969)
        let sydneyBounds = GMSCoordinateBounds(coordinate: southWestSyndneyCord, coordinate: northEastSydneyCord)
        let config = GMSPlacePickerConfig(viewport: sydneyBounds)
        placePicker = GMSPlacePicker(config: config)
        placePicker.pickPlaceWithCallback { (place:GMSPlace?, error:NSError?) -> Void in

            self.configureViewWithPlace(place)
            
//            if place != nil {
//                self.configureViewWithPlace(place)
//            }
//            else if error != nil {
//
//            } else {
//
//            }
            
        }
        */
    }
    
    func configureViewWithPlace(place:GMSPlace?) {
        
        if place != nil {
            self.searchField.text = place!.formattedAddress!
            let camera = GMSCameraPosition.cameraWithLatitude(place!.coordinate.latitude,
                longitude:place!.coordinate.longitude, zoom:16)
            self.mapView.camera = camera
            
            fetchAllNearbyLocations()
        }
        
    }
    
    override func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var txtAfterUpdate:NSString = NSString(string: textField.text!)
        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
        
        searchWithText(txtAfterUpdate)
        
        return true
    }
    
    // MARK:- ACTIONS
    
    func searchWithText(searchText:NSString) {
        print(searchText)
    }

    // MARK:- GMSMapViewDelegate
    
    func mapView(mapView: GMSMapView!, willMove gesture: Bool) {
        
        if self.childViewControllers.count > 0 {
            UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                
                self.parkingLotShortSummaryController.view.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 360.0)
                
                }, completion: { (completed:Bool) -> Void in
                    
                    self.removeChildView(PKParkingLotOverViewTableViewController.classForCoder())
                    
            })
        }
        
    }
    
    func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        
        //fetchAllNearbyLocations()
        
    }
    
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        
        if self.childViewControllers.count == 0 {
            
            let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameParkingLot, bundle: nil)
            parkingLotShortSummaryController = storyboard.instantiateInitialViewController() as! PKParkingLotOverViewTableViewController
            parkingLotShortSummaryController.parkingLot = marker.userData as! PKParkingLot
            parkingLotShortSummaryController.searchFare = String(sliderView.value)
            view.addSubview(parkingLotShortSummaryController.view)
            addChildViewController(parkingLotShortSummaryController)
            parkingLotShortSummaryController.willMoveToParentViewController(self)
            parkingLotShortSummaryController.favoriteCompletion = { success in
                
                self.fetchAllNearbyLocations()
            }
            UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in

                self.parkingLotShortSummaryController.didMoveToParentViewController(self)
                
//               self.parkingLotShortSummaryController.view.translatesAutoresizingMaskIntoConstraints = false
//               self.parkingLotShortSummaryController.view.addLeftEdgeAttachConstraint(self.view)
//               self.parkingLotShortSummaryController.view.addRightEdgeAttachConstraint(self.view)
//               self.parkingLotShortSummaryController.view.addBottomEdgeAttachConstraint(self.view)
//               self.parkingLotShortSummaryController.view.addHeightConstraint(330)

                }, completion: { (completed:Bool) -> Void in
                    

            })
            
        }else {
            
            parkingLotShortSummaryController.parkingLot = marker.userData as! PKParkingLot
            parkingLotShortSummaryController.customizeSubViews()
        }
        
        return true
    }

    func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
        
        searchField.text = ""
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.fetchAllNearbyLocations()
        }
        
        return false
    }
    // MARK:- UISearchControllerDelegate
    
    func didDismissSearchController(searchingController: UISearchController) {
        searchController = nil
    }
}
