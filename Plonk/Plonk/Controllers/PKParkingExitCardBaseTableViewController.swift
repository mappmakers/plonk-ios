//
//  PKParkingExitCardBaseTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 09/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

enum PKParkingExitCardType:Int {
    case Cash, Paytm
}

class PKParkingExitCardBaseTableViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!

    var userParkingDetail:PKUserParkingDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if userParkingDetail.payment_mode == "cash" {
            
            imageView.image = UIImage(named:"bg_yellow_card")
            
        }else {
            
            imageView.image = UIImage(named:"bg_green_card")
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ParkingExitSegue" {
            let parkingExitController = segue.destinationViewController as! PKParkingExitCardTableViewController
            parkingExitController.userParkingDetail = userParkingDetail
            
        }
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
