//
//  PKSideMenuTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import ENSwiftSideMenu

private struct PKSideMenuConstants {
    
    static let CellSizeSideMenu:CGFloat = 50
    
    static let HeaderSubViewsSpacing:CGFloat = 20
    static let HeaderTitleTopOffset:CGFloat = 5
    static let HeaderLabelsHeight:CGFloat = 21
    static let HeaderViewHeight:CGFloat = 80
    
    static let CellTitleMyParkingHistory:String = "My Parking History"
//    static let CellTitleWallet:String = "Wallet"
    static let CellTitleMyParkingLots:String = "My Parking Lots"
    static let CellTitleFavourites:String = "Favourites"
    static let CellTitleShare:String = "Share"
    static let CellTitleHelp:String = "Help"
    static let CellTitleAbout:String = "About"
    static let CellTitleContactUs:String = "Contact Us"
    static let CellTitleSignout:String = "Sign Out"
}

class PKSideMenuTableViewController: UITableViewController {
    
    var selectedBackGroundView:UIView!
    
    var authenticationDelegate:PKAuthenticationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeForStaticTableStyle()
        
        self.tableView.backgroundColor = UIColor.whiteColor()
        self.tableView.contentInset = UIEdgeInsetsMake(PKConstants.AppNavigationBarOffset,0,0,0)
        self.tableView.contentSize = CGSizeMake(tableView.contentSize.width, tableView.contentSize.height+64)
        
        tableView.registerClass(PKLeftMenuTableViewCell.classForCoder(), forCellReuseIdentifier: PKConstants.CellBasicIdentifier)
        
        tableView.backgroundColor = UIColor.whiteColor()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadSideMenuDataSource", name: PKConstants.NotificationUserLoggedIn, object: nil)
        
        loadSideMenuDataSource()

    }
    
    func loadSideMenuDataSource() {
        
        if PKAppearance.sharedInstance().leftMenuItemsArray.count == 0 {
            
            let titlesArray = [PKSideMenuConstants.CellTitleMyParkingHistory,PKSideMenuConstants.CellTitleMyParkingLots,PKSideMenuConstants.CellTitleFavourites,PKSideMenuConstants.CellTitleShare,PKSideMenuConstants.CellTitleHelp,PKSideMenuConstants.CellTitleAbout,PKSideMenuConstants.CellTitleSignout]
            
            var iconsArray = ["ic_history","ic_parking","ic_favorite","ic_share","ic_help","ic_alert_gray","ic_signout"]
            
            for title in titlesArray {
                let index:Int! = titlesArray.indexOf(title)
                let iconName = iconsArray[index]

                let menuItem:PKLeftMenuItem = PKLeftMenuItem.leftItem(
                    [
                        PKLeftMenuProperties.PropertyTitle:title,
                        PKLeftMenuProperties.PropertyNormalIconName:iconName,
                        PKLeftMenuProperties.PropertySelectedIconName:iconName,
                        PKLeftMenuProperties.PropertyNormalTextColor:UIColor.darkGrayColor(),
                        PKLeftMenuProperties.PropertySelectedTextColor:UIColor.darkGrayColor(),
                        PKLeftMenuProperties.PropertyIsSelected:false
                    ])
                
                PKAppearance.sharedInstance().leftMenuItemsArray.append(menuItem)
            }
            
        }
        
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return PKSideMenuConstants.HeaderViewHeight
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectZero)
        headerView.backgroundColor = UIColor.appThemeColor()
        
        let headerTapGesture = UITapGestureRecognizer(target: self, action: "didPressProfile:")
        headerView.addGestureRecognizer(headerTapGesture)
        
        let titlelabel = UILabel(frame: CGRectZero)
        titlelabel.text = PKDataModel.sharedInstance.loggedUser.user_name
        titlelabel.numberOfLines = 2
        titlelabel.textColor = UIColor.whiteColor()
        titlelabel.font = UIFont.PKAllerBold(PKConstants.FontLargeSize)
        headerView.addSubview(titlelabel)
        
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        
        titlelabel.addLeftEdgeAttachConstraint(headerView, offset: PKSideMenuConstants.HeaderSubViewsSpacing)
//        titlelabel.addTopEdgeAttachConstraint(headerView, offset: PKSideMenuConstants.HeaderTitleTopOffset)
        titlelabel.addRightEdgeAttachConstraint(headerView, offset: PKSideMenuConstants.HeaderSubViewsSpacing)
        titlelabel.addHeightConstraint(21)
        
        let subtitleLabel = UILabel(frame: CGRectZero)
        subtitleLabel.text = PKDataModel.sharedInstance.loggedUser.user_email
        subtitleLabel.numberOfLines = 2
        subtitleLabel.textColor = UIColor.whiteColor()
        subtitleLabel.font = UIFont.PKAllerRegular(PKConstants.FontSmallSize)
        headerView.addSubview(subtitleLabel)
        
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        subtitleLabel.addLeftEdgeAttachConstraint(titlelabel)
        subtitleLabel.addRightEdgeAttachConstraint(titlelabel)
        subtitleLabel.addTopEdgeAttachConstraint(titlelabel, viewEdge: NSLayoutAttribute.Bottom, offset: 0)
        subtitleLabel.addBottomEdgeAttachConstraint(headerView, offset: PKSideMenuConstants.HeaderTitleTopOffset)
        subtitleLabel.addHeightConstraint(34)
        
        return headerView
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PKAppearance.sharedInstance().leftMenuItemsArray.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return PKSideMenuConstants.CellSizeSideMenu
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(PKConstants.CellBasicIdentifier, forIndexPath: indexPath) as! PKLeftMenuTableViewCell
        
        let leftMenuItem:PKLeftMenuItem = PKAppearance.sharedInstance().leftMenuItemsArray[indexPath.row]
        
        cell.leftMenuItem = leftMenuItem
        
        cell.textLabel?.font = UIFont.PKAllerRegular(PKConstants.FontLargeSize)
        cell.textLabel?.text = leftMenuItem.title
        cell.textLabel!.textAlignment = NSTextAlignment.Left
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel?.textColor = leftMenuItem.normalTextColor
        cell.imageView?.image = UIImage(named: leftMenuItem.normalIconName)
        cell.textLabel?.highlightedTextColor = leftMenuItem.selectedTextColor
        cell.imageView?.highlightedImage = UIImage(named: leftMenuItem.selectedIconName)
        cell.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        
        cell.selectionStyle = .None
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        switch PKAppearance.sharedInstance().leftMenuItemsArray[indexPath.row].title {
            
        case PKSideMenuConstants.CellTitleShare:
            
            let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameShare, bundle: nil)
            let shareViewController:PKShareViewController = storyboard.instantiateInitialViewController() as! PKShareViewController
            pushToNewDestination(shareViewController)
            break
            
        case PKSideMenuConstants.CellTitleMyParkingHistory:
            
            let myParkingHistory = PKParkingHistoryTableViewController()
            pushToNewDestination(myParkingHistory)
            
            break
     
        case PKSideMenuConstants.CellTitleMyParkingLots:
            
            //let myParkingVC = PKParkingLotsTableViewController()
            let parentParkingVC = PKMyParkingParentViewController()
            
            pushToNewDestination(parentParkingVC)
            break
            
        case PKSideMenuConstants.CellTitleSignout:
            
            authenticationDelegate?.controller(self, didLogoutUser: PKDataModel.sharedInstance.loggedUser)
            
            break
            
        case PKSideMenuConstants.CellTitleAbout:
            
            pushToNewDestination(PKAboutTableViewController(style:UITableViewStyle.Plain))
            
            break
        case PKSideMenuConstants.CellTitleFavourites:
            
            let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameMyParkingLot, bundle: nil)
            let favouritesViewController = storyboard.instantiateViewControllerWithIdentifier("favVC") as! PKFavouritesTableViewController
            
            pushToNewDestination(favouritesViewController)
            
            break
        case PKSideMenuConstants.CellTitleHelp:
            
            UIApplication.sharedApplication().openURL(NSURL(string: "http://goplonk.com")!)
            
            break
            
        default:
            break
        }
    }
    
    func didPressProfile(gesture:UITapGestureRecognizer) {
        let profileVC = UIStoryboard(name: PKConstants.StoryboardNameProfile, bundle: nil).instantiateInitialViewController() as! PKProfileTableViewController
        profileVC.updatedEmailCompletion = { updateEmail in
            
            self.tableView.reloadData()
            
        }
        pushToNewDestination(profileVC)
    }
    
    func pushToNewDestination(destination: UIViewController) {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        (appDelegate.window?.rootViewController as! UINavigationController).pushViewController(destination, animated: true)
        hideSideMenuView()

    }

}
