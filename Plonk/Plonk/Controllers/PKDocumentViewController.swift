//
//  PKDocumentViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 11/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import HPGrowingTextView
import MaterialKit

typealias AddDocumentCompletion = (description : String) -> Void

class PKDocumentViewController: UIViewController, HPGrowingTextViewDelegate {

    var imageObject : UIImage!
    
    var addDocumentCompletion:AddDocumentCompletion?
    
    var growingTextView:HPGrowingTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Add Document"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        view.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        let imageView = UIImageView(image: UIImage(named: "dotted_large"))
        
        view.addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.addTopEdgeAttachConstraint(view, offset: 20)
        imageView.addLeftEdgeAttachConstraint(view, offset: 20)
        imageView.addRightEdgeAttachConstraint(view, offset: 20)
        
        let imageContent = UIImageView(image: imageObject)
        imageContent.contentMode = .ScaleAspectFit
        view.addSubview(imageContent)
        
        imageContent.translatesAutoresizingMaskIntoConstraints = false
        imageContent.addTopEdgeAttachConstraint(imageView, offset: 2)
        imageContent.addLeftEdgeAttachConstraint(imageView, offset: 2)
        imageContent.addRightEdgeAttachConstraint(imageView, offset: 2)
        
//        imageView.addBottomEdgeAttachConstraint(view, offset: 158)
        
//        let containerView = UIView(frame: CGRectZero)
//        containerView.backgroundColor = UIColor.whiteColor()
        
        growingTextView = HPGrowingTextView(frame: CGRectMake(0, view.frame.height-202, UIScreen.mainScreen().bounds.size.width, 64))
        growingTextView.internalTextView.scrollEnabled = true
        growingTextView.isScrollable = false
        growingTextView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
        
        growingTextView.minNumberOfLines = 2
        growingTextView.maxNumberOfLines = 2
        // you can also set the maximum height in points with maxHeight
        growingTextView.maxHeight = 64
        growingTextView.returnKeyType = .Next
        growingTextView.font = UIFont.PKAllerRegular(12)
        growingTextView.delegate = self;
        growingTextView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
        growingTextView.backgroundColor = UIColor.whiteColor()
        growingTextView.placeholder = "Document Description"
        growingTextView.tintColor = UIColor.appThemeColor()
        growingTextView.animateHeightChange = false //turns off animation
        growingTextView.autoresizingMask = .FlexibleWidth
        
        view.addSubview(growingTextView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: "didTapView:")
        view.addGestureRecognizer(tapGesture)
        
        let addButton = MKButton(type: .Custom)
        addButton.backgroundColor = UIColor.appThemeColor()
        addButton.setTitle("Add", forState: .Normal)
        addButton.titleLabel?.font = UIFont.PKAllerRegular(14)
        addButton.addTarget(self, action: "addPressed:", forControlEvents: .TouchUpInside)
        view.addSubview(addButton)
        
        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.addBottomEdgeAttachConstraint(view)
        addButton.addHeightConstraint(44)
        addButton.addRightEdgeAttachConstraint(view)
        addButton.addLeftEdgeAttachConstraint(view)
        
        imageView.addBottomEdgeAttachConstraint(addButton, viewEdge: .Top, offset: 104)

        imageContent.addBottomEdgeAttachConstraint(imageView, offset: 2)

    }

    func didTapView(sender:UITapGestureRecognizer) {
        
        view.endEditing(true)
        
    }
    
    func addPressed(sender:UIButton) {
        
        if growingTextView.text.characters.count>0 {
            if addDocumentCompletion != nil {
                addDocumentCompletion!(description: growingTextView.text)
            }
        }else {
            self.handleError("Sorry", message: "Please provide some descriptions before adding this document")
        }
 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        
    }
    
    func keyboardWillShow(notification:NSNotification) {
        
        growingTextView.refreshHeight()

        // get keyboard size and loctaion
        let info:NSDictionary = notification.userInfo!
        var keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()

        let duration:NSNumber = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve:NSNumber = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        // Need to translate the bounds to account for rotation.
        keyboardSize = self.view.convertRect(keyboardSize, fromView: nil)

        // get a rect for the textView frame
        var containerFrame = growingTextView.frame
        containerFrame.origin.y = self.view.bounds.size.height - (keyboardSize.size.height + containerFrame.size.height)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(duration.doubleValue)
        UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: curve.integerValue)!)
        
        // set views with new info
        growingTextView.frame = containerFrame
        
        UIView.commitAnimations()
    }
    
    func keyboardWillHide(notification:NSNotification) {
        
        let info:NSDictionary = notification.userInfo!
        let duration:NSNumber = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve:NSNumber = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        var containerFrame = growingTextView.frame
//        containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height
        
        containerFrame.origin.y = view.frame.height-138
        containerFrame.size.height = 64
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(duration.doubleValue)
        UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: curve.integerValue)!)
        
        // set views with new info
        growingTextView.frame = containerFrame
        
        UIView.commitAnimations()
        
    }
    
    func growingTextView(growingTextView: HPGrowingTextView!, willChangeHeight height: Float) {
        let diff:CGFloat = growingTextView.frame.size.height - CGFloat(height)
        
        var rect:CGRect = growingTextView.frame;
        rect.size.height -= diff
        rect.origin.y += diff
        growingTextView.frame = rect
    }
}
