//
//  PKParkingExitTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import Cosmos

enum PKParkingExitLabelType:Int {
    case TimeValue, EnterTime, ExitTime, AmountValue
}

struct PKParkingExitConstants {
    static let TitleParkingExit = "Parking Exit"
}
class PKParkingExitTableViewController: UITableViewController {

    @IBOutlet var cellThanks: UITableViewCell!
    @IBOutlet var labels: [UILabel]!
    @IBOutlet var btnPassCard: UIButton!
    @IBOutlet var btnFavorite: UIButton!
    
    var userParkingDetail:PKUserParkingDetail!
    var parkingLotID:String?
    
    @IBOutlet var ratingView: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = PKParkingExitConstants.TitleParkingExit
        
        createDoneButton()
//        let backBtn = createBackButton()
//        backBtn.addTarget(self, action: "backPressed:", forControlEvents: .TouchUpInside)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        labels[PKParkingExitLabelType.EnterTime.rawValue].text = userParkingDetail.entry_time
        
        labels[PKParkingExitLabelType.ExitTime.rawValue].text = userParkingDetail.exit_time
        
        labels[PKParkingExitLabelType.AmountValue.rawValue].text = "₹ "+userParkingDetail.amount!
        
        labels[PKParkingExitLabelType.TimeValue.rawValue].text = userParkingDetail.total_duration
        
        btnPassCard.layer.cornerRadius = 5
        btnPassCard.layer.masksToBounds = true
        
        ratingView.didTouchCosmos = touchedTheStar
        
        passCard(btnPassCard)
    }

    func didTapDone(barButton:UIBarButtonItem) {

        navigationController?.popToRootViewControllerAnimated(true)
    }
    
//    func backPressed(sender:UIButton) {
//        navigationController?.popToRootViewControllerAnimated(true)
//    }
    
    private func touchedTheStar(rating: Double) {

        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"prkLot_id":parkingLotID ?? "","rating":"\(rating)"]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.RateParkingLot.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            if success == 1 {
                
//                let json = result as! Dictionary<String, String>
//                
//                if json["success"] == "true" {
//                    self.handleError("Thank You", message: "You have successfully rated this parking lot")
//                }else {
//                    self.handleError("Sorry", message: "This parking lot couldn't be rated at this time")
//                }
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func passCard(sender: AnyObject) {
        
        let cardController = self.storyboard!.instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingExitCard) as! PKParkingExitCardBaseTableViewController
        cardController.userParkingDetail = userParkingDetail
        navigationController?.presentViewController(cardController, animated: true, completion: { () -> Void in
            
        })
        
    }
    
    @IBAction func favoritePressed(sender: UIButton) {
        sender.selected = !sender.selected
        
        /*
        let parameters = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"prkLot_id":parkingLotID]
        var pathString:String
        if sender.selected {
            pathString = PKWebServiceEndPoints.RemoveFavorite.rawValue
        }else {
            pathString = PKWebServiceEndPoints.AddFavorite.rawValue
        }
        
        sender.selected = !sender.selected
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(pathString), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            if success != 1
            {
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
            }
            
            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.FavoriteList.rawValue), parameters: ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]) { (success:Int!, result:AnyObject?) -> Void in
                
                if success == 1
                {
                    PKParkingLot.MR_truncateAll()
                    
                    let json = result as! Dictionary<String,AnyObject>
                    let myFavoriteLotArray = json["fav_prkLots"] as! NSArray
                    
                    for parkingLotDict in myFavoriteLotArray {
                        
                        let aParkingLot = PKParkingLot.MR_createEntityInContext(NSManagedObjectContext.MR_defaultContext())
                        let favoriteId = (parkingLotDict as! Dictionary<String,AnyObject>)["favourite_id"]
                        aParkingLot.favourite_id = "\(favoriteId!)"
                        aParkingLot.updateParkingLotWithInfo(parkingLotDict as! Dictionary<String,AnyObject>)
                        
                    }
                    
                    
                }
                
                if self.favoriteCompletion != nil {
                    self.favoriteCompletion!(success: true)
                }
                
            }
        }*/
    }
}
