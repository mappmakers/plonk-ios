//
//  PKParkingLotDetailsTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

struct PKParkingLotDetailsConstants  {
    static let TitleParkingLotDetail = "Parking lot details"
}

class PKParkingLotDetailsTableViewController: UITableViewController {

    
    @IBOutlet weak var parkingImageView: PKRoundedImageView!
    @IBOutlet weak var parkingLotAddress: UILabel!
    @IBOutlet weak var parkingLotName: UILabel!
    @IBOutlet weak var parkingCapacity: UILabel!
    
    @IBOutlet weak var parkingAddressAndCity: UILabel!
    @IBOutlet weak var parkingType: UILabel!
    
    var parkingId:String!
    
    var parkingDetailsDic:Dictionary<String,AnyObject>!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        title = PKParkingLotDetailsConstants.TitleParkingLotDetail
        
        self.parkingLotName.text = parkingDetailsDic["parking_lot_name"] as? String ?? "NA"
        
        self.parkingLotAddress.text = parkingDetailsDic["parking_lot_address"] as? String ?? "NA"
        
        self.parkingCapacity.text = parkingDetailsDic["slots_total_count"] as? String ?? "NA"
        
        self.parkingType.text = parkingDetailsDic["parking_type"] as? String ?? "NA"
        
        let parkingCity = parkingDetailsDic["parking_city"] as? String ?? "NA"
        
        let fullAdress = "At \(self.parkingLotAddress.text!), \(parkingCity)"
        
        self.parkingAddressAndCity.text = fullAdress

        parkingImageView.loadImageFromURLString(PKAmazonRoute.ImagePath(parkingDetailsDic["image_name"] as? String ?? "").URLString, placeholderImage: nil) { (finished, error) -> Void in
            
            if error != nil {
                self.parkingImageView.image = UIImage(named: "ic_goplonk")
            }
            
        }
    }

   /* override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 4 {
    
        let headerView = UIView(frame: CGRectZero)
         view.addSubview(headerView)
            return headerView
            
        }
    }
 */
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            if DeviceType.IS_IPHONE_6 {
                return super.tableView(tableView, heightForRowAtIndexPath: indexPath)+54.0
            }else if DeviceType.IS_IPHONE_6P {
                return super.tableView(tableView, heightForRowAtIndexPath: indexPath)+64.0
                
            }
        }
         return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 4 {
            if DeviceType.IS_IPHONE_6 {
                return super.tableView(tableView, heightForHeaderInSection: section)+51.0
            }
            else if DeviceType.IS_IPHONE_6P {
                return super.tableView(tableView, heightForHeaderInSection: section)+90.0
            }
           
        }
        if section > 1 && DeviceType.IS_IPHONE_6P {
            return super.tableView(tableView, heightForHeaderInSection: section)+15.0
        }
        return super.tableView(tableView, heightForHeaderInSection: section)+5
    }

    @IBAction func rentHourlyAction(sender: AnyObject) {
       //rentHourlyVC
        let storyboard = UIStoryboard(name: PKConstants.StoryboardNameMyParkingLot, bundle: nil)
        let rentHourlyVC:PKRentHourlyTableViewController = storyboard.instantiateViewControllerWithIdentifier("rentHourlyVC") as! PKRentHourlyTableViewController
        rentHourlyVC.parkingIdRent = parkingId
        rentHourlyVC.parkingLotData = parkingDetailsDic
        self.navigationController?.pushViewController(rentHourlyVC, animated: true)
        
        
    }
    
    @IBAction func planDetaisAction(sender: AnyObject) {
        
        let planListVC:PKPlansListTableViewController=self.storyboard?.instantiateViewControllerWithIdentifier("planListVC") as! PKPlansListTableViewController
        planListVC.parkingLotId = parkingId
        self.navigationController?.pushViewController(planListVC, animated: true)
        
    }

}
