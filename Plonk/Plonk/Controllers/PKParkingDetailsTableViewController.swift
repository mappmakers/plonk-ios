//
//  PKParkingDetailsTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import Cosmos

typealias FavoriteCompletion = (success : Bool) -> Void

struct PKParkingDetailsConstants {
    
    static let TitleParkingDetail = "Parking lot details"
    
}
class PKParkingDetailsTableViewController: UITableViewController {

    @IBOutlet var imageViewParkingLot: PKRoundedImageView!
    @IBOutlet var labelsParkingDetail: [UILabel]!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var ratingView: CosmosView!
    @IBOutlet var btnFavorite: UIButton!
    @IBOutlet var labelRatin: UILabel!
    
    @IBOutlet var daysView: PKSegmentControl!
    
    var parkingLot:PKParkingLot!
    
    var favoriteCompletion:FavoriteCompletion?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = PKParkingDetailsConstants.TitleParkingDetail
        
        labelsParkingDetail[PKParkingLotOverViewType.Name.rawValue].text = parkingLot.parking_lot_name
        //labelsParkingDetail[PKParkingLotOverViewType.Status.rawValue].text = NSString(string: parkingLot.difference!).intValue<5 ? "Filling Fast": "Available"
        labelsParkingDetail[PKParkingLotOverViewType.Status.rawValue].text = parkingLot.parking_lot_address

//        labelsParkingDetail[PKParkingLotOverViewType.Status.rawValue].textColor = NSString(string: parkingLot.difference!).intValue<5 ? UIColor.redColor(): UIColor.darkGrayColor()
        labelsParkingDetail[PKParkingLotOverViewType.Price.rawValue].text = "₹ "+parkingLot.fare_range!+""
        labelsParkingDetail[PKParkingLotOverViewType.Timing.rawValue].text = parkingLot.time_range
        labelsParkingDetail[PKParkingLotOverViewType.ParkingType.rawValue].text = parkingLot.parking_type
        labelsParkingDetail[PKParkingLotOverViewType.Capacity.rawValue].text = parkingLot.difference
        
        btnFavorite.selected = parkingLot.favourite_id != nil
        
        let arrayDays = parkingLot.parking_days!.componentsSeparatedByString(",")
        for day in arrayDays {
            let dayButton = daysView.segmentButtons[NSString(string: day).intValue-1]
            dayButton.selected = true
            dayButton.customizeForSelected()
        }
        
        imageViewParkingLot.loadImageFromURLString(PKAmazonRoute.ImagePath(parkingLot.parking_lot_image ?? "").URLString, placeholderImage: nil) { (finished, error) -> Void in
            
            if error != nil {
                self.imageViewParkingLot.image = UIImage(named: "ic_goplonk")
            }
            
        }
        
        let camera = GMSCameraPosition.cameraWithLatitude(NSString(string: parkingLot.parking_lot_latitude!).doubleValue,
            longitude:NSString(string: parkingLot.parking_lot_longitude!).doubleValue, zoom:16)
        mapView.settings.compassButton = true;
        mapView.settings.myLocationButton = true
        mapView.camera = camera
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(NSString(string: parkingLot.parking_lot_latitude!).doubleValue, NSString(string: parkingLot.parking_lot_longitude!).doubleValue)
        marker.userData = parkingLot
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.icon = UIImage(named: "ic_plonk_small")
        marker.map = mapView
        
        ratingView.rating = NSString(string: parkingLot.rating ?? "0").doubleValue
        
        labelRatin.text = "Rating: \(parkingLot.rating ?? "0")/5"
        
        dispatch_async(dispatch_get_main_queue(), {
            self.mapView.myLocationEnabled = true
        });
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 6 {
            if DeviceType.IS_IPHONE_6P {
                return super.tableView(tableView, heightForRowAtIndexPath: indexPath)+65.0
            }
        }
        return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }
    
    @IBAction func favouritePressed(sender: UIButton) {
       
        let parameters = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"prkLot_id":parkingLot.parking_lot_id!]
        var pathString:String
        if sender.selected {
            pathString = PKWebServiceEndPoints.RemoveFavorite.rawValue
        }else {
            pathString = PKWebServiceEndPoints.AddFavorite.rawValue
        }
        
        sender.selected = !sender.selected
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(pathString), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            if success != 1
            {
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
            }
            
            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.FavoriteList.rawValue), parameters: ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]) { (success:Int!, result:AnyObject?) -> Void in
                
                if success == 1
                {
                    PKParkingLot.MR_truncateAll()
                    
                    let json = result as! Dictionary<String,AnyObject>
                    let myFavoriteLotArray = json["fav_prkLots"] as! NSArray
                    
                    for parkingLotDict in myFavoriteLotArray {
                        
                        let aParkingLot = PKParkingLot.MR_createEntityInContext(NSManagedObjectContext.MR_defaultContext())
                        let favoriteId = (parkingLotDict as! Dictionary<String,AnyObject>)["favourite_id"]
                        aParkingLot.favourite_id = "\(favoriteId!)"
                        aParkingLot.updateParkingLotWithInfo(parkingLotDict as! Dictionary<String,AnyObject>)
                        
                    }
                    
                    
                }
                
                if self.favoriteCompletion != nil {
                    self.favoriteCompletion!(success: true)
                }
                
            }
            
            /*
            else if pathString == PKWebServiceEndPoints.RemoveFavorite.rawValue {
                
                self.parkingLot.favourite_id = nil
                
                NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreWithCompletion { (success, error) -> Void in
                    
                    if (success) {
                        print("You successfully saved your context.");
                    } else if ((error) != nil) {
                        print("Error saving context: %@", error.description);
                    }
                    
                }
                
            }else {
                
                let favoriteLot = PKParkingLot.MR_findFirstByAttribute("parking_lot_id", withValue: self.parkingLot.parking_lot_id, inContext: NSManagedObjectContext.MR_defaultContext())
                
                if favoriteLot == nil {
                    NSManagedObjectContext.MR_defaultContext().insertObject(self.parkingLot)
                }else {
                    favoriteLot.favourite_id = "999"
                }
                
                NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreWithCompletion { (success, error) -> Void in
                    
                    if (success) {
                        print("You successfully saved your context.");
                    } else if ((error) != nil) {
                        print("Error saving context: %@", error.description);
                    }
                    
                }
                
            }
            */
            
        }
    }
    
    @IBAction func locationIconPressed(sender: UIButton) {
        
        self.gotoMapWithSearchLocation(parkingLot.parking_lot_landmark!, latitude: parkingLot.parking_lot_latitude!, longitude: parkingLot.parking_lot_longitude!)
        
    }
}
