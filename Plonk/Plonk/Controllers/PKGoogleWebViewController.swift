
//
//  PKGoogleWebViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 04/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKGoogleWebViewController: UIViewController, UIWebViewDelegate {

    var googleURL:NSURL!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let webView = UIWebView(frame: CGRectZero)
        webView.delegate = self
        webView.loadRequest(NSURLRequest(URL: googleURL))
        
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if request.URL!.absoluteString.hasPrefix("com.mappmakers.plonk:/oauth2callback") {
            
            GIDSignIn.sharedInstance().handleURL(request.URL!, sourceApplication: "com.apple.mobilesafari", annotation: nil)
            
            self.navigationController?.popViewControllerAnimated(true)
            
            return false
            
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
