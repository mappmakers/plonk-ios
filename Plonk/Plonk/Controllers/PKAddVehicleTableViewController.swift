//
//  PKAddVehicleTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 13/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

struct PKAddVehicleConstants {
    static let TitleAddVehicle = "Add Vehicle"
    static let PlaceholderCompanyName = "Company Name"
    static let PlaceholderVehicleModelName = "Vehicle Model Name"
    static let PlaceholderVehicleRegisterNumber = "Vehicle Register Number"
    static let PlaceholderVehicleColor = "Your Car's Color"
    static let TitleSave = "Save"
}

typealias AddVehicleCompletionCompletion = (success : Bool) -> Void

class PKAddVehicleTableViewController: UITableViewController, UIPickerViewDelegate {

    var addVehicleCompletion:AddVehicleCompletionCompletion?
    
    var arrayCells:Array<UITableViewCell>?
    
    var accessoryViewColorView:UIView?
    
    var vehicles:Array<PKUserVehicle>!
    
    private var vehicleRegCell:PKOTPInputTableViewCell?

    override func viewDidLoad() {
        super.viewDidLoad()

        customizeForStaticTableStyle()
        
        title = PKAddVehicleConstants.TitleAddVehicle
        
        let companyCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        companyCell.textField.placeholder = PKAddVehicleConstants.PlaceholderCompanyName
//        companyCell.textField.textColor = UIColor.appThemeColor()
        companyCell.textField.attachFieldWithPickerData(NSArray(array: vehicles).valueForKeyPath("@distinctUnionOfObjects.make_name") as! NSArray, selectedItem: nil)
        companyCell.backgroundColor = UIColor.whiteColor()
        companyCell.textField.floatingLabelTextColor = UIColor.appThemeColor()
        companyCell.textField.tintColor = UIColor.appThemeColor()
        companyCell.textField.textColor = UIColor.grayColor()
        companyCell.textField.delegate = self
        
        let vehicleModelCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        vehicleModelCell.textField.placeholder = PKAddVehicleConstants.PlaceholderVehicleModelName
        vehicleModelCell.textField.attachFieldWithPickerData(NSArray(array: vehicles).valueForKeyPath("@distinctUnionOfObjects.model_name") as! NSArray, selectedItem: nil)
//        vehicleModelCell.textField.textColor = UIColor.appThemeColor()
        vehicleModelCell.backgroundColor = UIColor.whiteColor()
        vehicleModelCell.textField.floatingLabelTextColor = UIColor.appThemeColor()
        vehicleModelCell.textField.userInteractionEnabled = false
        vehicleModelCell.textField.textColor = UIColor.grayColor()
        vehicleModelCell.textField.tintColor = UIColor.appThemeColor()
        
        let vehicleRegcell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        vehicleRegcell.textField.placeholder = PKAddVehicleConstants.PlaceholderVehicleRegisterNumber
        vehicleRegcell.textField.delegate = self
        vehicleRegcell.textField.returnKeyType = UIReturnKeyType.Next
//        vehicleRegcell.textField.textColor = UIColor.appThemeColor()
        vehicleRegcell.backgroundColor = UIColor.whiteColor()
        vehicleRegcell.textField.floatingLabelTextColor = UIColor.appThemeColor()
        vehicleRegcell.textField.textColor = UIColor.grayColor()
        
//        let vehicleColorCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
//        vehicleColorCell.textField.placeholder = PKAddVehicleConstants.PlaceholderVehicleColor
//        vehicleColorCell.textField.delegate = self
//        vehicleColorCell.textField.attachFieldWithPickerData(["Red","Blue","Green","Yellow"], selectedItem: nil)
//        vehicleColorCell.textField.picker.delegate = self
//        vehicleColorCell.textField.textColor = UIColor.appThemeColor()
//        vehicleColorCell.backgroundColor = UIColor.whiteColor()
//        vehicleColorCell.textField.floatingLabelTextColor = UIColor.appThemeColor()
        
        accessoryViewColorView = UIView(frame: CGRectMake(0, 0, 30, 30))
        accessoryViewColorView?.backgroundColor = UIColor.clearColor()
        accessoryViewColorView?.layer.cornerRadius = 15
        accessoryViewColorView?.layer.masksToBounds = true
//        vehicleColorCell.accessoryView = accessoryViewColorView
        
        let saveCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        saveCell.textLabel?.text = PKAddVehicleConstants.TitleSave
        saveCell.backgroundColor = UIColor.appThemeColor()
        
        arrayCells = [companyCell,vehicleModelCell, createVehicleRegNumberCell(),saveCell]
        
    }

    func createVehicleRegNumberCell() -> UITableViewCell {
        
        vehicleRegCell = PKOTPInputTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        vehicleRegCell!.textFieldFirst.delegate = self
        vehicleRegCell!.textFieldSecond.delegate = self
        vehicleRegCell!.textFieldThird.delegate = self
        vehicleRegCell!.textFieldForth.delegate = self
        
        vehicleRegCell!.textFieldFirst.textColor = UIColor.grayColor()
        vehicleRegCell!.textFieldSecond.textColor = UIColor.grayColor()
        vehicleRegCell!.textFieldThird.textColor = UIColor.grayColor()
        vehicleRegCell!.textFieldForth.textColor = UIColor.grayColor()
        
        vehicleRegCell!.textFieldFirst.tintColor = UIColor.appThemeColor()
        vehicleRegCell!.textFieldSecond.tintColor = UIColor.appThemeColor()
        vehicleRegCell!.textFieldThird.tintColor = UIColor.appThemeColor()
        vehicleRegCell!.textFieldForth.tintColor = UIColor.appThemeColor()
        
        vehicleRegCell!.textFieldFirst.addTarget(self, action: "textDidChange:", forControlEvents: .EditingChanged)
        vehicleRegCell!.textFieldSecond.addTarget(self, action: "textDidChange:", forControlEvents: .EditingChanged)
        vehicleRegCell!.textFieldThird.addTarget(self, action: "textDidChange:", forControlEvents: .EditingChanged)
        vehicleRegCell!.textFieldForth.addTarget(self, action: "textDidChange:", forControlEvents: .EditingChanged)
        
        vehicleRegCell!.textFieldFirst.placeholder = "KA"
        vehicleRegCell!.textFieldSecond.placeholder = "01"
        vehicleRegCell!.textFieldThird.placeholder = "AB"
        vehicleRegCell!.textFieldForth.placeholder = "1234"
        
        vehicleRegCell!.textFieldFirst.font = UIFont.PKAllerBold(12)
        vehicleRegCell!.textFieldSecond.font = UIFont.PKAllerBold(12)
        vehicleRegCell!.textFieldThird.font = UIFont.PKAllerBold(12)
        vehicleRegCell!.textFieldForth.font = UIFont.PKAllerBold(12)
        
        vehicleRegCell!.textFieldFirst.keyboardType = .Default
        vehicleRegCell!.textFieldThird.keyboardType = .Default
        
        vehicleRegCell!.textFieldFirst.autocapitalizationType = .AllCharacters
        vehicleRegCell!.textFieldThird.autocapitalizationType = .AllCharacters
        
        vehicleRegCell!.textFieldFirst.layer.borderWidth = 2.0
        vehicleRegCell!.textFieldSecond.layer.borderWidth = 2.0
        vehicleRegCell!.textFieldThird.layer.borderWidth = 2.0
        vehicleRegCell!.textFieldForth.layer.borderWidth = 2.0
        
        vehicleRegCell!.textFieldFirst.layer.borderColor = UIColor.appThemeColor().CGColor
        vehicleRegCell!.textFieldSecond.layer.borderColor = UIColor.appThemeColor().CGColor
        vehicleRegCell!.textFieldThird.layer.borderColor = UIColor.appThemeColor().CGColor
        vehicleRegCell!.textFieldForth.layer.borderColor = UIColor.appThemeColor().CGColor
        
        vehicleRegCell!.textFieldFirst.attachDoneToolbar()
        vehicleRegCell!.textFieldSecond.attachDoneToolbar()
        vehicleRegCell!.textFieldThird.attachDoneToolbar()
        vehicleRegCell!.textFieldForth.attachDoneToolbar()
        
//        if UIDevice.currentDevice().modelName != "iPhone 4" && UIDevice.currentDevice().modelName != "iPhone 4S" {
//            
//            vehicleRegCell!.textFieldFirst.becomeFirstResponder()
//            
//        }
        
        return vehicleRegCell!
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return arrayCells?.count ?? 0
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return arrayCells![indexPath.section]
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return PKConstants.CellTextFieldHeight-10
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 3 {
            
            if (arrayCells![0] as! PKTextFieldLoginTableViewCell).textField.text?.characters.count > 0 {
            
                if (arrayCells![1] as! PKTextFieldLoginTableViewCell).textField.text?.characters.count > 0 {
                    let models = NSArray(array: vehicles).filteredArrayUsingPredicate(NSPredicate(format: "%K == %@", argumentArray: ["model_name",(arrayCells![1] as! PKTextFieldLoginTableViewCell).textField.text ?? ""]))
                    
                    if models.count > 0 {
                        
                        let vehicle = models[0] as! PKUserVehicle
                        
                        do {
                            try addVehicle(vehicle)
                        } catch let error as PKAddVehicleDataError {
                            print(error.description)
                            self.handleError(PKConstants.TitleErrorHeader, message: error.description)
                        } catch {
                            self.handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
                        }
                        
                    }else {
                        self.handleError(PKConstants.TitleErrorHeader, message: "Please choose the model of your vehicle")
                    }
                }else {
                    self.handleError(PKConstants.TitleErrorHeader, message: "Please choose the model of your vehicle")
                }
                
            }else {
                self.handleError(PKConstants.TitleErrorHeader, message: "Please choose the vehicle company name")
            }

        }
    }

    func textFieldDidEndEditing(textField: UITextField) {
    
        if textField == (arrayCells![0] as! PKTextFieldLoginTableViewCell).textField {
            
            let models = NSArray(array: vehicles).filteredArrayUsingPredicate(NSPredicate(format: "%K == %@", argumentArray: ["make_name",textField.text!]))
            
            let modelTextField = (arrayCells![1] as! PKTextFieldLoginTableViewCell).textField
            if models.count > 0 {
                
                modelTextField.userInteractionEnabled = true
                modelTextField.attachFieldWithPickerData(NSArray(array: models).valueForKeyPath("@distinctUnionOfObjects.model_name") as! NSArray, selectedItem: nil)
                
            }else {
                modelTextField.userInteractionEnabled = false
            }
            
        }

    }
    
    func addVehicle(vehicle:PKUserVehicle) throws {
        
        let regNumCell = arrayCells![2] as! PKOTPInputTableViewCell
         let isValid = (regNumCell.textFieldFirst.text!.characters.count==2)&&(regNumCell.textFieldSecond.text!.characters.count>=1)&&(regNumCell.textFieldThird.text!.characters.count>=1)&&(regNumCell.textFieldForth.text!.characters.count>=1)
        
        guard vehicle.make_id?.characters.count > 0 else {throw PKAddVehicleDataError.NoMakes}
        guard vehicle.model_id?.characters.count > 0 else {throw PKAddVehicleDataError.NoModels}
        guard isValid == true else {throw PKAddVehicleDataError.NoRegId}
        
        let regNo = regNumCell.textFieldFirst.text!+"-"+regNumCell.textFieldSecond.text!+"-"+regNumCell.textFieldThird.text!+"-"+regNumCell.textFieldForth.text!

        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"vehicle_type":vehicle.vehicle_type!,"carmake_id":vehicle.make_id!,"carmodel_id":vehicle.model_id!,"vehicle_reg_no":regNo]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.AddUserVehicle.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            if success == 1 {
                
                self.vehicles = []
                
                let json = result as! Dictionary<String, AnyObject>
                
                if json["success"] as! String == "true" {
                    
                    if self.addVehicleCompletion != nil {
                        self.addVehicleCompletion!(success: true)
                    }
                    
                }
                
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
    }
    
    func textDidChange(textField:PKTextFieldBordered) {

        switch textField.tag {
        case 1:
            if textField.text!.characters.count == 2 {
                if textField.nextField != nil {
                    textField.nextField!.becomeFirstResponder()
                }
            }
            break
        case 2:
            if textField.text!.characters.count == 2 {
                if textField.nextField != nil {
                    textField.nextField!.becomeFirstResponder()
                }
            }
            break
        case 3:
            if textField.text!.characters.count == 2 {
                if textField.nextField != nil {
                    textField.nextField!.becomeFirstResponder()
                }
            }
            break
        case 4:
            if textField.text!.characters.count == 4 {
                textField.resignFirstResponder()
            }
            break
        default:
            break
        }
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
        
        switch textField.tag {
        case 1:
            if txtAfterUpdate.length > 2 {
                return false
            }
        case 2:
            if txtAfterUpdate.length > 2 {
                return false
            }
        case 3:
            if txtAfterUpdate.length > 2 {
                return false
            }
        case 4:
            if txtAfterUpdate.length > 4 {
                return false
            }
        default:
            break
        }
        
        return true
    }
    
    /*
    // MARK: UIPickerViewDataSource
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }
    
    
    // MARK: UIPickerViewDelegate
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        let myView = UIView(frame: CGRectMake(0, 0, pickerView.bounds.width - 30, 40))
        
        let myColorView = UIView(frame: CGRectMake(0, 5, 30, 30))
        myColorView.layer.cornerRadius = 15
        myColorView.layer.masksToBounds = true
        
        var rowString = String()
        switch row {
        case 0:
            rowString = "Red"
            myColorView.backgroundColor = UIColor.redColor()
        case 1:
            rowString = "Blue"
            myColorView.backgroundColor = UIColor.blueColor()
        case 2:
            rowString = "Green"
            myColorView.backgroundColor = UIColor.greenColor()
        case 3:
            rowString = "Yellow"
            myColorView.backgroundColor = UIColor.yellowColor()
        default:
            rowString = "Error: too many rows"
        }
        let myLabel = UILabel(frame: CGRectMake(60, 0, pickerView.bounds.width - 90, 40 ))
        myLabel.font = UIFont.PKAllerRegular(12)
        myLabel.text = rowString
        
        myView.addSubview(myLabel)
        myView.addSubview(myColorView)
        
        return myView
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        // do something with selected row
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if textField == (arrayCells![3] as! PKTextFieldLoginTableViewCell).textField {
            
            switch textField.text! {
            case "Red":
                textField.textColor = UIColor.redColor()
                accessoryViewColorView?.backgroundColor = UIColor.redColor()
            case "Blue":
                textField.textColor = UIColor.blueColor()
                accessoryViewColorView?.backgroundColor = UIColor.blueColor()
            case "Green":
                textField.textColor = UIColor.greenColor()
                accessoryViewColorView?.backgroundColor = UIColor.greenColor()
            case "Yellow":
                textField.textColor = UIColor.yellowColor()
                accessoryViewColorView?.backgroundColor = UIColor.yellowColor()
            default:
                 print("Error: too many rows")
            }
            
        }
        return true
    }
*/
}
