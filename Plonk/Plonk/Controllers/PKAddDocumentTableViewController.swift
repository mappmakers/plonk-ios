//
//  PKAddDocumentTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 11/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD
import AFNetworking

enum PKUpdateParkingDetailError: ErrorType {
    case NoBankName, NoAccNumber, NoIFSC, NoBranchName, NoPAN, NoServiceTax
}

extension PKUpdateParkingDetailError: CustomStringConvertible {
    var description: String {
        switch self {
        case NoBankName: return "Please enter the Bank name"
        case NoAccNumber: return "Please enter the Account Number"
        case NoIFSC: return "Please enter the IFSC Code"
        case NoBranchName: return "Please enter the branch name"
        case NoPAN: return "Please enter the PAN card number"
        case NoServiceTax: return "Please enter the Service Tax Number"
        }
    }
}


class PKAddDocumentTableViewController: UITableViewController {

    let textFieldArray = ["Bank name","A/c number","IFSC Code","Branch Name","PAN Card No.","Service Tax No."]
//    let bankNames = ["HDFC","Standard Chartered","SBI"]
    
    var parkingAddDelegate: PKParkingAddDelegate?
    
    var imageDesciptions : Array<String>?
    
    var dataArray:Array<UITableViewCell>?
    
    var images:Array<UIImage>?
    
    var selectedIndexPath : NSIndexPath?
    
    var imageDetailedDescription : Array<String>?
    var parkingLotDictionary:Dictionary<String,AnyObject>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataArray = []
        
        imageDesciptions = ["Khata Document","Latest Tax Paid Receipt","Schedule Of Property Document"]
        
        imageDetailedDescription = ["for owner/lessee","for owner/lessee","for lessee only"]
        
        let btnBack = createBackButton()
        btnBack.addTarget(self, action: "backPressed:", forControlEvents: .TouchDown)
        
        title = "Add more details"
        
        tableView.keyboardDismissMode = .Interactive
        
        for title in textFieldArray {
            
            let cell:PKTextFieldLoginTableViewCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
            cell.textField.placeholder = title
            cell.backgroundColor = UIColor.whiteColor()
            cell.textField.textColor = UIColor.grayColor()
            cell.textField.delegate=self
            cell.textField.floatingLabelTextColor = UIColor.appThemeColor()
            cell.textField.tintColor = UIColor.appThemeColor()
            
            if title == "A/c number" {
                cell.textField.keyboardType = .NumbersAndPunctuation
//                cell.textField.attachDoneToolbar()
            }
            
            dataArray?.append(cell)
            
        }
        
        if let imageArray = parkingLotDictionary!["image"] {
            
            images = imageArray as? [UIImage]
            
            imageDesciptions = (parkingLotDictionary!["doc_desc"] as! String).componentsSeparatedByString(",")
            
        }else {
            images = [UIImage(named: "dotted_small")!,UIImage(named: "dotted_small")!,UIImage(named: "dotted_small")!]
        }

        (dataArray![0] as! PKTextFieldLoginTableViewCell).textField.text = parkingLotDictionary!["user_bnk_name"] as? String
        (dataArray![1] as! PKTextFieldLoginTableViewCell).textField.text = parkingLotDictionary!["user_acc_no"] as? String
        (dataArray![2] as! PKTextFieldLoginTableViewCell).textField.text = parkingLotDictionary!["user_ifsc_code"] as? String
        (dataArray![3] as! PKTextFieldLoginTableViewCell).textField.text = parkingLotDictionary!["user_branch"] as? String
        (dataArray![4] as! PKTextFieldLoginTableViewCell).textField.text = parkingLotDictionary!["user_pan_no"] as? String
        (dataArray![5] as! PKTextFieldLoginTableViewCell).textField.text = parkingLotDictionary!["user_service_no"] as? String
        
    }

    func backPressed(sender:UIButton) {
        
        if parkingAddDelegate != nil {
            
            updateParkingDetailParameters()
            
            parkingAddDelegate?.parkingLotAddDetailController(self, didUpdateDictionaryValue: parkingLotDictionary)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        return 4
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 {
            return dataArray!.count
        }else if section == 1 {
            return images!.count
        }
        return 1
    }

    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 || section == 1 {
            
            let headerView = UIView(frame: CGRectZero)
            
            let labelHeader = UILabel(frame: CGRectZero)
            labelHeader.textColor = UIColor.darkGrayColor()
            labelHeader.font = UIFont.PKAllerRegular(14)
            
            if section == 0 {
                labelHeader.text = "Bank Details"
            }else {
                labelHeader.text = "Documents"
            }
            
            headerView.addSubview(labelHeader)
            
            labelHeader.translatesAutoresizingMaskIntoConstraints = false
            
            labelHeader.addTopEdgeAttachConstraint(headerView)
            labelHeader.addLeftEdgeAttachConstraint(headerView, offset:  15)
            labelHeader.addBottomEdgeAttachConstraint(headerView)
            
            let optionalLabel = UILabel(frame: CGRectZero)
            optionalLabel.textColor = UIColor.darkGrayColor()
            optionalLabel.font = UIFont.PKAllerRegular(12)
            optionalLabel.text = "*optional"
            optionalLabel.textAlignment = .Right
            headerView.addSubview(optionalLabel)
            
            optionalLabel.translatesAutoresizingMaskIntoConstraints = false
            optionalLabel.addLeftEdgeAttachConstraint(labelHeader, viewEdge: .Right, offset: 5)
            optionalLabel.addRightEdgeAttachConstraint(headerView, offset: 10)
            optionalLabel.addWidthConstraint(100)
            optionalLabel.addTopEdgeAttachConstraint(labelHeader)
            optionalLabel.addBottomEdgeAttachConstraint(labelHeader)
            
            return headerView
            
        }
        
        return UIView(frame: CGRectZero)
        
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 {
            return 40
        }
        
        return 0.0001
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 1 {
            
            return 80
        }
        
        return PKConstants.CellTextFieldHeight-10
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            return dataArray![indexPath.row]
        }else if indexPath.section == 1 {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier") as? PKAttachmentTableViewCell
            
            if cell == nil {
                cell = PKAttachmentTableViewCell(style: .Subtitle, reuseIdentifier: "reuseIdentifier")
            }
            
            cell!.accessoryCompletion = { (sender) -> Void in
                
                if indexPath.row>3 {
                    self.images?.removeAtIndex(indexPath.row)
                    self.imageDesciptions?.removeAtIndex(indexPath.row)
                }else {
                    self.images![indexPath.row] = UIImage(named: "dotted_small")!
                }
                
                self.tableView.reloadSections(NSIndexSet(index: indexPath.section), withRowAnimation: .None)
                
            }

            // Configure the cell...
            cell!.imageView?.image = images![indexPath.row]
            cell!.imageView?.contentMode = .ScaleAspectFill
            cell!.imageView?.clipsToBounds = true
            cell!.textLabel?.text = imageDesciptions![indexPath.row]
            
            if indexPath.row < 3 {
                cell!.detailTextLabel?.text = imageDetailedDescription![indexPath.row]
            }
            
            return cell!
            
        }else if indexPath.section == 2 {
            
            let moreDocCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
            moreDocCell.textLabel?.text = "Add more document"
            moreDocCell.backgroundColor = UIColor.appThemeColor()
            
            return moreDocCell
            
        }
        
        let finishCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        finishCell.textLabel?.text = "Finish"
        finishCell.backgroundColor = UIColor.appThemeColor()
        
        return finishCell
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        cell.imageView?.contentMode = .ScaleAspectFill
        cell.imageView?.clipsToBounds = true
        
        
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.view.endEditing(true)
        
        if indexPath.section == 1 {
            
            selectedIndexPath = indexPath
            
            btnImagePickerClicked(tableView.cellForRowAtIndexPath(indexPath)!)
            
        }else if indexPath.section == 2 {
            
            selectedIndexPath = indexPath
            
            btnImagePickerClicked(tableView.cellForRowAtIndexPath(indexPath)!)

        }else if indexPath.section == 3 {
            
            do {
                try updateParkingDocumentDetails()
            } catch let error as PKUpdateParkingDetailError {
                print(error.description)
                handleError(PKConstants.TitleErrorHeader, message: error.description)
            } catch {
                handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
            }
            
        }
        
    }
    
    func updateParkingDetailParameters() {
        
        let bankName = (dataArray![0] as! PKTextFieldLoginTableViewCell).textField.text ?? ""
        let accNum = (dataArray![1] as! PKTextFieldLoginTableViewCell).textField.text ?? ""
        let ifscCode = (dataArray![2] as! PKTextFieldLoginTableViewCell).textField.text ?? ""
        let branchName = (dataArray![3] as! PKTextFieldLoginTableViewCell).textField.text ?? ""
        let panNo = (dataArray![4] as! PKTextFieldLoginTableViewCell).textField.text ?? ""
        let serviceTaxNo = (dataArray![5] as! PKTextFieldLoginTableViewCell).textField.text ?? ""
        
        var status = "0"
        for image in images! {
            
            if image != UIImage(named: "dotted_small") {
                
                status = "1"
                
            }
            
        }
        
        let parameters =
        [
            "user_id":PKDataModel.sharedInstance.loggedUser.user_id!,
            "user_acc_no":accNum,
            "user_bnk_name":bankName,
            "user_branch":branchName,
            "user_ifsc_code":ifscCode,
            "user_pan_no":panNo,
            "user_service_no":serviceTaxNo,
            "docmnt_status":status,
            "doc_desc":imageDesciptions!.joinWithSeparator(","),
            "image":images!
        ]
        
        parkingLotDictionary?.update(parameters as! Dictionary<String, AnyObject>)
        
    }
    func updateParkingDocumentDetails() throws {
        
        let bankName = (dataArray![0] as! PKTextFieldLoginTableViewCell).textField.text
        let accNum = (dataArray![1] as! PKTextFieldLoginTableViewCell).textField.text
        let ifscCode = (dataArray![2] as! PKTextFieldLoginTableViewCell).textField.text
        let branchName = (dataArray![3] as! PKTextFieldLoginTableViewCell).textField.text
        let panNo = (dataArray![4] as! PKTextFieldLoginTableViewCell).textField.text
        let serviceTaxNo = (dataArray![5] as! PKTextFieldLoginTableViewCell).textField.text
        
//        guard bankName?.characters.count > 0 else {throw PKUpdateParkingDetailError.NoBankName}
//        guard accNum?.characters.count > 0 else {throw PKUpdateParkingDetailError.NoAccNumber}
//        guard ifscCode?.characters.count > 0 else {throw PKUpdateParkingDetailError.NoIFSC}
//        guard branchName?.characters.count > 0 else {throw PKUpdateParkingDetailError.NoBranchName}
//        guard micrCode?.characters.count > 0 else {throw PKUpdateParkingDetailError.NoMICR}
        
        var docDescrArray:[String] = []
        var selectedImages:[UIImage] = []
        for image in images! {
            if image != UIImage(named: "dotted_small") {
                let index = images!.indexOf(image)
                debugPrint(index)
                selectedImages.append(image)
                docDescrArray.append(imageDesciptions![index!])
            }
        }
        
        let parameters : Dictionary<String, AnyObject> =
        [
            "user_id":PKDataModel.sharedInstance.loggedUser.user_id!,
            "user_acc_no":accNum ?? "",
            "user_bnk_name":bankName ?? "",
            "user_branch":branchName ?? "",
            "user_ifsc_code":ifscCode ?? "",
            "user_pan_no":panNo ?? "",
            "user_service_no":serviceTaxNo ?? "",
            "docmnt_status":docDescrArray.count > 0 ? "1":"0",
            "doc_desc":docDescrArray.count > 0 ? docDescrArray.joinWithSeparator(","):"",
        ]
        
        parkingLotDictionary?.update(parameters)
        
        parkingLotDictionary?.updateValue(NSUserDefaults.standardUserDefaults().objectForKey(PKConstants.UserDefaultTokenValue)!, forKey: "secure_key")
        
        print(parkingLotDictionary)

        SVProgressHUD.showWithMaskType(.Gradient)
        let manager = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json","text/plain") as? Set<NSObject>
        let operation = manager.POST(PKWebServiceRoute.Path(PKWebServiceEndPoints.AddParkingLot.rawValue).URLString, parameters: parkingLotDictionary, constructingBodyWithBlock: { (formData: AFMultipartFormData!) -> Void in
            
            for image in selectedImages {
                let data = UIImageJPEGRepresentation(image, 0.5)
                let index = selectedImages.indexOf(image)
                
                formData.appendPartWithFileData(data!, name: "doc_upload_image\(index!+1)", fileName: "photo.jpg", mimeType: "image/jpeg")
            }
            
            
            }, success: { (operation, responseObject) -> Void in
                print(responseObject)
                
                SVProgressHUD.dismiss()

                if let responseJson = responseObject as? Dictionary<String, AnyObject> {
                    
                    if let errorMessage = responseJson["error"] as? String {
                        
                        if errorMessage == "Authentication failed" {
                            
                            self.handleError("Session Expired", message: "Your current ssession has been expired. You wil now be redirected to login page. Please login to continue.", handler: { (action) -> Void in
                                
                                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                
                                appDelegate.customizeForAuthenticationRequired()
                                
                            })
                            
                            return
                            
                        }
                        
//                        self.handleError("Sorry!", message: "Something went wrong\n\nError Log: \(responseObject)")
                        
                    }
                    
                    if let successMessage = responseJson["success"] as? String {
                    
                        if successMessage == "true" {
                            
                            let addParkingImagesController = PKParkingLotImagesTableViewController(style: .Grouped)
                            addParkingImagesController.parkingLotId = responseJson["Parking lot id"] as? String ?? ""
                            self.navigationController?.pushViewController(addParkingImagesController, animated: true)
                            
                        }

                    }

                }else {
                    
                    self.handleError("", message: "Your parking lot has been successfully added and will be reviewed as soon as possible.", handler: { (action) -> Void in
                        
                        for (_, value) in self.navigationController!.viewControllers.enumerate() {
                            
                            if value.classForCoder == PKMyParkingParentViewController.classForCoder() {
                                self.navigationController!.popToViewController(value, animated: true)
                            }
                            
                        }
                        
                    })
                    
                }

            }) { (operation, error) -> Void in
                print(error)
                SVProgressHUD.dismiss()
                self.handleError("Sorry!", message: "Something went wrong")
        }
        
        operation!.start()
        
        
        /*
        PKAddParkingLotManager.sharedInstance.addParkingLotSynchronously(parkingLotDictionary, imageArray: selectedImages) { (success, result) -> Void in
            
            if success == 1 {
                
                for (_, value) in self.navigationController!.viewControllers.enumerate() {
                    
                    if value.classForCoder == PKMyParkingParentViewController.classForCoder() {
                        self.navigationController!.popToViewController(value, animated: true)
                    }
                    
                }
                
            }else {
                
                //                let error:NSError = (result as! NSError)
                //                let title = String(error.code) + ": " + error.domain
                //                let message = error.localizedDescription
                self.handleError("Sorry!", message: "Something went wrong")
                
            }
            
        }
        */
        /*
        SVProgressHUD.showWithMaskType(.Gradient)
        
        PKAddParkingLotManager.sharedInstance.addParkingLot(parkingLotDictionary, imageArray: selectedImages) { (success, result) -> Void in
            

            if success == 1 {
                
                for (_, value) in self.navigationController!.viewControllers.enumerate() {
                    
                    if value.classForCoder == PKMyParkingParentViewController.classForCoder() {
                        self.navigationController!.popToViewController(value, animated: true)
                    }
                    
                }
                
            }else {
                
                //                let error:NSError = (result as! NSError)
                //                let title = String(error.code) + ": " + error.domain
                //                let message = error.localizedDescription
                self.handleError("Sorry!", message: "Something went wrong")
                
            }
            
        }
        */
        /*
        PKWebService.sharedInstance.uploadParkingDetails(PKWebServiceRoute.Path(PKWebServiceEndPoints.AddParkingLot.rawValue), parameters: parkingLotDictionary, imageArray: selectedImages, completionClosure: { (success, result) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                /*
                
                if (json["success"] == "true") {
                
                let parkingId = json["Parking lot id"]
                
                print(parkingId)
                
                for (_, value) in self.navigationController!.viewControllers.enumerate() {
                
                if value.classForCoder == PKMyParkingParentViewController.classForCoder() {
                self.navigationController!.popToViewController(value, animated: true)
                }
                
                }
                
                }
                
                */
                for (_, value) in self.navigationController!.viewControllers.enumerate() {
                   
                    if value.classForCoder == PKMyParkingParentViewController.classForCoder() {
                        self.navigationController!.popToViewController(value, animated: true)
                    }
                    
                }
                
            }else {
                
//                let error:NSError = (result as! NSError)
//                let title = String(error.code) + ": " + error.domain
//                let message = error.localizedDescription
                self.handleError("Sorry!", message: "Something went wrong")
                
            }
            
        })
        */
    }
    
    internal func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
        dismissViewControllerAnimated(true) { () -> Void in
            
            if self.selectedIndexPath?.section == 1 {
                
                self.images![self.selectedIndexPath!.row] = image
                
                self.tableView.reloadData()
                
            }else {

                let documentViewController = PKDocumentViewController()
                documentViewController.imageObject = image
                documentViewController.addDocumentCompletion = { (description) in  Void()
                
                    self.images!.append(image)
                    
                    self.imageDesciptions!.append(description)
                    
                    self.tableView.reloadData()
                    
                    self.navigationController?.popViewControllerAnimated(true)
                    
                }
                self.navigationController?.pushViewController(documentViewController, animated: true)
            }
            
            
            
        }
    }
}
