//
//  PKParkingMeterStartTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 07/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import Foundation
import MZTimerLabel
import SVProgressHUD

struct PKParkingMeterConstants {
    
    static let TitleParkingMeter = "Parking Meter"
}

enum PKParkingStopDataError: ErrorType {
    case NoEntryTime, NoExitTime, NoDuration, NoAmount
}

extension PKParkingStopDataError: CustomStringConvertible {
    var description: String {
        switch self {
        case NoEntryTime: return "Entry Time not found"
        case NoExitTime: return "Exit Time not found"
        case NoDuration: return "Total Duration not found"
        case NoAmount: return "Amount not found"
        }
    }
}


class PKParkingMeterStartTableViewController: UITableViewController, PGTransactionDelegate {

    @IBOutlet var labelSecond: MZTimerLabel!
    @IBOutlet var labelHour: MZTimerLabel!
    @IBOutlet var labelEnterTime: UILabel!
    @IBOutlet var labelCar: UILabel!
    @IBOutlet var labelUsername: UILabel!
    @IBOutlet var labelSlots: UILabel!
    @IBOutlet var textFieldSlot: UITextField!
    @IBOutlet var labelRate: UILabel!
    @IBOutlet var cellStop: PKCenteredLabelTableViewCell!
    @IBOutlet var labelParkingName: UILabel!

    @IBOutlet var labelMinimumDuration: UILabel!
    var parkingSlot:PKParkingSlot!
    var selectedVehicle:PKUserVehicle!
    var parkingId:String!
    
    var startDate:NSDate?
    
    private var qrController:PKQRCodeExitViewController?
    private var resultDic:[String:AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeForStaticTableStyle()
        
        let backBtn = createBackButton()
        backBtn.addTarget(self, action: "backPressed:", forControlEvents: .TouchUpInside)
        title = PKParkingMeterConstants.TitleParkingMeter
                
        cellStop.textLabel?.textColor = UIColor.redColor()
        
        let btnHelp = UIButton(type: .Custom)
        btnHelp.setImage(UIImage(named: "ic_question"), forState: .Normal)
        
        cellStop.contentView.addSubview(btnHelp)
        
        btnHelp.translatesAutoresizingMaskIntoConstraints = false
        btnHelp.addCenterYConstraint(cellStop.contentView)
        btnHelp.addRightEdgeAttachConstraint(cellStop.contentView, offset: 10)
        btnHelp.addWidthConstraint(25)
        btnHelp.addHeightConstraint(25)

        labelHour.timeFormat = "HH:mm"
        labelHour.shouldCountBeyondHHLimit = true

        let rangeOfSecond = NSMakeRange(1, 2)
        let attributesSecond = [NSForegroundColorAttributeName:UIColor.appThemeColor()]
        labelSecond.text = ":00"
        labelSecond.attributedDictionaryForTextInRange = attributesSecond
        labelSecond.textRange = rangeOfSecond
        labelSecond.timeFormat = "ss"
        
        labelSecond.start()
        labelHour.start()
        
        if startDate == nil {
            startDate = NSDate()
        }else {
            labelHour.addTimeCountedByTime(NSDate().timeIntervalSinceDate(startDate!))
            labelSecond.addTimeCountedByTime(NSDate().timeIntervalSinceDate(startDate!))
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm a 'on' dd MMM, yyyy"
        labelEnterTime.text = dateFormatter.stringFromDate(startDate!)
        
        labelCar.text = "\(selectedVehicle.make_name!) [\(selectedVehicle.registeration_no!)]"
//        labelUsername.text = parkingSlot.owner_name!
        labelSlots.text = parkingSlot.display_message!
        textFieldSlot.text = "₹ "+parkingSlot.extra_charge!+"/"+parkingSlot.increments!
        labelRate.text = "₹ \(parkingSlot.fare!)"
        
        labelMinimumDuration.text = "Min Duration: \(parkingSlot.min_duration!)"
        
//        labelParkingName.text = "\(parkingSlot.parking_lot_name!)"
    }

    func backPressed(sender:UIButton) {
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 4 {

            qrController = PKQRCodeExitViewController()
            qrController!.codeInputCompletion = { (inputCode, qrCodeStatus) in

                print(" entering code completed \(inputCode) \(qrCodeStatus)")
                self.exitQRParkingWithCode(inputCode,qrController: self.qrController!)
            }
            navigationController?.pushViewController(qrController!, animated: true)
            //stopParkingTimer()
        }
        
    }
    
    func exitQRParkingWithCode(qrCode:String,qrController:PKQRCodeExitViewController) {
        
        SVProgressHUD.showWithMaskType(.Gradient)

        let parameters:Dictionary<String, String> = ["parking_id":parkingId,"prkLot_id":parkingSlot.parking_lot_id!,"qr_code":qrCode];
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ExitQRParking.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            print("dismissing progress")
            
            SVProgressHUD.dismiss()
            
            if success == 1 {
                
                let json = result as! Dictionary<String, AnyObject>
                
                let resultDic = json["res"] as! Dictionary<String, AnyObject>
                if (resultDic["success"] as! String == "true") {

                    print("sending ntifi")
                    self.resultDic = resultDic
                    
                    if self.parkingSlot.parking_pymnt_mode == "cash" {
                        
                        self.completedTransaction()
                        
                    }else {
                        
                        let mc = PGMerchantConfiguration.defaultConfiguration()
                        
//                        mc.checksumGenerationURL = "https://pguat.paytm.com/paytmchecksum/paytmCheckSumGenerator.jsp" //https://pguat.paytm.com/oltp-web/processTransaction?orderid=%3COrder_ID
//                        mc.checksumValidationURL = "https://pguat.paytm.com/paytmchecksum/paytmCheckSumVerify.jsp"
                        
                        mc.checksumGenerationURL = "http://54.169.255.87/Paytm/generateChecksum.php"
                        mc.checksumValidationURL = "http://54.169.255.87/Paytm/verifyChecksum.php"
                        
//                        mc.checksumValidationURL = "http://54.169.255.87/Paytm/verifyChecksum_new.php"
                        
                        //Step 3: Create the order with whatever params you want to add. But make sure that you include the merchant mandatory params
                        var orderDict:[String:AnyObject] = [String:AnyObject]()
                        //Merchant configuration in the order object
                        orderDict["MID"] = "MappMa76585565859815"
                        orderDict["CHANNEL_ID"] = "WAP"
                        orderDict["INDUSTRY_TYPE_ID"] = "Retail"
                        orderDict["WEBSITE"] = "Mappwap"
                        orderDict["EMAIL"] = "farhanyousuf.official@gmail.com";
                        orderDict["MOBILE_NO"] = "9746976499";
                        orderDict["THEME"] = "merchant";
                        //Order configuration in the order object
                        orderDict["TXN_AMOUNT"] = "1"
                        orderDict["ORDER_ID"] = "\(resultDic["transaction_id"] ?? PKParkingMeterStartTableViewController.generateOrderIDWithPrefix(""))"
                        orderDict["REQUEST_TYPE"] = "DEFAULT"
                        orderDict["CUST_ID"] = "1234567890"
                        
                        let order = PGOrder(params: orderDict)
                        
                        PGServerEnvironment.selectServerDialog(self.view, completionHandler: { (type) -> Void in
                            
                            let txnController:PGTransactionViewController = PGTransactionViewController(transactionForOrder: order)
                            if (type != eServerTypeNone) {
                                
                                txnController.serverType = type
                                txnController.merchant = mc
                                txnController.delegate = self
                                
                                self.showViewController(txnController)
                            }
                        })
                        
                    }
                    
                }else {
                    
                    qrController.startScanningSession()
                    
                    self.handleError(PKConstants.TitleErrorHeader, message: "Please check the Exit QRCode you submitted")
                }
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
    }
    
    class func generateOrderIDWithPrefix(prefix:String) -> String {
        let diceRoll = Int(arc4random_uniform(UInt32(6)))
        return "\(prefix)\(diceRoll)"
    }
    
    private func showViewController(controller:PGTransactionViewController) {
        
        if navigationController != nil {
            
            navigationController!.pushViewController(controller, animated: true)
            
        }else {
            presentViewController(controller, animated: true, completion: nil)
        }
    
    }
    
    private func removeController(controller:PGTransactionViewController) {
        
        if navigationController != nil {
            
            navigationController!.popViewControllerAnimated(false)
            
        }else {
            dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    
    // MARK:- PGTransactionDelegate
    
    func didSucceedTransaction(controller: PGTransactionViewController!, response: [NSObject : AnyObject]!) {
        
        print("ViewController::didSucceedTransactionresponse= \(response)")
        
        removeController(controller)
        
        completedTransaction()
    }
    
    func didFailTransaction(controller: PGTransactionViewController!, error: NSError!, response: [NSObject : AnyObject]!) {
        print("ViewController::didFailTransaction error = \(error) response= \(response)")
        
        if response != nil
        {
            handleError(error.localizedDescription, message: response.description)
        }
        else if error != nil
        {
            handleError("Error", message: error.localizedDescription)
        }
        
        removeController(controller)
        
        self.parkingSlot.parking_pymnt_mode = "cash"
        
        completedTransaction()
    }
    
    func didCancelTransaction(controller: PGTransactionViewController!, error: NSError!, response: [NSObject : AnyObject]!) {
        print("ViewController::didCancelTransaction error = \(error) response= \(response)")
        
        var msg:String?
        
        if error == nil {
            msg = "Successful"
        }else {
            msg = "UnSuccessful"
        }
        
        handleError("Transaction Cancelled", message: msg!)

        removeController(controller)

        self.parkingSlot.parking_pymnt_mode = "cash"
        
        completedTransaction()
    }
    
    func didFinishCASTransaction(controller: PGTransactionViewController!, response: [NSObject : AnyObject]!) {
        print("ViewController::didFinishCASTransaction:response = \(response)")
    }
    
    /*
    func stopParkingTimer() {
    
        SVProgressHUD.showWithMaskType(.Gradient)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
        let parameters:Dictionary<String, String> = ["parking_id":parkingId,"end_time":dateFormatter.stringFromDate(NSDate())]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ParkingStop.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                let json = result as! Dictionary<String, AnyObject>
                
                let resultDic = json["res"] as! Dictionary<String, String>
                if (resultDic["success"] == "true") {

                    NSNotificationCenter.defaultCenter().postNotificationName(PKConstants.NotificationParkingMeterStopped, object: nil)
                    
                    do {
                        try self.pushToExitCard(resultDic)
                    } catch let error as PKConfirmParkingError {
                        print(error.description)
                        self.handleError(PKConstants.TitleErrorHeader, message: error.description)
                    } catch {
                        self.handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
                    }
                    
                }
   
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
    }
    */
    
    func completedTransaction() {
        
        NSNotificationCenter.defaultCenter().postNotificationName(PKConstants.NotificationParkingMeterStopped, object: nil)
        
        do {
            try self.pushToExitCard(resultDic!,qrController: qrController!)
        } catch let error as PKConfirmParkingError {
            print(error.description)
            self.handleError(PKConstants.TitleErrorHeader, message: error.description)
        } catch {
            self.handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
        }
        
    }
    
    func pushToExitCard(resultDic:Dictionary<String, AnyObject>, qrController:PKQRCodeExitViewController) throws {
        
        guard resultDic["entry_time"] != nil else { throw PKParkingStopDataError.NoEntryTime }
        guard resultDic["exit_time"] != nil else { throw PKParkingStopDataError.NoExitTime }
        guard resultDic["total_duration"] != nil else { throw PKParkingStopDataError.NoDuration }
        guard resultDic["amount"] != nil else { throw PKParkingStopDataError.NoAmount }
        
        self.labelSecond.pause()
        self.labelHour.pause()
        
        print("stopping scanner")
        
        qrController.scanner.stopScanning()
        
        print("popping ALL")
        
        self.navigationController?.popToViewController(self, animated: false)
        
        let parkingExitViewController = self.storyboard!.instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingExit) as! PKParkingExitTableViewController
        
        let userParkingDetail = PKUserParkingDetail.MR_createEntityInContext(nil)
        var userParkingDictionary = resultDic
        userParkingDictionary["make_name"] = selectedVehicle.make_name
        userParkingDictionary["model_name"] = selectedVehicle.model_name
        userParkingDictionary["registeration_no"] = selectedVehicle.registeration_no
        userParkingDictionary["user_name"] = PKDataModel.sharedInstance.loggedUser.user_name
        userParkingDictionary["user_parking_id"] = parkingId
        userParkingDictionary["slot_name"] = self.parkingSlot.slot_name
        userParkingDictionary["payment_mode"] = self.parkingSlot.parking_pymnt_mode
        userParkingDictionary["parking_lot_name"] = self.parkingSlot.parking_lot_name
        
        userParkingDetail.updateUserParkingWithDictionary(userParkingDictionary)

        parkingExitViewController.userParkingDetail = userParkingDetail
        parkingExitViewController.parkingLotID = self.parkingSlot.parking_lot_id
        parkingExitViewController.navigationItem.hidesBackButton = true
        
         print("pushing new")
        self.navigationController?.pushViewController(parkingExitViewController, animated: true)
        
    }
    
}
