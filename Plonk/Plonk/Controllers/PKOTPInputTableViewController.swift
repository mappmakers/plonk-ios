//
//  PKOTPInputTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 16/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

private struct PKOTPNumberInputConstants {
    
    static let TitleHeaderOTP:String = "Enter verification code"
    static let TitleFooterFirstOTP:String = "Hang On! This may take a minute\nDidn't get it? "
    static let TitleFooterSecondOTP:String = "Tap here to resend"
}

class PKOTPInputTableViewController: PKNumberInputTableViewController {

    private var otpCell:PKOTPInputTableViewCell?
    
    var mobileNumber : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let headerCell = self.createHeaderCell()
        headerCell.textLabel?.text = PKOTPNumberInputConstants.TitleHeaderOTP
        headerCell.textLabel?.font = UIFont.PKAllerLight(18)
        
        let footerCell = self.createFooterCell()
        footerCell.customizeForAttributedTextWith(PKOTPNumberInputConstants.TitleFooterFirstOTP, secondText: PKOTPNumberInputConstants.TitleFooterSecondOTP)
        
        arrayCells = [headerCell, createOTPCell(), footerCell]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func createOTPCell() -> UITableViewCell {
        
        otpCell = PKOTPInputTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
        otpCell!.textFieldFirst.delegate = self
        otpCell!.textFieldSecond.delegate = self
        otpCell!.textFieldThird.delegate = self
        otpCell!.textFieldForth.delegate = self
        
        otpCell!.textFieldFirst.addTarget(self, action: "textDidChange:", forControlEvents: .EditingChanged)
        otpCell!.textFieldSecond.addTarget(self, action: "textDidChange:", forControlEvents: .EditingChanged)
        otpCell!.textFieldThird.addTarget(self, action: "textDidChange:", forControlEvents: .EditingChanged)
        otpCell!.textFieldForth.addTarget(self, action: "textDidChange:", forControlEvents: .EditingChanged)
        
        otpCell!.textFieldFirst.inputAccessoryView = accessoryViewContinue
        otpCell!.textFieldSecond.inputAccessoryView = accessoryViewContinue
        otpCell!.textFieldThird.inputAccessoryView = accessoryViewContinue
        otpCell!.textFieldForth.inputAccessoryView = accessoryViewContinue
        
        if UIDevice.currentDevice().modelName != "iPhone 4" && UIDevice.currentDevice().modelName != "iPhone 4S" {
            
            otpCell!.textFieldFirst.becomeFirstResponder()
            
        }
        
        return otpCell!
        
    }

    func continuePressed(sender:UIButton) {
        view.endEditing(true)
        
        let isValid = (otpCell!.textFieldFirst.text!.characters.count==1)&&(otpCell!.textFieldSecond.text!.characters.count==1)&&(otpCell!.textFieldThird.text!.characters.count==1)&&(otpCell!.textFieldForth.text!.characters.count==1)
        
        if isValid {
            
//            let parameters:Dictionary<String, AnyObject> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"user_otp":PKDataModel.sharedInstance.loggedUser.otp_pasword!,"user_mobile":PKDataModel.sharedInstance.loggedUser.user_mobileno!]
            let parameters:Dictionary<String, AnyObject> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"user_otp":otpCell!.textFieldFirst.text!+otpCell!.textFieldSecond.text!+otpCell!.textFieldThird.text!+otpCell!.textFieldForth.text!,"user_mobile":self.mobileNumber]
            
            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserVerifyOTP.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
                
                switch success {
                case 0:
                    let error:NSError = (result as! NSError)
                    let title = String(error.code) + ": " + error.domain
                    let message = error.localizedDescription
                    self.handleError(title, message: message)
                    break
                case 1:
                    
                    let json = result as! Dictionary<String, AnyObject>
                    
                    if json["success"] as! String == "true" {
                        
                        PKDataModel.sharedInstance.loggedUser.otp_verified = "1"
                        
                        PKDataModel.sharedInstance.loggedUser.user_mobileno = self.mobileNumber

                        self.authenticationDelegate?.authenticationController(self, didSuccessfullyAuthenticateUser: PKDataModel.sharedInstance.loggedUser)
                        
                    }else {
                        
                        let title = "Sorry"
                        let message = "The one time password you entered is incorrect"
                        
                        self.otpCell!.textFieldFirst.text = ""
                        self.otpCell!.textFieldSecond.text = ""
                        self.otpCell!.textFieldThird.text = ""
                        self.otpCell!.textFieldForth.text = ""
                        
                        self.handleError(title, message: message)
                        
                    }
                    
                    break
                default:break
                }
                
            }
        }else {
            
            self.handleError(PKConstants.TitleErrorHeader, message: "Please enter in all the fields")
        }
        
        
    }
    
    func textDidChange(textField:PKTextFieldBordered) {
        if textField.text!.characters.count == 1 {
            if textField.nextField != nil {
                textField.nextField!.becomeFirstResponder()
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
        if txtAfterUpdate.length > 1 {
            return false
        }
        return true
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        view.endEditing(true)
        
        if indexPath.row == 2 {
            
            handleError("", message: "You will shortly receive a new One Time Password once you dismiss this alert. Please note that all previously sent One Time Passwords will be invalidated.", handler: { (action) -> Void in
                self.resendPressed()
            })
            
        }
    }
    
    func resendPressed() {
        
        let parameters:Dictionary<String, AnyObject> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"user_mobile":mobileNumber]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.UserRegisterMobile.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            switch success {
            case 0:
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                break
            case 1:
                let json = result as! Dictionary<String, AnyObject>
                
                /*
                let title = ""
                let message = ""
                self.handleError(title, message: message)
                */
                if (json["success"] as! String) == "true" {
                    PKDataModel.sharedInstance.loggedUser.otp_pasword = json["user_otp"] as? String
                    
                }else {
                    self.handleError(PKConstants.TitleErrorHeader, message: "Mobile Number already exists")
                }
                
                
                break
                
            default:break
            }
            
        }
        
    }
}
