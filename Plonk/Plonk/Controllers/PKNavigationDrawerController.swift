//
//  PKNavigationDrawerController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 16/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import ENSwiftSideMenu

class PKNavigationDrawerController: ENSideMenuNavigationController, ENSideMenuDelegate  {
 
    var overlayView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.hideBottomHairline()
        
        self.navigationBar.translucent = false
        
        overlayView = UIView(frame: self.view.frame) as UIView
        overlayView!.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        
        // Add left swipe gesture recognizer
//        let leftSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleOverlayGesture:")
//        leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirection.Left
//        overlayView!.addGestureRecognizer(leftSwipeGestureRecognizer)
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleOverlayTapped:")
        tapGesture.numberOfTapsRequired = 1
        overlayView!.addGestureRecognizer(tapGesture)
        
    }

    override init(menuViewController: UIViewController, contentViewController: UIViewController?) {
        
        super.init(menuViewController: menuViewController, contentViewController: contentViewController)
        
        self.sideMenu?.delegate = self
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

    }
    
//    func handleOverlayGesture(gesture: UISwipeGestureRecognizer) {
//        
//        self.toggleSideMenuView()
//        
//    }
    
    func handleOverlayTapped(gesture: UITapGestureRecognizer) {
        
        sideMenuController()?.sideMenu?.hideSideMenu()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sideMenuWillOpen() {
     
        self.topViewController!.view.addSubview(overlayView!)
        self.topViewController!.view.bringSubviewToFront(overlayView!)
        
    }
    func sideMenuWillClose() {
        overlayView!.removeFromSuperview()
    }
    
}