//
//  PKFavouritesTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class PKFavouritesTableViewController: UITableViewController, NSFetchedResultsControllerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, GMSMapViewDelegate {

    var myFavoriteParkingFRC:NSFetchedResultsController?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Favourites"

        tableView.separatorStyle = .None

        myFavoriteParkingFRC = PKParkingLot.MR_fetchAllSortedBy("parking_lot_name", ascending: true, withPredicate: nil, groupBy: nil, delegate: self, inContext: NSManagedObjectContext.MR_defaultContext())

        myFavoriteParkingLotList()
    }

    func myFavoriteParkingLotList()
    {
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.FavoriteList.rawValue), parameters: ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]) { (success:Int!, result:AnyObject?) -> Void in
            
            if success == 1
            {
                PKParkingLot.MR_truncateAll()
                
                let json = result as! Dictionary<String,AnyObject>
                let myFavoriteLotArray = json["fav_prkLots"] as! NSArray
                
                if myFavoriteLotArray.count > 0
                {
                    for parkingLotDict in myFavoriteLotArray {
                        
                        let aParkingLot = PKParkingLot.MR_createEntityInContext(NSManagedObjectContext.MR_defaultContext())
                        let favoriteId = (parkingLotDict as! Dictionary<String,AnyObject>)["favourite_id"]
                        aParkingLot.favourite_id = "\(favoriteId!)"
                        aParkingLot.updateParkingLotWithInfo(parkingLotDict as! Dictionary<String,AnyObject>)
                        
                    }
                    
                }else {
                    self.tableView.emptyDataSetSource = self
                    self.tableView.emptyDataSetDelegate = self
                    
                    self.tableView.reloadData()
                }
            }
            else
            {
                
                self.tableView.emptyDataSetSource = self
                self.tableView.emptyDataSetDelegate = self
                
                self.tableView.reloadData()
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
            }
            
            
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return myFavoriteParkingFRC?.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        let sectionInfo = myFavoriteParkingFRC!.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func configureCell(cell:PKFavouritesTableViewCell, atIndexPath indexPath:NSIndexPath) {
        
        let parkingLot = myFavoriteParkingFRC!.objectAtIndexPath(indexPath) as! PKParkingLot
        
        let camera = GMSCameraPosition.cameraWithLatitude(NSString(string: parkingLot.parking_lot_latitude!).doubleValue,
            longitude:NSString(string: parkingLot.parking_lot_longitude!).doubleValue, zoom:16)
//        cell.gmsMapView.settings.compassButton = true;
//        cell.gmsMapView.settings.myLocationButton = true
        cell.gmsMapView.camera = camera
        cell.gmsMapView.delegate = self
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(NSString(string: parkingLot.parking_lot_latitude!).doubleValue, NSString(string: parkingLot.parking_lot_longitude!).doubleValue)
        marker.userData = parkingLot
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.icon = UIImage(named: "ic_plonk_small")
        marker.map = cell.gmsMapView
        
        cell.labelSubtiteInfo.text = parkingLot.parking_lot_address
        
        /*
        let availableSlots = NSString(string: parkingLot.difference!).floatValue
        let totalSlots = NSString(string: parkingLot.parking_total_slots!).floatValue
        let percentage = (availableSlots/totalSlots)*100
        
        if percentage == 0 {
            
            cell.labelSubtiteInfo.text = "Unavailable"
            cell.labelSubtiteInfo.textColor = UIColor.redColor()
            
        }else {
            cell.labelSubtiteInfo.text = percentage<50 ? "Filling Fast": "Available"
            cell.labelSubtiteInfo.textColor = NSString(string: parkingLot.difference!).intValue<5 ? UIColor.redColor(): UIColor.darkGrayColor()
        }
        */

        cell.labelParkingName.text = parkingLot.parking_lot_name
        
        cell.favoriteCompletion = { (sender) -> Void in
        
            self.favoritePressed(sender, parkingLot: parkingLot)
            
        }
        
        cell.mapNavigationCompletion = { (sender) -> Void in
            self.gotoMapWithSearchLocation(parkingLot.parking_lot_landmark!, latitude: parkingLot.parking_lot_latitude!, longitude: parkingLot.parking_lot_longitude!)
        }
    }
    
    // MARK: - NSFetchedResultsControllerDelegate
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        
        
        let tableView:UITableView = self.tableView
        
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            break
            
        case .Update:
            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!) as! PKFavouritesTableViewCell, atIndexPath: indexPath!)
            break
        default:
            break
        }
    }
    
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
            
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            break
        default:break
        }
        
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       // var cell = tableView.dequeueReusableCellWithIdentifier("favoriteCell") as? PKFavouritesTableViewCell
        
        let cell = tableView.dequeueReusableCellWithIdentifier(PKConstants.CellFavoritesIdentifier, forIndexPath: indexPath) as! PKFavouritesTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None

        configureCell(cell, atIndexPath: indexPath)
        
        return cell
    }
    

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameParkingLot, bundle: nil)
        let parkingDetailsVC = storyboard.instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingDetail) as! PKParkingDetailsTableViewController
        parkingDetailsVC.parkingLot = myFavoriteParkingFRC?.objectAtIndexPath(indexPath) as! PKParkingLot
        self.navigationController?.pushViewController(parkingDetailsVC, animated: true)
        
    }
    
    func favoritePressed(sender: UIButton, parkingLot:PKParkingLot) {
        
        let parameters = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"prkLot_id":parkingLot.parking_lot_id!]
        var pathString:String
        if sender.selected {
            pathString = PKWebServiceEndPoints.RemoveFavorite.rawValue
        }else {
            pathString = PKWebServiceEndPoints.AddFavorite.rawValue
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            // time-consuming task
            dispatch_async(dispatch_get_main_queue(), {
                sender.selected = !sender.selected
            })
        })

        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(pathString), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            if success != 1
            {
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
                
            }
            else
            {
                let json = result as! Dictionary<String,AnyObject>
                if json["success"] as! String == "true" {
                    
                    self.myFavoriteParkingLotList()
                    
                }
                
                NSNotificationCenter.defaultCenter().postNotificationName(PKConstants.NotificationUserLotAdded, object: nil)
            }
            
        }
        
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        
       // return UIImage(named: "ic_broken_heart")
        return UIImage(named: "ic_sad")
        
        
        
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let emptyTitle:String = ""
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        let emptyTitle:String = "You dont have any favourite parking lots as yet"
        let attributedEmptyTitle:NSAttributedString = NSAttributedString(string: emptyTitle, attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        return attributedEmptyTitle
        
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView!) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    // MARK:- GMSMapViewDelegate
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        
        let storyboard:UIStoryboard = UIStoryboard(name: PKConstants.StoryboardNameParkingLot, bundle: nil)
        let parkingDetailController:PKParkingDetailsTableViewController = storyboard.instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingDetail) as! PKParkingDetailsTableViewController
        parkingDetailController.parkingLot = marker.userData! as! PKParkingLot
        navigationController?.pushViewController(parkingDetailController, animated: true)
        
        return true
    }
}
