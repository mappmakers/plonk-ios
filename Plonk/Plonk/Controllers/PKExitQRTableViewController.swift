//
//  PKExitQRTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 28/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKExitQRTableViewController: PKQRTextInputTableViewController {
    
    private var lastSelectedIndexPath:NSIndexPath?
    override func viewDidLoad() {
        super.viewDidLoad()

        let cellOption1 = PKBasicTableViewCell(style: .Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cellOption1.textLabel?.text = "QRCode damaged"
        
        let cellOption2 = PKBasicTableViewCell(style: .Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cellOption2.textLabel?.text = "Low light to scan"
        
        let cellOption3 = PKBasicTableViewCell(style: .Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cellOption3.textLabel?.text = "QRCode not found"
        
        arrayCells?.append(cellOption1)
        
        arrayCells?.append(cellOption2)
        
        arrayCells?.append(cellOption3)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func exitCodeStatusCell() -> UITableViewCell {
        
        let choiceCell = UITableViewCell(style: .Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        
        let labelReason = UILabel(frame: CGRectZero)
        labelReason.text = "Please choose the reason for doing this manually"
        labelReason.font = UIFont.PKAllerBold(12)
        labelReason.textColor = UIColor.appThemeColor()
        
        choiceCell.contentView.addSubview(labelReason)
        
        labelReason.translatesAutoresizingMaskIntoConstraints = false
        labelReason.addLeftEdgeAttachConstraint(choiceCell.contentView, offset: 10)
        labelReason.addRightEdgeAttachConstraint(choiceCell.contentView, offset: 10)
        labelReason.addTopEdgeAttachConstraint(choiceCell.contentView, offset: 5)
        labelReason.addHeightConstraint(21)
        
        let segmentControl = UISegmentedControl(items: ["Damaged", "Low light", "Not found"])
        segmentControl.frame = CGRectMake(10, 31, UIScreen.mainScreen().bounds.size.width - 20, 30)
        segmentControl.tintColor = UIColor.appThemeColor()

        choiceCell.contentView.addSubview(segmentControl)

        return choiceCell
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 2 {
            return 1
        }
        return 3
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return arrayCells![indexPath.section]
        }else if indexPath.section == 2 {
            return arrayCells![1]
        }
        return arrayCells![indexPath.section+1+indexPath.row]
        
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {

            let headerView = UIView(frame: CGRectZero)
            headerView.backgroundColor = UIColor.clearColor()
            
            let labelReason = UILabel(frame: CGRectZero)
            labelReason.text = "Please choose the reason for doing this manually"
            labelReason.font = UIFont.PKAllerBold(12)
            labelReason.textColor = UIColor.lightGrayColor()
            
            headerView.addSubview(labelReason)
            
            labelReason.translatesAutoresizingMaskIntoConstraints = false
            labelReason.addLeftEdgeAttachConstraint(headerView, offset: 10)
            labelReason.addRightEdgeAttachConstraint(headerView, offset: 10)
            labelReason.addBottomEdgeAttachConstraint(headerView, offset: 5)
            labelReason.addHeightConstraint(21)
            
            return headerView
        }
        return super.tableView(tableView, viewForHeaderInSection: section)
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 30
        }
        return super.tableView(tableView, heightForHeaderInSection: section)
    }
    
//    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if section == 1 {
//            return "Please choose the reason for doing this manually"
//        }
//        return ""
//    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 2 {

            navigationController?.popViewControllerAnimated(true)
            if codeInputCompletion != nil {
                codeInputCompletion!(inputCode: (arrayCells![0] as! PKTextFieldLoginTableViewCell).textField.text ?? "", status: qrCodeStatus)
            }
            
//            if qrCodeStatus != nil {
//                navigationController?.popViewControllerAnimated(true)
//                if codeInputCompletion != nil {
//                    codeInputCompletion!(inputCode: (arrayCells![0] as! PKTextFieldLoginTableViewCell).textField.text ?? "", status: qrCodeStatus)
//                }
//            }
            
        }else if indexPath.section == 1 {
            
            if lastSelectedIndexPath != nil {
                let lastSelectedCell = tableView.cellForRowAtIndexPath(lastSelectedIndexPath!)
                lastSelectedCell!.accessoryType = .None
            }
            
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            qrCodeStatus = cell?.textLabel?.text
            
            cell!.accessoryType = .Checkmark
            
            lastSelectedIndexPath = indexPath

        }
        
    }
    
}
