//
//  PKAboutTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

struct PKAboutConstants {
    
    static let Title = "About"
    
    static let TableHeaderHeight:CGFloat = 150
    static let TableFooterHeight:CGFloat = 105
}

class PKAboutTableViewController: PKBaseStaticTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = PKAboutConstants.Title
        
        let emptyCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        emptyCell.selectionStyle = .None
        
        let websiteCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellCenterTextIdentifier)
        websiteCell.textLabel?.text = "goplonk.com"
        websiteCell.textLabel?.textColor = UIColor.darkGrayColor()
        websiteCell.rippleLayerColor = UIColor.appThemeColor()
        
        let termsCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellCenterTextIdentifier)
        termsCell.textLabel?.text = "Terms of Use"
        termsCell.textLabel?.textColor = UIColor.darkGrayColor()
        termsCell.rippleLayerColor = UIColor.appThemeColor()
        
        let privacyCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellCenterTextIdentifier)
        privacyCell.textLabel?.text = "Privacy Policy"
        privacyCell.textLabel?.textColor = UIColor.darkGrayColor()
        privacyCell.rippleLayerColor = UIColor.appThemeColor()
        
        let versionCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellCenterTextIdentifier)
        versionCell.textLabel?.text = "© 2015 Plonk - Version 1.0"
        versionCell.textLabel?.textColor = UIColor.darkGrayColor()
        websiteCell.rippleLayerColor = UIColor.appThemeColor()
        
        let headerCell = UITableViewCell.headerCell()
       
        let logoImageView = headerCell.viewWithTag(1) as! UIImageView
        logoImageView.image = UIImage(named: "ic_plonk_large_whitebg")
        
        let logoTitle = headerCell.viewWithTag(2) as! UILabel
        logoTitle.textColor = UIColor.darkGrayColor()

        arrayCells = [headerCell,emptyCell,websiteCell,termsCell,privacyCell,versionCell]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return PKAboutConstants.TableHeaderHeight
        case 1:
            return 100
        case 2:
            return 44
        case 3:
            return 44
        default:
            return 44
        }
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            break;
        case 1:
            break;
        case 2:
            UIApplication.sharedApplication().openURL(NSURL(string: "http://www.goplonk.com")!)
            break
        case 3:
            let webViewController = PKWebViewController()
            webViewController.htmlKey = "terms_cond"
            self.navigationController?.pushViewController(webViewController, animated: true)
            break
        case 4:
            let webViewController = PKWebViewController()
            webViewController.htmlKey = "terms_cond"
            webViewController.title = "Privacy Policy"
            self.navigationController?.pushViewController(webViewController, animated: true)
            break
        default:
            break;
        }
    }
}
