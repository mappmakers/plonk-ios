//
//  PKWebViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 03/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD

class PKWebViewController: UIViewController {

    var htmlKey:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.appThemeColor()
        
        if title != "Privacy Policy" {
            
            if htmlKey == "supp_agrmnt" {
                title = "Supplier Agreement"
            }else {
                title = "Terms Of Use"
            }
        }
        

        // Do any additional setup after loading the view.
        let webView = UIWebView(frame: CGRectZero)
        webView.userInteractionEnabled = true
//        webView.scalesPageToFit = true
        UIView.layoutView(webView, fitView: view, transparent: true)
        
        SVProgressHUD.show()
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.TermsAgreement.rawValue), parameters:nil, completionClosure: { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                let json = result as! Dictionary<String, AnyObject>
                let dataDIC = json[self.htmlKey] as! Array<Dictionary<String, AnyObject>>
                
                let htmlDic = dataDIC[0]
                
                let htmlString = htmlDic["file_name"] as! String
                
                webView.loadRequest(NSURLRequest(URL: NSURL(string: htmlString, relativeToURL: NSURL(string: "https://s3-ap-southeast-1.amazonaws.com/plonk/"))!))

            }
            else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
            
            
        })
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = false
        navigationController?.navigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
