//
//  PKAddParkingLotViewController.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 12/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD

enum PKAddParkingLotError: ErrorType {
    case NoLotName, NoLotAddress, NoLotType, NoParkingLotSlotCount, NoLandmark, NoLatitude, NoLongitude
}

extension PKAddParkingLotError: CustomStringConvertible {
    var description: String {
        switch self {
        case NoLotName: return "Enter Parking Lot name"
        case NoLotAddress: return "Enter Parking lot address"
        case NoLotType: return "Enter Parking Lot type"
        case NoParkingLotSlotCount: return "Enter Parking Lot slots count"
        case NoLandmark: return "Please check if you have entered the nearest landmark"
        case NoLatitude: return "No Latitude info found"
        case NoLongitude: return "No Longitude info found"
        }
    }
}


class PKAddParkingLotViewController: UITableViewController, CLLocationManagerDelegate, PKParkingAddDelegate {

    let locationManager = CLLocationManager()
    var locValue:CLLocationCoordinate2D!
    
    var hasAcceptedTerms : Bool?

    var rowNumber:Int!
    
    let textFieldArray = ["Parking lot name","Nearest landmark","Parking lot type","Total slots available"]
    let pickerItems = ["open","covered","basement"]
    
    var dataArray:Array<UITableViewCell>?
    
//    var placemark : GMSPlace?

    var placemark : GMSAddress?
    
    var parkingLotAddDictionary:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
    
    //var bankDetailDic:Dictionary<String, AnyObject>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        initializeCurrentLocation()
        
        /*if bankDetailDic != nil {
            
            self.parkingLotAddDictionary.update(bankDetailDic!)
            
        }*/
        
        dataArray = []
        
        for title in textFieldArray {
            
            let cell:PKTextFieldLoginTableViewCell = PKTextFieldLoginTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextFieldIdentifier)
            cell.textField.placeholder = title
            cell.backgroundColor = UIColor.whiteColor()
            cell.textField.textColor = UIColor.grayColor()
            cell.textField.delegate=self
            cell.textField.floatingLabelTextColor = UIColor.appThemeColor()
            cell.textField.tintColor = UIColor.appThemeColor()
            
            if title == "Parking lot type"
            {
                cell.textField.attachFieldWithPickerData(pickerItems, selectedItem: nil)
                
            }else if title == "Nearest landmark" {

                let addressCell = PKTextViewTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellTextViewIdentifier)
                addressCell.textView.placeholder = "Address"
                dataArray?.append(addressCell)
            }else if title == "Total slots available" {
                cell.textField.keyboardType = .NumberPad
                cell.textField.attachDoneToolbar()
            }
            
            dataArray?.append(cell)
            
        }
        
        //dataArray?.append(termConditionCell())
        
        let createNewCell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        createNewCell.textLabel?.text = "Next"
        createNewCell.backgroundColor = UIColor.appThemeColor()
        
        dataArray?.append(createNewCell)
        
        getBankDetails()
    }
    
    func getBankDetails() {
        
        let parameters = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]
        
        SVProgressHUD.showWithMaskType(.Gradient)
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.GetBankDetails.rawValue), parameters: parameters) { (success, result) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                let json = result as! Dictionary<String,AnyObject>
                let bankDetailsArray = json["bank_details"] as! NSArray
                
                if bankDetailsArray.count > 0
                {
                    var bankDetailDic = bankDetailsArray[0] as! Dictionary<String, AnyObject>
                    bankDetailDic.updateValue(bankDetailDic["user_bank_name"] as! String, forKey: "user_bnk_name")
                    bankDetailDic.updateValue(bankDetailDic["user_bank_branch"] as! String, forKey: "user_branch")
                    
                    bankDetailDic.removeValueForKey("user_bank_name")
                    bankDetailDic.removeValueForKey("user_bank_branch")
                    
                    self.parkingLotAddDictionary.update(bankDetailDic)
                }
                
            }
            
        }
        
    }
    
    func initializeCurrentLocation()
    {
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      
        locValue = manager.location!.coordinate
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        manager.stopUpdatingLocation()
    }
    
    private func termConditionCell() -> UITableViewCell  {
        let cell = PKCenteredLabelTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellBasicIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        let titleString = "By proceeding, you agree to the supplier agreement terms of use."
        
        let termsString:NSMutableAttributedString = NSMutableAttributedString(string: titleString, attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName:UIFont.PKAllerRegular(11)])
        termsString.addAttributes([NSUnderlineStyleAttributeName : NSNumber(integer: 1),NSUnderlineColorAttributeName:UIColor.whiteColor()], range: NSString(string: titleString).rangeOfString("supplier agreement terms of use"))
        cell.textLabel?.attributedText = termsString
        
        //        cell.customizeForAttributedTextWith("By singing in, you are agreeing to the ", secondText: "terms and conditions & privacy policy.")
        
        return cell
        
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 5 {
            
            let headerView = UIView(frame: CGRectZero)
            
            let checkbox = UIButton(type: .Custom)
            checkbox.setImage(UIImage(named: "ic_checked"), forState: .Selected)
            checkbox.setImage(UIImage(named: "ic_unchecked"), forState: .Normal)
            checkbox.addTarget(self, action: "checkboxTapped:", forControlEvents: .TouchDown)
            checkbox.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
            
            headerView.addSubview(checkbox)
            
            checkbox.translatesAutoresizingMaskIntoConstraints = false
            checkbox.addTopEdgeAttachConstraint(headerView, offset: 20)
            checkbox.addLeftEdgeAttachConstraint(headerView, offset: 10)
            checkbox.addWidthConstraint(25)
            checkbox.addHeightConstraint(25)
            
            let agreeView = PKClickableLabel(frame: CGRectZero)
            agreeView.parentController = self
            
//            agreeView.termsCompletion = { (sender) in Void()
//            
//                
//            }
            
            headerView.addSubview(agreeView)
            agreeView.translatesAutoresizingMaskIntoConstraints = false
            agreeView.addLeftEdgeAttachConstraint(checkbox, viewEdge: .Right, offset: 10)
            agreeView.addBottomEdgeAttachConstraint(headerView)
            agreeView.addRightEdgeAttachConstraint(headerView, offset: 10)
            agreeView.addTopEdgeAttachConstraint(headerView)
            
//            var termsString:String = "By proceeding, you agree to the supplier #agreement #<ts>terms and conditions."
//            if UIScreen.mainScreen().bounds.size.height > 568 {
//                termsString = "By proceeding, you agree to the supplier agreement #<ts>terms and conditions."
//            }
            agreeView.buildAgreeTextViewFromString("By proceeding, you agree to the supplier #agreement #<ts>terms of use.")

            return headerView

            
        }
        return UIView(frame: CGRectZero)
    }

    func checkboxTapped(sender:UIButton) {
        
        sender.selected = !sender.selected
        
        hasAcceptedTerms = sender.selected
        
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        if section == 5 {
             return 80
        }
         return 10
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            return 140
        }
        return PKConstants.CellTextFieldHeight-10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        return dataArray![indexPath.section]
    
    }
    
    func addParkingLot() throws
    {

        let parkingLotName = (dataArray![0] as! PKTextFieldLoginTableViewCell).textField.text
        
        let parkingAddress = (dataArray![1] as! PKTextViewTableViewCell).textView.text
        
        let parkingLandmark = (dataArray![2] as! PKTextFieldLoginTableViewCell).textField.text
        
        let parkingType = (dataArray![3] as! PKTextFieldLoginTableViewCell).textField.text
        
        let parkingNumSlots = (dataArray![4] as! PKTextFieldLoginTableViewCell).textField.text
        
        guard parkingLotName?.characters.count > 0 else { throw PKAddParkingLotError.NoLotName }
        guard parkingAddress?.characters.count > 0 else { throw PKAddParkingLotError.NoLotAddress }
        guard parkingLandmark?.characters.count > 0 else { throw PKAddParkingLotError.NoLandmark }
        guard parkingType?.characters.count > 0 else { throw PKAddParkingLotError.NoLotType }
        guard parkingNumSlots?.characters.count > 0 else { throw PKAddParkingLotError.NoParkingLotSlotCount }
        guard String(placemark!.coordinate.latitude).characters.count > 0 else { throw PKAddParkingLotError.NoLatitude }
        guard String(placemark!.coordinate.longitude).characters.count > 0  else { throw PKAddParkingLotError.NoLongitude }
        
        let parameters =
        [
            "user_id":PKDataModel.sharedInstance.loggedUser.user_id!,
            "prk_lot_name":parkingLotName!,
            "prk_lot_landmark":parkingLandmark!,
            "prk_lot_latitude":String(placemark!.coordinate.latitude),
            "prk_lot_longitude":String(placemark!.coordinate.longitude),
            "prk_lot_address":parkingAddress,
            "slots_total_count":parkingNumSlots!,
            "parking_type":parkingType!,
            "mode_of_payment":"cash",
            "image_status":"0"
        ] 
        
        parkingLotAddDictionary.update(parameters)
        
        let parkingAddDocumentController = PKAddDocumentTableViewController(style: .Grouped)
        parkingAddDocumentController.parkingLotDictionary = parkingLotAddDictionary
        parkingAddDocumentController.parkingAddDelegate = self
        parkingAddDocumentController.navigationItem.hidesBackButton = true
        self.navigationController?.pushViewController(parkingAddDocumentController, animated: true)

    }
    
    func parkingLotAddDetailController(controller: UITableViewController, didUpdateDictionaryValue paramters: Dictionary<String, AnyObject>!) {
        
        parkingLotAddDictionary.update(paramters)
        
        navigationController?.popViewControllerAnimated(true)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 5 {
            
            if hasAcceptedTerms == true {
                
                do {
                    try addParkingLot()
                } catch let error as PKAddParkingLotError {
                    print(error.description)
                    handleError(PKConstants.TitleErrorHeader, message: error.description)
                } catch {
                    handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
                }
                
            }else {
                handleError("", message: "Please agree to the terms of use to proceed")
            }
            
        }
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 6
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }
    
    @IBAction func uploadImage(sender: AnyObject) {

    }
    
}

