//
//  PKParkingSlotsTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 16/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD
struct PKParkingSlotsConstants {
    
    static let TitlePKParkingSlots = "Parking Slots"
}

typealias PKChooseSlotCompletion = (slot : PKParkingSlot) -> Void

class PKParkingSlotsTableViewController: UITableViewController {

    private var parkingSlots:Array<PKParkingSlot>?
    
    var chooseSlotCompletion:PKChooseSlotCompletion?
    
    var parkingLotId:String!
    var vehicles:Array<PKUserVehicle>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = PKParkingSlotsConstants.TitlePKParkingSlots
        fetchParkingSlots()
    }

    func fetchParkingSlots() {
        
        SVProgressHUD.showWithMaskType(.Gradient)
        
        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"prkLot_id":parkingLotId]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ChangeParkingSlot.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                self.parkingSlots = []
                
                let json = result as! Dictionary<String, AnyObject>
                
                let dataDIC = json["parking_slots"] as! Array<Dictionary<String, AnyObject>>
                
                for parkingSlot in dataDIC {
                    
                    self.parkingSlots?.append(PKParkingSlot.parkingSlotWithDictionaryData(parkingSlot))
                }
                
                self.tableView.reloadData()
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return parkingSlots?.count ?? 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 106
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("parkingSlotCell", forIndexPath: indexPath) as! PKParkingSlotTableViewCell

        let parkingSlot = parkingSlots![indexPath.section]
        // Configure the cell...
        cell.labelSlots.text = parkingSlot.display_message
        cell.labelClosingTime.text = "parking closes at "+parkingSlot.end_time!
        
        let availableSlots = NSString(string: parkingSlot.available_slots!).floatValue
        let totalSlots = NSString(string: parkingSlot.parking_total_slots!).floatValue
        let percentage = (availableSlots/totalSlots)*100
        
        percentage<50 ? "Filling Fast": "Available"

        if percentage < 50 {
            
            if percentage == 0 {
                
                cell.labelParkingStatus.text = "Unavailable"
                cell.labelParkingStatus.backgroundColor = UIColor(rgba: "#A4000D")

            }else {

                cell.labelParkingStatus.text = "Fast Filling"
                cell.labelParkingStatus.textColor = UIColor.darkGrayColor()
                cell.labelParkingStatus.backgroundColor = UIColor(rgba: "#FEE92E")
            }
            
        }else {
            
            cell.labelParkingStatus.text = "Available"
            cell.labelParkingStatus.backgroundColor = UIColor(rgba: "#14993F")
            
        }
        
        cell.labelFare.text = "₹ \(parkingSlot.fare!)"
        cell.labelMinimumDuration.text = "Min Duration: \(parkingSlot.min_duration!)"
        cell.labelExtraFare.text = "Increments: ₹ \(parkingSlot.extra_charge!)/\(parkingSlot.increments!)"
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if chooseSlotCompletion != nil {
            chooseSlotCompletion!(slot: parkingSlots![indexPath.section])
        }
    }

}
