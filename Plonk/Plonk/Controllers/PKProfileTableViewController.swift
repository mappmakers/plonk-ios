//
//  PKProfileTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 13/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import KFSwiftImageLoader
import ENSwiftSideMenu
import SVProgressHUD
import AFNetworking

struct PKProfileConstants {
    static let TitleProfile = "My Profile"
    static let CellHeightProfilePic:CGFloat = 160
    static let CellHeightVehicle:CGFloat = 70
}

enum PKAddVehicleDataError: ErrorType {
    case NoMakes, NoModels, NoRegId
}

extension PKAddVehicleDataError: CustomStringConvertible {
    var description: String {
        switch self {
        case .NoMakes: return "No Vehicle Make info"
        case .NoModels: return "No Vehicle Model info"
        case .NoRegId: return "Please enter a valid registeration number"
        }
    }
}

class PKProfileTableViewController: UITableViewController, PKAuthenticationDelegate {
    
    private var vehicles:Array<PKUserVehicle>?
    
    private var vehicleModels:Array<PKUserVehicle>?
    
    var currentPlace:GMSPlace?
    
    var updatedEmailCompletion:UpdateEmailCompletion?
    
    var selectedProfileImage : UIImage?
    
    /*
    func initializeCurrentLocation()
    {
        let locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        locationManager.requestWhenInUseAuthorization()
        
        GMSPlacesClient.sharedClient().currentPlaceWithCallback({ (placeLikelihood, error) -> Void in
            if error != nil {
                print("Current Place error: \(error!.localizedDescription)")
                return
            }
            
            var currentLikelihood:Double = 0.0
            for likelihood in placeLikelihood!.likelihoods {
                if let likelihood = likelihood as? GMSPlaceLikelihood {
                    let place = likelihood.place
                    print("Current Place name \(place.name) at likelihood \(likelihood.likelihood)")
                    print("Current Place address \(place.formattedAddress)")
                    print("Current Place attributions \(place.attributions)")
                    print("Current PlaceID \(place.placeID)")
                    
                    if currentLikelihood == 0.0 {
                        self.currentPlace = likelihood.place
                    }else if likelihood.likelihood > currentLikelihood {
                        currentLikelihood = likelihood.likelihood
                        self.currentPlace = likelihood.place
                    }
                    
                }
            }
            
            self.tableView.reloadData()
        })
        
    }
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = PKProfileConstants.TitleProfile

        createAddVehicleIcon()
        
        fetchVehicles { (success) -> Void in
            
            self.tableView.reloadData()

        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        dismissViewControllerAnimated(true, completion: { () -> Void in

            let parameters : Dictionary<String, AnyObject> =
            [
                "user_id":PKDataModel.sharedInstance.loggedUser.user_id!,
                "secure_key":NSUserDefaults.standardUserDefaults().objectForKey(PKConstants.UserDefaultTokenValue)!
            ]
            
            SVProgressHUD.showWithMaskType(.Gradient)
            let manager = AFHTTPRequestOperationManager()
            manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json","text/plain") as? Set<NSObject>
            let operation = manager.POST(PKWebServiceRoute.Path(PKWebServiceEndPoints.AddProfileImage.rawValue).URLString, parameters: parameters, constructingBodyWithBlock: { (formData: AFMultipartFormData!) -> Void in
                
                let data = UIImageJPEGRepresentation(image, 0.5)
                
                formData.appendPartWithFileData(data!, name: "profile_pic", fileName: "photo.jpg", mimeType: "image/jpeg")
                
                
                }, success: { (operation, responseObject) -> Void in
                    print(responseObject)
                    
                    SVProgressHUD.dismiss()
                    
                    if let responseJson = responseObject as? Dictionary<String, AnyObject> {
                        
                        if let errorMessage = responseJson["error"] as? String {
                            
                            if errorMessage == "Authentication failed" {
                                
                                self.handleError("Session Expired", message: "Your current ssession has been expired. You wil now be redirected to login page. Please login to continue.", handler: { (action) -> Void in
                                    
                                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                    
                                    appDelegate.customizeForAuthenticationRequired()
                                    
                                })
                                
                                return
                                
                            }
                            
                        }
                        
                    }
                    
                    self.handleError("", message: "Your profile image has been successully updated", handler: { (action) -> Void in
                        
                        self.selectedProfileImage = image
                        
                        self.tableView.reloadData()
                        
                    })
                    
                }) { (operation, error) -> Void in
                    print(error)
                    SVProgressHUD.dismiss()
                    self.handleError("Sorry!", message: "Something went wrong")
            }
            
            operation!.start()
        
        })

    }
    
    func createAddVehicleIcon() {
        
        let btnLeft:UIButton = UIButton(type: UIButtonType.Custom)

        let buttonImage:UIImage = UIImage(named: "ic_add_vehicle_white")!
        btnLeft.setImage(buttonImage, forState: UIControlState.Normal)
        btnLeft.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height)
        btnLeft.addTarget(self, action: "didTapAddVehicleButton:", forControlEvents: UIControlEvents.TouchDown)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnLeft)
        
    }

    func fetchVehicles(completionClosure: (success:Bool) ->Void) {
        
        SVProgressHUD.showWithMaskType(.Gradient)
        
        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.GetUserVehicles.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                self.vehicles = []
                
                let json = result as! Dictionary<String, AnyObject>
                let profileDetails = json["prof_img_details"] as! Array<Dictionary<String,AnyObject>>
                let profileDIC = profileDetails[0]

                let profileImage = profileDIC["profile_img"] as! String
                
                PKDataModel.sharedInstance.loggedUser.user_image_path = profileImage
                
                let dataDIC = json["vehicle_details"] as! Array<Dictionary<String, AnyObject>>
                
                for vehicle in dataDIC {
                    
                    self.vehicles?.append(PKUserVehicle.vehicleWithDictionaryData(vehicle))
                }
                
                completionClosure(success: true)
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
    }
    
    func didTapAddVehicleButton(sender:UIButton) {
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.GetVehicleTypes.rawValue), parameters: nil) { (success:Int!, result:AnyObject?) -> Void in
            
            if success == 1 {
                
                self.vehicleModels = []
                
                let json = result as! Dictionary<String, AnyObject>
                
                let vehicleModelResultsArray = json["carmodel_details"] as! Array<Dictionary<String, AnyObject>>
                
                for vehicleModel in vehicleModelResultsArray {
                    
                    self.vehicleModels?.append(PKUserVehicle.vehicleListWithDictionaryData(vehicleModel))
                    
                }
                
                do {
                    try self.pushToAddVehicle()
                } catch let error as PKParkingDataError {
                    print(error.description)
                    self.handleError(PKConstants.TitleErrorHeader, message: error.description)
                } catch {
                    self.handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
                }
                
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
        
        
    }
    
    func pushToAddVehicle() throws {
        
        guard self.vehicleModels?.count > 0 else {throw PKAddVehicleDataError.NoModels}
        
        let addVehicleController = PKAddVehicleTableViewController(style:.Grouped)
        addVehicleController.vehicles = vehicleModels
        addVehicleController.addVehicleCompletion = { success in
        
            self.fetchVehicles({ (success) -> Void in
                
                self.tableView.reloadData()
                
                self.navigationController?.popViewControllerAnimated(true)
            })
        
        }
        navigationController?.pushViewController(addVehicleController, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1+(vehicles?.count ?? 0)
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return PKProfileConstants.CellHeightProfilePic
        }
        return PKProfileConstants.CellHeightVehicle
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier(PKConstants.CellHeaderIdentifier, forIndexPath: indexPath)
            
            let imageView = cell.viewWithTag(1) as! UIImageView
            imageView.layer.cornerRadius = 58
            imageView.layer.masksToBounds = true
            imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
            imageView.layer.borderWidth = 1.0
            
            if selectedProfileImage != nil {
                imageView.image = selectedProfileImage
            }else {
                imageView.loadImageFromURLString(PKAmazonRoute.ImagePath(PKDataModel.sharedInstance.loggedUser.user_image_path ?? "").URLString)
            }

//            let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "profileImageViewGesture:")
//            imageView.addGestureRecognizer(tap)
            
            let labelContactNumber = cell.viewWithTag(2) as! UILabel
            
            if let number = PKDataModel.sharedInstance.loggedUser.user_mobileno {
                
                if number == "0" || number.characters.count == 0 {
                    labelContactNumber.text = "Add Mobile number"
                }else {
                    labelContactNumber.text = number
                }
                
            }

            let labelEmail = cell.viewWithTag(3) as! UILabel
            labelEmail.text = PKDataModel.sharedInstance.loggedUser.user_email ?? "Add Email"
            
            let labelLocation = cell.viewWithTag(4) as! UILabel
            if let location = PKDataModel.sharedInstance.loggedUser.user_place_mark {
                if location == "0" || location.characters.count == 0 {
                    labelLocation.text = "Add Location"
                }else {
                    labelLocation.text = location
                }
                
            }else {
                labelLocation.text = "Add Location"
            }

            cell.selectionStyle = .None
            
            return cell

        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(PKConstants.CellBasicIdentifier, forIndexPath: indexPath) as! PKProfileVehicleTableViewCell
        
        let vehicle = vehicles![indexPath.section-1]
        cell.labelVehicleName.text = vehicle.make_name
        cell.labelVehicleModelName.text = vehicle.model_name
        cell.labelVehicleNumber.text = vehicle.registeration_no
        
        cell.defaultColor = UIColor.whiteColor()
        cell.setSwipeGestureWithView(self.deleteView(), color: UIColor.appThemeColor(), mode: .Exit, state: .State3) { (cell, state, mode) -> Void in
            
            let parameters = ["vech_id":vehicle.user_vehicle_id ?? ""]

            self.handleDecisionPrompt("Confirm Delete ?", message: "Are you sure you want to remove this vehicle from your profile", confirmDecisionHandler: { (action) -> Void in
                
                PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.DeleteVehicle.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
                    
                    if success != 1
                    {
                        let error:NSError = (result as! NSError)
                        let title = String(error.code) + ": " + error.domain
                        let message = error.localizedDescription
                        self.handleError(title, message: message)
                    }else {
                        
                        let json = result as! Dictionary<String,AnyObject>
                        if json["success"] as! String == "true" {
                            
                            self.vehicles?.removeAtIndex(indexPath.section-1)
                            self.tableView.reloadData()
                            
                        }

                    }
                }
                
            })

            cell.swipeToOriginWithCompletion { () -> Void in
                
            }
        }

        cell.selectionStyle = .None
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        btnImagePickerClicked(tableView)
        
    }
    
    // MARK: - ACTIONS
    
    @IBAction func emailUpdatePressed(sender: AnyObject) {
        print("Email Tapped")
        
        let updateEmailController = PKChangeEmailTableViewController(style:.Grouped)
        updateEmailController.updatedEmailCompletion = { updateEmail in
            
            let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
            let labelEmail = cell!.viewWithTag(3) as! UILabel
            labelEmail.text = updateEmail ?? ""
            
            if self.updatedEmailCompletion != nil {
                
                self.updatedEmailCompletion!(updatedEmail: updateEmail)
                
            }
            
        }
        navigationController?.pushViewController(updateEmailController, animated: true)
        
    }
    
    @IBAction func mobileUpdatePressed(sender: AnyObject) {
        print("Mobile Tapped")
        
        let updateMobileController = PKChangeMobileTableViewController(style:.Grouped)
        updateMobileController.authenticationDelegate = self
        navigationController?.pushViewController(updateMobileController, animated: true)
    }
    
    @IBAction func locationPressed(sender: AnyObject) {
        
        let updateLocationController = PKChangeLocationTableViewController(style:.Grouped)
        updateLocationController.updatedLocationCompletion = { updatedLocation in
            
            let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
            let labelEmail = cell!.viewWithTag(4) as! UILabel
            labelEmail.text = updatedLocation ?? ""
            
        }
        navigationController?.pushViewController(updateLocationController, animated: true)
        
    }

    func authenticationController(controller: UITableViewController, didSuccessfullyAuthenticateUser user: PKUser!) {
        
        let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
        let labelMobile = cell!.viewWithTag(2) as! UILabel
        labelMobile.text = PKDataModel.sharedInstance.loggedUser.user_mobileno ?? ""

        navigationController?.popToViewController(self, animated: true)
        
    }
    
    func authenticationController(controller: UITableViewController, didFailToAuthenticateUser error: NSError!) {
        
    }
    
    func controller(controller: UITableViewController, didLogoutUser user: PKUser!) {
        
    }
}
