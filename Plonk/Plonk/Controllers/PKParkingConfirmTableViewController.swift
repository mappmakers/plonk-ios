//
//  PKParkingConfirmTableViewController.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 07/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD

struct PKParkingConfirmConstants {
    static let PKParkingVehicleCellIdentifier:String = "VehicleCellIdentifier"
    static let TitleParkingConfirm = "Parking Confirm"
}

enum PKParkingConfirmLabelType: Int {
    case Venue, UserName, Slot, EndTime, Rate, MinimumDuration, Increments
}

enum PKConfirmParkingError: ErrorType {
    case NoVehicleId, NoOwnerId, NoParkingLotId, NoTenantLotId, NoTenantPlanId, NoTenantSlotId, NoPaymentMode
}

extension PKConfirmParkingError: CustomStringConvertible {
    var description: String {
        switch self {
        case NoVehicleId: return "No Vehicles found"
        case NoOwnerId: return "No Owner info found"
        case NoParkingLotId: return "No parking lot info found"
        case NoTenantLotId: return "No tenant lot info found"
        case NoTenantPlanId: return "No tenant plan info found"
        case NoTenantSlotId: return "No tenant slot info found"
        case NoPaymentMode: return "No payment mode info found"
        }
    }
}

class PKParkingConfirmTableViewController: UITableViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet var labels: [UILabel]!
    @IBOutlet var collectionViewVehicles: UICollectionView!
    @IBOutlet var paytmBtn: UIButton!
    @IBOutlet var cashBtn: UIButton!
    @IBOutlet var confirmCell: PKCenteredLabelTableViewCell!
    @IBOutlet var btnChangeSlot: UIButton!
    
    @IBOutlet var labelClickHere: UILabel!
    @IBOutlet var viewAddVehicle: UIView!
    private var selectedIndexPath:NSIndexPath?
    
    var parkingSlot:PKParkingSlot!
    var vehicles:Array<PKUserVehicle>?
    
    var selectedVehicle:PKUserVehicle?
    
    private var vehicleModels:Array<PKUserVehicle>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = PKParkingConfirmConstants.TitleParkingConfirm
        
        if vehicles == nil {
            
            vehicles = []
            
        }
        
        btnChangeSlot.setAttributedTitle(NSAttributedString(string: "choose another parking slot", attributes: [NSForegroundColorAttributeName : UIColor.darkGrayColor(),NSUnderlineStyleAttributeName : NSNumber(integer: 1),NSUnderlineColorAttributeName:UIColor.darkGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(12)]), forState: .Normal)
        
        paytmBtn.layer.cornerRadius = 5
        paytmBtn.layer.masksToBounds = true
        
        paytmBtn.layer.borderWidth = 2
        
        cashBtn.layer.cornerRadius = 5
        cashBtn.layer.masksToBounds = true
        
        cashBtn.layer.borderWidth = 2
        
        confirmCell.backgroundColor = UIColor.appThemeColor()

        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSizeMake(130, 80)
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.sectionInset = UIEdgeInsetsMake(7, 10, 0, 0)
        flowLayout.scrollDirection = .Horizontal
        
        collectionViewVehicles.collectionViewLayout = flowLayout
        
        labels[PKParkingConfirmLabelType.Venue.rawValue].text = parkingSlot.parking_lot_name!
        labels[PKParkingConfirmLabelType.UserName.rawValue].text = parkingSlot.owner_name!

        configureSlot()

        labelClickHere.attributedText = NSAttributedString(string: "click here to add", attributes: [NSForegroundColorAttributeName : UIColor.darkGrayColor(),NSUnderlineStyleAttributeName : NSNumber(integer: 1),NSUnderlineColorAttributeName:UIColor.darkGrayColor(), NSFontAttributeName:UIFont.PKAllerBold(12)])
        
        if vehicles?.count == 0 {
            viewAddVehicle.hidden = false
        }
        
        labels[PKParkingConfirmLabelType.EndTime.rawValue].text = "parking closes at "+parkingSlot.end_time!
        labels[PKParkingConfirmLabelType.Rate.rawValue].text = "₹ \(parkingSlot.fare!)"
        labels[PKParkingConfirmLabelType.MinimumDuration.rawValue].text = "Min Duration: \(parkingSlot.min_duration!)"
        labels[PKParkingConfirmLabelType.Increments.rawValue].text = "Increments: ₹ \(parkingSlot.extra_charge!)/\(parkingSlot.increments!)"
        
        if parkingSlot.parking_pymnt_mode == "cash" {
            paytmBtn.enabled = false
            paytmBtn.layer.borderColor = UIColor.whiteColor().CGColor
            cashBtn.layer.borderColor = UIColor.appThemeColor().CGColor
        }else {
            paytmBtn.layer.borderColor = UIColor.appThemeColor().CGColor
            cashBtn.layer.borderColor = UIColor.whiteColor().CGColor
        }
    }
    
    func configureSlot() {
        
        let attributedTitleFirst:NSAttributedString = NSAttributedString(string: "from ", attributes: [NSForegroundColorAttributeName : UIColor.darkGrayColor(), NSFontAttributeName:UIFont.PKAllerRegular(14)])
        
        let attributedTitleSecond:NSAttributedString = NSAttributedString(string: parkingSlot.display_message!, attributes: [NSForegroundColorAttributeName : UIColor.appThemeColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        
        let joinedTitle:NSMutableAttributedString = NSMutableAttributedString(attributedString: attributedTitleFirst)
        joinedTitle.appendAttributedString(attributedTitleSecond)
        
        labels[PKParkingConfirmLabelType.Slot.rawValue].attributedText = joinedTitle
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        if vehicles?.count == 0 && indexPath.row == 3 {
            didTapAddVehicleButton()
        }
        
        if indexPath.row == 5 {

            do {
                try confirmParking()
            } catch let error as PKConfirmParkingError {
                print(error.description)
                handleError(PKConstants.TitleErrorHeader, message: error.description)
            } catch {
                handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
            }
            
        }
    }
    
    func confirmParking() throws {
        
        guard selectedVehicle?.user_vehicle_id != nil else { throw PKConfirmParkingError.NoVehicleId }
        guard parkingSlot.owner_id != nil else { throw PKConfirmParkingError.NoOwnerId }
        guard parkingSlot.parking_lot_id != nil else { throw PKConfirmParkingError.NoParkingLotId }
        guard parkingSlot.tenant_lot_id != nil else { throw PKConfirmParkingError.NoTenantLotId }
        guard parkingSlot.tenant_plan_id != nil else { throw PKConfirmParkingError.NoTenantPlanId }
        guard parkingSlot.tenant_slot_id != nil else { throw PKConfirmParkingError.NoTenantSlotId }
        guard parkingSlot.parking_pymnt_mode != nil else { throw PKConfirmParkingError.NoPaymentMode }
        
        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"vehicle_id":selectedVehicle!.user_vehicle_id!,"owner_id":parkingSlot.owner_id!,"parking_lot_id":parkingSlot.parking_lot_id!,"tenant_lot_id":parkingSlot.tenant_lot_id!,"tenant_plan_id":parkingSlot.tenant_plan_id!,"tenant_slot_id":parkingSlot.tenant_slot_id!,"payment_mode":parkingSlot.parking_pymnt_mode!]
        
        SVProgressHUD.showWithMaskType(.Gradient)
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.ParkingSlotConfirm.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                let json = result as! Dictionary<String, AnyObject>
                
                if ((json["success"] as? String) == "true") {
                    
                    if let errorMessage = json["error"] as? String {
                        
                        if errorMessage == "prk meter already running" {
                            
                            self.handleError("Sorry!", message: "Parking meter is already running for the selected parking slot", handler: { (action) -> Void in
                                
                                
                            })
                            
                            return
                            
                        }
                        
                    }
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(PKConstants.NotificationParkingSlotConfirmed, object: nil)
                    NSNotificationCenter.defaultCenter().postNotificationName(PKConstants.NotificationUserLotAdded, object: nil)
                    
                    let parkingMeterStartViewController = self.storyboard! .instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingMeterStart) as! PKParkingMeterStartTableViewController
                    parkingMeterStartViewController.selectedVehicle = self.selectedVehicle
                    parkingMeterStartViewController.parkingId = json["Parking id"] as? String
                    parkingMeterStartViewController.parkingSlot = self.parkingSlot
                    self.navigationController?.pushViewController(parkingMeterStartViewController, animated: true)
                    
                }
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
    }
    
    func didTapAddVehicleButton() {
        
        SVProgressHUD.showWithMaskType(.Gradient)
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.GetVehicleTypes.rawValue), parameters: nil) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })

            if success == 1 {
                
                self.vehicleModels = []
                
                let json = result as! Dictionary<String, AnyObject>
                
                //                let vehicleMakeResultsArray = json["carmake_details"] as!  Array<Dictionary<String, AnyObject>>
                
                //                for vehicleMake in vehicleMakeResultsArray {
                //
                //
                //
                //                }
                
                let vehicleModelResultsArray = json["carmodel_details"] as! Array<Dictionary<String, AnyObject>>
                
                for vehicleModel in vehicleModelResultsArray {
                    
                    self.vehicleModels?.append(PKUserVehicle.vehicleListWithDictionaryData(vehicleModel))
                    
                }
                
                do {
                    try self.pushToAddVehicle()
                } catch let error as PKParkingDataError {
                    print(error.description)
                    self.handleError(PKConstants.TitleErrorHeader, message: error.description)
                } catch {
                    self.handleError(PKConstants.TitleErrorHeader, message: PKConstants.TitleErrorMessage)
                }
                
                
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
    }
    
    func pushToAddVehicle() throws {
        
        guard self.vehicleModels?.count > 0 else {throw PKAddVehicleDataError.NoModels}
        
        let addVehicleController = PKAddVehicleTableViewController(style:.Grouped)
        addVehicleController.vehicles = vehicleModels
        addVehicleController.addVehicleCompletion = { success in
            
            self.fetchVehicles({ (success) -> Void in
                
                if self.vehicles?.count > 0 {
                    self.viewAddVehicle.hidden = true
                    self.collectionViewVehicles.reloadData()

                }
                
                self.navigationController?.popViewControllerAnimated(true)
            })
            
        }
        navigationController?.pushViewController(addVehicleController, animated: true)
        
    }
    
    func fetchVehicles(completionClosure: (success:Bool) ->Void) {
        
        SVProgressHUD.showWithMaskType(.Gradient)
        
        let parameters:Dictionary<String, String> = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.GetUserVehicles.rawValue), parameters: parameters) { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                    })
            })
            

            if success == 1 {
                
                self.vehicles = []
                
                let json = result as! Dictionary<String, AnyObject>
                
                let dataDIC = json["vehicle_details"] as! Array<Dictionary<String, AnyObject>>
                
                for vehicle in dataDIC {
                    
                    self.vehicles?.append(PKUserVehicle.vehicleWithDictionaryData(vehicle))
                }
                
                completionClosure(success: true)
                
//                self.tableView.reloadData()
            }else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
        }
        
    }
    
    @IBAction func chooseAnotherPressed(sender: AnyObject) {
        
        let parkingSlotsController = self.storyboard?.instantiateViewControllerWithIdentifier(PKConstants.StoryboardIDParkingSlots) as! PKParkingSlotsTableViewController
        parkingSlotsController.parkingLotId = parkingSlot.parking_lot_id
        parkingSlotsController.chooseSlotCompletion = { (slot) -> Void in
            
            self.parkingSlot = slot
            
            self.configureSlot()
            
            self.navigationController?.popViewControllerAnimated(true)
        }
        navigationController?.pushViewController(parkingSlotsController, animated: true)
        
    }

    @IBAction func buttonPressed(sender: AnyObject) {
        
        if sender.tag == 1 {
            // Paytm
            paytmBtn.layer.borderColor = UIColor.appThemeColor().CGColor
            cashBtn.layer.borderColor = UIColor.whiteColor().CGColor

            parkingSlot.parking_pymnt_mode = "paytm"
        }else {
            
            paytmBtn.layer.borderColor = UIColor.whiteColor().CGColor
            cashBtn.layer.borderColor = UIColor.appThemeColor().CGColor
            
            parkingSlot.parking_pymnt_mode = "cash"
        }
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vehicles?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 2
        
        if selectedIndexPath != nil {
            
            if selectedIndexPath != indexPath {
                cell.layer.borderColor = UIColor.lightGrayColor().CGColor
                
                let vehicleImage = cell.viewWithTag(2) as! UIImageView
                vehicleImage.image = UIImage(named: "ic_car_gray")
                
            }else {
                cell.layer.borderColor = UIColor.appThemeColor().CGColor
                
                let vehicleImage = cell.viewWithTag(2) as! UIImageView
                vehicleImage.image = UIImage(named: "ic_car_gray")
            }
            
        }else if vehicles?.count > 1{
            cell.layer.borderColor = UIColor.lightGrayColor().CGColor
        }else if vehicles?.count == 1 {
            cell.layer.borderColor = UIColor.appThemeColor().CGColor
            
            let vehicleImage = cell.viewWithTag(2) as! UIImageView
            vehicleImage.image = UIImage(named: "ic_car_gray")
            
            selectedIndexPath = indexPath
            
            selectedVehicle = vehicles![indexPath.item]
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let collectionCell = collectionView.dequeueReusableCellWithReuseIdentifier(PKParkingConfirmConstants.PKParkingVehicleCellIdentifier, forIndexPath: indexPath)
        
        let vehicleNumber = collectionCell.viewWithTag(1) as! UILabel
        vehicleNumber.text = vehicles![indexPath.item].registeration_no!
        
        return collectionCell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        selectedIndexPath = indexPath
        
        selectedVehicle = vehicles![indexPath.item]
        collectionView.reloadData()
    }
}
