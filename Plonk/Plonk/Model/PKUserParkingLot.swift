//
//  PKUserParkingLot.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 16/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation
import CoreData

class PKUserParkingLot: NSManagedObject {

    static  func userParkingLotWithParameters(parameters:Dictionary<String,AnyObject>)-> PKUserParkingLot {
        
        let userParkingLot = PKUserParkingLot.MR_createEntityInContext(NSManagedObjectContext.MR_defaultContext())
        
        userParkingLot.parking_lot_address = parameters["parking_lot_address"] as? String
        userParkingLot.parking_lot_id = parameters["parking_lot_id"] as? String
        userParkingLot.parking_lot_landmark = parameters["parking_lot_landmark"] as? String
        userParkingLot.parking_lot_name = parameters["parking_lot_name"] as? String
        userParkingLot.tenant_lot_id = parameters["tenant_lot_id"] as? String
        
        return userParkingLot
        
    }


// Insert code here to add functionality to your managed object subclass

}
