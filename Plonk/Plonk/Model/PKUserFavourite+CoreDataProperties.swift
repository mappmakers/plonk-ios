//
//  PKUserFavourite+CoreDataProperties.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PKUserFavourite {

    @NSManaged var creation_date: String?
    @NSManaged var favourite_id: String?
    @NSManaged var modification_date: String?
    @NSManaged var parking_lot_id: String?
    @NSManaged var user_id: String?
    @NSManaged var parkingLot: PKParkingLot?

}
