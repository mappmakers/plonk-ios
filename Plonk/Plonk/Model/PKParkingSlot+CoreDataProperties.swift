//
//  PKParkingSlot+CoreDataProperties.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PKParkingSlot {

    @NSManaged var difference: String?
    @NSManaged var display_message: String?
    @NSManaged var end_date: String?
    @NSManaged var end_time: String?
    @NSManaged var extra_charge: String?
    @NSManaged var fare: String?
    @NSManaged var increments: String?
    @NSManaged var min_duration: String?
    @NSManaged var owner_id: String?
    @NSManaged var owner_name: String?
    @NSManaged var parking_lot_id: String?
    @NSManaged var parking_lot_name: String?
    @NSManaged var parking_pymnt_mode: String?
    @NSManaged var slot_name: String?
    @NSManaged var start_date: String?
    @NSManaged var tenant_lot_id: String?
    @NSManaged var tenant_plan_id: String?
    @NSManaged var tenant_slot_id: String?
    @NSManaged var parking_total_slots: String?
    @NSManaged var available_slots: String?
}
