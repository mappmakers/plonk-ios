//
//  PKUserParkingDetail+CoreDataProperties.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 02/12/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PKUserParkingDetail {

    @NSManaged var amount: String?
    @NSManaged var creation_date: String?
    @NSManaged var display_message: String?
    @NSManaged var entry_time: String?
    @NSManaged var exit_time: String?
    @NSManaged var exitDate: NSDate?
    @NSManaged var fine_amount: String?
    @NSManaged var fine_status: String?
    @NSManaged var make_name: String?
    @NSManaged var model_name: String?
    @NSManaged var modification_date: String?
    @NSManaged var paid_status: String?
    @NSManaged var parking_lot_name: String?
    @NSManaged var payment_mode: String?
    @NSManaged var registeration_no: String?
    @NSManaged var slot_name: String?
    @NSManaged var tenant_slot_id: String?
    @NSManaged var tenant_user_id: String?
    @NSManaged var total_duration: String?
    @NSManaged var user_name: String?
    @NSManaged var user_parking_id: String?
    @NSManaged var user_vehicle_id: String?

}
