//
//  PKParkingSlot.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation
import CoreData

class PKParkingSlot: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    static func parkingSlotWithDictionaryData(parkingLotDictionary: Dictionary<String, AnyObject>) -> PKParkingSlot {
        let aParkingSlot = PKParkingSlot.MR_createEntityInContext(nil)
        
        aParkingSlot.difference = parkingLotDictionary["difference"] as? String
        aParkingSlot.end_date = parkingLotDictionary["end_date"] as? String
        
        aParkingSlot.owner_id = parkingLotDictionary["owner_id"] as? String
        aParkingSlot.owner_name = parkingLotDictionary["owner_name"] as? String
        
        aParkingSlot.parking_lot_name = parkingLotDictionary["parking_lot_name"] as? String
        aParkingSlot.parking_pymnt_mode = parkingLotDictionary["parking_pymnt_mode"] as? String
        aParkingSlot.slot_name = parkingLotDictionary["slot_name"] as? String
        
        aParkingSlot.tenant_plan_id = parkingLotDictionary["tenant_plan_id"] as? String
        aParkingSlot.tenant_slot_id = parkingLotDictionary["tenant_slot_id"] as? String
        aParkingSlot.start_date = parkingLotDictionary["start_date"] as? String
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        
        let parsedEndTime = parkingLotDictionary["end_time"] as? String
        var endTime = "23:59:59"
        
        endTime = parsedEndTime! == "24:00:00" ? endTime: parsedEndTime!
        
        let exitTime = dateFormatter.dateFromString(endTime)
        
        dateFormatter.dateFormat = "hh:mm a"
        let formattedExitTime = dateFormatter.stringFromDate(exitTime!)
        
        aParkingSlot.end_time = formattedExitTime

        
        aParkingSlot.updateSlotWithDictionary(parkingLotDictionary)
        
        return aParkingSlot
    }

    func updateSlotWithDictionary(parkingLotDictionary: Dictionary<String, AnyObject>) {
        
        parking_lot_id = parkingLotDictionary["parking_lot_id"] as? String
        tenant_lot_id = parkingLotDictionary["tenant_lot_id"] as? String
        display_message = parkingLotDictionary["display_message"] as? String
        extra_charge = parkingLotDictionary["extra_charge"] as? String
        fare = parkingLotDictionary["fare"] as? String
        
        let minimumRawValue = parkingLotDictionary["min_duration"] as? String
        
        let timeRangeCompenents = minimumRawValue?.componentsSeparatedByString(":")
        
        min_duration = ""
        
        if timeRangeCompenents?.count == 3 {
            let hour = timeRangeCompenents![0] == "00" ? "": "\(NSString(string: timeRangeCompenents![0]).intValue)hr"
            let minute = timeRangeCompenents![1] == "00" ? "" : "\(NSString(string: timeRangeCompenents![1]).intValue)min"
            let seconds = timeRangeCompenents![2] == "00" ? "" : "\(NSString(string: timeRangeCompenents![2]).intValue)sec"
            
            min_duration = (hour.characters.count == 0 ? "" : hour)+(minute.characters.count == 0 ? "" : minute)+(seconds.characters.count == 0 ? "" : seconds)
            
        }
        
        let incrementRawValue = parkingLotDictionary["increments"] as? String
        
        let incrementRangeCompenents = incrementRawValue?.componentsSeparatedByString(":")
        
        increments = ""
        
        if incrementRangeCompenents?.count == 3 {
            let hour = incrementRangeCompenents![0] == "00" ? "": "\(NSString(string: incrementRangeCompenents![0]).intValue)hr"
            let minute = incrementRangeCompenents![1] == "00" ? "" : "\(NSString(string: incrementRangeCompenents![1]).intValue)min"
            let seconds = incrementRangeCompenents![2] == "00" ? "" : "\(NSString(string: incrementRangeCompenents![2]).intValue)sec"
            
            increments = (hour.characters.count == 0 ? "" : hour)+(minute.characters.count == 0 ? "" : minute)+(seconds.characters.count == 0 ? "" : seconds)
            
        }
        
        if let totalSlots = parkingLotDictionary["parking_total_slots"] as? String {
            
            parking_total_slots = totalSlots
            
        }
        
        if let availableSlots = parkingLotDictionary["available_slots"] as? String {
            
            available_slots = availableSlots
            
        }
    }
}
