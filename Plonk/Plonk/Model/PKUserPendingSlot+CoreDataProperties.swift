//
//  PKUserPendingSlot+CoreDataProperties.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 28/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PKUserPendingSlot {

    @NSManaged var parking_lot_address: String?
    @NSManaged var parking_lot_id: String?
    @NSManaged var parking_lot_landmark: String?
    @NSManaged var parking_lot_name: String?
    @NSManaged var tenant_lot_id: String?
    @NSManaged var user_id: String?

}
