//
//  PKUserParkingDetail.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation
import CoreData

class PKUserParkingDetail: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    static func userParkingHistoryWithParameters(parameters:Dictionary<String,AnyObject>)-> PKUserParkingDetail {
        
        let userParkingDetail = PKUserParkingDetail.MR_createEntityInContext(NSManagedObjectContext.MR_defaultContext())
        
        userParkingDetail.updateUserParkingWithDictionary(parameters)

        NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreWithCompletion { (success, error) -> Void in
            
            if (success) {
                print("You successfully saved your context.");
            } else if ((error) != nil) {
                print("Error saving context: %@", error.description);
            }
            
        }
        
        return userParkingDetail
        
    }
    
    func updateUserParkingWithDictionary(parameters:Dictionary<String,AnyObject>) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let entryDate = dateFormatter.dateFromString(parameters["entry_time"] as? String ?? "")
        
        exitDate = dateFormatter.dateFromString(parameters["exit_time"] as? String ?? "")
        
        dateFormatter.dateFormat = "hh:mm a 'on' dd MMM, yyyy"
        
        entry_time = dateFormatter.stringFromDate(entryDate!)
        exit_time = dateFormatter.stringFromDate((exitDate == nil ? entryDate : exitDate)!)

        make_name = parameters["make_name"] as? String
        parking_lot_name = parameters["parking_lot_name"] as? String
        registeration_no = parameters["registeration_no"] as? String
        slot_name = parameters["slot_name"] as? String
        user_name = parameters["user_name"] as? String
        user_parking_id = parameters["user_parking_id"] as? String
        model_name = parameters["model_name"] as? String

        let totalTime = parameters["total_duration"] as? String
        let timeComponents = totalTime?.componentsSeparatedByString(":")
        if timeComponents?.count == 3 {
            var timeIntegerStrings:[String] = []
            for timeText in timeComponents! {
                let timeInt:Int = Int(timeText)!
                timeIntegerStrings.append(String(format: "%02d", timeInt))
            }
            
            total_duration = timeIntegerStrings.joinWithSeparator(":")
        }
        
        let finalAmount = parameters["amount"]
        amount = "\(finalAmount!)"
        
        let fineStatus = parameters["fine_status"]
        if fineStatus != nil {
            fine_status = "\(fineStatus!)"
            
            if fine_status == "1" {
                let fineAmount = parameters["fine_amount"]
                fine_amount = "\(fineAmount!)"
            }
        }
        
        payment_mode = parameters["payment_mode"] as? String

    }
}
