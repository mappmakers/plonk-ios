//
//  PKParkingLot+CoreDataProperties.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 17/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PKParkingLot {

    @NSManaged var creation_date: String?
    @NSManaged var difference: String?
    @NSManaged var distance: String?
    @NSManaged var fare_range: String?
    @NSManaged var favourite_count: String?
    @NSManaged var modification_date: String?
    @NSManaged var parking_city: String?
    @NSManaged var parking_days: String?
    @NSManaged var parking_filled_slots: String?
    @NSManaged var parking_lot_address: String?
    @NSManaged var parking_lot_id: String?
    @NSManaged var parking_lot_image: String?
    @NSManaged var parking_lot_landmark: String?
    @NSManaged var parking_lot_latitude: String?
    @NSManaged var parking_lot_longitude: String?
    @NSManaged var parking_lot_name: String?
    @NSManaged var parking_lot_status: String?
    @NSManaged var parking_pymnt_mode: String?
    @NSManaged var parking_qrcode_image: String?
    @NSManaged var parking_region: String?
    @NSManaged var parking_total_slots: String?
    @NSManaged var parking_type: String?
    @NSManaged var tenant_lot_id: String?
    @NSManaged var tenant_plan_id: String?
    @NSManaged var tenant_slot_id: String?
    @NSManaged var time_range: String?
    @NSManaged var rating: String?
    @NSManaged var favourite_id: String?
}
