//
//  PKUserPendingSlot.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 28/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation
import CoreData

class PKUserPendingSlot: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    static  func userParkingLotWithParameters(parameters:Dictionary<String,AnyObject>)-> PKUserPendingSlot {
        
        let userParkingLot = PKUserPendingSlot.MR_createEntityInContext(NSManagedObjectContext.MR_defaultContext())
        
        userParkingLot.parking_lot_address = parameters["tenant_lot_address"] as? String
        userParkingLot.parking_lot_id = parameters["parking_lot_id"] as? String
        userParkingLot.parking_lot_landmark = parameters["tenant_lot_landmark"] as? String
        userParkingLot.parking_lot_name = parameters["tenant_lot_name"] as? String
        userParkingLot.tenant_lot_id = parameters["tenant_lot_id"] as? String
        
        return userParkingLot
        
    }

    

}
