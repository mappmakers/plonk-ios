//
//  PKParkingLot.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation
import CoreData

//{"favourite_id":"13","user_id":"94","":"70","":"Center square mall","":"Ernakulam","":"9.977957","":"76.28321000000005","":"South,ernakulam","":"7","":"20-35","":"07:00:00-23:30:00","":"1,2,3,4,5","":"open","":"3"}

class PKParkingLot: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    static func parkingLotWithDictionaryData(parkingLotDictionary: Dictionary<String, AnyObject>) -> PKParkingLot {
        let aParkingLot = PKParkingLot.MR_createEntityInContext(nil)
        
        aParkingLot.updateParkingLotWithInfo(parkingLotDictionary)

        aParkingLot.parking_pymnt_mode = parkingLotDictionary["parking_pymnt_mode"] as? String//
        aParkingLot.parking_qrcode_image = parkingLotDictionary["parking_qrcode_image"] as? String//
        aParkingLot.parking_city = parkingLotDictionary["parking_city"] as? String//
        aParkingLot.parking_filled_slots = parkingLotDictionary["parking_filled_slots"] as? String//
        aParkingLot.parking_region = parkingLotDictionary["parking_region"] as? String//
        aParkingLot.tenant_lot_id = parkingLotDictionary["tenant_lot_id"] as? String//
        aParkingLot.tenant_plan_id = parkingLotDictionary["tenant_plan_id"] as? String//
        aParkingLot.tenant_slot_id = parkingLotDictionary["tenant_slot_id"] as? String//
        aParkingLot.distance = parkingLotDictionary["distance"] as? String//
        aParkingLot.favourite_count = parkingLotDictionary["favourite_count"] as? String//
        
        return aParkingLot
    }
    
    func updateParkingLotWithInfo(parkingLotDictionary: Dictionary<String, AnyObject>) {
        
        parking_lot_image = (parkingLotDictionary["parking_lot_image"] as? String) ?? ""//
        rating = (parkingLotDictionary["rating"] as? String) ?? "0"//
        
        parking_lot_address = parkingLotDictionary["parking_lot_address"] as? String
        parking_lot_id = parkingLotDictionary["parking_lot_id"] as? String
        
        parking_lot_landmark = parkingLotDictionary["parking_lot_landmark"] as? String
        parking_lot_latitude = parkingLotDictionary["parking_lot_latitude"] as? String
        parking_lot_longitude = parkingLotDictionary["parking_lot_longitude"] as? String
        parking_lot_name = parkingLotDictionary["parking_lot_name"] as? String
        
        parking_total_slots = parkingLotDictionary["parking_total_slots"] as? String
        
        parking_days = parkingLotDictionary["parking_days"] as? String
        
        
        parking_type = parkingLotDictionary["parking_type"] as? String
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        
        let timeRange = parkingLotDictionary["time_range"] as? String
        
        let timeRangeCompenents = timeRange?.componentsSeparatedByString("-")
        
        var startTime = "00:00:00"
        var endTime = "23:59:59"
        
        if timeRangeCompenents?.count == 2 {
            startTime = timeRangeCompenents![0]
            endTime = timeRangeCompenents![1] == "24:00:00" ? endTime: timeRangeCompenents![1]
        }
        
        let entryTime = dateFormatter.dateFromString(startTime)
        let exitTime = dateFormatter.dateFromString(endTime)
        
        dateFormatter.dateFormat = "hh:mm a"
        let formattedEntryTime = dateFormatter.stringFromDate(entryTime!)
        let formattedExitTime = dateFormatter.stringFromDate(exitTime!)
        
        time_range = "\(formattedEntryTime) - \(formattedExitTime)"
        difference = parkingLotDictionary["difference"] as? String
        fare_range = parkingLotDictionary["fare_range"] as? String
        
    }
}
