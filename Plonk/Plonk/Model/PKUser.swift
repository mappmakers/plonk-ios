//
//  PKUser.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation
import CoreData

class PKUser: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    static func userWithDictionaryData(userDictionary: Dictionary<String, AnyObject>) -> PKUser {
        let aUser = PKUser.MR_createEntityInContext(NSManagedObjectContext.MR_defaultContext())
        
        aUser.otp_verified = userDictionary["otp_status"] as? String
        aUser.user_cons_no = userDictionary["user_consumerno"] as? String
        aUser.user_email = userDictionary["user_email"] as? String
        let aUserId = userDictionary["user_id"]
        aUser.user_id = "\(aUserId!)"
        aUser.user_mobileno = userDictionary["user_mobile"] as? String
        aUser.user_name = userDictionary["user_name"] as? String
        aUser.user_place_mark = userDictionary["user_place_mark"] as? String
        
        NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreWithCompletion { (success, error) -> Void in
            
            if (success) {
                print("You successfully saved your context.");
            } else if ((error) != nil) {
                print("Error saving context: %@", error.description);
            }
            
        }
        
        return aUser
    }
}
