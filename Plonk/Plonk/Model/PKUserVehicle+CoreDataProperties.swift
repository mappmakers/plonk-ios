//
//  PKUserVehicle+CoreDataProperties.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PKUserVehicle {

    @NSManaged var creation_date: String?
    @NSManaged var make_id: String?
    @NSManaged var model_id: String?
    @NSManaged var modification_date: String?
    @NSManaged var registeration_no: String?
    @NSManaged var status: String?
    @NSManaged var user_id: String?
    @NSManaged var user_vehicle_id: String?
    @NSManaged var vehicle_type: String?
    @NSManaged var make_name: String?
    @NSManaged var model_name: String?

}
