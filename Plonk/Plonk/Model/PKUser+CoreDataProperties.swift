//
//  PKUser+CoreDataProperties.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PKUser {

    @NSManaged var user_image_path: String?
    @NSManaged var creation_date: String?
    @NSManaged var device_key: String?
    @NSManaged var device_type: String?
    @NSManaged var fb_oauth_id: String?
    @NSManaged var google_oauth_id: String?
    @NSManaged var modification_date: String?
    @NSManaged var otp_pasword: String?
    @NSManaged var otp_verified: String?
    @NSManaged var user_alterate_no: String?
    @NSManaged var user_cons_no: String?
    @NSManaged var user_email: String?
    @NSManaged var user_id: String?
    @NSManaged var user_mobileno: String?
    @NSManaged var user_name: String?
    @NSManaged var user_password: String?
    @NSManaged var user_place_mark: String?
    @NSManaged var user_status: String?
    @NSManaged var favourites: NSSet?
    @NSManaged var parkingDetails: NSSet?
    @NSManaged var vehicles: NSSet?

}
