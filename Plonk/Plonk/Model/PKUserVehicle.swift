//
//  PKUserVehicle.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation
import CoreData

class PKUserVehicle: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    static func vehicleWithDictionaryData(vehicleDictionary: Dictionary<String, AnyObject>) -> PKUserVehicle {
        let aVehicle = PKUserVehicle.MR_createEntityInContext(nil)
        
        aVehicle.make_id = vehicleDictionary["make_id"] as? String
        aVehicle.make_name = vehicleDictionary["make_name"] as? String
        aVehicle.model_id = vehicleDictionary["model_id"] as? String
        aVehicle.model_name = vehicleDictionary["model_name"] as? String
        aVehicle.registeration_no = vehicleDictionary["registeration_no"] as? String
        aVehicle.user_id = vehicleDictionary["user_id"] as? String
        aVehicle.user_vehicle_id = vehicleDictionary["user_vehicle_id"] as? String
        aVehicle.vehicle_type = vehicleDictionary["vehicle_type"] as? String
        
        return aVehicle
    }
    
    static func vehicleListWithDictionaryData(vehicleDictionary: Dictionary<String, AnyObject>) -> PKUserVehicle {
        let aVehicle = PKUserVehicle.MR_createEntityInContext(nil)
        
        aVehicle.make_id = vehicleDictionary["make_id"] as? String
        aVehicle.make_name = vehicleDictionary["make_name"] as? String
        aVehicle.model_id = vehicleDictionary["model_id"] as? String
        aVehicle.model_name = vehicleDictionary["model_name"] as? String
        aVehicle.vehicle_type = vehicleDictionary["vehicle_type"] as? String
        
        return aVehicle
    }
    
}
