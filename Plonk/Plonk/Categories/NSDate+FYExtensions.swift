//
//  NSDate+FYExtensions.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 30/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation

extension NSDate {
    var startOfDay: NSDate {
        return NSCalendar.currentCalendar().startOfDayForDate(self)
    }
    
    var endOfDay: NSDate? {
        let components = NSDateComponents()
        components.day = 1
        components.second = -1
        return NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: startOfDay, options: NSCalendarOptions())
    }
    
    var maxStartDay: NSDate? {
        let components = NSDateComponents()
        components.day = 1
        components.second = -2
        return NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: startOfDay, options: NSCalendarOptions())
    }
}