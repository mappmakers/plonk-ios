//
//  UILabel+AddOns.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit


extension UILabel {
    
    func makeCenteredWhiteRegularLabel() {
        
        font = UIFont.PKAllerRegular(PKConstants.FontSmallSize)
        backgroundColor = UIColor.clearColor()
        textAlignment = NSTextAlignment.Center
        textColor = UIColor.whiteColor()
        
    }
    
}