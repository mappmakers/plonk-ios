//
//  UITableViewCelll+AddOns.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 21/09/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit

extension UITableViewCell {
    
    public static func headerCell() -> MKTableViewCell {
        let cell = MKTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: PKConstants.CellHeaderIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
//        cell.rippleLocation = .TapLocation
//        cell.rippleLayerColor = UIColor.appThemeColor()
        
        let headerImage = MKImageView(image: UIImage(named: "ic_plonk_logo"))
//        headerImage.rippleLocation = .TapLocation
//        headerImage.rippleLayerColor = UIColor.appThemeColor()
//        headerImage.backgroundColor = UIColor.redColor()
        headerImage.tag = 1
        cell.contentView.addSubview(headerImage)
        
        headerImage.translatesAutoresizingMaskIntoConstraints = false
        headerImage.addCenterXConstraint(cell.contentView)
        headerImage.addCenterYConstraint(cell.contentView, offset:  -10)
        headerImage.addHeightConstraint(PKConstants.LogoImageViewHeight)
        headerImage.addWidthConstraint(PKConstants.LogoImageViewWidth)
        
        let headerLabel = MKLabel(frame: CGRectZero)
//        headerLabel.rippleLayerColor = UIColor.appThemeColor()
//        headerLabel.rippleLocation = .TapLocation
//        headerLabel.backgroundColor = UIColor.greenColor()
        headerLabel.font = UIFont.PKLaChataNormal(21)
        headerLabel.text = PKConstants.AppTitle
        headerLabel.textAlignment = NSTextAlignment.Center
        headerLabel.textColor = UIColor.whiteColor()
        headerLabel.tag = 2
        cell.contentView.addSubview(headerLabel)
        
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        headerLabel.addLeftEdgeAttachConstraint(headerImage)
        headerLabel.addRightEdgeAttachConstraint(headerImage)
        headerLabel.addTopEdgeAttachConstraint(headerImage, viewEdge: NSLayoutAttribute.Bottom, offset: PKConstants.LogoImageViewVerticalBorderOffset)
        headerLabel.addHeightConstraint(21)
        
        return cell
    }
    
}