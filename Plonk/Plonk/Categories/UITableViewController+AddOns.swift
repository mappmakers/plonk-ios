//
//  UITableViewController+AddOns.swift
//  maersk
//
//  Created by Focaloid Technologies Pvt. Ltd on 20/08/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

extension UITableViewController {
    
    func showThemeBackground() {
        let imageView = UIImageView(image: UIImage(named: "MKSplash"))
        self.tableView.backgroundView = imageView
        
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        self.tableView.separatorColor = UIColor.appThemeColor()
    }
    
    func customizeForStaticTableStyle() {
        self.tableView.bounces = false
        self.tableView.bouncesZoom = false
        self.tableView.showsVerticalScrollIndicator = false;
        self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.Interactive
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
    }
    
    func addTapToDismiss() {
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tapped")
        tapGesture.numberOfTapsRequired = 1
        self.tableView.addGestureRecognizer(tapGesture)
    }
    
    func tapped() {
        self.view.endEditing(true)
    }
}