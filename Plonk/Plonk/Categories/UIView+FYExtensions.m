//
//  UIView+FYExtensions.m
//  FYIOSLite
//
//  Created by Farhan Yousuf on 10/06/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

#import "UIView+FYExtensions.h"
#import <QuartzCore/QuartzCore.h>
#import <GLKit/GLKMath.h>

@implementation UIView (FYExtensions)

#pragma mark - Class Methods

+ (void) alignView:(UIView *)viewToLayout toLeftOfView:(UIView *)rightView insideOfView:(UIView *)viewToFit withWidth:(CGFloat)width padding:(CGFloat)padding
{
    //AUTO LAYOUT Cell Object
    
    
    [viewToLayout setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint * constraint;
    
    //if its the first object, align its right edge to the right of the cell's content view
    if (rightView == nil)
    {
        //right
        constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                                  attribute:NSLayoutAttributeRight
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:viewToFit
                                                  attribute:NSLayoutAttributeRight
                                                 multiplier:1.0f
                                                   constant:-padding];
        [viewToFit addConstraint:constraint];
        
    }
    else //other wise align its right edge to the left of the previous object
    {
        
        
        //right
        constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                                  attribute:NSLayoutAttributeRight
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:rightView
                                                  attribute:NSLayoutAttributeLeft
                                                 multiplier:1.0f
                                                   constant:-padding];
        [viewToFit addConstraint:constraint];
    }
    
    //width
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeWidth
                                              relatedBy:nil
                                                 toItem:nil
                                              attribute:nil
                                             multiplier:1.0f
                                               constant:width];
    [viewToFit addConstraint:constraint];
    
    
    //top
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeTop
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:viewToFit
                                              attribute:NSLayoutAttributeTop
                                             multiplier:1.0f
                                               constant:0.0f];
    [viewToFit addConstraint:constraint];
    
    
    //bottom
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeBottom
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:viewToFit
                                              attribute:NSLayoutAttributeBottom
                                             multiplier:1.0f
                                               constant:0.0f];
    [viewToFit addConstraint:constraint];
    
    
    
    [viewToFit layoutIfNeeded];
}

+ (void) alignView:(UIView *)viewToLayout toRightOfView:(UIView *)leftView insideOfView:(UIView *)viewToFit withWidth:(CGFloat)width padding:(CGFloat)padding
{
    //AUTO LAYOUT Cell Object
    
    
    [viewToLayout setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint * constraint;
    
    //if its the first object, align its left edge to the left of the cell's content view
    if (leftView == nil)
    {
        //right
        constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                                  attribute:NSLayoutAttributeLeft
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:viewToFit
                                                  attribute:NSLayoutAttributeLeft
                                                 multiplier:1.0f
                                                   constant:padding];
        [viewToFit addConstraint:constraint];
        
    }
    else //other wise align its left edge to the right of the previous object
    {
        
        
        //right
        constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                                  attribute:NSLayoutAttributeLeft
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:leftView
                                                  attribute:NSLayoutAttributeRight
                                                 multiplier:1.0f
                                                   constant:padding];
        [viewToFit addConstraint:constraint];
    }
    
    //width
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeWidth
                                              relatedBy:nil
                                                 toItem:nil
                                              attribute:nil
                                             multiplier:1.0f
                                               constant:width];
    [viewToFit addConstraint:constraint];
    
    
    //top
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeTop
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:viewToFit
                                              attribute:NSLayoutAttributeTop
                                             multiplier:1.0f
                                               constant:0.0f];
    [viewToFit addConstraint:constraint];
    
    
    //bottom
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeBottom
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:viewToFit
                                              attribute:NSLayoutAttributeBottom
                                             multiplier:1.0f
                                               constant:0.0f];
    [viewToFit addConstraint:constraint];
    
    [leftView setNeedsUpdateConstraints];
    [leftView setNeedsLayout];
    [leftView layoutIfNeeded];
    
    [viewToLayout setNeedsUpdateConstraints];
    [viewToLayout setNeedsLayout];
    [viewToLayout layoutIfNeeded];
    
    
    [viewToFit setNeedsUpdateConstraints];
    [viewToFit setNeedsLayout];
    [viewToFit layoutIfNeeded];
}

+ (void)layoutView:(UIView *)viewToLayout fitView:(UIView *)viewToFit transparent:(BOOL)transparent
{
    [viewToFit addSubview:viewToLayout];
    
    [viewToLayout setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint * constraint;
    
    //left
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeLeft
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:viewToFit
                                              attribute:NSLayoutAttributeLeft
                                             multiplier:1.0f
                                               constant:0.0f];
    [viewToFit addConstraint:constraint];
    
    //right
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeRight
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:viewToFit
                                              attribute:NSLayoutAttributeRight
                                             multiplier:1.0f
                                               constant:0.0f];
    [viewToFit addConstraint:constraint];
    
    //top
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeTop
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:viewToFit
                                              attribute:NSLayoutAttributeTop
                                             multiplier:1.0f
                                               constant:0.0f];
    [viewToFit addConstraint:constraint];
    
    
    //bottom
    constraint = [NSLayoutConstraint constraintWithItem:viewToLayout
                                              attribute:NSLayoutAttributeBaseline
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:viewToFit
                                              attribute:NSLayoutAttributeBottom
                                             multiplier:1.0f
                                               constant:0.0f];
    [viewToFit addConstraint:constraint];
    
    [viewToFit layoutIfNeeded];
    
    if (transparent)
    {
        [viewToLayout setBackgroundColor:[UIColor clearColor]];
    }
    
}




+ (void)setAnchorPoint:(CGPoint)anchorPoint forView:(UIView *)view
{
    CGPoint newPoint = CGPointMake(view.bounds.size.width * anchorPoint.x, view.bounds.size.height * anchorPoint.y);
    CGPoint oldPoint = CGPointMake(view.bounds.size.width * view.layer.anchorPoint.x, view.bounds.size.height * view.layer.anchorPoint.y);
    
    newPoint = CGPointApplyAffineTransform(newPoint, view.transform);
    oldPoint = CGPointApplyAffineTransform(oldPoint, view.transform);
    
    CGPoint position = view.layer.position;
    
    position.x -= oldPoint.x;
    position.x += newPoint.x;
    
    position.y -= oldPoint.y;
    position.y += newPoint.y;
    
    view.layer.position = position;
    view.layer.anchorPoint = anchorPoint;
}


#pragma mark - View styling

- (void) addDropShadowToEdge:(enum UIViewEdge)edge
{
    CGSize shadowOffset;
    
    switch (edge) {
        case UIViewEdgeTop:
            shadowOffset = CGSizeMake(1.0, 1.0);
            break;
        case UIViewEdgeRight:
            shadowOffset = CGSizeMake(1.0, 1.0);
            break;
        case UIViewEdgeBottom:
            shadowOffset = CGSizeMake(1.0, 1.0);
            break;
        case UIViewEdgeLeft:
            shadowOffset = CGSizeMake(-1.0, 1.0);
            break;
            
        default:
            shadowOffset = CGSizeMake(1.0, 1.0);
            break;
    }
    
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.layer setShadowOffset:shadowOffset];
    [self.layer setShadowRadius:1.0];
    [self.layer setShadowOpacity:1.0];
    [self.layer setShadowPath:[UIBezierPath bezierPathWithRect: CGRectMake( self.bounds.origin.x, self.bounds.origin.y, 1, self.frame.size.height)].CGPath];
}

#pragma mark -  animations
+ (float) animationDuration
{
    return .5;
}
+ (float) animationDelay
{
    return 0;
}
+ (float) animationSpringVelocity
{
    return 1;
}
+ (float) animationSpringDamping
{
    return 1;
}
+ (UIViewAnimationOptions) animationOption
{
    return UIViewAnimationOptionCurveEaseIn;
}



//------------------------------------------------------------------------------
// MARK: - Find Constraints
//------------------------------------------------------------------------------


- (NSLayoutConstraint *)myConstraintWithAttribute:(NSLayoutAttribute)attribute
{
    /* Find constraint with attribute in my constraints */
    __block NSLayoutConstraint *resultConstraint;
    [self.constraints enumerateObjectsUsingBlock:^(NSLayoutConstraint *constraint, NSUInteger idx, BOOL *stop)
     {
         //  DebugLog(@"constraint %@", constraint);
         if ([NSStringFromClass([NSLayoutConstraint class]) isEqualToString:NSStringFromClass([constraint class])])
         {
             if (constraint.firstAttribute == attribute || constraint.secondAttribute == attribute)
             {
                 resultConstraint = constraint;
                 *stop = YES;
             }
         }
     }];
    
    return resultConstraint;
}


- (NSLayoutConstraint *)superviewConstraintWithAttribute:(NSLayoutAttribute)attribute
{
    /* Find constraint with attribute in my superview's constraints */
    __block NSLayoutConstraint *resultConstraint;
    [self.superview.constraints enumerateObjectsUsingBlock:^(NSLayoutConstraint *constraint, NSUInteger idx, BOOL *stop)
     {
         if (constraint.firstItem == self && constraint.firstAttribute == attribute)
             //|| (constraint.secondItem == self && constraint.secondAttribute == attribute))
         {
             resultConstraint = constraint;
             *stop = YES;
         }
     }];
    
    return resultConstraint;
}


- (NSLayoutConstraint *)constraintWithAttribute:(NSLayoutAttribute)attribute
{
    /* Find constraint with attribute in my constraints */
    NSLayoutConstraint *resultConstraint = [self myConstraintWithAttribute:attribute];
    
    /* Find constraint with attribute in my superview's constraints */
    if (!resultConstraint)
    {
        resultConstraint = [self superviewConstraintWithAttribute:attribute];
    }
    return resultConstraint;
}


- (BOOL)removeConstraintWithAttribute:(NSLayoutAttribute)attribute
{
    NSLayoutConstraint *constraint = [self superviewConstraintWithAttribute:attribute];
    if (constraint)
    {
        [self.superview removeConstraint:constraint];
        return YES;
    }
    constraint = [self myConstraintWithAttribute:attribute];
    if (constraint)
    {
        [self removeConstraint:constraint];
        return YES;
    }
    return NO;
}




//------------------------------------------------------------------------------
// MARK: - Remove Constraints
//------------------------------------------------------------------------------


- (void)removeMyConstraints
{
    /* Remove all my constraitns from superview */
    [self.superview removeConstraints:[self mySuperviewConstraints]];
    
    /* Remove my constraitns */
    
    [self removeConstraints:self.constraints];
}


- (NSArray *)mySuperviewConstraints
{
    NSMutableArray *mySuperviewConstraints = [NSMutableArray array];
    [self.superview.constraints enumerateObjectsUsingBlock:^(NSLayoutConstraint *constraint, NSUInteger idx, BOOL *stop)
     {
         if (constraint.firstItem == self || constraint.secondItem == self)
         {
             [mySuperviewConstraints addObject:constraint];
         }
     }];
    return mySuperviewConstraints;
}


- (void)removeMyConstraintsButKeepMySubviewConstraints
{
    /* Remove all my constraitns from superview */
    [self.superview removeConstraints:[self mySuperviewConstraints]];
    
    /* Remove my constraitns */
    
    [self removeConstraints:[self myConstraints]];
}


- (NSArray *)myConstraints
{
    NSMutableArray *myConstraints = [NSMutableArray array];
    [self.constraints enumerateObjectsUsingBlock:^(NSLayoutConstraint *constraint, NSUInteger idx, BOOL *stop)
     {
         if (constraint.firstItem == self && constraint.secondItem == nil)
         {
             [myConstraints addObject:constraint];
         }
     }];
    return myConstraints;
}




//------------------------------------------------------------------------------
// MARK: - Size Constraints
//------------------------------------------------------------------------------


- (void)addWidthConstraint:(CGFloat)width
{
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:0
                                                                 multiplier:1
                                                                   constant:width];
    [self addConstraint:constraint];
}

- (void)addWidthConstraintFromLabel:(UILabel *)label
                         withOffset:(CGFloat)offset
{
    NSDictionary *attributes = @{NSFontAttributeName : label.font};
    return [self addWidthConstraint:[label.text sizeWithAttributes:attributes].width + offset];
}

- (void)addHeightConstraint:(CGFloat)height
{
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:0
                                                                 multiplier:1
                                                                   constant:height];
    [self addConstraint:constraint];
}

- (void)addMaximumHeightConstraint:(CGFloat)maxHeight
{
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationLessThanOrEqual
                                                                     toItem:nil
                                                                  attribute:0
                                                                 multiplier:1
                                                                   constant:maxHeight];
    [self addConstraint:constraint];
    
}


- (void)addWidthConstraintFromImage:(UIImage *)image
{
    [self addWidthConstraint:image.size.width];
}


- (void)addHeightConstraintFromImage:(UIImage *)image
{
    [self addHeightConstraint:image.size.height];
}




//------------------------------------------------------------------------------
// MARK: - Center Contraints
//------------------------------------------------------------------------------


- (void)addCenterConstraint:(UIView *)view
            centerDirection:(NSLayoutAttribute)centerDirection
                     offset:(CGFloat)offset
{
    UIView *viewItem = (view) ? view : self.superview;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:centerDirection
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:viewItem
                                                                  attribute:centerDirection
                                                                 multiplier:1
                                                                   constant:offset];
    [self.superview addConstraint:constraint];
}


- (void)addCenterXConstraint:(UIView *)view
{
    [self addCenterConstraint:view
              centerDirection:NSLayoutAttributeCenterX
                       offset:0];
}


- (void)addCenterYConstraint:(UIView *)view
{
    [self addCenterConstraint:view
              centerDirection:NSLayoutAttributeCenterY
                       offset:0];
}


- (void)addCenterXConstraint:(UIView *)view
                      offset:(CGFloat)offset
{
    [self addCenterConstraint:view
              centerDirection:NSLayoutAttributeCenterX
                       offset:offset];
}


- (void)addCenterYConstraint:(UIView *)view
                      offset:(CGFloat)offset
{
    [self addCenterConstraint:view
              centerDirection:NSLayoutAttributeCenterY
                       offset:offset];
}




//------------------------------------------------------------------------------
// MARK: - Edge Attach Constraints
//------------------------------------------------------------------------------


- (void)addEdgeAttachConstraint:(UIView *)view
                       viewEdge:(NSLayoutAttribute)viewLayoutAttribute
                         offset:(CGFloat)offset
                           edge:(NSLayoutAttribute)layoutAttribute
{
    UIView *viewItem = (view) ? view : self.superview;
    
    /* Reverse offset for right side and bottom */
    CGFloat fixedOffset = offset;
    if (layoutAttribute == NSLayoutAttributeRight
        || layoutAttribute == NSLayoutAttributeBottom
        || layoutAttribute == NSLayoutAttributeTrailing)
    {
        fixedOffset = -offset;
    }
    
    /* Add contraint */
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:layoutAttribute
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:viewItem
                                                                  attribute:viewLayoutAttribute
                                                                 multiplier:1
                                                                   constant:fixedOffset];
    [self.superview addConstraint:constraint];
}




- (void)addLeftEdgeAttachConstraint:(UIView *)view
                             offset:(CGFloat)offset
{
    [self addEdgeAttachConstraint:view
                         viewEdge:NSLayoutAttributeLeft
                           offset:offset
                             edge:NSLayoutAttributeLeft];
}


- (void)addRightEdgeAttachConstraint:(UIView *)view
                              offset:(CGFloat)offset
{
    [self addEdgeAttachConstraint:view
                         viewEdge:NSLayoutAttributeRight
                           offset:offset
                             edge:NSLayoutAttributeRight];
}


- (void)addTopEdgeAttachConstraint:(UIView *)view
                            offset:(CGFloat)offset
{
    [self addEdgeAttachConstraint:view
                         viewEdge:NSLayoutAttributeTop
                           offset:offset
                             edge:NSLayoutAttributeTop];
}


- (void)addBottomEdgeAttachConstraint:(UIView *)view
                               offset:(CGFloat)offset
{
    [self addEdgeAttachConstraint:view
                         viewEdge:NSLayoutAttributeBottom
                           offset:offset
                             edge:NSLayoutAttributeBottom];
}




- (void)addLeftEdgeAttachConstraint:(UIView *)view
{
    [self addLeftEdgeAttachConstraint:view
                               offset:0];
}


- (void)addRightEdgeAttachConstraint:(UIView *)view
{
    [self addRightEdgeAttachConstraint:view
                                offset:0];
}


- (void)addTopEdgeAttachConstraint:(UIView *)view
{
    [self addTopEdgeAttachConstraint:view
                              offset:0];
}


- (void)addBottomEdgeAttachConstraint:(UIView *)view
{
    [self addBottomEdgeAttachConstraint:view
                                 offset:0];
}




- (void)addEdgeAttachConstraints:(UIView *)view
                      leftOffset:(CGFloat)leftOffset
                     rightOffset:(CGFloat)rightOffset
                       topOffset:(CGFloat)topOffset
                    bottomOffset:(CGFloat)bottomOffset
{
    [self addLeftEdgeAttachConstraint:view
                               offset:leftOffset];
    [self addRightEdgeAttachConstraint:view
                                offset:rightOffset];
    [self addTopEdgeAttachConstraint:view
                              offset:topOffset];
    [self addBottomEdgeAttachConstraint:view
                                 offset:bottomOffset];
}


- (void)addEdgeAttachConstraints:(UIView *)view
{
    [self addLeftEdgeAttachConstraint:view];
    [self addRightEdgeAttachConstraint:view];
    [self addTopEdgeAttachConstraint:view];
    [self addBottomEdgeAttachConstraint:view];
}




//------------------------------------------------------------------------------
// MARK: - Edge Constraint To Different Edge
//------------------------------------------------------------------------------


- (void)addLeftEdgeAttachConstraint:(UIView *)view
                           viewEdge:(NSLayoutAttribute)viewLayoutAttribute
                             offset:(CGFloat)offset
{
    [self addEdgeAttachConstraint:view
                         viewEdge:viewLayoutAttribute
                           offset:offset
                             edge:NSLayoutAttributeLeft];
}


- (void)addRightEdgeAttachConstraint:(UIView *)view
                            viewEdge:(NSLayoutAttribute)viewLayoutAttribute
                              offset:(CGFloat)offset
{
    [self addEdgeAttachConstraint:view
                         viewEdge:viewLayoutAttribute
                           offset:offset
                             edge:NSLayoutAttributeRight];
}


- (void)addTopEdgeAttachConstraint:(UIView *)view
                          viewEdge:(NSLayoutAttribute)viewLayoutAttribute
                            offset:(CGFloat)offset
{
    [self addEdgeAttachConstraint:view
                         viewEdge:viewLayoutAttribute
                           offset:offset
                             edge:NSLayoutAttributeTop];
}


- (void)addBottomEdgeAttachConstraint:(UIView *)view
                             viewEdge:(NSLayoutAttribute)viewLayoutAttribute
                               offset:(CGFloat)offset
{
    [self addEdgeAttachConstraint:view
                         viewEdge:viewLayoutAttribute
                           offset:offset
                             edge:NSLayoutAttributeBottom];
}






//------------------------------------------------------------------------------
// MARK: - Size Attach Constaints
//------------------------------------------------------------------------------


- (void)addSizeAndSuperviewAttachConstraints:(NSString *)sizeConstraint
                                 firstOffset:(CGFloat)firstOffset
                                secondOffset:(CGFloat)secondOffset
                                   direction:(NSString *)direction
{
    NSDictionary *viewDict = NSDictionaryOfVariableBindings(self);
    NSString *visualFormatString;
    
    if (sizeConstraint)
    {
        visualFormatString = [NSString stringWithFormat:@"%@:|-%f-[self(%@)]-%f-|", direction, firstOffset, sizeConstraint, secondOffset];
    }
    else
    {
        visualFormatString = [NSString stringWithFormat:@"%@:|-%f-[self]-%f-|", direction, firstOffset, secondOffset];
    }
    
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:visualFormatString
                                                                   options:0
                                                                   metrics:0
                                                                     views:viewDict];
    [self.superview addConstraints:constraints];
}


- (void)addWidthAndSuperviewAttachConstraints:(NSString *)widthConstraint
                                   leftOffset:(CGFloat)leftOffset
                                  rightOffset:(CGFloat)rightOffset
{
    [self addSizeAndSuperviewAttachConstraints:widthConstraint
                                   firstOffset:leftOffset
                                  secondOffset:rightOffset
                                     direction:@"H"];
}


- (void)addHeightAndSuperviewAttachConstraints:(NSString *)heightConstraint
                                     topOffset:(CGFloat)topOffset
                                  bottomOffset:(CGFloat)bottomOffset
{
    [self addSizeAndSuperviewAttachConstraints:heightConstraint
                                   firstOffset:topOffset
                                  secondOffset:bottomOffset
                                     direction:@"V"];
}




//------------------------------------------------------------------------------
// MARK: - Row & Column Layout Constraints
//------------------------------------------------------------------------------


- (void)addLayoutConstraintsForMySubviews:(NSArray *)views
                              firstOffset:(CGFloat)firstOffset
                             secondOffset:(CGFloat)secondOffset
                            betweenOffset:(NSString *)betweenOffset
                                direction:(NSString *)direction
                                equalSize:(BOOL)equalSize
{
    /* Create viewDict and visualFormatString */
    NSMutableString *visualFormatString = [[NSMutableString alloc] initWithFormat:@"%@:|-%.0f-", direction, firstOffset];
    NSMutableDictionary *viewDict = [[NSMutableDictionary alloc] init];
    [views enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL *stop)
     {
         NSString *viewName = [NSString stringWithFormat:@"view%i", idx];
         [viewDict setObject:view
                      forKey:viewName];
         
         if (idx < [views count] - 1)
         {
             /* Add each view */
             if (betweenOffset) /* Add offset between view */
             {
                 /* Add equal size to prev view for all but index 0 */
                 if (equalSize && idx > 0)
                 {
                     NSString *prevViewName = [NSString stringWithFormat:@"view%i", idx - 1];
                     [visualFormatString appendFormat:@"[%@(==%@)]-%@-", viewName, prevViewName, betweenOffset];
                 }
                 else
                 {
                     [visualFormatString appendFormat:@"[%@]-%@-", viewName, betweenOffset];
                 }
             }
             else /* No offset between views */
             {
                 /* Add equal size to prev view for all but index 0 */
                 if (equalSize && idx > 0)
                 {
                     NSString *prevViewName = [NSString stringWithFormat:@"view%i", idx - 1];
                     [visualFormatString appendFormat:@"[%@(==%@)]", viewName, prevViewName];
                 }
                 else
                 {
                     [visualFormatString appendFormat:@"[%@]", viewName];
                 }
             }
         }
         else
         {
             /* Add equal size to prev view for all but index 0 */
             if (equalSize && idx > 0)
             {
                 NSString *prevViewName = [NSString stringWithFormat:@"view%i", idx - 1];
                 [visualFormatString appendFormat:@"[%@(==%@)]-%.0f-|", viewName, prevViewName, secondOffset];
             }
             else
             {
                 [visualFormatString appendFormat:@"[%@]-%.0f-|", viewName, secondOffset];
             }
         }
     }];
    
    //    DebugLog(@"viewDict %@", viewDict);
    //    DebugLog(@"visualFormatString %@", visualFormatString);
    
    /* Add constraints */
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:visualFormatString
                                                                   options:0
                                                                   metrics:0
                                                                     views:viewDict];
    [self addConstraints:constraints];
}


- (void)addRowLayoutConstraintsForMySubviews:(NSArray *)subviews
                                  leftOffset:(CGFloat)leftOffset
                                 rightOffset:(CGFloat)rightOffset
                               betweenOffset:(NSString *)betweenOffset
                                  equalWidth:(BOOL)equalWidth
{
    [self addLayoutConstraintsForMySubviews:subviews
                                firstOffset:leftOffset
                               secondOffset:rightOffset
                              betweenOffset:betweenOffset
                                  direction:@"H"
                                  equalSize:equalWidth];
}


- (void)addColumnLayoutConstraintsForMySubviews:(NSArray *)subviews
                                      topOffset:(CGFloat)topOffset
                                   bottomOffset:(CGFloat)bottomOffset
                                  betweenOffset:(NSString *)betweenOffset
                                    equalHeight:(BOOL)equalHeight
{
    [self addLayoutConstraintsForMySubviews:subviews
                                firstOffset:topOffset
                               secondOffset:bottomOffset
                              betweenOffset:betweenOffset
                                  direction:@"V"
                                  equalSize:equalHeight];
}




//------------------------------------------------------------------------------
// MARK: - Row & Column Equal Size Layout Constraints
//------------------------------------------------------------------------------


- (void)addEqualSizeLayoutConstraintsForMySubviews:(NSArray *)views
                                       firstOffset:(CGFloat)firstOffset
                                      secondOffset:(CGFloat)secondOffset
                                     betweenOffset:(NSString *)betweenOffset
                                         direction:(NSString *)direction
{
    /* Create viewDict and visualFormatString */
    NSMutableString *visualFormatString = [[NSMutableString alloc] initWithFormat:@"%@:|-%.0f-", direction, firstOffset];
    NSMutableDictionary *viewDict = [[NSMutableDictionary alloc] init];
    [views enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL *stop)
     {
         NSString *viewName = [NSString stringWithFormat:@"view%i", idx];
         [viewDict setObject:view
                      forKey:viewName];
         
         if (idx < [views count] - 1)
         {
             if (betweenOffset)
             {
                 [visualFormatString appendFormat:@"[%@]-%@-", viewName, betweenOffset];
             }
             else
             {
                 [visualFormatString appendFormat:@"[%@(>=40)]", viewName];
             }
         }
         else
         {
             [visualFormatString appendFormat:@"[%@(>=40)]-%.0f-|", viewName, secondOffset];
         }
     }];
    
    
    //    DebugLog(@"viewDict %@", viewDict);
    
    /* Add constraints */
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view0]-2-[view1(==view0)]-2-[view2(==view1)]-2-[view3(==view2)]-2-[view4(==view3)]-2-[view5(==view4)]-0-|"
                                                                   options:0
                                                                   metrics:0
                                                                     views:viewDict];
    [self addConstraints:constraints];
}


- (void)addRowLayoutEqualWidthConstraintsForMySubviews:(NSArray *)subviews
                                            leftOffset:(CGFloat)leftOffset
                                           rightOffset:(CGFloat)rightOffset
                                         betweenOffset:(NSString *)betweenOffset
{
    [self addEqualSizeLayoutConstraintsForMySubviews:subviews
                                         firstOffset:leftOffset
                                        secondOffset:rightOffset
                                       betweenOffset:betweenOffset
                                           direction:@"H"];
}

@end
