//
//  NSData+FYExtionsions.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 30/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation

extension NSData {
    func hexString() -> String {
        
        let tokenChars = UnsafePointer<CChar>(self.bytes)
        var tokenString = ""
        
        for var i = 0; i < self.length; i++ {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        print("tokenString: \(tokenString)")
        
        return tokenString
    }
}