//
//  NSString+FYExtensions.swift
//  maersk
//
//  Created by Focaloid Technologies Pvt. Ltd on 25/08/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import Foundation

extension String {
    
    func isValidEmail() -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(self)
    }
    
    static func bulletinedText(text:String) -> String {
        return String(Character(UnicodeScalar(0x2022)))+" "+text
    }
    
}