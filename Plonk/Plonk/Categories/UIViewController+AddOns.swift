//
//  UIViewController+AddOns.swift
//  maersk
//
//  Created by Focaloid Technologies Pvt. Ltd on 20/08/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import ENSwiftSideMenu
import MaterialKit
import OpenInGoogleMaps
import AVFoundation
import Photos

extension UIViewController:UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showSplashBackground() {
        let backgroundView = UIImageView(image: UIImage(named: "MKSplash"))
        self.view.addSubview(backgroundView)
        
    }
    
    func createBurgerIcon() {
        
        let btnLeft:MKButton = MKButton(type: UIButtonType.Custom)
        btnLeft.rippleLocation = .TapLocation
        btnLeft.rippleLayerColor = UIColor.whiteColor()
        
        let buttonImage:UIImage = UIImage(named: "ic_burger")!
        btnLeft.setImage(buttonImage, forState: UIControlState.Normal)
        btnLeft.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height)
        btnLeft.addTarget(self, action: "didTapOpenButton:", forControlEvents: UIControlEvents.TouchDown)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnLeft)
        
    }
    
    func createBackButton() -> UIButton {
        
        let btnLeft:MKButton = MKButton(type: UIButtonType.Custom)
        btnLeft.rippleLocation = .TapLocation
        btnLeft.rippleLayerColor = UIColor.whiteColor()
        
        let buttonImage:UIImage = UIImage(named: "ic_back")!
        btnLeft.setImage(buttonImage, forState: UIControlState.Normal)
        btnLeft.frame = CGRectMake(0, 0, 30, 30)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnLeft)
        
        return btnLeft
        
    }
    
    func createDoneButton() -> UIBarButtonItem {
        
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: "didTapDone:")
        self.navigationItem.rightBarButtonItem = doneBarButton
        
        return doneBarButton
        
    }
    
    func createRightIcon() -> MKButton {
        
        let btnLeft:MKButton = MKButton(type: UIButtonType.Custom)
        btnLeft.rippleLocation = .TapLocation
        btnLeft.rippleLayerColor = UIColor.whiteColor()

        let theAnimation = CABasicAnimation (keyPath: "opacity")
        theAnimation.duration = 1.0
        theAnimation.repeatCount = 1000000;
        theAnimation.autoreverses = true;
        theAnimation.fromValue = NSNumber(double: 1.0);
        theAnimation.toValue = NSNumber(double: 0.0)
        btnLeft.layer.addAnimation(theAnimation, forKey: "animateOpacity")
        
        let buttonImage:UIImage = UIImage(named: "ic_running_clock")!
        btnLeft.setImage(buttonImage, forState: UIControlState.Normal)
        btnLeft.frame = CGRectMake(0, 0, 25, 25)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnLeft)
        
        return btnLeft
    }

    func didTapOpenButton(sender: UIBarButtonItem) {
        toggleSideMenuView()
    }
    
    func handleAlertError(title: String, message: String) {
        
        handleAlertError(title, message: message, handler: nil)
    }
    
    func handleError(title: String, message: String) {
        
        handleError(title, message: message, handler: nil)
    }
    
    func handleError(title: String, message: String, handler: ((UIAlertAction!) -> Void)!) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: handler))
        alert.view.tintColor = UIColor.blackColor()
        self.presentViewController(alert, animated: true, completion: { () -> Void in
            alert.view.tintColor = UIColor.blackColor()
        })
        alert.view.tintColor = UIColor.blackColor()
    }
    
    func handleAlertError(title: String, message: String, handler: ((UIAlertAction!) -> Void)!) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: handler))
        alert.view.tintColor = UIColor.blackColor()
        self.presentViewController(alert, animated: true, completion: { () -> Void in
            alert.view.tintColor = UIColor.blackColor()
        })
        alert.view.tintColor = UIColor.blackColor()
    }
    
    func handleDecisionPrompt(title: String, message: String, confirmDecisionHandler: ((UIAlertAction!) -> Void)!) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: confirmDecisionHandler))
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Default, handler: nil))
        alert.view.tintColor = UIColor.blackColor()
        self.presentViewController(alert, animated: true, completion: { () -> Void in
            alert.view.tintColor = UIColor.blackColor()
        })
        alert.view.tintColor = UIColor.blackColor()
    }
    func removeChildView(childClass: AnyClass) {
        
        if self.childViewControllers.count>0 {
            
            for childView:UIViewController in (self.childViewControllers ) {
                
                if childView.isKindOfClass(childClass) {
                    
                    childView.view.removeFromSuperview()
                    childView.removeFromParentViewController()
                    
                }
                
            }
            
        }
    }
    public func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        let nextField:PKTextField = (textField as! PKTextField)
        
        if nextField.resignFirstResponder() {
            
            let keyType = textField.returnKeyType
            
            if keyType == .Next {
                if nextField.nextField != nil {
                    nextField.nextField!.becomeFirstResponder()
                }
            }
            
            return true
        }
        
        return false
        
    }
    
    public func gotoMapWithSearchLocation(destinationName:String, latitude:String, longitude:String) {
        OpenInGoogleMapsController.sharedInstance().fallbackStrategy = .ChromeThenSafari
        let definition = GoogleDirectionsDefinition()
        definition.startingPoint = nil
        let destination = GoogleDirectionsWaypoint(location: CLLocationCoordinate2DMake(NSString(string: latitude).doubleValue, NSString(string: longitude).doubleValue))
        destination.queryString = destinationName
        definition.destinationPoint = destination
        definition.travelMode = .Driving
        OpenInGoogleMapsController.sharedInstance().openDirections(definition)
    }
    
    func deleteView() -> UIView {
        
        let deleteView = UIView(frame: CGRectZero)

        let labelDelete = UILabel(frame: CGRectZero)
        labelDelete.text = "Delete"
        labelDelete.textColor = UIColor.whiteColor()
        labelDelete.font = UIFont.PKAllerBold(14)
        
        deleteView.addSubview(labelDelete)
        
        labelDelete.translatesAutoresizingMaskIntoConstraints = false
        labelDelete.addLeftEdgeAttachConstraint(deleteView, offset: 10)
        labelDelete.addCenterYConstraint(deleteView)
        labelDelete.addWidthConstraint(100)
        labelDelete.addHeightConstraint(21)
        
        return deleteView
    }
    
    @IBAction func btnImagePickerClicked(sender: AnyObject)
    {
        showOptions()
        
    }
    
    func showOptions() {
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
            { UIAlertAction in
                
                self.checkCamera()
                
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default)
            { UIAlertAction in
                
                self.photoLibraryAvailabilityCheck()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
            {
                UIAlertAction in
                
        }
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.blackColor()
        // Present the actionsheet
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: { () -> Void in
                alert.view.tintColor = UIColor.blackColor()
            })
            alert.view.tintColor = UIColor.blackColor()
        }
        //        else
        //        {
        //            let popover:UIPopoverController? = UIPopoverController(contentViewController: alert)
        //            popover!.presentPopoverFromRect(sender.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        //        }
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            let picker:UIImagePickerController? = UIImagePickerController()
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            picker!.delegate = self
//            picker!.view.tintColor = UIColor.blackColor()
            self.presentViewController(picker!, animated: true, completion: { () -> Void in
//                picker!.view.tintColor = UIColor.blackColor()
            })
//            picker!.view.tintColor = UIColor.blackColor()
        }
        else
        {
            openGallery()
        }
    }
    
    func openGallery()
    {
        let picker:UIImagePickerController? = UIImagePickerController()
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker!.delegate = self
//        picker!.view.tintColor = UIColor.blackColor()
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(picker!, animated: true, completion: { () -> Void in
//                picker!.view.tintColor = UIColor.blackColor()
            })
        }
        //        else
        //        {
        //            let popover:UIPopoverController? = UIPopoverController(contentViewController: picker!)
        //            popover!.presentPopoverFromRect(sender!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        //        }
    }
    
    
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        switch authStatus {
        case .Authorized:
            
            self.openCamera()
            
            break // Do you stuffer here i.e. allowScanning()
        case .Denied: alertToEncourageCameraAccessInitially()
        case .NotDetermined: alertPromptToAllowCameraAccessViaSetting()
        default: alertToEncourageCameraAccessInitially()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing Photos",
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .Cancel, handler: { (alert) -> Void in
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        }))
        alert.view.tintColor = UIColor.blackColor()
        presentViewController(alert, animated: true) { () -> Void in
            alert.view.tintColor = UIColor.blackColor()
        }
        alert.view.tintColor = UIColor.blackColor()
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Please allow camera access for capturing Photos",
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .Cancel) { alert in
            if AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo) { granted in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.checkCamera() } }
            }
            }
        )
        alert.view.tintColor = UIColor.blackColor()
        presentViewController(alert, animated: true) { () -> Void in
            alert.view.tintColor = UIColor.blackColor()
        }
        alert.view.tintColor = UIColor.blackColor()
    }
    
    func photoLibraryAvailabilityCheck()
    {
        self.openGallery()
        /*
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.Authorized
        {
            self.openGallery()
        }
        else
        {
            let cameraUnavailableAlertController = UIAlertController (title: "Photo Library Unavailable", message: "Please check to see if device settings doesn't allow photo library access", preferredStyle: .Alert)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .Destructive) { (_) -> Void in
                let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let cancelAction = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            cameraUnavailableAlertController .addAction(settingsAction)
            cameraUnavailableAlertController .addAction(cancelAction)
            cameraUnavailableAlertController.view.tintColor = UIColor.blackColor()
            presentViewController(cameraUnavailableAlertController, animated: true, completion: { () -> Void in
                cameraUnavailableAlertController.view.tintColor = UIColor.blackColor()
            })
            cameraUnavailableAlertController.view.tintColor = UIColor.blackColor()
        }
        */
    }
    
    public func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}