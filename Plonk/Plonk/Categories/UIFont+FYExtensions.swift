//
//  UIFont+MKExtensions.swift
//  maersk
//
//  Created by Focaloid Technologies Pvt. Ltd on 20/08/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func PKAllerRegular(size:CGFloat) -> UIFont {
        return UIFont(name: "Aller", size: size)!
    }
    
    class func PKAllerLight(size:CGFloat) -> UIFont {
        return UIFont(name: "Aller-Light", size: size)!
    }
    
//    class func PKAllerLightItalic(size:CGFloat) -> UIFont {
//        return UIFont(name: "Aller-Light Italic", size: size)!
//    }
    
    class func PKAllerItalic(size:CGFloat) -> UIFont {
        return UIFont(name: "Aller-Italic", size: size)!
    }
    
    class func PKAllerBold(size:CGFloat) -> UIFont {
        return UIFont(name: "Aller-Bold", size: size)!
    }
    
//    class func PKAllerBoldItalic(size:CGFloat) -> UIFont {
//        return UIFont(name: "Aller-Bold Italic", size: size)!
//    }
    
    class func PKAvenirHeavy(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Heavy", size: size)!
    }
    
    class func PKAvenirOblique(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Oblique", size: size)!
    }
    
    class func PKAvenirBlack(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Black", size: size)!
    }
    
    class func PKAvenirBook(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Book", size: size)!
    }
    
    class func PKAvenirBlackOblique(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-BlackOblique", size: size)!
    }
    
    class func PKAvenirHeavyOblique(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-HeavyOblique", size: size)!
    }
    
    class func PKAvenirLight(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Light", size: size)!
    }
    
    class func PKAvenirMediumOblique(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-MediumOblique", size: size)!
    }
    
    class func PKAvenirMedium(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Medium", size: size)!
    }
    
    class func PKAvenirLightOblique(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-LightOblique", size: size)!
    }
    
    class func PKAvenirRoman(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Roman", size: size)!
    }
    
    class func PKAvenirBookOblique(size:CGFloat) -> UIFont {
        return UIFont(name: "Avenir-BookOblique", size: size)!
    }
    
    class func PKLaChataNormal(size:CGFloat) -> UIFont {
        return UIFont(name: "Lachata", size: size)!
    }
}