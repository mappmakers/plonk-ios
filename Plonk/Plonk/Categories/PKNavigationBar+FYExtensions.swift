//
//  PKNavigationBar+FYExtensions.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 20/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func hideBottomHairline()
    {
        hairlineImageViewInNavigationBar(self)?.hidden = true
    }
    
    func showBottomHairline()
    {
        hairlineImageViewInNavigationBar(self)?.hidden = false
    }
    
    private func hairlineImageViewInNavigationBar(view: UIView) -> UIImageView?
    {
        if let imageView = view as? UIImageView where imageView.bounds.height <= 1
        {
            return view as? UIImageView
        }
        
        let subviews = (view.subviews)
        for subview: UIView in subviews
        {
            if let imageView = hairlineImageViewInNavigationBar(subview)
            {
                return imageView
            }
        }
        
        return nil
    }
}

extension UIToolbar {
    
    func hideHairline()
    {
        hairlineImageViewInToolbar(self)?.hidden = true
    }
    
    func showHairline()
    {
        hairlineImageViewInToolbar(self)?.hidden = false
    }
    
    private func hairlineImageViewInToolbar(view: UIView) -> UIImageView?
    {
        if let imageView = view as? UIImageView where imageView.bounds.height <= 1
        {
            return imageView
        }
        
        let subviews = (view.subviews)
        for subview: UIView in subviews
        {
            if let imageView = hairlineImageViewInToolbar(subview)
            {
                return imageView
            }
        }
        
        return nil
    }
}
