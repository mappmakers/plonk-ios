//
//  Dictionary+Extensions.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 20/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import Foundation

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}