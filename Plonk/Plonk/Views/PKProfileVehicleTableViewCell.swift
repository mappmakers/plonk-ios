//
//  PKProfileVehicleTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 13/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MCSwipeTableViewCell

class PKProfileVehicleTableViewCell: MCSwipeTableViewCell {

    @IBOutlet var viewVehicleColor: UIView!
    @IBOutlet var labelVehicleName: UILabel!
    @IBOutlet var labelVehicleModelName: UILabel!
    @IBOutlet var labelVehicleNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewVehicleColor.layer.cornerRadius = 12
        viewVehicleColor.layer.masksToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }

}
