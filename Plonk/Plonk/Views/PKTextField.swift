//
//  PKTextField.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit

class PKTextField: MKTextField, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet var nextField: AnyObject?
    
    var picker:UIPickerView!
    private var toolbar:UIToolbar!
    
    var pickerData:NSArray!
    
    var datePicker:UIDatePicker?
    private var dateFormatter:NSDateFormatter?
    
    internal var selectedPickerItem:NSObject?
    
//    override func drawPlaceholderInRect(rect: CGRect) {
//        setValue(UIColor.whiteColor().colorWithAlphaComponent(0.4), forKeyPath: "_placeholderLabel.textColor")
//        super.drawPlaceholderInRect(rect)
//    }
    
//    var horizontalPadding:CGFloat = 15
//    var verticalPadding:CGFloat = 0
//    
//    override func textRectForBounds(bounds: CGRect) -> CGRect {
//        super.textRectForBounds(bounds)
//        return CGRectMake(bounds.origin.x + horizontalPadding, bounds.origin.y + verticalPadding, bounds.size.width - horizontalPadding*2, bounds.size.height)
//    }
//    
//    override func editingRectForBounds(bounds: CGRect) -> CGRect {
//        super.editingRectForBounds(bounds)
//        return self.textRectForBounds(bounds)
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        font = UIFont.PKAllerRegular(14)
        textColor = UIColor.whiteColor()
        tintColor = UIColor.whiteColor().colorWithAlphaComponent(0.6)
        floatingLabelTextColor = UIColor.whiteColor()
        rippleLocation = .TapLocation
        floatingPlaceholderEnabled = true
        layer.borderColor = UIColor.clearColor().CGColor
        rippleLayerColor = UIColor.whiteColor()
        borderStyle = UITextBorderStyle.None
        bottomBorderEnabled = true
        bottomBorderColor = UIColor.whiteColor().colorWithAlphaComponent(0.3)
        floatingLabelFont = UIFont.PKAllerBold(12)
        padding = CGSizeMake(15, 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }

    func attachFieldWithPickerData(data:NSArray, selectedItem:String?) {
        
        picker = UIPickerView(frame: CGRectZero)
        picker.dataSource = self
        picker.delegate = self
        pickerData = data
        picker.reloadAllComponents()
        if selectedItem != nil {
            if pickerData.containsObject(selectedItem!) {
                let index = pickerData.indexOfObject(selectedItem!)
                picker.selectRow(index, inComponent: 0, animated: false)
            }
            selectedPickerItem = selectedItem
            text = selectedItem
        }
        //        else {
        //            text = pickerData[0] as! String
        //        }
        inputView = picker
        
        attachPickerDoneToolbar()
    }
    
    func attachDoneToolbar() {
        
        let cancelButton:UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelPressed:")
        let attributes = [NSForegroundColorAttributeName:UIColor.appThemeColor(),NSFontAttributeName:UIFont.PKAllerBold(14)]
        cancelButton.setTitleTextAttributes(attributes, forState: .Normal)
        let flexibleButton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolbar = UIToolbar(frame: CGRectMake(0, 0, frame.size.width, 44))
        toolbar.barStyle = UIBarStyle.Black
        toolbar.barTintColor = UIColor.whiteColor()
        toolbar.backgroundColor = UIColor.whiteColor()
        toolbar.setItems([flexibleButton,cancelButton], animated: false)
        
        inputAccessoryView = toolbar
    }
    
    // MARK:- UIPickerViewDataSource
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row] as? String
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    //    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    //        text = pickerData[row] as! String
    //    }
    
    func cancelPressed(sender:UIBarButtonItem) {
        resignFirstResponder()
    }
    
    func donePressed(sender:UIBarButtonItem) {
        
        if pickerData?.count > 0 {
            let pickerSelected = pickerData[picker.selectedRowInComponent(0)] as? String
            selectedPickerItem = pickerSelected
            text = pickerSelected
        }else if datePicker != nil {
            text = dateFormatter?.stringFromDate(datePicker!.date)
            selectedPickerItem = datePicker!.date
        }
        
        
        resignFirstResponder()
    }
    
    func attachDatePickerWithMode(mode:UIDatePickerMode) {
        datePicker = UIDatePicker(frame: CGRectZero)
        datePicker!.datePickerMode = mode
        
        dateFormatter = NSDateFormatter()
        if mode == .Date {
            dateFormatter?.dateFormat = "yyyy-MM-dd"
        }else {
            dateFormatter?.dateFormat = "hh:mm a"
        }
        
        inputView = datePicker
        
        attachPickerDoneToolbar()
    }
    
    private func attachPickerDoneToolbar() {
        let cancelButton:UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelPressed:")
        let attributes = [NSForegroundColorAttributeName:UIColor.appThemeColor(),NSFontAttributeName:UIFont.PKAllerRegular(14)]
        cancelButton.setTitleTextAttributes(attributes, forState: .Normal)
        let flexibleButton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneButton:UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: "donePressed:")
        doneButton.setTitleTextAttributes(attributes, forState: .Normal)
        toolbar = UIToolbar(frame: CGRectMake(0, 0, frame.size.width, 44))
        toolbar!.barTintColor = UIColor.whiteColor()
        toolbar!.backgroundColor = UIColor.whiteColor()
        toolbar!.setItems([cancelButton,flexibleButton,doneButton], animated: false)
        
        inputAccessoryView = toolbar
        
    }
}
