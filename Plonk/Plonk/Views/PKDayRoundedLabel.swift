//
//  PKDayRoundedLabel.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 20/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKDayRoundedLabel: UILabel {

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
       // self.layer.cornerRadius=self.frame.size.width/2
        self.layer.borderWidth=1
        self.layer.borderColor=UIColor.appThemeColor().CGColor
        self.clipsToBounds=true
        
        
        
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
