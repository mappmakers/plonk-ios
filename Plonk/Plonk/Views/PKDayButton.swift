//
//  PKDayButton.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 29/09/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKDayButton: UIButton {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.layer.cornerRadius=self.frame.size.width/2
        self.layer.borderWidth=1
        self.layer.borderColor=UIColor.appThemeColor().CGColor
        self.clipsToBounds=true
        
        setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        setTitleColor(UIColor.whiteColor(), forState: .Selected)
        
    }

    func customizeForSelected()->Void
    {
        self.backgroundColor=UIColor.appThemeColor()
        
    }
    func customizeForUnSelected()->Void
    {
        self.backgroundColor=UIColor.whiteColor()
        
    }
   
}
