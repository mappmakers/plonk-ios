//
//  PKParkingSlotTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 16/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKParkingSlotTableViewCell: UITableViewCell {

    @IBOutlet var labelSlots: UILabel!
    @IBOutlet var labelClosingTime: UILabel!
    @IBOutlet var labelParkingStatus: UILabel!
    @IBOutlet var labelFare: UILabel!
    @IBOutlet var labelExtraFare: UILabel!
    @IBOutlet var labelMinimumDuration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
