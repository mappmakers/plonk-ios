//
//  PKBasicTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 28/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKBasicTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        textLabel?.font = UIFont.PKAllerRegular(14)
        textLabel?.highlightedTextColor = UIColor.appThemeColor()
        
        tintColor = UIColor.appThemeColor()
        
        let selectedView = UIView(frame: CGRectZero)
        selectedView.backgroundColor = UIColor.whiteColor()
        selectedBackgroundView = selectedView
    
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
