//
//  PKOTPInputTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKOTPInputTableViewCell: UITableViewCell {

    var textFieldFirst:PKTextFieldBordered!
    var textFieldSecond:PKTextFieldBordered!
    var textFieldThird:PKTextFieldBordered!
    var textFieldForth:PKTextFieldBordered!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        let containerView = UIView(frame: CGRectZero)
        containerView.backgroundColor = UIColor.clearColor()
        
        contentView.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addCenterXConstraint(contentView)
        containerView.addCenterYConstraint(contentView)
        containerView.addWidthConstraint(270)
        containerView.addHeightConstraint(50)
        
        textFieldFirst = PKTextFieldBordered(frame: CGRectZero)
        
        textFieldFirst.tag = 1
        containerView.addSubview(textFieldFirst)
        textFieldFirst.translatesAutoresizingMaskIntoConstraints = false
        
        textFieldFirst.addLeftEdgeAttachConstraint(containerView)
        textFieldFirst.addTopEdgeAttachConstraint(containerView)
        textFieldFirst.addBottomEdgeAttachConstraint(containerView)
        textFieldFirst.addWidthConstraint(60)
        
        textFieldSecond = PKTextFieldBordered(frame: CGRectZero)
        
        textFieldFirst.nextField = textFieldSecond
        textFieldSecond.tag = 2
        containerView.addSubview(textFieldSecond)
        textFieldSecond.translatesAutoresizingMaskIntoConstraints = false
        
        textFieldSecond.addLeftEdgeAttachConstraint(textFieldFirst, viewEdge: NSLayoutAttribute.Right, offset: 10)
        textFieldSecond.addTopEdgeAttachConstraint(textFieldFirst)
        textFieldSecond.addBottomEdgeAttachConstraint(textFieldFirst)
        textFieldSecond.addWidthConstraint(60)
        
        textFieldThird = PKTextFieldBordered(frame: CGRectZero)
        
        textFieldThird.tag = 3
        textFieldSecond.nextField = textFieldThird
        containerView.addSubview(textFieldThird)
        textFieldThird.translatesAutoresizingMaskIntoConstraints = false
        
        textFieldThird.addLeftEdgeAttachConstraint(textFieldSecond, viewEdge: NSLayoutAttribute.Right, offset: 10)
        textFieldThird.addTopEdgeAttachConstraint(textFieldSecond)
        textFieldThird.addBottomEdgeAttachConstraint(textFieldSecond)
        textFieldThird.addWidthConstraint(60)
        
        textFieldForth = PKTextFieldBordered(frame: CGRectZero)
        
        textFieldForth.tag = 4
        textFieldThird.nextField = textFieldForth
        containerView.addSubview(textFieldForth)
        textFieldForth.translatesAutoresizingMaskIntoConstraints = false
        
        textFieldForth.addLeftEdgeAttachConstraint(textFieldThird, viewEdge: NSLayoutAttribute.Right, offset: 10)
        textFieldForth.addTopEdgeAttachConstraint(textFieldThird)
        textFieldForth.addBottomEdgeAttachConstraint(textFieldThird)
        textFieldForth.addWidthConstraint(60)
        
        backgroundColor = UIColor.clearColor()
        selectionStyle = UITableViewCellSelectionStyle.None
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }

}
