//
//  PKClickableLabel.m
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 03/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

#import "PKClickableLabel.h"
#import "Plonk-Swift.h"

@implementation PKClickableLabel

- (void)buildAgreeTextViewFromString:(NSString *)localizedString
{
    // 1. Split the localized string on the # sign:
    NSArray *localizedStringPieces = [localizedString componentsSeparatedByString:@"#"];
    
    // 2. Loop through all the pieces:
    NSUInteger msgChunkCount = localizedStringPieces ? localizedStringPieces.count : 0;
    CGPoint wordLocation = CGPointMake(0.0, 0.0);
    
    BOOL isAddParkingLot = ([localizedString containsString:@"<ts>"] && ![localizedString containsString:@"<pp>"]);

    for (NSUInteger i = 0; i < msgChunkCount; i++)
    {
        NSString *chunk = [localizedStringPieces objectAtIndex:i];
        if ([chunk isEqualToString:@""])
        {
            continue;     // skip this loop if the chunk is empty
        }
        
        // 3. Determine what type of word this is:
        BOOL isTermsOfServiceLink = [chunk hasPrefix:@"<ts>"];
        BOOL isPrivacyPolicyLink  = [chunk hasPrefix:@"<pp>"];
        BOOL isLink = (BOOL)(isTermsOfServiceLink || isPrivacyPolicyLink);

        // 4. Create label, styling dependent on whether it's a link:
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont PKAllerRegular:14];
        label.userInteractionEnabled = isLink;
        UIColor *currentTextColor = isAddParkingLot?[UIColor appThemeColor]:[UIColor whiteColor];
        label.textColor = currentTextColor;

        if (isLink)
        {
            label.highlightedTextColor = [UIColor appThemeColor];
            
            // 5. Set tap gesture for this clickable text:
            SEL selectorAction = isTermsOfServiceLink ? @selector(tapOnTermsOfServiceLink:) : @selector(tapOnPrivacyPolicyLink:);
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:selectorAction];
            [label addGestureRecognizer:tapGesture];
            
            // Trim the markup characters from the label:
            if (isTermsOfServiceLink) {
                label.text = [chunk stringByReplacingOccurrencesOfString:@"<ts>" withString:@""];
            }
            if (isPrivacyPolicyLink) {
                label.text = [chunk stringByReplacingOccurrencesOfString:@"<pp>" withString:@""];
            }
            
            label.attributedText = [[NSAttributedString alloc] initWithString:label.text attributes:@{NSUnderlineColorAttributeName:currentTextColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:1],NSForegroundColorAttributeName:currentTextColor,NSFontAttributeName:[UIFont PKAllerRegular:14]}];

        }
        else
        {
            label.text = chunk;
        }
        
        // 6. Lay out the labels so it forms a complete sentence again:
        
        // If this word doesn't fit at end of this line, then move it to the next
        // line and make sure any leading spaces are stripped off so it aligns nicely:
        
        [label sizeToFit];
        
        BOOL shouldAdjust = false;
        
        if (isAddParkingLot) {
            shouldAdjust = self.frame.size.width < wordLocation.x + label.bounds.size.width && !isLink;
        }else {
            shouldAdjust = self.frame.size.width < wordLocation.x + label.bounds.size.width && isTermsOfServiceLink;
        }
        
        if (shouldAdjust)
        {
            wordLocation.x = 0.0;                       // move this word all the way to the left...
            wordLocation.y += label.frame.size.height;  // ...on the next line
            
            // And trim of any leading white space:
            NSRange startingWhiteSpaceRange = [label.text rangeOfString:@"^\\s*"
                                                                options:NSRegularExpressionSearch];
            if (startingWhiteSpaceRange.location == 0)
            {
                label.text = [label.text stringByReplacingCharactersInRange:startingWhiteSpaceRange
                                                                 withString:@""];
                if (isLink) {
                    
                    label.attributedText = [[NSAttributedString alloc] initWithString:label.text attributes:@{NSUnderlineColorAttributeName:currentTextColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:1],NSForegroundColorAttributeName:currentTextColor,NSFontAttributeName:[UIFont PKAllerRegular:14]}];
                }
                
                [label sizeToFit];
            }
        }
        
        // Set the location for this label:
        label.frame = CGRectMake(wordLocation.x,
                                 wordLocation.y,
                                 label.frame.size.width,
                                 label.frame.size.height);
        // Show this label:
        [self addSubview:label];
        
        // Update the horizontal position for the next word:
        wordLocation.x += label.frame.size.width;
    }
}

- (void)tapOnTermsOfServiceLink:(UITapGestureRecognizer *)tapGesture
{
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        NSLog(@"User tapped on the Terms of Service link");
        
        //[[[UIAlertView alloc] initWithTitle:[tapGesture.view valueForKey:@"text"] message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        
        PKWebViewController *webServiceController = [PKWebViewController new];
        webServiceController.htmlKey = @"supp_agrmnt";
        [_parentController.navigationController pushViewController:webServiceController animated:true];
        
        /*
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.TermsAgreement.rawValue), parameters:nil, completionClosure: { (success:Int!, result:AnyObject?) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if success == 1 {
                
                let json = result as! Dictionary<String, AnyObject>
                let dataDIC = json["terms_cond"] as! Array<Dictionary<String, AnyObject>>
                
                let htmlDic = dataDIC[0]
                
                let htmString = htmlDic["file_name"] as! String
                
                let webViewController = PKWebViewController()
                webViewController.hmtlString = htmString
                
                self.navigationController?.pushViewController(webViewController, animated: true)
            }
            else {
                
                let error:NSError = (result as! NSError)
                let title = String(error.code) + ": " + error.domain
                let message = error.localizedDescription
                self.handleError(title, message: message)
                
            }
            
            
            
        })
         */
    }
}


- (void)tapOnPrivacyPolicyLink:(UITapGestureRecognizer *)tapGesture
{
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        NSLog(@"User tapped on the Privacy Policy link");
        
        //[[[UIAlertView alloc] initWithTitle:[tapGesture.view valueForKey:@"text"] message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

    }
}

@end
