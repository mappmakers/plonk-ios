//
//  PKMobileInputTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 15/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

private struct PKMobileInputConstants {

    static let PlaceholderMobileNumber:String = "Mobile Number"
}

class PKMobileInputTableViewCell: UITableViewCell {

    var btnCountryCode:UIButton!
    var textFieldMobileNumber:PKTextFieldBordered!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let containerView = UIView(frame: CGRectZero)
        containerView.backgroundColor = UIColor.clearColor()
        
        contentView.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addCenterXConstraint(contentView)
        containerView.addCenterYConstraint(contentView)
        containerView.addWidthConstraint(280)
        containerView.addHeightConstraint(44)
        
        btnCountryCode = UIButton(type: UIButtonType.Custom)
        btnCountryCode.setImage(UIImage(named: "ic_flag"), forState: UIControlState.Normal)
        btnCountryCode.setTitle("+91", forState: UIControlState.Normal)
        btnCountryCode.titleLabel?.font = UIFont.PKAllerLight(11)
        btnCountryCode.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 25)
        btnCountryCode.layer.cornerRadius = 5
        btnCountryCode.layer.masksToBounds = true
        btnCountryCode.layer.borderWidth = 2.0
        btnCountryCode.layer.borderColor = UIColor.whiteColor().CGColor
        
        containerView.addSubview(btnCountryCode)
        btnCountryCode.translatesAutoresizingMaskIntoConstraints = false
        
        btnCountryCode.addLeftEdgeAttachConstraint(containerView)
        btnCountryCode.addTopEdgeAttachConstraint(containerView)
        btnCountryCode.addBottomEdgeAttachConstraint(containerView)
        btnCountryCode.addWidthConstraint(80)
        
        textFieldMobileNumber = PKTextFieldBordered(frame: CGRectZero)
        textFieldMobileNumber.placeholder = PKMobileInputConstants.PlaceholderMobileNumber
        textFieldMobileNumber.font = UIFont.PKAllerBold(16)
        
        containerView.addSubview(textFieldMobileNumber)
        textFieldMobileNumber.translatesAutoresizingMaskIntoConstraints = false
        
        textFieldMobileNumber.addTopEdgeAttachConstraint(btnCountryCode)
        textFieldMobileNumber.addBottomEdgeAttachConstraint(btnCountryCode)
        textFieldMobileNumber.addLeftEdgeAttachConstraint(btnCountryCode, viewEdge: NSLayoutAttribute.Right, offset: 10)
        textFieldMobileNumber.addRightEdgeAttachConstraint(containerView)
        
        backgroundColor = UIColor.clearColor()
        selectionStyle = UITableViewCellSelectionStyle.None
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
}
