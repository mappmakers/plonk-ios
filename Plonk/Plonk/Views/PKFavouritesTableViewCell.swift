//
//  PKFavouritesTableViewCell.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 12/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

typealias PKFavoriteCellCompletion = (sender : UIButton) -> Void
typealias PKMapNavigationCompletion = (sender : UIButton) -> Void

class PKFavouritesTableViewCell: UITableViewCell {
    @IBOutlet weak var gmsMapView: GMSMapView!
    @IBOutlet var labelParkingName: UILabel!
    @IBOutlet var labelSubtiteInfo: UILabel!

    var favoriteCompletion:PKFavoriteCellCompletion?
    var mapNavigationCompletion:PKMapNavigationCompletion?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func mapNavigationPressed(sender: UIButton) {
        
        if mapNavigationCompletion != nil {
            mapNavigationCompletion!(sender: sender)
        }
        
    }

    @IBAction func favoritePressed(sender: UIButton) {
        
        if favoriteCompletion != nil {
            favoriteCompletion!(sender: sender)
        }
        
    }
}
