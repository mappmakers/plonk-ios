//
//  PKCenteredLabelTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 04/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit

class PKCenteredLabelTableViewCell: MKTableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        customizeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        customizeCell()
    }
    
    func customizeCell() {
        
        rippleLocation = .TapLocation
        rippleLayerColor = UIColor.whiteColor()
        
        textLabel?.textAlignment = NSTextAlignment.Center
        textLabel?.numberOfLines = 2
        
        textLabel?.textColor = UIColor.whiteColor()
        
        textLabel?.font = UIFont.PKAllerLight(16)
        
        selectionStyle = UITableViewCellSelectionStyle.None
        
        backgroundColor = UIColor.whiteColor()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        textLabel!.frame = CGRectMake(0, textLabel!.frame.origin.y, frame.size.width, textLabel!.frame.size.height)
    }

    func customizeForAttributedTextWith(firstText: String?, secondText:String?) {
        
        backgroundColor = UIColor.clearColor()
        let attributednoAccountTitleFirst:NSAttributedString = NSAttributedString(string: firstText ?? "", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName:UIFont.PKAllerRegular(14)])
        
        let attributedSignupTitleSecond:NSAttributedString = NSAttributedString(string: secondText ?? "", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName:UIFont.PKAllerBold(14)])
        
        let joinedSignupTitle:NSMutableAttributedString = NSMutableAttributedString(attributedString: attributednoAccountTitleFirst)
        joinedSignupTitle.appendAttributedString(attributedSignupTitleSecond)
        
        textLabel?.attributedText = joinedSignupTitle
        
    }
}
