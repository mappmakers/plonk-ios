//
//  PKTextFieldBordered.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 16/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKTextFieldBordered: PKTextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        floatingPlaceholderEnabled = false
        font = UIFont.PKAllerBold(24)
        textAlignment = NSTextAlignment.Center
        keyboardType = UIKeyboardType.NumberPad
        layer.cornerRadius = 5
        layer.masksToBounds = true
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.whiteColor().CGColor

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
}