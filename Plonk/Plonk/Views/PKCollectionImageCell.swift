//
//  PKCollectionImageCell.swift
//  Plonk
//
//  Created by Farhan Yousuf on 04/12/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKCollectionImageCell: UICollectionViewCell {
    
    var collectionImageView : UIImageView?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        customizeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        customizeCell()
    }
    
    func customizeCell() {
        
        collectionImageView = UIImageView(frame: CGRectZero)
        collectionImageView!.image = UIImage(named: "add_parking")
        collectionImageView!.contentMode = .ScaleAspectFit
        collectionImageView!.clipsToBounds = true
        
        collectionImageView!.layer.borderWidth = 2.0
        collectionImageView!.layer.borderColor = UIColor.appThemeColor().CGColor
        
        UIView.layoutView(collectionImageView, fitView: contentView, transparent: true)
        
        backgroundColor = UIColor.whiteColor()
    }


}
