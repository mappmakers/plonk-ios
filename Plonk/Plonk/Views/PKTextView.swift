//
//  PKTextView.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 12/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class PKTextView: JVFloatLabeledTextView {

    private var toolbar:UIToolbar!
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.font = UIFont.PKAllerRegular(PKConstants.FontExtraVerySmallSize)
        self.textAlignment = NSTextAlignment.Left
        self.textColor = UIColor.appThemeColor()
        self.backgroundColor = UIColor.clearColor()
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    func attachDoneToolbar() {
        
        let cancelButton:UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelPressed:")
        let attributes = [NSForegroundColorAttributeName:UIColor.appThemeColor(),NSFontAttributeName:UIFont.PKAllerRegular(14)]
        cancelButton.setTitleTextAttributes(attributes, forState: .Normal)
        let flexibleButton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolbar = UIToolbar(frame: CGRectMake(0, 0, frame.size.width, 44))
        toolbar.barStyle = UIBarStyle.Black
        toolbar.barTintColor = UIColor.whiteColor()
        toolbar.backgroundColor = UIColor.whiteColor()
        toolbar.setItems([flexibleButton,cancelButton], animated: false)
        
        inputAccessoryView = toolbar
    }
    
    func cancelPressed(sender:UIBarButtonItem) {
        resignFirstResponder()
    }

}
