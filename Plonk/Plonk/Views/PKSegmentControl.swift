//
//  PKSegmentControl.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

typealias PKSegmentControlCompletion = (sender : UIButton) -> Void

class PKSegmentControl: UIView {

    @IBOutlet var segmentButtons: [PKDayButton]!
    
    var segmentControlCompletion:PKSegmentControlCompletion?
    
    var selectedSegmentIndex:Int {
        get {
            return self.selectedSegmentIndex
        }
        set {
            for button in segmentButtons {
                button.selected = false
            }
            segmentButtons[self.selectedSegmentIndex].selected = true
            if (segmentControlCompletion != nil) {
                segmentControlCompletion!(sender :segmentButtons[self.selectedSegmentIndex])
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var tag = 0
        for button in segmentButtons {
            button.tag = tag++
            button.addTarget(self, action: "changeSegment:", forControlEvents: .TouchDown)
        }
    }

    func changeSegment(sender:UIButton) {
        
    }
    
}
