//
//  PKClickableLabel.h
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 03/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TermsCompletion)(id senderText);

@interface PKClickableLabel : UIView
@property (nonatomic, strong) UIViewController *parentController;
@property (nonatomic, assign) TermsCompletion termsCompletion;
- (void)buildAgreeTextViewFromString:(NSString *)localizedString;

@end
