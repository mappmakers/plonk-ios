//
//  PKRoundedImageView.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 29/09/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKRoundedImageView: UIImageView {

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.layer.cornerRadius=5
        self.clipsToBounds=true
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
