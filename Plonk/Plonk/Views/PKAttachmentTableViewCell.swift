//
//  PKAttachmentTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 12/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit

typealias PKAttachmentCellAccessoryCompletion = (sender : UIButton) -> Void

class PKAttachmentTableViewCell: MKTableViewCell {

    var accessoryCompletion:PKAttachmentCellAccessoryCompletion?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        rippleLocation = .TapLocation
        rippleLayerColor = UIColor.appThemeColor()
        
        self.contentMode = UIViewContentMode.Left
        self.imageView?.frame = CGRectMake(15, 15, 50, 50)
        self.textLabel?.frame = CGRectMake(75, 15, UIScreen.mainScreen().bounds.size.width - 120, 34)
        self.detailTextLabel?.frame = CGRectMake(75, 50, UIScreen.mainScreen().bounds.size.width - 120, 21)
        self.imageView?.clipsToBounds = true
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.imageView?.contentMode = .ScaleAspectFill
        self.imageView?.clipsToBounds = true
        self.textLabel?.numberOfLines = 0
        self.textLabel?.font = UIFont.PKAllerRegular(14)
        self.textLabel?.backgroundColor = UIColor.redColor()
        self.detailTextLabel?.numberOfLines = 0
        self.detailTextLabel?.textColor = UIColor.lightGrayColor()
        self.detailTextLabel?.font = UIFont.PKAllerRegular(12)
        
        let accessoryButton = UIButton(type: .Custom)
        accessoryButton.setImage(UIImage(named: "ic_cross"), forState: .Normal)
        accessoryButton.frame = CGRectMake(0, 0, 26, 26)
        accessoryButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        accessoryButton.backgroundColor = UIColor.darkGrayColor()
        accessoryButton.addTarget(self, action: "accessoryTapped:", forControlEvents: .TouchDown)
        accessoryButton.layer.cornerRadius = 13
        accessoryButton.layer.masksToBounds = true
        
        accessoryView = accessoryButton
        
        selectionStyle = .None
    }

    func accessoryTapped(sender: UIButton) {
        
        if accessoryCompletion != nil {
            
            accessoryCompletion!(sender: sender)
            
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
