//
//  PKTextViewTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 12/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKTextViewTableViewCell: UITableViewCell {

    private var textContainerView: UIView!
    var textView:PKTextView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        textContainerView = UIView(frame: CGRectZero)
        textContainerView.backgroundColor = UIColor.clearColor()
//        textContainerView.layer.borderColor = UIColor.appThemeColor().CGColor
//        textContainerView.layer.borderWidth = 2.0
        
        self.contentView.addSubview(textContainerView)
        
        textContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        textContainerView.addLeftEdgeAttachConstraint(contentView, offset:10)
        textContainerView.addRightEdgeAttachConstraint(contentView, offset:10)
        textContainerView.addTopEdgeAttachConstraint(contentView)
        textContainerView.addBottomEdgeAttachConstraint(contentView)
        
        textView = PKTextView(frame: CGRectZero)
        textView.tintColor = UIColor.appThemeColor()
        textView.placeholderTextColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        textView.placeholderLabel.font = UIFont.PKAllerRegular(14)
        textView.floatingLabel.font = UIFont.PKAllerBold(12)
        textView.floatingLabelTextColor = UIColor.clearColor()
        textView.floatingLabelActiveTextColor = UIColor.clearColor()
        textView.font = UIFont.PKAllerRegular(PKConstants.FontSmallSize)
        textView.textAlignment = NSTextAlignment.Left
        textView.textColor = UIColor.grayColor()
        
        backgroundColor = UIColor.clearColor()
        
        textContainerView.addSubview(textView)
        
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        textView.addLeftEdgeAttachConstraint(textContainerView, offset:10)
        textView.addRightEdgeAttachConstraint(textContainerView, offset:10)
        textView.addTopEdgeAttachConstraint(textContainerView)
        textView.addBottomEdgeAttachConstraint(textContainerView)
        
        textView.attachDoneToolbar()
        
        self.backgroundColor = UIColor.whiteColor()
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }

}
