//
//  PKTextFieldLoginTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit

class PKTextFieldLoginTableViewCell: MKTableViewCell {

    var textField: PKTextField!
    var accessoryAlertIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        rippleLocation = .TapLocation
        rippleLayerColor = UIColor.appThemeColor()
        
        textField = PKTextField(frame: CGRectZero)
        self.contentView.addSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addLeftEdgeAttachConstraint(contentView)
        textField.addRightEdgeAttachConstraint(contentView)
        textField.addBottomEdgeAttachConstraint(contentView)
        textField.addTopEdgeAttachConstraint(contentView, offset:10)
        
        backgroundColor = UIColor.clearColor()
        
        accessoryAlertIcon = UIImageView(image: UIImage(named: "ic_alert"))
        accessoryAlertIcon.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(accessoryAlertIcon)
        
        accessoryAlertIcon.addRightEdgeAttachConstraint(contentView, offset: 20)
        accessoryAlertIcon.addCenterYConstraint(textField)
        accessoryAlertIcon.addHeightConstraint(25)
        accessoryAlertIcon.addWidthConstraint(25)
        
        accessoryAlertIcon.hidden = true
        
        selectionStyle = UITableViewCellSelectionStyle.None
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }

}
