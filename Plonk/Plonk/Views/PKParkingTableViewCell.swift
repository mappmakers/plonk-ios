//
//  PKParkingTableViewCell.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 29/09/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKParkingTableViewCell: UITableViewCell {

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textLabel?.font=UIFont(name: "Aller_Lt", size:20)
        self.detailTextLabel?.font=UIFont(name: "Aller_Lt", size: 10)
        self.textLabel?.textColor=UIColor.grayColor()
        self.detailTextLabel?.textColor=UIColor.grayColor()
        
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.textLabel?.font=UIFont(name: "Aller", size:15)
        self.detailTextLabel?.font=UIFont(name: "Aller", size: 10)
        self.textLabel?.textColor=UIColor.grayColor()
        self.detailTextLabel?.textColor=UIColor.grayColor()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
