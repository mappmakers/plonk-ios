//
//  PKLeftMenuTableViewCell.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 16/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MaterialKit

class PKLeftMenuTableViewCell: MKTableViewCell {

    var leftMenuItem:PKLeftMenuItem!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        rippleLocation = .TapLocation
        rippleLayerColor = UIColor.appThemeColor()
        
        self.contentMode = UIViewContentMode.Left
        self.imageView?.frame = CGRectMake(20, 15, 20, 20)
        self.textLabel?.frame = CGRectMake(70, 10, UIScreen.mainScreen().bounds.size.width - 130, 30)
        self.imageView?.clipsToBounds = true
        self.textLabel?.textAlignment = NSTextAlignment.Left
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
