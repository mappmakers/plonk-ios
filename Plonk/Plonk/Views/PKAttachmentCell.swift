//
//  PKAttachmentCell.swift
//  Plonk
//
//  Created by Farhan Yousuf on 04/12/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKAttachmentCell: UITableViewCell {
    
    var collectionView:UICollectionView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSizeMake(150, 100)
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.minimumLineSpacing = 10
        flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        flowLayout.scrollDirection = .Horizontal
        
        collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: flowLayout)
        collectionView?.backgroundColor = UIColor.whiteColor()
        collectionView?.showsHorizontalScrollIndicator = false
        
        contentView.addSubview(collectionView!)
        
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView?.addLeftEdgeAttachConstraint(contentView)
        collectionView?.addRightEdgeAttachConstraint(contentView)
        collectionView?.addTopEdgeAttachConstraint(contentView, offset:10)
        collectionView?.addBottomEdgeAttachConstraint(contentView, offset:10)
        
        self.backgroundColor = UIColor.whiteColor()
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    

}
