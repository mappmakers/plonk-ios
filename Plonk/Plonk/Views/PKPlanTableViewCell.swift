//
//  PKPlanTableViewCell.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 19/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKPlanTableViewCell: UITableViewCell {
    @IBOutlet weak var planCost: UILabel!

    @IBOutlet weak var planDateRange: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        planCost.layer.cornerRadius=5
        planCost.clipsToBounds=true
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
