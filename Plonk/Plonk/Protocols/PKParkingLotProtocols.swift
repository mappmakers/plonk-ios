//
//  PKParkingLotProtocols.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import Foundation

@objc protocol PKParkingAddDelegate {
    
    func parkingLotAddDetailController(controller: UITableViewController, didUpdateDictionaryValue paramters: Dictionary<String, AnyObject>!)

}