//
//  PKAuthenticationProtocols.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import Foundation

@objc protocol PKAuthenticationDelegate {
    
    func authenticationController(controller: UITableViewController, didSuccessfullyAuthenticateUser user: PKUser!)
    func authenticationController(controller: UITableViewController, didFailToAuthenticateUser error: NSError!)
    func controller(controller: UITableViewController, didLogoutUser user: PKUser!)
}