//
//  PKDataModel.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

import CoreData

class PKDataModel: NSObject {
    
    var managedObjectContext:NSManagedObjectContext!
    var loggedUser:PKUser!
    
    class var sharedInstance: PKDataModel {
        struct Static {
            static var instance: PKDataModel?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = PKDataModel()
            //Static.instance?.managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        }
        
        return Static.instance!
    }
    
    func fetchResultsControllerForEntity(entityName:String, predicate:NSPredicate?, sortOrder:NSSortDescriptor?) -> NSFetchedResultsController {
        
        return fetchResultsControllerForEntity(entityName, predicate: predicate, sortOrder: sortOrder, sectionNameKey: nil)
        
    }
    
    func fetchResultsControllerForEntity(entityName:String, predicate:NSPredicate?, sortOrder:NSSortDescriptor?, sectionNameKey:String?) -> NSFetchedResultsController {
        
        let fetchRequest = NSFetchRequest(entityName: entityName)
        if (predicate != nil) {
            fetchRequest.predicate = predicate
        }
        if (sortOrder != nil) {
            fetchRequest.sortDescriptors = [sortOrder!]
        }
        
        fetchRequest.returnsObjectsAsFaults = false
        
        let fetchedResultsController:NSFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("An error occurred: \(error.localizedDescription)", terminator: "")
            abort()
        }
        return fetchedResultsController
        
    }
    
}
