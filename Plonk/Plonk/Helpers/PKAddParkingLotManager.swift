//
//  PKAddParkingLotManager.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 25/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MobileCoreServices
import SVProgressHUD

class PKAddParkingLotManager: NSObject {

    /// Create request
    ///
    /// - parameter userid:   The userid to be passed to web service
    /// - parameter password: The password to be passed to web service
    /// - parameter email:    The email address to be passed to web service
    ///
    /// - returns:            The NSURLRequest that was created
    
    func createRequest (param:Dictionary<String, AnyObject>!, imageArray:Array<UIImage>) -> NSURLRequest {
        
        let boundary = generateBoundaryString()
        
        let url = NSURL(string: "http://54.169.255.87/mobile_parkingLots/add_tenant_prkLots")!
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = createBodyWithParameters(param, filePathKey: "doc_upload_image", imagesArray: imageArray, boundary: boundary)
        
        return request
    }
    
    /// Create body of the multipart/form-data request
    ///
    /// - parameter parameters:   The optional dictionary containing keys and values to be passed to web service
    /// - parameter filePathKey:  The optional field name to be used when uploading files. If you supply paths, you must supply filePathKey, too.
    /// - parameter paths:        The optional array of file paths of the files to be uploaded
    /// - parameter boundary:     The multipart/form-data boundary
    ///
    /// - returns:                The NSData of the body of the request
    
    func createBodyWithParameters(parameters: [String: AnyObject]?, filePathKey: String?, imagesArray: [UIImage]?, boundary: String) -> NSData {
        let body = NSMutableData()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        if imagesArray != nil {
            for image in imagesArray! {
//                let url = NSURL(fileURLWithPath: path)
//                let data = //NSData(contentsOfURL: url)!
                let mimetype = "application/octet-stream"//mimeTypeForPath(path)
                
                let data = UIImageJPEGRepresentation(image, 0.6)
                let index = imagesArray!.indexOf(image)
                
                let filename = "doc_upload_image\(index!).jpg"//url.lastPathComponent

                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\(index!)\"; filename=\"\(filename)\"\r\n")
                body.appendString("Content-Type: \(mimetype)\r\n\r\n")
                body.appendData(data!)
                body.appendString("\r\n")
            }
        }
        
        body.appendString("--\(boundary)--\r\n")
        return body
    }
    
    /// Create boundary string for multipart/form-data request
    ///
    /// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().UUIDString)"
    }
    
    /// Determine mime type on the basis of extension of a file.
    ///
    /// This requires MobileCoreServices framework.
    ///
    /// - parameter path:         The path of the file for which we are going to determine the mime type.
    ///
    /// - returns:                Returns the mime type if successful. Returns application/octet-stream if unable to determine mime type.
    
    func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream";
    }
    
    func addParkingLot(parameters:Dictionary<String, AnyObject>!, imageArray:Array<UIImage>, completionClosure: (success:Int!,result :AnyObject?) ->Void) {

        let request = createRequest(parameters, imageArray: imageArray)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            if error != nil {
                // handle error here
                print(error)
                return
            }
            
            // if response was JSON, then parse it
            
            do {
                if let responseDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? NSDictionary {
                    print("success == \(responseDictionary)")
                    
                    // note, if you want to update the UI, make sure to dispatch that to the main queue, e.g.:
                    //
                    // dispatch_async(dispatch_get_main_queue()) {
                    //     // update your UI and model objects here
                    // }
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                        // time-consuming task
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            let responseParseError = responseDictionary["error"] as? String
                            if responseParseError != nil {
                                
                                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                appDelegate.window?.rootViewController?.handleError("Session Expired", message: "Your current ssession has been expired. You wil now be redirected to login page. Please login to continue.", handler: { (action) -> Void in
                                    
                                    appDelegate.customizeForAuthenticationRequired()
                                    
                                })
                                
                                return
                            }
                            
                        })
                    })
                    
                    
                    completionClosure(success: 1,result: responseDictionary)
                }
            } catch {
                print(error)
                
//                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                print("responseString = \(responseString)")
                
                completionClosure(success: 0,result: error as NSError)
            }
        }
        task.resume()
        
    }
    
    func addParkingLotSynchronously(parameters:Dictionary<String, AnyObject>!, imageArray:Array<UIImage>, completionClosure: (success:Int!,result :AnyObject?) ->Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string:"Ehttp://54.169.255.87/mobile_parkingLots/add_tenant_prkLots")!)
//        let session = NSURLSession.sharedSession()
        
        let boundary = generateBoundaryString()

        request.HTTPMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = createBodyWithParameters(parameters, filePathKey: "doc_upload_image", imagesArray: imageArray, boundary: boundary)
        
        
        do {
            
            let returnData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: nil)
            
            let returnString = NSString(data: returnData, encoding: NSUTF8StringEncoding)
            
//            print("returnString \(returnString)")
            
            completionClosure(success: 1,result: returnString)
            
        } catch {

            completionClosure(success: 0,result: error as NSError)
            
        }
        
        
    }
    
    class var sharedInstance: PKAddParkingLotManager {
        struct Static {
            static var instance: PKAddParkingLotManager?
            static var token: dispatch_once_t = 0
            
        }
        
        dispatch_once(&Static.token) {
            Static.instance = PKAddParkingLotManager()
        }
        
        return Static.instance!
    }
}
