//
//  PKAppearance.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import SVProgressHUD

class PKAppearance: NSObject {
    
    var leftMenuItemsArray:Array<PKLeftMenuItem> = Array<PKLeftMenuItem>()
    
    var pageMenuParameters:[String: AnyObject]?
    
    static func sharedInstance() -> PKAppearance {
        struct Static {
            static var instance: PKAppearance?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = PKAppearance()
            Static.instance!.applyGoldTintForTextField()
            Static.instance!.applyGlobalTintForNavigationBar()
            Static.instance!.applyProgressHudStyling()
        }
        
        return Static.instance!
    }
    
    func applyGoldTintForTextField() {
        UITextField.appearance().tintColor = UIColor.whiteColor()
    }
    
    func applyGlobalTintForNavigationBar() {
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics: UIBarMetrics.Default)
        
        UINavigationBar.appearance().barTintColor = UIColor.appThemeColor()
//        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        let textAttributes:Dictionary = [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont.PKLaChataNormal(16)]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
    }
    
    func applyProgressHudStyling() {
        SVProgressHUD.setForegroundColor(UIColor.appThemeColor())
        SVProgressHUD.setFont(UIFont.PKAllerBold(14))
    }
 
    func pageMenuTabsAppearanceProperties() -> [String: AnyObject] {
        
        if pageMenuParameters == nil {
            
            // Customize menu (Optional)
            pageMenuParameters = ["menuItemSeparatorWidth": 4.3,
                "scrollMenuBackgroundColor": UIColor.whiteColor(),
                "viewBackgroundColor": UIColor.whiteColor(),
                "bottomMenuHairlineColor": UIColor.appThemeColor(),
                "selectionIndicatorColor": UIColor.appThemeColor(),
                "menuMargin": PKConstants.PageMenuMargin,
                "menuHeight": PKConstants.PageMenuHeight,
                "selectedMenuItemLabelColor": UIColor.appThemeColor(),
                "unselectedMenuItemLabelColor": UIColor.whiteColor(),
                "menuItemFont": UIFont.PKAllerBold(PKConstants.FontExtraVerySmallSize),
                "useMenuLikeSegmentedControl": true,
                "menuItemSeparatorRoundEdges": true,
                "selectionIndicatorHeight": 2.0,
                "menuItemSeparatorPercentageHeight": 0.1, "menuItemSeparatorColor":UIColor.blackColor()]
            
        }
        
        return pageMenuParameters!
    }
}
