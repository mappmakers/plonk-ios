
//
//  PKWebService.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import Alamofire

enum PKWebServiceCommonKeys: String {
    case Token = "token"
    case UserDeviceKey = "usr_device_key"
}
    
enum PKWebServiceEndPoints: String {
    case UserLogin = "mobile_login/user_login"
    case UserRegisterMobile = "mobile_register/add_user_mobileno"
    case UserVerifyOTP = "mobile_register/verify_user_otp"
    case UserSignUp = "mobile_register/add_new_users"
    case UserGplusSignIn = "mobile_login/google_user_login"
    case UserFacebookSignIn = "mobile_login/fb_user_login"
    case UserForgotPassword = "mobile_login/recover_user_pswd"
    case SearchParkingLots = "mobile_scheduleLots/search_prkLots"
    case ScanParkingLots = "mobile_scheduleLots/scan_qrcode"
    case MyParkingLots = "mobile_parkingLots/get_tenant_prkLot_list"
    case ParkingLotDetails = "mobile_parkingLots/get_tenant_prkLot_details"
    case MyParkingLotHistory = "mobile_parkingLots/get_parking_history"
    case ParkingSlotConfirm = "mobile_scheduleLots/confirm_parking"
    case ParkingStop = "mobile_scheduleLots/stop_parking"
    case GetUserVehicles = "mobile_userDetails/get_user_vehicles"
    case ChangeParkingSlot = "mobile_scheduleLots/change_prkLot_tenants_slots"
    case RateParkingLot = "mobile_parkingLots/add_prkLot_rating"
    case AddParkingLotSlabPlan = "mobile_parkingLots/add_slot_plans"
    case GetVehicleTypes = "mobile_userDetails/get_vehicle_types"
    case AddUserVehicle = "mobile_userDetails/add_user_vehicles"
    case ListUserPlans = "mobile_parkingLots/get_prkLot_plan_list"
    case PlanDetailsUrl = "mobile_parkingLots/get_prkLot_plan_details"
    case DeactivatePlan = "mobile_parkingLots/deactivate_plan_details"
    case FavoriteList = "mobile_parkingLots/get_favourite_prkLots"
    case RemoveFavorite = "mobile_parkingLots/delete_favourite_prkLots"
    case AddFavorite = "mobile_parkingLots/add_favourite_prkLot"
    case ActiveParkingTimer = "mobile_scheduleLots/check_parking_meter_status"
    case ExitQRParking = "mobile_scheduleLots/exit_qrcode_parking"
    case DeleteVehicle = "mobile_userDetails/delete_user_vehicles"
    case UpdateEmail = "mobile_userDetails/update_user_email"
    case UpdatePlaceMark = "mobile_userDetails/update_user_placemark"
    case AddPushDetails = "mobile_login/add_push_details"
    case AddParkingLot = "mobile_parkingLots/add_tenant_prkLots"
    case TermsAgreement = "mobile_login/get_terms_policies"
    case AddParkingDocument = "mobile_parkingLots/add_tenant_documents"
    case GetBankDetails = "mobile_parkingLots/get_user_bnkdetails"
    case GetUserDefaultPrice = "mobile_userDetails/get_user_dfprice"
    case UpdateUserDefaultPrice = "mobile_userDetails/update_default_price"
    case AddProfileImage = "mobile_userDetails/update_profile_image"
    case AddParkingLotImage = "mobile_parkingLots/add_tenant_Lots_image"
}

enum PKWebServiceRoute: URLStringConvertible {
    case Path(String)
    
    var URLString: String {
        let baseURLString = "http://54.255.128.122"
        
//        let baseURLString = "http://54.169.255.87"
        let path: String = {
            switch self {
            case .Path(let pathString):
                return "/\(pathString)"
            }
            }()
        
        return NSURL(string: path, relativeToURL: NSURL(string: baseURLString))!.absoluteString
    }
    
}

enum PKAmazonRoute: URLStringConvertible {
    case ImagePath(String)
    
    var URLString: String {
        let baseURLString = "https://s3-ap-southeast-1.amazonaws.com/plonk/"
        let path: String = {
            switch self {
            case .ImagePath(let imageName):
                return "\(imageName)"

            }
            }()
        
        return NSURL(string: path, relativeToURL: NSURL(string: baseURLString))!.absoluteString
    }
    
}

class PKWebService: NSObject {
    
    static let arraySkipTokenEndpoints : NSArray =
    [
        PKWebServiceRoute.Path(PKWebServiceEndPoints.UserLogin.rawValue).URLString,
        PKWebServiceRoute.Path(PKWebServiceEndPoints.UserSignUp.rawValue).URLString,
        PKWebServiceRoute.Path(PKWebServiceEndPoints.UserGplusSignIn.rawValue).URLString,
        PKWebServiceRoute.Path(PKWebServiceEndPoints.UserFacebookSignIn.rawValue).URLString,
        PKWebServiceRoute.Path(PKWebServiceEndPoints.UserForgotPassword.rawValue).URLString,
        PKWebServiceRoute.Path(PKWebServiceEndPoints.TermsAgreement.rawValue).URLString,
        PKWebServiceRoute.Path(PKWebServiceEndPoints.GetVehicleTypes.rawValue).URLString,
    ]
    
    class var sharedInstance: PKWebService {
        struct Static {
            static var instance: PKWebService?
            static var token: dispatch_once_t = 0
            
        }
        
        dispatch_once(&Static.token) {
            Static.instance = PKWebService()
        }
        
        return Static.instance!
    }
    
    func request(path: PKWebServiceRoute, parameters: Dictionary<String, AnyObject>?, completionClosure: (success:Int!,result :AnyObject?) ->Void) {
        
        var parametersToSend : Dictionary<String, AnyObject>?
        
        if parameters != nil {
            parametersToSend = parameters
            if !PKWebService.arraySkipTokenEndpoints.containsObject(path.URLString) {
                parametersToSend!.updateValue(NSUserDefaults.standardUserDefaults().objectForKey(PKConstants.UserDefaultTokenValue)!, forKey: "secure_key")
                
                if let user = PKDataModel.sharedInstance.loggedUser {
                    parametersToSend?.updateValue(user.user_id!, forKey: "user_id")
                }
            }
            
            
        }
        
        Alamofire.request(.POST, path.URLString, parameters: parametersToSend)
            .responseString { response in
//                print("Response String: \(response.result.value)")
            }
            .responseJSON { response in
//                print("Response JSON: \(response.result.value)")
                switch response.result {
                case .Success(let JSON):
//                    print("Success with JSON: \(JSON)")
                    
                    if let responseParameters = JSON as? Dictionary<String, AnyObject> {
                        
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

                        if let responseParseError = responseParameters["error"] as? String {
                            
                            if responseParseError == "Authentication failed" {
                                
                                appDelegate.window?.rootViewController?.handleError("Session Expired", message: "Your current ssession has been expired. You wil now be redirected to login page. Please login to continue.", handler: { (action) -> Void in
                                    
                                    appDelegate.customizeForAuthenticationRequired()
                                    
                                })
                                
                                return
                                
                            }else {
                                appDelegate.window?.rootViewController?.handleError("", message: responseParseError)
                                
                                return
                            }
                            
                        }
                    }
                    
                    completionClosure(success: 1,result: JSON)
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    completionClosure(success: 0,result: error)
                }
        }
    }
    
//    func cancelAllRequests() {
//        Alamofire.Manager.sharedInstance.session.invalidateAndCancel()
//    }
    
    func uploadParkingDetails(path:PKWebServiceRoute, parameters:Dictionary<String, AnyObject>!, imageArray:Array<UIImage>, completionClosure: (success:Int!,result :AnyObject?) ->Void) {
        
        Alamofire.upload(
            .POST,
            path.URLString,
            multipartFormData: {
                multipartFormData in
                
                for image in imageArray {
                    let data = UIImageJPEGRepresentation(image, 0.6)
                    let index = imageArray.indexOf(image)
                    multipartFormData.appendBodyPart(data: data!, name: "doc_upload_image\(index!+1)")
                }

                
//                let data = UIImagePNGRepresentation(UIImage(named: "dotted_small")!)
//                multipartFormData.appendBodyPart(data: data!, name: "doc_upload_image1")
                
                for (key, value) in parameters {
                    multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                }
                
            },
            encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _ ):
                    upload.responseJSON { response in
                        debugPrint(response)
                        switch response.result {
                        case .Success(let JSON):
                            print("Success with JSON: \(JSON)")
                            
                            if let responseParameters = JSON as? Dictionary<String, AnyObject> {
                                let responseParseError = responseParameters["error"] as? String
                                if responseParseError != nil {
                                    
                                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                    appDelegate.window?.rootViewController?.handleError("Session Expired", message: "Your current ssession has been expired. You wil now be redirected to login page. Please login to continue.", handler: { (action) -> Void in
                                        
                                        appDelegate.customizeForAuthenticationRequired()
                                        
                                    })
                                    
                                    return
                                }
                            }
                            
                            completionClosure(success: 1,result: JSON)
                        case .Failure(let error):
                            print("Request failed with error: \(error)")
                            completionClosure(success: 0,result: error)
                        }
                    }
                case .Failure(let encodingError):
                    debugPrint("Failure")
                    debugPrint(encodingError)
                    
                    completionClosure(success: 0,result: "")
                }
            }
        )
        
    }
    
}
