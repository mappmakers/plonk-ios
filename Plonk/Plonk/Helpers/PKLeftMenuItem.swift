//
//  PKLeftMenuItem.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
struct PKLeftMenuProperties {
    
    static let PropertyTitle:String = "title"
    static let PropertyNormalIconName:String = "iconName"
    static let PropertySelectedIconName:String = "selectedIconName"
    static let PropertyNormalTextColor:String = "normalTextColor"
    static let PropertySelectedTextColor:String = "selectedTextColor"
    static let PropertyIsSelected:String = "isSelected"
}

class PKLeftMenuItem: NSObject {
    
    var title:String!
    var normalIconName:String!
    var selectedIconName:String!
    var normalTextColor:UIColor!
    var selectedTextColor:UIColor!
    var isSelected:Bool!
    
    static func leftItem(parameters:Dictionary<String, AnyObject>) -> PKLeftMenuItem {
        
        let leftItem:PKLeftMenuItem = PKLeftMenuItem()
        leftItem.title = parameters[PKLeftMenuProperties.PropertyTitle] as! String
        leftItem.normalIconName = parameters[PKLeftMenuProperties.PropertyNormalIconName] as! String
        leftItem.selectedIconName = parameters[PKLeftMenuProperties.PropertySelectedIconName] as! String
        leftItem.normalTextColor = parameters[PKLeftMenuProperties.PropertyNormalTextColor] as! UIColor
        leftItem.selectedTextColor = parameters[PKLeftMenuProperties.PropertySelectedTextColor] as! UIColor
        leftItem.isSelected = parameters[PKLeftMenuProperties.PropertyIsSelected] as! Bool
        
        return leftItem
        
    }
    
    override init() {
        super.init()
    }
}
