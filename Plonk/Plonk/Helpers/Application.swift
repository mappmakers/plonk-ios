//
//  Application.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 04/11/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

struct ApplicationConstants {
    static let applicationOpenGoogleAuthNotification : String = "ApplicationOpenGoogleAuthNotification"
}

class Application: UIApplication {

    override func openURL(url: NSURL) -> Bool {
        if url.absoluteString.hasPrefix("googlechrome-x-callback:") {
            return false
        }else if url.absoluteString.hasPrefix("https://accounts.google.com/o/oauth2/auth") {
            
            NSNotificationCenter.defaultCenter().postNotificationName(ApplicationConstants.applicationOpenGoogleAuthNotification, object: url)
            
            return false
        }
        
        return super.openURL(url)
    }
}
