//
//  PKConstants.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

struct PKConstants {
    
    static let AppTitle:String = "PLONK"
    static let AppNavigationBarOffset:CGFloat = 64
    static let AppSideMenuMargin:CGFloat = 100
    
    static let LogoImageViewVerticalBorderOffset:CGFloat = 10
    static let LogoImageViewWidth:CGFloat = 70
    static let LogoImageViewHeight:CGFloat = 70
    
    static let FontExtraVerySmallSize:CGFloat = 12
    static let FontExtraSmallSize:CGFloat = 13
    static let FontSmallSize:CGFloat = 14
    static let FontMediumSize:CGFloat = 15
    static let FontLargeSize:CGFloat = 16
    static let FontExtraLargeSize:CGFloat = 17
    static let FontExtraVeryLargeSize:CGFloat = 18
    
    static let PageMenuMargin:CGFloat = 0
    static let PageMenuHeight:CGFloat = 44
    
    static let NotificationUserLoggedIn:String = "PKUserLoggedInNotification"
    static let NotificationPushReceived:String = "PKPushNotificationReceivedNotification"
    static let NotificationPushRegister:String = "RegisteredForPushNotification"
    static let NotificationParkingSlotConfirmed:String = "PKParkingSlotConfirmed"
    static let NotificationParkingMeterStopped:String = "PKParkingMeterStopped"
    static let NotificationUserLotAdded:String = "NotificationUserLotAdded"
    
    
    static let CellBasicHeight:CGFloat = 80
    static let CellTextFieldHeight:CGFloat = 60
    
    static let CellTextFieldIdentifier:String = "textFieldCell"
    static let CellTextViewIdentifier:String = "textViewCell"
    static let CellBasicIdentifier:String = "basicCell"
    static let CellCenterTextIdentifier:String = "centerTextCell"
    static let CellHeaderIdentifier:String = "headerCell"
    static let CellFooterIdentifier:String = "footerCell"
    static let CellFavoritesIdentifier:String = "favoriteCell"
    
    static let PlaceHolderEmailAddress:String = "Email Address"
    static let PlaceHolderPassword:String = "Password"
    
    static let GSMAPIKey = "AIzaSyAArIPqraq7LyW12DPgCXkr3zWUcqtMvyk"
    static let GATrackingKey = "UA-68252185-2"
    
    static let FBScheme = "fb728253533968800"
    
    static let UserDefaultSliderValue:String = "UserDefaultSliderValue"
    static let UserDefaultTokenValue:String = "UserDefaultTokenValue"
    
    static let StoryboardNameShare = "Share"
    static let StoryboardNameParkingLot = "ParkingLot"
    static let StoryboardNameMyParkingLot = "MyParkingLotStoryboard"
    static let StoryboardNameProfile = "Profile"
    
    static let StoryboardIDParkingDetail = "PKParkingDetailControllerIdentifier"
    static let StoryboardIDParkingConfirm = "PKParkingConfirmControllerIdentifier"
    static let StoryboardIDParkingMeterStart = "PKParkingMeterStartControllerIdentifier"
    static let StoryboardIDParkingExit = "ParkingExitControllerIdentifier"
    static let StoryboardIDParkingExitCard = "PaymentExitCardController"
    static let StoryboardIDParkingSlots = "ParkingSlotsControllerIdentifier"
    
    static let TitleErrorHeader = ""
    static let TitleErrorMessage = "i dunno what went wrong"
}

enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}
