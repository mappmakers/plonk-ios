//
//  AppDelegate.swift
//  Plonk
//
//  Created by Focaloid Technologies Pvt. Ltd on 01/09/15.
//  Copyright (c) 2015 Farhan Yousuf. All rights reserved.
//

import UIKit
import MagicalRecord
import FBSDKCoreKit
import FBSDKLoginKit
import Fabric
import Crashlytics
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PKAuthenticationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        application.applicationIconBadgeNumber = 0;
        
        //printFonts()
        Fabric.with([Crashlytics.self])

        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")

        GMSServices.provideAPIKey(PKConstants.GSMAPIKey)
        
        GINInvite.setGoogleAnalyticsTrackingId(PKConstants.GATrackingKey)
        
        GINInvite.applicationDidFinishLaunching()

        if (GIDSignIn.sharedInstance().hasAuthInKeychain()) {
            GIDSignIn.sharedInstance().signInSilently()
        }
        
        application.statusBarStyle = UIStatusBarStyle.LightContent

        PKAppearance.sharedInstance()
        
        // Setup CoreData with MagicalRecord
        MagicalRecord.setupCoreDataStack()

        if window == nil {
            window = UIWindow(frame: UIScreen.mainScreen().bounds)
            window?.makeKeyAndVisible()
            
        }
        window?.tintColor = UIColor.whiteColor()
        window?.clipsToBounds = true
        
        if !UIApplication.sharedApplication().isRegisteredForRemoteNotifications() {
            let setting = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(setting)
            UIApplication.sharedApplication().registerForRemoteNotifications()
        }
        
        if let results = PKUser.MR_findAllInContext(NSManagedObjectContext.MR_defaultContext()) {
            if (results.count > 0) {
                for result in results as! [PKUser] {
                    PKDataModel.sharedInstance.loggedUser = result
                }
                if PKDataModel.sharedInstance.loggedUser.otp_verified == "1" {
                    customizeForAuthenticationDone()
                }else {
                    FBSDKLoginButton.self
                    customizeForAuthenticationRequired()
                }
            } else {
                print("No Users")
                FBSDKLoginButton.self
                customizeForAuthenticationRequired()
            }
        }
        
        return true
    }

    func customizeForAuthenticationRequired() {
        let loginController:PKSignInTableViewController = PKSignInTableViewController(style: UITableViewStyle.Plain)
        loginController.authenticationDelegate = self
        let navController = UINavigationController(rootViewController: loginController)
//        navController.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        navController.navigationBar.shadowImage = UIImage()
        navController.navigationBar.translucent = true
        window?.rootViewController = navController
        
        let navBarHeight:CGFloat = 20.0
        let viewStatusBar = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, navBarHeight))
        viewStatusBar.backgroundColor = UIColor.appThemeColor()
        window?.rootViewController?.view.addSubview(viewStatusBar)
     
    }
    
    func customizeForAuthenticationDone() {

        SVProgressHUD.showWithMaskType(.Gradient)

        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.FavoriteList.rawValue), parameters: ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]) { (success:Int!, result:AnyObject?) -> Void in
            
            if success == 1
            {
                PKParkingLot.MR_truncateAll()
                
                let json = result as! Dictionary<String,AnyObject>
                let myFavoriteLotArray = json["fav_prkLots"] as! NSArray
                
                for parkingLotDict in myFavoriteLotArray {
                    
                    let aParkingLot = PKParkingLot.MR_createEntityInContext(NSManagedObjectContext.MR_defaultContext())
                    let favoriteId = (parkingLotDict as! Dictionary<String,AnyObject>)["favourite_id"]
                    aParkingLot.favourite_id = "\(favoriteId!)"
                    aParkingLot.updateParkingLotWithInfo(parkingLotDict as! Dictionary<String,AnyObject>)
                    
                }
            }
            
            PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.AddPushDetails.rawValue), parameters: ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!,"device_type":"ios","device_key":NSUserDefaults.standardUserDefaults().objectForKey(PKWebServiceCommonKeys.UserDeviceKey.rawValue) ?? ""]) { (success:Int!, result:AnyObject?) -> Void in
                
                
            }
            
            self.getDefaultSliderValue()
            
        }

    }
    
    
    func getDefaultSliderValue() {
        
        let parameters = ["user_id":PKDataModel.sharedInstance.loggedUser.user_id!]
        
        PKWebService.sharedInstance.request(PKWebServiceRoute.Path(PKWebServiceEndPoints.GetUserDefaultPrice.rawValue), parameters: parameters) { (success, result) -> Void in
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // time-consuming task
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                })
            })
            
            let json = result as! Dictionary<String,AnyObject>
            let defPrefArray = json["default_price"] as! NSArray
            
            if defPrefArray.count > 0
            {
                let defaultPrefDetailDic = defPrefArray[0] as! Dictionary<String, AnyObject>
                let prefPrice = defaultPrefDetailDic["pref_price"] as? String ?? ""
                let currentSliderValue = Float(prefPrice.characters.count>0 ? prefPrice:"10")!
                
                NSUserDefaults.standardUserDefaults().setObject(NSNumber(float: currentSliderValue), forKey: PKConstants.UserDefaultSliderValue)
                NSUserDefaults.standardUserDefaults().synchronize()
            }
            
            let contentViewController = PKMapOverViewController()
            let sideMenuViewController = PKSideMenuTableViewController(style:UITableViewStyle.Plain)
            sideMenuViewController.authenticationDelegate = self
            self.window?.rootViewController = PKNavigationDrawerController(menuViewController: sideMenuViewController, contentViewController: contentViewController)
            
            //            let qrInputController = PKExitQRTableViewController(style: .Grouped)
            //            self.window?.rootViewController = UINavigationController(rootViewController: qrInputController)
            
            let statusBarView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 20))
            statusBarView.backgroundColor = UIColor.appThemeColor()
            
            self.window?.rootViewController?.view.addSubview(statusBarView)
            
        }
        
    }
    
//    func printFonts() {
//        let fontFamilyNames = UIFont.familyNames()
//        for familyName in fontFamilyNames {
//            print("------------------------------")
//            print("Font Family Name = [\(familyName)]")
//            let names = UIFont.fontNamesForFamilyName(familyName)
//            print("Font Names = [\(names)]")
//        }
//    }
    
    func application(application: UIApplication,
        openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
            
            if (url.scheme == PKConstants.FBScheme) {
                return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
            }
            
            let invite = GINInvite.handleURL(url, sourceApplication:sourceApplication, annotation:annotation)
            
            if (invite != nil) {
                GINInvite.completeInvitation()
                let matchType =
                (invite.matchType == GINReceivedInviteMatchType.Weak) ? "Weak" : "Strong"
                print("Invite received from: \(sourceApplication) Deeplink: \(invite.deepLink)," +
                    "Id: \(invite.inviteId), Type: \(matchType)")
                GINInvite.convertInvitation(invite.inviteId)
                return true
            }
            
            return GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        MagicalRecord.saveWithBlock { (context:NSManagedObjectContext!) -> Void in
            
        }
    }

    // MARK: - PKAuthenticationDelegate
    
    func authenticationController(controller: UITableViewController, didSuccessfullyAuthenticateUser user: PKUser!) {
        
        customizeForAuthenticationDone()
    }
    func authenticationController(controller: UITableViewController, didFailToAuthenticateUser error: NSError!) {
    }
    
    func controller(controller: UITableViewController, didLogoutUser user: PKUser!) {
        
        PKAppearance.sharedInstance().leftMenuItemsArray.removeAll(keepCapacity: false)
        PKDataModel.sharedInstance.loggedUser = nil
        
        PKUser.MR_truncateAll()
        PKUserParkingDetail.MR_truncateAll()
        PKParkingLot.MR_truncateAll()
        PKUserRejectSlot.MR_truncateAll()
        PKUserPendingSlot.MR_truncateAll()
        PKUserParkingLot.MR_truncateAll()
        PKParkingSlot.MR_truncateAll()
        PKUserFavourite.MR_truncateAll()
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey(PKConstants.UserDefaultTokenValue)
        NSUserDefaults.standardUserDefaults().removeObjectForKey(PKConstants.UserDefaultSliderValue)
        NSUserDefaults.standardUserDefaults().synchronize()
        
        NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreWithCompletion { (success, error) -> Void in
            
            if (success) {
                print("You successfully saved your context.");
            } else if ((error) != nil) {
                print("Error saving context: %@", error.description);
            }
            
        }
        
        GIDSignIn.sharedInstance().signOut()
        FBSDKLoginManager().logOut()
        customizeForAuthenticationRequired()
    }
    
    func application(application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        //send this device token to server
        NSNotificationCenter.defaultCenter().postNotificationName(PKConstants.NotificationPushRegister, object: NSNumber(bool: true))
        let token = deviceToken.hexString()
        
        NSUserDefaults.standardUserDefaults().setObject(token, forKey: PKWebServiceCommonKeys.UserDeviceKey.rawValue)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    //Called if unable to register for APNS.
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        
        print("Faiiled: \(error)")
        NSNotificationCenter.defaultCenter().postNotificationName(PKConstants.NotificationPushRegister, object: NSNumber(bool: false))
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        application.applicationIconBadgeNumber = 0
        
        print("Recived: \(userInfo)")
        
        NSNotificationCenter.defaultCenter().postNotificationName(PKConstants.NotificationPushReceived, object: nil, userInfo: userInfo)
        
    }
}

