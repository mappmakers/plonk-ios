//
//  HeaderView.swift
//  Plonk
//
//  Created by Dhruv Nambiar on 14/10/15.
//  Copyright © 2015 Farhan Yousuf. All rights reserved.
//

import UIKit

class PKAccordionHeaderView: UIView {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var CostLabel: UILabel!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }

}
