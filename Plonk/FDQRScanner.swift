//
//  FDQRScanner.swift
//  SiftTest
//
//  Created by Focaloid on 6/29/15.
//  Copyright (c) 2015 Focaloid. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

typealias ScanCompletion = (decodedString : String, type:String) -> Void


class FDQRScanner: NSObject,AVCaptureMetadataOutputObjectsDelegate
{
    
    var captureSession : AVCaptureSession?
    var videoPreviewlayer : AVCaptureVideoPreviewLayer?
    var scanCompletion : ScanCompletion?

    
//    static func sharedScanner() ->(FDQRScanner)
//    {
//        
//        struct Static
//        {
//            static var instance : FDQRScanner?
//            static var token : dispatch_once_t = 0
//        }
//        
//        dispatch_once(&Static.token,
//            {
//                Static.instance = FDQRScanner()
//            })
//        
//        return Static.instance!
//    }
    
//MARK: EPublic functions
    
    func startScanning(view : UIView, metadataObjectTypes:[AnyObject]?,completion:ScanCompletion) ->Bool
    {
        self.scanCompletion = completion
        return self.startReading(view, metadataObjectTypes: metadataObjectTypes)
    }
    
    func stopScanning()
    {
        self.stopReading()
    }
    
//MARK: Internal reading and stopping fuctions
    
    
    internal func startReading(view : UIView , metadataObjectTypes : [AnyObject]?) ->(Bool)
    {
        var captureDevice : AVCaptureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        var error : NSError?
        
        var deviceInput : AVCaptureDeviceInput! = AVCaptureDeviceInput.deviceInputWithDevice(captureDevice, error: &error) as! AVCaptureDeviceInput
        if (error != nil)
        {
            print("error input device :")
            println(error)
            return false
        }
        
        var captureMetadataOutput : AVCaptureMetadataOutput! = AVCaptureMetadataOutput()
        
        
        captureSession = AVCaptureSession()
//        captureSession?.sessionPreset = avcapturesessionpres
        
        captureSession?.addInput(deviceInput)
        captureSession?.addOutput(captureMetadataOutput)
        
        var queue : dispatch_queue_t! ;
        queue = dispatch_queue_create("scannerQueue", nil)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: queue)
        if metadataObjectTypes==nil
        {
            captureMetadataOutput.metadataObjectTypes = captureMetadataOutput.availableMetadataObjectTypes
        }
        else
        {
            captureMetadataOutput.metadataObjectTypes = metadataObjectTypes
        }
        
        videoPreviewlayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewlayer?.frame = view.bounds
        videoPreviewlayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        view.layer.addSublayer(videoPreviewlayer)
        
        captureSession?.startRunning()
        
        return true
        
    }
    
    internal func stopReading()
    {
        captureSession?.stopRunning()
        captureSession = nil
        videoPreviewlayer?.removeFromSuperlayer()
    }

//MARK: CaptureMetadata Delegate methods
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!)
    {
        
        for metadataObject in metadataObjects
        {
            if metadataObject.isKindOfClass(AVMetadataMachineReadableCodeObject)
            {
                var metadataObject : AVMetadataMachineReadableCodeObject = metadataObject as! AVMetadataMachineReadableCodeObject
                self.stopReading();
                dispatch_async(dispatch_get_main_queue(),
                    {
                        self.scanCompletion?(decodedString: metadataObject.stringValue,type: metadataObject.type)
                    })
                break
            }
        }

    }

    
}

